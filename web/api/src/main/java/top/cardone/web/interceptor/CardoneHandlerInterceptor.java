package top.cardone.web.interceptor;

import org.springframework.web.servlet.HandlerInterceptor;
import top.cardone.web.support.WebSupport;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CardoneHandlerInterceptor implements HandlerInterceptor {
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        WebSupport.INPUT_STREAM_STRING_THREAD_LOCAL.remove();
    }
}
