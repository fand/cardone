package top.cardone.net.util;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSchException;
import lombok.val;
import org.apache.commons.net.ftp.FTPClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import top.cardone.CardOneApplication;
import top.cardone.core.util.action.Action1;

import java.io.IOException;
import java.util.UUID;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = CardOneApplication.class)
public class FtpUtilsTest {
    @Test
    public void testFtpAction() throws IOException {
        FtpUtils.ftpAction("192.168.1.68", 21, "admin", "888888", (Action1<FTPClient>) ftpClient -> {
            val uuid = UUID.randomUUID().toString();

            try {
                FtpUtils.upload(ftpClient, "d:/xa/install.cmd", "test/install-" + uuid + ".cmd");
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    @Test
    public void testSftpAction() throws JSchException {
        FtpUtils.sftpAction("192.168.1.198", 22, "root", "xa1234567890", (Action1<ChannelSftp>) channelSftp -> {
            val uuid = UUID.randomUUID().toString();

            try {
                FtpUtils.upload(channelSftp, "d:/xa/install.cmd", "/test/install-" + uuid + ".cmd");
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}
