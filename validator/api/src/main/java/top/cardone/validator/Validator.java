package top.cardone.validator;

import java.util.Map;

/**
 * @author yao hai tao
 * @date 2016/9/19
 */
public interface Validator {
    Map<String, Object> validate(Map<String, Object> validatorMap, Map<String, Object> map);

    Map<String, Object> validate(Map<String, Object> rules, Map<String, Object> messages, Map<String, Object> map);
}