package top.cardone.validator.func;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections.MapUtils;

import java.util.Map;
import java.util.Objects;

/**
 * @author yao hai tao
 * @date 2016/9/19
 */
@Log4j2
public class EqualToValidatorFunc extends RequiredValidatorFunc {
    public EqualToValidatorFunc() {
        this.code = "equalTo";
    }

    @Override
    public Map<String, Object> func(Map<String, Object> map, String key, Object rule, Object message) {
        if (log.isDebugEnabled()) {
            log.debug(this.code + ":" + key);
        }

        Object val = MapUtils.getString(map, key);

        Object equalToVal = MapUtils.getString(map, rule);

        if (Objects.equals(val, equalToVal)) {
            return null;
        }

        return this.getMessage(key, message, new Object[0]);
    }
}