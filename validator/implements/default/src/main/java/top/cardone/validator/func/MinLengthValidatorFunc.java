package top.cardone.validator.func;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

/**
 * @author yao hai tao
 * @date 2016/9/19
 */
@Log4j2
public class MinLengthValidatorFunc extends RequiredValidatorFunc {
    public MinLengthValidatorFunc() {
        this.code = "minlength";
    }

    @Override
    public Map<String, Object> func(Map<String, Object> map, String key, Object rule, Object message) {
        if (log.isDebugEnabled()) {
            log.debug(this.code + ":" + key);
        }

        String val = MapUtils.getString(map, key);

        if (StringUtils.isBlank(val)) {
            return null;
        }

        int minLength = ((Double) rule).intValue();

        if (top.cardone.context.util.StringUtils.length(val) >= minLength) {
            return null;
        }

        return this.getMessage(key, message, new Object[]{minLength});
    }
}