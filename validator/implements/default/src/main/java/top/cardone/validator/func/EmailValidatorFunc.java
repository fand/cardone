package top.cardone.validator.func;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;

import java.util.Map;

/**
 * @author yao hai tao
 * @date 2016/9/19
 */
@Log4j2
public class EmailValidatorFunc extends RequiredValidatorFunc {
    public EmailValidatorFunc() {
        this.code = "email";
    }

    @Override
    public Map<String, Object> func(Map<String, Object> map, String key, Object rule, Object message) {
        if (log.isDebugEnabled()) {
            log.debug(this.code + ":" + key);
        }

        if (rule instanceof Boolean) {
            if (BooleanUtils.isFalse((Boolean) rule)) {
                return null;
            }
        }

        String val = MapUtils.getString(map, key);

        if (StringUtils.isBlank(val)) {
            return null;
        }

        if (GenericValidator.isEmail(val)) {
            return null;
        }

        return this.getMessage(key, message, new Object[0]);
    }
}