package top.cardone.validator.func;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.DateValidator;

import java.util.Map;

/**
 * @author yao hai tao
 * @date 2016/9/19
 */
@Log4j2
public class DateISOValidatorFunc extends RequiredValidatorFunc {
    public DateISOValidatorFunc() {
        this.code = "dateISO";
    }

    @Override
    public Map<String, Object> func(Map<String, Object> map, String key, Object rule, Object message) {
        if (log.isDebugEnabled()) {
            log.debug(this.code + ":" + key);
        }

        String val = MapUtils.getString(map, key);

        if (StringUtils.isBlank(val)) {
            return null;
        }

        if (DateValidator.getInstance().isValid(val, StringUtils.defaultIfBlank((String) rule, "yyyy-MM-dd"), org.springframework.context.i18n.LocaleContextHolder.getLocale())) {
            return null;
        }

        return this.getMessage(key, message, new Object[0]);
    }
}