package top.cardone.validator.func;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;

import java.util.Map;

/**
 * @author yao hai tao
 * @date 2016/9/19
 */
@Log4j2
public class DigitsValidatorFunc extends RequiredValidatorFunc {
    public DigitsValidatorFunc() {
        this.code = "digits";
    }

    @Override
    public Map<String, Object> func(Map<String, Object> map, String key, Object rule, Object message) {
        if (log.isDebugEnabled()) {
            log.debug(this.code + ":" + key);
        }

        if (rule instanceof Boolean) {
            if (BooleanUtils.isFalse((Boolean) rule)) {
                return null;
            }
        }

        Integer val = MapUtils.getInteger(map, key);

        if (val != null) {
            return null;
        }

        return this.getMessage(key, message, new Object[0]);
    }
}