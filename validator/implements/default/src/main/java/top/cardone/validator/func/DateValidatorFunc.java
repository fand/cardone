package top.cardone.validator.func;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.validator.GenericValidator;

import java.util.Map;
import java.util.Objects;

/**
 * @author yao hai tao
 * @date 2016/9/19
 */
@Log4j2
public class DateValidatorFunc extends RequiredValidatorFunc {
    public DateValidatorFunc() {
        this.code = "date";
    }

    @Override
    public Map<String, Object> func(Map<String, Object> map, String key, Object rule, Object message) {
        if (log.isDebugEnabled()) {
            log.debug(this.code + ":" + key);
        }

        if (rule instanceof Boolean) {
            if (BooleanUtils.isFalse((Boolean) rule)) {
                return null;
            }
        }

        String val = MapUtils.getString(map, key);

        if (Objects.isNull(val)) {
            return null;
        }

        if (GenericValidator.isDate(val, org.springframework.context.i18n.LocaleContextHolder.getLocale())) {
            return null;
        }

        return this.getMessage(key, message, new Object[0]);
    }
}