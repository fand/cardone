package top.cardone.core.util.action;

/**
 * 动作
 *
 * @param <T1> 输入泛型1
 * @author yao hai tao
 */
@FunctionalInterface
public interface Action1<T1> extends java.io.Serializable {
    /**
     * 运行
     *
     * @param t1 输入泛型参数1
     */
    void action(final T1 t1);
}