package top.cardone.core.util.action;

/**
 * 动作
 *
 * @param <T1> 输入泛型1
 * @param <T2> 输入泛型2
 * @param <T3> 输入泛型3
 * @param <T4> 输入泛型4
 * @author yao hai tao
 */
@FunctionalInterface
public interface Action5<T1, T2, T3, T4, T5> extends java.io.Serializable {
    /**
     * 运行
     *
     * @param t1 输入泛型参数1
     * @param t2 输入泛型参数2
     * @param t3 输入泛型参数3
     * @param t4 输入泛型参数4
     */
    void action(final T1 t1, final T2 t2, final T3 t3, final T4 t4, final T5 t5);
}