package top.cardone.core.util.action;

/**
 * 动作
 *
 * @param <T1> 输入泛型1
 * @param <T2> 输入泛型2
 * @param <T3> 输入泛型3
 * @author yao hai tao
 */
@FunctionalInterface
public interface Action3<T1, T2, T3> extends java.io.Serializable {
    /**
     * 运行
     *
     * @param t1 输入泛型参数1
     * @param t2 输入泛型参数2
     * @param t3 输入泛型参数3
     */
    void action(final T1 t1, final T2 t2, final T3 t3);
}