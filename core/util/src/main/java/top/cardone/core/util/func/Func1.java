package top.cardone.core.util.func;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.util.StringUtils;

/**
 * 方法
 *
 * @param <R>  输出泛型
 * @param <T1> 输入泛型1
 * @author yao hai tao
 */
@FunctionalInterface
@CacheConfig(cacheNames = {"top.cardone.core.util.func.Func1"})
public interface Func1<R, T1> extends java.io.Serializable {
    /**
     * 执行
     *
     * @param t1 输入泛型参数1
     * @return 输出泛型值
     */
    R func(final T1 t1);

    @Cacheable
    default R funcCache(final T1 t1) {
        return this.func(t1);
    }

    @CacheEvict(allEntries = true)
    default String cleanCache(final T1 t1) {
        return StringUtils.arrayToCommaDelimitedString(new Object[]{t1});
    }
}
