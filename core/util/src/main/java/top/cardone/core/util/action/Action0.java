package top.cardone.core.util.action;

/**
 * 动作
 *
 * @author yao hai tao
 */
@FunctionalInterface
public interface Action0 extends java.io.Serializable {
    /**
     * 运行
     */
    void action();
}