package top.cardone.core.util.func;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.util.StringUtils;

/**
 * 方法
 *
 * @param <R>  输出泛型
 * @param <T1> 输入泛型1
 * @param <T2> 输入泛型2
 * @param <T3> 输入泛型3
 * @param <T4> 输入泛型4
 * @author yao hai tao
 */
@FunctionalInterface
@CacheConfig(cacheNames = {"top.cardone.core.util.func.Func5"})
public interface Func5<R, T1, T2, T3, T4, T5> extends java.io.Serializable {
    /**
     * 执行
     *
     * @param t1 输入泛型参数1
     * @param t2 输入泛型参数2
     * @param t3 输入泛型参数3
     * @param t4 输入泛型参数4
     * @return 输出泛型值
     */
    R func(final T1 t1, final T2 t2, final T3 t3, final T4 t4, final T5 t5);

    @Cacheable
    default R funcCache(final T1 t1, final T2 t2, final T3 t3, final T4 t4, final T5 t5) {
        return this.func(t1, t2, t3, t4, t5);
    }

    @CacheEvict(allEntries = true)
    default String cleanCache(final T1 t1, final T2 t2, final T3 t3, final T4 t4, final T5 t5) {
        return StringUtils.arrayToCommaDelimitedString(new Object[]{t1, t2, t3, t4, t5});
    }
}
