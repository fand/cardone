package top.cardone.core.util.func;

/**
 * 方法
 *
 * @param <R> 输出泛型
 * @author yao hai tao
 */
@FunctionalInterface
public interface Func0<R> extends java.io.Serializable {
    /**
     * 执行
     *
     * @return 输出泛型值
     */
    R func();
}
