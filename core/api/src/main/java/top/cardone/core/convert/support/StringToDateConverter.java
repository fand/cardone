package top.cardone.core.convert.support;

import org.springframework.core.convert.converter.Converter;

import java.text.ParseException;
import java.util.Date;

/**
 * 时间工具类
 *
 * @author yao hai tao
 */
@lombok.extern.log4j.Log4j2
public class StringToDateConverter implements Converter<String, Date> {
    /**
     * 常用时间格式化字符串
     */
    @lombok.Setter
    private static String[] parsePatterns = {"yyyy-MM-dd HH:mm:ss.SSS", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm", "yyyy-MM-dd HH", "yyyy-MM-dd", "yyyy-MM", "yyyyMMdd HH:mm:ss.SSS", "yyyyMMdd HH:mm:ss", "yyyyMMdd HH:mm", "yyyyMMdd HH", "yyyyMMddHHmmss", "yyyyMMddHHmm", "yyyyMMddHH", "yyyyMMdd", "yyyyMM", "yyyy"};

    @Override
    public Date convert(String source) {
        try {
            return org.apache.commons.lang3.time.DateUtils.parseDate(source, parsePatterns);
        } catch (ParseException e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }
}