package top.cardone.core.convert.support;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.core.convert.converter.Converter;

import java.util.Date;

/**
 * 时间工具类
 *
 * @author yao hai tao
 */
@lombok.extern.log4j.Log4j2
public class DateToStringConverter implements Converter<Date, String> {
    /**
     * 时间格式化字符串
     */
    @lombok.Setter
    private String pattern = DateFormatUtils.ISO_8601_EXTENDED_DATE_FORMAT.getPattern() + StringUtils.SPACE + DateFormatUtils.ISO_8601_EXTENDED_TIME_FORMAT.getPattern();

    @Override
    public String convert(Date source) {
        return DateFormatUtils.format(source, pattern);
    }
}
