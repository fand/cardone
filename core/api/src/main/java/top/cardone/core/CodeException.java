package top.cardone.core;

import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

/**
 * @author yao hai tao
 */
@lombok.Getter
@lombok.Setter
@AllArgsConstructor
@lombok.experimental.Accessors(chain = true)
public class CodeException extends RuntimeException {
    private final Object[] args;

    private String errorCode;

    public CodeException(final String errorCode, final Object[] args, String defaultMessage, final Throwable cause) {
        super(Objects.isNull(cause) ? defaultMessage : StringUtils.defaultIfBlank(defaultMessage, cause.getMessage()), cause);
        this.errorCode = errorCode;
        this.args = args;
    }

    public CodeException(final String errorCode, String defaultMessage, final Object... args) {
        this(errorCode, args, defaultMessage, null);
    }

    public CodeException(final String errorCode, final Object[] args, final Throwable cause) {
        this(errorCode, args, errorCode, cause);
    }

    public CodeException(final String errorCode) {
        this(errorCode, null, errorCode, null);
    }

    public CodeException(final String errorCode, final String defaultMessage) {
        this(errorCode, null, defaultMessage, null);
    }

    public CodeException(final String errorCode, final Throwable cause) {
        this(errorCode, null, errorCode, cause);
    }
}