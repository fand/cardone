package top.cardone.context.action;

import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import top.cardone.core.util.action.Action3;

/**
 * @author yao hai tao
 * @date 2016/4/18
 */
@Log4j2
public class BatchAction3<T1, T2, T3> implements Action3<T1, T2, T3> {
    @Setter
    private Action3<T1, T2, T3>[] actions;

    @Setter
    private boolean isExitSystem = false;

    @Setter
    private long sleep = 3000;

    @Override
    public void action(T1 t1, T2 t2, T3 t3) {
        actionAll(t1, t2, t3);

        if (this.isExitSystem) {
            System.exit(0);
        }
    }

    private void actionAll(T1 t1, T2 t2, T3 t3) {
        for (Action3<T1, T2, T3> action : actions) {
            try {
                action.action(t1, t2, t3);

                Thread.sleep(sleep);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
    }
}