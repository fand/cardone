package top.cardone.context.event;

import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

/**
 * @author yao hai tao
 * @date 2017/7/2
 */
public class SimpleBeforeEvent extends ApplicationEvent {
    @Setter
    @Getter
    private String[] flags;

    @Setter
    @Getter
    private String[] configs;

    @Setter
    @Getter
    private Object[] args;

    public SimpleBeforeEvent(String[] flags, Object[] args, String[] configs) {
        super(flags);

        this.flags = flags;

        this.args = args;

        this.configs = configs;
    }

    public SimpleBeforeEvent(Object source, String[] flags, Object[] args, String[] configs) {
        super(source);

        this.flags = flags;

        this.args = args;

        this.configs = configs;
    }
}
