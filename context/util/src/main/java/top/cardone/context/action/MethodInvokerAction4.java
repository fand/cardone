package top.cardone.context.action;

import top.cardone.context.TaskExecutorMethodInvoker;
import top.cardone.core.util.action.Action4;

/**
 * Created by cardo on 2018/2/10 0010.
 */
public class MethodInvokerAction4<T1, T2, T3, T4> extends TaskExecutorMethodInvoker implements Action4<T1, T2, T3, T4> {
    @Override
    public void action(T1 t1, T2 t2, T3 t3, T4 t4) {
        super.taskExecutorInvoke(new Object[]{t1, t2, t3, t4});
    }
}