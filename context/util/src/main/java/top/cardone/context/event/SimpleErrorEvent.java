package top.cardone.context.event;

import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

/**
 * @author yao hai tao
 * @date 2017/7/2
 */
public class SimpleErrorEvent extends ApplicationEvent {
    @Setter
    @Getter
    private String[] flags;

    @Setter
    @Getter
    private String[] configs;

    @Setter
    @Getter
    private Object[] args;

    @Setter
    @Getter
    private Throwable throwable;

    public SimpleErrorEvent(String[] flags, Object[] args, String[] configs, Throwable throwable) {
        super(flags);

        this.flags = flags;

        this.args = args;

        this.configs = configs;

        this.throwable = throwable;
    }

    public SimpleErrorEvent(Object source, String[] flags, Object[] args, String[] configs, Throwable throwable) {
        super(source);

        this.flags = flags;

        this.args = args;

        this.configs = configs;

        this.throwable = throwable;
    }
}
