package top.cardone.context.action;

import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import top.cardone.core.util.action.Action2;

/**
 * @author yao hai tao
 * @date 2016/4/18
 */
@Log4j2
public class BatchAction2<T1, T2> implements Action2<T1, T2> {
    @Setter
    private Action2<T1, T2>[] actions;

    @Setter
    private boolean isExitSystem = false;

    @Setter
    private long sleep = 3000;

    @Override
    public void action(T1 t1, T2 t2) {
        actionAll(t1, t2);

        if (this.isExitSystem) {
            System.exit(0);
        }
    }

    private void actionAll(T1 t1, T2 t2) {
        for (Action2<T1, T2> action : actions) {
            try {
                action.action(t1, t2);

                Thread.sleep(sleep);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
    }
}