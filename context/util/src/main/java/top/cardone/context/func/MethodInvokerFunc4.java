package top.cardone.context.func;

import top.cardone.context.TaskExecutorMethodInvoker;
import top.cardone.core.util.func.Func4;

/**
 * Created by cardo on 2018/2/10 0010.
 */
public class MethodInvokerFunc4<R, T1, T2, T3, T4> extends TaskExecutorMethodInvoker implements Func4<R, T1, T2, T3, T4> {
    @Override
    public R func(T1 t1, T2 t2, T3 t3, T4 t4) {
        return (R) super.taskExecutorInvoke(new Object[]{t1, t2, t3, t4});
    }
}