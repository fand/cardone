package top.cardone.context.action;

import top.cardone.context.TaskExecutorMethodInvoker;
import top.cardone.core.util.action.Action1;

/**
 * Created by cardo on 2018/2/10 0010.
 */
public class MethodInvokerAction1<T1> extends TaskExecutorMethodInvoker implements Action1<T1> {
    @Override
    public void action(T1 t1) {
        super.taskExecutorInvoke(new Object[]{t1});
    }
}