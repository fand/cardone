package top.cardone.context.action;

import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import top.cardone.core.util.action.Action4;

/**
 * @author yao hai tao
 * @date 2016/4/18
 */
@Log4j2
public class BatchAction4<T1, T2, T3, T4> implements Action4<T1, T2, T3, T4> {
    @Setter
    private Action4<T1, T2, T3, T4>[] actions;

    @Setter
    private boolean isExitSystem = false;

    @Setter
    private long sleep = 3000;

    @Override
    public void action(T1 t1, T2 t2, T3 t3, T4 t4) {
        actionAll(t1, t2, t3, t4);

        if (this.isExitSystem) {
            System.exit(0);
        }
    }

    private void actionAll(T1 t1, T2 t2, T3 t3, T4 t4) {
        for (Action4<T1, T2, T3, T4> action : actions) {
            try {
                action.action(t1, t2, t3, t4);

                Thread.sleep(sleep);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
    }
}