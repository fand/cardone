package top.cardone.context.action;

import top.cardone.context.TaskExecutorMethodInvoker;
import top.cardone.core.util.action.Action3;

/**
 * Created by cardo on 2018/2/10 0010.
 */
public class MethodInvokerAction3<T1, T2, T3> extends TaskExecutorMethodInvoker implements Action3<T1, T2, T3> {
    @Override
    public void action(T1 t1, T2 t2, T3 t3) {
        super.taskExecutorInvoke(new Object[]{t1, t2, t3});
    }
}