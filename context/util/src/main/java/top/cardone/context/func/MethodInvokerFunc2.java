package top.cardone.context.func;

import top.cardone.context.TaskExecutorMethodInvoker;
import top.cardone.core.util.func.Func2;

/**
 * Created by cardo on 2018/2/10 0010.
 */
public class MethodInvokerFunc2<R, T1, T2> extends TaskExecutorMethodInvoker implements Func2<R, T1, T2> {
    @Override
    public R func(T1 t1, T2 t2) {
        return (R) super.taskExecutorInvoke(new Object[]{t1, t2});
    }
}