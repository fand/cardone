package top.cardone.context.util;

import org.springframework.util.StringUtils;
import top.cardone.context.ApplicationContextHolder;
import top.cardone.core.util.func.Func3;

import java.util.Locale;

/**
 * 消息工具类
 *
 * @author yao hai tao
 */
public class MessageUtils {
    private MessageUtils() {
    }

    /**
     * 获取消息
     *
     * @param code           代码
     * @param defaultMessage 默认消息
     * @return 消息
     */
    public static String getMessage(String code, String defaultMessage) {
        return getMessage(code, null, org.apache.commons.lang3.StringUtils.defaultIfBlank(defaultMessage, code));
    }

    /**
     * 获取消息
     *
     * @param code           代码
     * @param args           参数
     * @param defaultMessage 默认消息
     * @return 消息
     */
    public static String getMessage(String code, Object[] args, String defaultMessage) {
        if (ApplicationContextHolder.getApplicationContext() == null) {
            return org.apache.commons.lang3.StringUtils.defaultIfBlank(defaultMessage, code);
        }

        return getMessage(code, args, org.apache.commons.lang3.StringUtils.defaultIfBlank(defaultMessage, code), org.springframework.context.i18n.LocaleContextHolder.getLocale());
    }

    /**
     * 获取消息
     *
     * @param code           代码
     * @param args           参数
     * @param defaultMessage 默认消息
     * @param locale         时区
     * @return 消息
     */
    public static String getMessage(String code, Object[] args, String defaultMessage, Locale locale) {
        if (ApplicationContextHolder.getApplicationContext() == null) {
            return org.apache.commons.lang3.StringUtils.defaultIfBlank(defaultMessage, code);
        }

        return ApplicationContextHolder.getApplicationContext().getMessage(code, args, org.apache.commons.lang3.StringUtils.defaultIfBlank(defaultMessage, code), locale);
    }

    /**
     * @param url                      url
     * @param code                     代码
     * @param args                     参数
     * @param defaultString            默认字符
     * @param readOneMessageFuncBeanId 读取方法beanId
     * @return 字符串
     */
    public static String getMessage(String url, String code, Object[] args, String defaultString, String readOneMessageFuncBeanId) {
        if (StringUtils.startsWithIgnoreCase(code, "dynamic")) {
            return getMessage(defaultString, code, args, org.apache.commons.lang3.StringUtils.defaultIfBlank(defaultString, code));
        }

        String message = ApplicationContextHolder.func(Func3.class, func -> (String) func.func(url, code, org.apache.commons.lang3.StringUtils.defaultIfBlank(defaultString, code)), readOneMessageFuncBeanId);

        return getMessage(message, code, args, org.apache.commons.lang3.StringUtils.defaultIfBlank(defaultString, code));
    }

    /**
     * 转换为消息
     *
     * @param message        消息
     * @param code           代码
     * @param args           参数
     * @param defaultMessage 默认消息
     * @return 消息
     */
    public static String getMessage(String message, String code, Object[] args, String defaultMessage) {
        if (!StringUtils.isEmpty(message)) {
            return MessageFormatUtils.getMessageFormat(message, org.springframework.context.i18n.LocaleContextHolder.getLocale()).format(args);
        }

        return getMessage(code, args, defaultMessage, org.springframework.context.i18n.LocaleContextHolder.getLocale());
    }
}