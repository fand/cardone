package top.cardone.context.util;

import com.google.common.util.concurrent.Striped;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;

import java.util.concurrent.locks.Lock;

@Log4j2
public class LockUtils {
    @Getter(lazy = true)
    private final static Striped<Lock> lazyWeakLock = lazyWeakLock();

    private static Striped<Lock> lazyWeakLock() {
        return Striped.lazyWeakLock(Runtime.getRuntime().availableProcessors() * 10);
    }
}