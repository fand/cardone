package top.cardone.context.func;

import top.cardone.context.TaskExecutorMethodInvoker;
import top.cardone.core.util.func.Func1;

/**
 * Created by cardo on 2018/2/10 0010.
 */
public class MethodInvokerFunc1<R, T1> extends TaskExecutorMethodInvoker implements Func1<R, T1> {
    @Override
    public R func(T1 t1) {
        return (R) super.taskExecutorInvoke(new Object[]{t1});
    }
}