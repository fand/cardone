package top.cardone.context.event;

import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

/**
 * @author yao hai tao
 * @date 2017/7/1
 */
public class SimpleEvent extends ApplicationEvent {
    @Setter
    @Getter
    private String[] flags;

    @Setter
    @Getter
    private String[] configs;

    @Setter
    @Getter
    private Object[] args;

    @Setter
    @Getter
    private Object output;

    public SimpleEvent(String[] flags, Object[] args, Object output, String[] configs) {
        super(flags);

        this.flags = flags;

        this.args = args;

        this.output = output;

        this.configs = configs;
    }

    public SimpleEvent(Object source, String[] flags, Object[] args, Object output, String[] configs) {
        super(source);

        this.flags = flags;

        this.args = args;

        this.output = output;

        this.configs = configs;
    }
}
