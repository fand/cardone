package top.cardone.context.action;

import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import top.cardone.core.util.action.Action0;

/**
 * @author yao hai tao
 * @date 2016/4/18
 */
@Log4j2
public class BatchAction0 implements Action0 {
    @Setter
    private Action0[] actions;

    @Setter
    private boolean isExitSystem = false;

    @Setter
    private long sleep = 3000;

    @Override
    public void action() {
        actionAll();

        if (this.isExitSystem) {
            System.exit(0);
        }
    }

    private void actionAll() {
        for (Action0 action : actions) {
            if (action == null) {
                continue;
            }

            try {
                action.action();

                Thread.sleep(sleep);
            } catch (Exception e) {
                log.error(e.getMessage() + ":" + action.getClass().getName(), e);
            }
        }
    }
}