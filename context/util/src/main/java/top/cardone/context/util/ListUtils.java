package top.cardone.context.util;

import com.google.common.collect.Lists;
import org.apache.commons.collections.CollectionUtils;
import top.cardone.core.util.func.Func1;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author yao hai tao
 * @date 2016/4/20
 */
public class ListUtils extends org.apache.commons.collections.ListUtils {
    public static <V> List<Map<String, V>> newArrayList(List<Map<String, V>> list, Func1<Map<String, V>, Map<String, V>> func) {
        if (func == null || CollectionUtils.isEmpty(list)) {
            return Lists.newArrayList();
        }

        List<Map<String, V>> newList = Lists.newArrayList();

        for (Map<String, V> map : list) {
            newList.add(MapUtils.newMap(map, func));
        }

        return newList;
    }

    public static <E> ArrayList<E> newArrayList(Iterable<? extends E> elements) {
        if (elements == null) {
            return Lists.newArrayList();
        }

        return Lists.newArrayList(elements);
    }
}