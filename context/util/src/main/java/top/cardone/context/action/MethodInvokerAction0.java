package top.cardone.context.action;

import lombok.Setter;
import top.cardone.context.TaskExecutorMethodInvoker;
import top.cardone.core.util.action.Action0;

/**
 * Created by cardo on 2018/2/10 0010.
 */
public class MethodInvokerAction0 extends TaskExecutorMethodInvoker implements Action0 {
    @Setter
    private Object[] arguments = new Object[0];

    @Override
    public void action() {
        super.taskExecutorInvoke(arguments);
    }
}