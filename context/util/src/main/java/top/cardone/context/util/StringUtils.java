package top.cardone.context.util;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.Setter;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.CollectionUtils;
import org.springframework.util.PathMatcher;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 字符串工具类
 *
 * @author yao hai tao
 */
public class StringUtils extends org.apache.commons.lang3.StringUtils {
    @lombok.Setter
    private static java.util.Map<String, AntPathMatcher> pathMatcherMap = Maps.newConcurrentMap();

    @Setter
    private static Map<String, String> escapeSqlMap;

    static {
        escapeSqlMap = Maps.newHashMap();

        escapeSqlMap.put("'", "''");
        escapeSqlMap.put("\\\\", "\\\\\\\\");
        escapeSqlMap.put("%", "\\\\%");
        escapeSqlMap.put("_", "\\\\_");
    }

    private StringUtils() {
    }

    /**
     * 返回不为空的字符串
     *
     * @param strs 字符串集合
     * @return 不为空的字符串
     */
    public static String defaultIfBlank(final String... strs) {
        if (org.apache.commons.lang3.ArrayUtils.isEmpty(strs)) {
            return null;
        }

        for (final String str : strs) {
            if (org.apache.commons.lang3.StringUtils.isNotBlank(str)) {
                return str;
            }
        }

        return null;
    }

    /**
     * 获取路径
     *
     * @param paths 路径集合
     * @param url   路径
     * @return 路径
     */
    public static String getPathForMatch(final java.util.Collection<String> paths, final String url) {
        return getPathForMatch(null, paths, url);
    }

    /**
     * 获取路径
     *
     * @param pathsMap 路径集合
     * @param url      路径
     * @return 路径
     */
    public static String getPathForMatch(final Map<String, List<String>> pathsMap, final String url) {
        if (MapUtils.isEmpty(pathsMap)) {
            return null;
        }

        for (Map.Entry<String, List<String>> pathsEntry : pathsMap.entrySet()) {
            if (org.apache.commons.lang3.StringUtils.isNotBlank(top.cardone.context.util.StringUtils.getPathForMatch(pathsEntry.getValue(), url))) {
                return pathsEntry.getKey();
            }
        }

        return null;
    }

    /**
     * 获取路径
     *
     * @param pathSeparator 路径间隔
     * @param paths         路径集合
     * @param url           路径
     * @return 路径
     */
    public static String getPathForMatch(String pathSeparator, final Collection<String> paths, final String url) {
        if (CollectionUtils.isEmpty(paths)) {
            return null;
        }

        if (org.apache.commons.lang3.StringUtils.isBlank(url)) {
            return null;
        }

        PathMatcher pathMatcher = StringUtils.getPathMatcher(pathSeparator);

        for (final String path : paths) {
            if (StringUtils.equals(path, url)) {
                return path;
            }

            if (pathMatcher.match(path, url)) {
                return path;
            }
        }

        return null;
    }

    /**
     * 获取路径比较
     *
     * @return 路径比较
     */
    public static PathMatcher getPathMatcher() {
        return getPathMatcher(null);
    }

    /**
     * 获取路径比较
     *
     * @param pathSeparator 路径间隔
     * @return 路径比较
     */
    public static PathMatcher getPathMatcher(String pathSeparator) {
        String newPathSeparator = org.apache.commons.lang3.StringUtils.defaultIfBlank(pathSeparator, AntPathMatcher.DEFAULT_PATH_SEPARATOR);

        AntPathMatcher antPathMatcher = StringUtils.pathMatcherMap.get(newPathSeparator);

        if (antPathMatcher == null) {
            antPathMatcher = new AntPathMatcher(newPathSeparator);

            pathMatcherMap.put(newPathSeparator, antPathMatcher);
        }

        antPathMatcher.setCachePatterns(true);

        return antPathMatcher;
    }

    /**
     * 比较
     *
     * @param paths 路径集合
     * @param url   路径
     * @return 比较结果
     */
    public static boolean matchs(final Collection<String> paths, final String url) {
        if (CollectionUtils.isEmpty(paths)) {
            return false;
        }

        String path = StringUtils.getPathForMatch(paths, url);

        return org.apache.commons.lang3.StringUtils.isNotBlank(path);
    }

    /**
     * 比较
     *
     * @param pathsStr 路径集合
     * @param url      路径
     * @return 比较结果
     */
    public static boolean matchs(final String pathsStr, final String url) {
        final Collection<String> paths = Lists.newArrayList(org.apache.commons.lang3.StringUtils.split(pathsStr, ","));

        return StringUtils.matchs(paths, url);
    }

    /**
     * 比较
     *
     * @param pathSeparator 路径间隔
     * @param path          路径
     * @param url           路径
     * @return 比较结果
     */
    public static boolean match(String pathSeparator, final String path, final String url) {
        PathMatcher pathMatcher = StringUtils.getPathMatcher(pathSeparator);

        return pathMatcher.match(path, url);
    }

    /**
     * 比较
     *
     * @param path 路径
     * @param url  路径
     * @return 比较结果
     */
    public static boolean match(final String path, final String url) {
        return match(null, path, url);
    }

    /**
     * 批量替换字符串
     *
     * @param text           文本
     * @param replacementMap 替换字符串映射
     * @return 替换后字符串
     */
    public static String replaces(String text, Map<String, String> replacementMap) {
        if (isBlank(text)) {
            return text;
        }

        for (Map.Entry<String, String> replacementEntry : replacementMap.entrySet()) {
            text = org.apache.commons.lang3.StringUtils.replace(text, replacementEntry.getKey(), replacementEntry.getValue());
        }

        return text;
    }

    /**
     * 转义sql字符串
     *
     * @param sqlStr sql字符串
     * @return 转义后sql字符串
     */
    public static String escapeSql(String sqlStr) {
        return replaces(sqlStr, escapeSqlMap);
    }

    public static int length(final String str) {
        if (str == null) {
            return 0;
        }

        int strLength = 0;

        String chinese = "[\u0391-\uFFE5]";

        for (int i = 0; i < str.length(); i++) {
            strLength += str.substring(i, i + 1).matches(chinese) ? 2 : 1;
        }

        return strLength;
    }

    public String remove(String str, String... removes) {
        for (String remove : removes) {
            str = org.apache.commons.lang3.StringUtils.remove(str, remove);
        }

        return str;
    }
}