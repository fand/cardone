package top.cardone.context.action;

import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import top.cardone.core.util.action.Action1;

/**
 * @author yao hai tao
 * @date 2016/4/18
 */
@Log4j2
public class BatchAction1<T1> implements Action1<T1> {
    @Setter
    private Action1<T1>[] actions;

    @Setter
    private boolean isExitSystem = false;

    @Setter
    private long sleep = 3000;

    @Override
    public void action(T1 t1) {
        actionAll(t1);

        if (this.isExitSystem) {
            System.exit(0);
        }
    }

    private void actionAll(T1 t1) {
        for (Action1<T1> action : actions) {
            try {
                action.action(t1);

                Thread.sleep(sleep);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
    }
}