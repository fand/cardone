package top.cardone.context.action;

import top.cardone.context.TaskExecutorMethodInvoker;
import top.cardone.core.util.action.Action2;

/**
 * Created by cardo on 2018/2/10 0010.
 */
public class MethodInvokerAction2<T1, T2> extends TaskExecutorMethodInvoker implements Action2<T1, T2> {
    @Override
    public void action(T1 t1, T2 t2) {
        super.taskExecutorInvoke(new Object[]{t1, t2});
    }
}