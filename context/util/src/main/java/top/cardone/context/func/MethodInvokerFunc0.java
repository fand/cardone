package top.cardone.context.func;

import lombok.Setter;
import top.cardone.context.TaskExecutorMethodInvoker;
import top.cardone.core.util.func.Func0;

/**
 * Created by cardo on 2018/2/10 0010.
 */
public class MethodInvokerFunc0<R> extends TaskExecutorMethodInvoker implements Func0<R> {
    @Setter
    private Object[] arguments = new Object[0];

    @Override
    public R func() {
        return (R) super.taskExecutorInvoke(arguments);
    }
}