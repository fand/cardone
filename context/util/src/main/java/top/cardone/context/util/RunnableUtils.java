package top.cardone.context.util;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import lombok.extern.log4j.Log4j2;
import org.springframework.core.task.AsyncListenableTaskExecutor;
import org.springframework.util.concurrent.ListenableFuture;
import top.cardone.context.ApplicationContextHolder;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Log4j2
public class RunnableUtils {
    static ThreadLocal<Map<String, ListenableFuture<?>>> runnableMapThreadlocal = ThreadLocal.withInitial(() -> Maps.newConcurrentMap());

    /**
     * 多线程执行，执行前加入本地线程map,执行后或异常移出本地线程map
     *
     * @param runnable  异步
     * @param beanNames spring bean names
     */
    public static void execute(Runnable runnable, String... beanNames) {
        ListenableFuture<?> future = ApplicationContextHolder.getBean(AsyncListenableTaskExecutor.class, beanNames).submitListenable(runnable);

        Map<String, ListenableFuture<?>> runnableMap = runnableMapThreadlocal.get();

        String key = UUID.randomUUID().toString();

        future.addCallback(
                it -> runnableMap.remove(key),
                ex -> {
                    runnableMap.remove(key);

                    log.error(ex);
                }
        );

        runnableMap.put(key, future);
    }

    /**
     * 等持本地线程map中多线程全部执行完
     */
    public static void waitExecute() {
        Map<String, ListenableFuture<?>> runnableMap = runnableMapThreadlocal.get();

        if (runnableMap.isEmpty()) {
            return;
        }

        while (true) {
            runnableMap.keySet().iterator().forEachRemaining(key -> {
                ListenableFuture<?> future = runnableMap.get(key);

                if (future == null || future.isDone() || future.isCancelled()) {
                    runnableMap.remove(key);
                }
            });

            if (runnableMap.isEmpty()) {
                break;
            }

            try {
                TimeUnit.MICROSECONDS.sleep(100);
            } catch (InterruptedException e) {
                log.error(e);
            }
        }
    }
}