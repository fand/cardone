package top.cardone.context.func;

import top.cardone.context.TaskExecutorMethodInvoker;
import top.cardone.core.util.func.Func3;

/**
 * Created by cardo on 2018/2/10 0010.
 */
public class MethodInvokerFunc3<R, T1, T2, T3> extends TaskExecutorMethodInvoker implements Func3<R, T1, T2, T3> {
    @Override
    public R func(T1 t1, T2 t2, T3 t3) {
        return (R) super.taskExecutorInvoke(new Object[]{t1, t2, t3});
    }
}