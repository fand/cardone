package top.cardone;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

/**
 * @author yao hai tao
 */
public class CardOneApplication implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication.run(CardOneApplication.class, args);
    }

    @Override
    public void run(String... args) {
    }
}