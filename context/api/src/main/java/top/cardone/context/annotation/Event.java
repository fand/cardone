package top.cardone.context.annotation;

import org.springframework.context.ApplicationEvent;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 执行标注
 *
 * @author yao hai tao
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface Event {
    String[] configs() default {};

    @AliasFor("applicationEvent")
    Class<?> value() default ApplicationEvent.class;

    @AliasFor("value")
    Class<?> applicationEvent() default ApplicationEvent.class;
}
