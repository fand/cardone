package top.cardone.cache;

import top.cardone.core.util.func.Func0;

import java.util.List;
import java.util.UUID;

/**
 * @author yao hai tao
 * @date 2015/9/9
 */
public interface Cache {
    String ID = UUID.randomUUID().toString();

    /**
     * 清理缓存
     *
     * @param cacheNames 缓存名
     */
    void clear(String... cacheNames);

    void clear(int maxLevelSize, String... cacheNames);

    /**
     * 清理缓存
     *
     * @param skipCacheNames 跳过缓存名
     */
    void clearBySkipNames(String... skipCacheNames);

    void clearBySkipNames(int maxLevelSize, String... skipCacheNames);

    void evict(String cacheName, Object key);

    void evict(String cacheName, int maxLevelSize, Object key);

    /**
     * 获取缓存值
     *
     * @param requiredType 返回类型
     * @param cacheName    缓存名称
     * @param key          缓存键名
     * @return 缓存值
     */
    <T> T get(Class<T> requiredType, String cacheName, Object key);

    /**
     * 获取缓存值
     *
     * @param requiredType 返回类型
     * @param cacheName    缓存名称
     * @param maxLevelSize 最大层次大小
     * @param key          缓存键名
     * @return 缓存值
     */
    <T> T get(Class<T> requiredType, String cacheName, int maxLevelSize, Object key);

    /**
     * 获取缓存值
     *
     * @param cacheName 缓存名称
     * @param key       缓存键名
     * @return 缓存值
     */
    Object get(String cacheName, Object key);

    /**
     * 获取缓存值
     *
     * @param cacheName 缓存名称
     * @param key       缓存键名
     * @param func      方法
     * @return 缓存值
     */
    <T> T get(String cacheName, Object key, Func0<T> func);

    /**
     * 获取缓存值
     *
     * @param cacheName    缓存名称
     * @param maxLevelSize 最大层次大小
     * @param key          缓存键名
     * @param func         方法
     * @return 缓存值
     */
    <T> T get(String cacheName, int maxLevelSize, Object key, Func0<T> func);

    /**
     * 获取缓存值
     *
     * @param cacheName 缓存名称
     * @param key       缓存键名
     * @return 缓存值
     */
    org.springframework.cache.Cache.ValueWrapper getCacheValueWrapper(String cacheName, Object key);

    /**
     * 获取缓存
     *
     * @param cacheName 缓存名称
     * @return 缓存
     */
    List<? extends org.springframework.cache.Cache> getCaches(String cacheName);

    /**
     * 获取缓存
     *
     * @param cacheName    缓存名称
     * @param maxLevelSize 最大层次大小
     * @return 缓存
     */
    List<? extends org.springframework.cache.Cache> getCaches(String cacheName, int maxLevelSize);

    void put(String cacheName, Object key, Object value);

    void put(String cacheName, int maxLevelSize, Object key, Object value);

    org.springframework.cache.Cache.ValueWrapper putIfAbsent(String cacheName, Object key, Object value);

    org.springframework.cache.Cache.ValueWrapper putIfAbsent(String cacheName, int maxLevelSize, Object key, Object value);
}
