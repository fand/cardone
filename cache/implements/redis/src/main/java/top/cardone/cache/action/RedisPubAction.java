package top.cardone.cache.action;

import com.google.common.collect.Maps;
import lombok.Setter;
import org.springframework.data.redis.core.RedisTemplate;
import top.cardone.core.util.action.Action3;

import java.util.Map;

/**
 * Created by cardo on 2018/2/6 0006.
 */
public class RedisPubAction implements Action3<String, String, Object[]> {
    @Setter
    private RedisTemplate redisTemplate;

    @Setter
    private String channel = "topic:cache";

    @Override
    public void action(String id, String methodName, Object[] inputs) {
        Map<String, Object> message = Maps.newHashMap();

        message.put("id", id);
        message.put("methodName", methodName);
        message.put("inputs", inputs);

        redisTemplate.convertAndSend(channel, message);
    }
}