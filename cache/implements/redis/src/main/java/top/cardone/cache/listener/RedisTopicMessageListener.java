package top.cardone.cache.listener;

import lombok.Setter;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.core.RedisTemplate;
import top.cardone.cache.Cache;
import top.cardone.context.ApplicationContextHolder;
import top.cardone.context.util.MapUtils;

import java.util.Map;

/**
 * Created by cardo on 2018/2/6 0006.
 */
public class RedisTopicMessageListener implements MessageListener {
    @Setter
    private RedisTemplate redisTemplate;

    @Override
    public void onMessage(Message message, byte[] pattern) {
        Map<String, Object> messageMap = (Map<String, Object>) redisTemplate.getValueSerializer().deserialize(message.getBody());

        if (MapUtils.isEmpty(messageMap)) {
            return;
        }

        String cacheId = MapUtils.getString(messageMap, "id");

        if (Cache.ID.equals(cacheId)) {
            return;
        }

        String methodName = MapUtils.getString(messageMap, "methodName");

        if ("evict".equals(methodName)) {
            Object[] inputs = (Object[]) MapUtils.getObject(messageMap, "inputs");

            String cacheName = (String) inputs[0];

            ApplicationContextHolder.getBean(Cache.class).evict(cacheName, 1, inputs[1]);
        } else if ("clearBySkipNames".equals(methodName)) {
            ApplicationContextHolder.getBean(Cache.class).clearBySkipNames(1, (String[]) MapUtils.getObject(messageMap, "inputs"));
        } else if ("clear".equals(methodName)) {
            ApplicationContextHolder.getBean(Cache.class).clear(1, (String[]) MapUtils.getObject(messageMap, "inputs"));
        }
    }
}
