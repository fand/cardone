package top.cardone.cache.serializer;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.ArrayUtils;
import org.nustaq.serialization.FSTConfiguration;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.SerializationException;

@Log4j2
public class FstRedisSerializer<T> implements RedisSerializer<T> {
    private FSTConfiguration fstConfiguration ;

    public FstRedisSerializer() {
        fstConfiguration = FSTConfiguration.getDefaultConfiguration();
        fstConfiguration.setClassLoader(Thread.currentThread().getContextClassLoader());
    }

    @Override
    public byte[] serialize(T t) throws SerializationException {
        if (t == null) {
            return ArrayUtils.EMPTY_BYTE_ARRAY;
        }

        return fstConfiguration.asByteArray(t);
    }

    @Override
    public T deserialize(byte[] bytes) throws SerializationException {
        if (ArrayUtils.isEmpty(bytes)) {
            return null;
        }

        return (T) fstConfiguration.asObject(bytes);
    }
}
