package top.cardone.cache.impl;

import com.google.common.collect.Sets;
import com.google.gson.Gson;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.cache.Cache;
import org.springframework.cache.support.AbstractValueAdaptingCache;
import org.springframework.core.task.AsyncListenableTaskExecutor;
import top.cardone.context.ApplicationContextHolder;

import java.util.Set;
import java.util.concurrent.Callable;

/**
 * @author yao hai tao
 * @date 2016/1/14
 */
@Log4j2
public class MemcachedCacheImpl extends AbstractValueAdaptingCache implements Cache {
    private final String name;
    private final int expire;
    private final SpyMemcachedClientImpl spyMemcachedClient;
    @Setter
    private String taskExecutorBeanName = "slowTaskExecutor";

    private final String keySetKey;

    public MemcachedCacheImpl(String name, int expire, SpyMemcachedClientImpl spyMemcachedClient) {
        super(true);
        this.name = name;
        this.expire = expire;
        this.spyMemcachedClient = spyMemcachedClient;
        this.keySetKey = name + ".keySet";
    }

    @Override
    public <T> T get(Object key, Callable<T> callable) {
        String newKey = this.newKey(key);

        ValueWrapper valueWrapper = this.get(key);

        if (valueWrapper == null) {
            try {
                valueWrapper = toValueWrapper(callable.call());
            } catch (Exception e) {
                log.error(e);

                return null;
            }

            if (valueWrapper == null) {
                return null;
            }

            this.spyMemcachedClient.safeSet(newKey, expire, valueWrapper.get());
        }

        return (T) valueWrapper.get();
    }

    @Override
    public void clear() {
        ApplicationContextHolder.getBean(AsyncListenableTaskExecutor.class, this.taskExecutorBeanName).submitListenable(() -> {
            Set<String> keySet = this.spyMemcachedClient.get(this.keySetKey);

            if (keySet == null) {
                return;
            }

            for (String key : keySet) {
                this.spyMemcachedClient.safeDelete(key);
            }
        });
    }

    @Override
    public void evict(Object key) {
        ApplicationContextHolder.getBean(AsyncListenableTaskExecutor.class, this.taskExecutorBeanName).submitListenable(() -> {
            String newKey = this.newKey(key);

            this.spyMemcachedClient.safeDelete(newKey);
        });
    }

    @Override
    protected Object lookup(Object key) {
        String newKey = this.newKey(key);

        return this.spyMemcachedClient.get(newKey);
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public Object getNativeCache() {
        return this.spyMemcachedClient;
    }

    private String newKey(Object key) {
        String newKey = this.name + "." + DigestUtils.md5Hex(((key instanceof String) ? (String) key : ApplicationContextHolder.getBean(Gson.class).toJson(key)));

        ApplicationContextHolder.getBean(AsyncListenableTaskExecutor.class, this.taskExecutorBeanName).submitListenable(() -> {
            Set<String> keySet = this.spyMemcachedClient.get(this.keySetKey);

            if (keySet == null) {
                keySet = Sets.newConcurrentHashSet();
            }

            keySet.add(newKey);

            this.spyMemcachedClient.set(this.keySetKey, 0, keySet);
        });

        return newKey;
    }

    @Override
    public void put(Object key, Object value) {
        ApplicationContextHolder.getBean(AsyncListenableTaskExecutor.class, this.taskExecutorBeanName).submitListenable(() -> {
            String newKey = this.newKey(key);

            this.spyMemcachedClient.safeSet(newKey, expire, value);
        });
    }

    @Override
    public ValueWrapper putIfAbsent(Object key, Object value) {
        this.put(key, value);

        return toValueWrapper(value);
    }
}