package top.cardone.cache.impl;

import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.DisposableBean;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * @author yao hai tao
 * @date 2016/1/14
 */
@Log4j2
public class SpyMemcachedClientImpl implements DisposableBean {
    @Setter
    private net.spy.memcached.MemcachedClient memcachedClient;

    @Setter
    private long shutdownTimeout = 2500;

    @Setter
    private long updateTimeout = 2500;

    /**
     * 异步Decr方法, 不支持默认值, 若key不存在返回-1.
     */
    public Future<Long> asyncDecr(String key, int by) {
        return memcachedClient.asyncDecr(key, by);
    }

    /**
     * 异步Incr方法, 不支持默认值, 若key不存在返回-1.
     */
    public Future<Long> asyncIncr(String key, int by) {
        return memcachedClient.asyncIncr(key, by);
    }

    /**
     * Decr方法.
     */
    public long decr(String key, int by, long defaultValue) {
        return memcachedClient.decr(key, by, defaultValue);
    }

    /**
     * 异步 Delete方法, 不考虑执行结果.
     */
    public void delete(String key) {
        memcachedClient.delete(key);
    }

    @Override
    public void destroy() throws Exception {
        if (memcachedClient == null) {
            return;
        }

        memcachedClient.shutdown(shutdownTimeout, TimeUnit.SECONDS);
    }

    /**
     * Get方法, 转换结果类型并屏蔽异常, 仅返回Null.
     */
    public <T> T get(String key) {
        try {
            return (T) memcachedClient.get(key);
        } catch (RuntimeException e) {
            handleException(e, key);

            return null;
        }
    }

    /**
     * GetBulk方法, 转换结果类型并屏蔽异常.
     */
    public <T> Map<String, T> getBulk(Collection<String> keys) {
        try {
            return (Map<String, T>) memcachedClient.getBulk(keys);
        } catch (RuntimeException e) {
            handleException(e, StringUtils.join(keys, ","));

            return null;
        }
    }

    private void handleException(Exception e, String key) {
        if (log.isWarnEnabled()) {
            log.warn("spymemcached client receive an exception with key:" + key, e);
        }
    }

    /**
     * Incr方法.
     */
    public long incr(String key, int by, long defaultValue) {
        return memcachedClient.incr(key, by, defaultValue);
    }

    /**
     * 安全的Delete方法, 保证在updateTimeout秒内返回执行结果, 否则返回false并取消操作.
     */
    public boolean safeDelete(String key) {
        Future<Boolean> future = memcachedClient.delete(key);

        try {
            return future.get(updateTimeout, TimeUnit.SECONDS);
        } catch (Exception e) {
            future.cancel(false);
        }

        return false;
    }

    /**
     * 安全的Set方法, 保证在updateTimeout秒内返回执行结果, 否则返回false并取消操作.
     */
    public boolean safeSet(String key, int expiration, Object value) {
        Future<Boolean> future = memcachedClient.set(key, expiration, value);

        try {
            return future.get(updateTimeout, TimeUnit.SECONDS);
        } catch (Exception e) {
            future.cancel(false);
        }

        return false;
    }

    /**
     * 异步Set方法, 不考虑执行结果.
     */
    public void set(String key, int expiredTime, Object value) {
        memcachedClient.set(key, expiredTime, value);
    }
}
