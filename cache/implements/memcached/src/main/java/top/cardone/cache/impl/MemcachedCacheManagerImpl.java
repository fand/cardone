package top.cardone.cache.impl;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.Setter;
import org.apache.commons.collections.MapUtils;
import org.springframework.cache.Cache;
import org.springframework.cache.transaction.AbstractTransactionSupportingCacheManager;
import top.cardone.context.util.StringUtils;

import java.util.Collection;
import java.util.Map;

/**
 * @author yao hai tao
 * @date 2016/1/14
 */
public class MemcachedCacheManagerImpl extends AbstractTransactionSupportingCacheManager {
    @Setter
    private int defaultExpire = 30;
    @Setter
    private Map<String, Integer> expireMap = Maps.newHashMap();
    @Setter
    private SpyMemcachedClientImpl spyMemcachedClient;

    @Override
    protected Cache getMissingCache(String name) {
        String expireMapKey = StringUtils.getPathForMatch(".", expireMap.keySet(), name);

        int expire;

        if (StringUtils.isBlank(expireMapKey)) {
            expire = defaultExpire;
        } else {
            expire = MapUtils.getIntValue(expireMap, expireMapKey, defaultExpire);
        }

        return new MemcachedCacheImpl(name, expire, spyMemcachedClient);
    }

    @Override
    protected Collection<? extends Cache> loadCaches() {
        return Lists.newArrayList();
    }
}
