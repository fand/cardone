package top.cardone.cache.impl;

import lombok.extern.log4j.Log4j2;
import org.springframework.cache.Cache;

import java.util.concurrent.Callable;

/**
 * @author yao hai tao
 */
@Log4j2
public class CardOneCacheImpl implements Cache {
    @lombok.Getter
    private String name;

    private top.cardone.cache.Cache cache;

    public CardOneCacheImpl(top.cardone.cache.Cache cache, String name) {
        this.cache = cache;
        this.name = name;
    }

    @Override
    public <T> T get(Object key, Callable<T> callable) {
        return this.cache.get(this.name, key, () -> {
            try {
                return callable.call();
            } catch (Exception e) {
                log.error(e);

                return null;
            }
        });
    }

    @Override
    public void clear() {
        this.cache.clear(this.name);
    }

    @Override
    public void evict(Object key) {
        this.cache.evict(this.name, key);
    }

    @Override
    public Cache.ValueWrapper get(Object key) {
        return this.cache.getCacheValueWrapper(this.name, key);
    }

    @Override
    public <T> T get(Object key, Class<T> type) {
        return this.cache.get(type, this.name, key);
    }

    @Override
    public Object getNativeCache() {
        return this.cache.getCaches(this.name);
    }

    @Override
    public void put(Object key, Object value) {
        this.cache.put(this.name, key, value);
    }

    @Override
    public Cache.ValueWrapper putIfAbsent(Object key, Object value) {
        return this.cache.putIfAbsent(this.name, key, value);
    }
}