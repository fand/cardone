package top.cardone.cache.impl;

import com.google.common.collect.Lists;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.cache.CacheManager;
import org.springframework.cache.support.NullValue;
import org.springframework.cache.support.SimpleValueWrapper;
import org.springframework.core.task.AsyncListenableTaskExecutor;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import top.cardone.cache.Cache;
import top.cardone.context.ApplicationContextHolder;
import top.cardone.context.util.TableUtils;
import top.cardone.core.util.action.Action3;
import top.cardone.core.util.func.Func0;

import java.util.Collection;
import java.util.List;

/**
 * @author yao hai tao
 * @date 2015/9/9
 */
@Log4j2
public class CacheImpl implements Cache {
    @Setter
    private String pubActionBeanName = "redisPubAction";

    @lombok.Setter
    private List<CacheManager> cacheManagerList;

    @Setter
    private String appGroupName = "cardone";

    @Setter
    private String taskExecutorBeanName = "slowTaskExecutor";

    @Setter
    private Long countUpperLimit = 1000L;

    private Object newKey(Object key) {
        return StringUtils.replace(ObjectUtils.nullSafeToString(key), StringUtils.SPACE, "-");
    }

    @Override
    public void clearBySkipNames(String... skipCacheNames) {
        this.clearBySkipNames(cacheManagerList.size(), skipCacheNames);
    }

    @Override
    public void clearBySkipNames(int maxLevelSize, String... skipCacheNames) {
        if (ArrayUtils.isNotEmpty(skipCacheNames)) {
            for (int i = 0; i < skipCacheNames.length; i++) {
                skipCacheNames[i] = appGroupName + ":" + StringUtils.replace(skipCacheNames[i], StringUtils.SPACE, "-");
            }
        }

        for (int i = 0, s = NumberUtils.min(cacheManagerList.size(), maxLevelSize); i < s; i++) {
            CacheManager cacheManager = cacheManagerList.get(i);

            Collection<String> cacheNames = cacheManager.getCacheNames();

            for (String cacheName : cacheNames) {
                if (ArrayUtils.contains(skipCacheNames, cacheName)) {
                    continue;
                }

                if (log.isDebugEnabled()) {
                    log.debug(StringUtils.join(Cache.ID, "\r\nclear(cacheName:", cacheName, ")"));
                }

                cacheManager.getCache(cacheName).clear();
            }
        }

        if (NumberUtils.min(cacheManagerList.size(), maxLevelSize) > 1) {
            ApplicationContextHolder.getBean(AsyncListenableTaskExecutor.class, this.taskExecutorBeanName).submitListenable(() -> {
                ApplicationContextHolder.action(Action3.class, action3 -> action3.action(Cache.ID, "clearBySkipNames", skipCacheNames), this.pubActionBeanName);
            });
        }
    }

    @Override
    public void clear(String... cacheNames) {
        if (ArrayUtils.isEmpty(cacheNames)) {
            return;
        }

        this.clear(cacheManagerList.size(), cacheNames);
    }

    @Override
    public void clear(int maxLevelSize, String... cacheNames) {
        if (ArrayUtils.isEmpty(cacheNames)) {
            return;
        }

        if (log.isDebugEnabled()) {
            log.debug(StringUtils.join(Cache.ID, "\r\nclear(cacheNames:", StringUtils.join(cacheNames, ","), ")"));
        }

        for (String cacheName : cacheNames) {
            if (StringUtils.isBlank(cacheName)) {
                continue;
            }

            List<? extends org.springframework.cache.Cache> caches = getCaches(cacheName, maxLevelSize);

            if (org.apache.commons.collections.CollectionUtils.isEmpty(caches)) {
                continue;
            }

            for (int i = (caches.size() - 1); i >= 0; i--) {
                org.springframework.cache.Cache cache = caches.get(i);

                cache.clear();
            }
        }

        if (NumberUtils.min(cacheManagerList.size(), maxLevelSize) > 1) {
            ApplicationContextHolder.getBean(AsyncListenableTaskExecutor.class, this.taskExecutorBeanName).submitListenable(() -> {
                ApplicationContextHolder.action(Action3.class, action3 -> action3.action(Cache.ID, "clear", cacheNames), this.pubActionBeanName);
            });
        }
    }

    @Override
    public void evict(String cacheName, Object key) {
        if (key == null || StringUtils.isBlank(cacheName)) {
            return;
        }

        this.evict(cacheName, cacheManagerList.size(), key);
    }

    @Override
    public void evict(String cacheName, int maxLevelSize, Object key) {
        if (key == null || StringUtils.isBlank(cacheName)) {
            return;
        }

        if (log.isDebugEnabled()) {
            log.debug(StringUtils.join(Cache.ID, "\r\nevict(cacheName:", cacheName, ", key:", key, ")"));
        }

        List<? extends org.springframework.cache.Cache> caches = getCaches(cacheName, maxLevelSize);

        if (org.apache.commons.collections.CollectionUtils.isEmpty(caches)) {
            return;
        }

        Object newKey = newKey(key);

        for (int i = (caches.size() - 1); i >= 0; i--) {
            org.springframework.cache.Cache cache = caches.get(i);

            cache.evict(newKey);
        }

        if (NumberUtils.min(cacheManagerList.size(), maxLevelSize) > 1) {
            this.log(cacheName, newKey);

            ApplicationContextHolder.getBean(AsyncListenableTaskExecutor.class, this.taskExecutorBeanName).submitListenable(() -> {
                ApplicationContextHolder.action(Action3.class, action3 -> action3.action(Cache.ID, "evict", new Object[]{cacheName, key}), this.pubActionBeanName);
            });
        }
    }

    @Override
    public <T> T get(Class<T> requiredType, String cacheName, Object key) {
        if (key == null || requiredType == null || StringUtils.isBlank(cacheName)) {
            return null;
        }

        return this.get(requiredType, cacheName, cacheManagerList.size(), key);
    }

    @Override
    public <T> T get(Class<T> requiredType, String cacheName, int maxLevelSize, Object key) {
        if (key == null || requiredType == null || StringUtils.isBlank(cacheName)) {
            return null;
        }

        List<? extends org.springframework.cache.Cache> caches = getCaches(cacheName, maxLevelSize);

        if (org.apache.commons.collections.CollectionUtils.isEmpty(caches)) {
            return null;
        }

        Object newKey = newKey(key);

        if (maxLevelSize > 1) {
            this.log(cacheName, newKey);
        }

        List<org.springframework.cache.Cache> nullValueCaches = Lists.newArrayList();

        for (org.springframework.cache.Cache cache : caches) {
            org.springframework.cache.Cache.ValueWrapper valueWrapper = cache.get(newKey);

            if (valueWrapper == null) {
                nullValueCaches.add(cache);

                continue;
            }

            nullValueCachePut(newKey, valueWrapper, nullValueCaches);

            return (T) valueWrapper.get();
        }

        return null;
    }

    private void log(String cacheName, Object key) {
        ApplicationContextHolder.getBean(AsyncListenableTaskExecutor.class, this.taskExecutorBeanName).submitListenable(() -> {
            if (countUpperLimit > 0L) {
                String rowKey = this.getClass().getName();

                String columnKey = StringUtils.join(cacheName, ", ", StringUtils.split((String) key, "(")[0]);

                if (StringUtils.startsWith(columnKey, "top.cardone.security.shiro.session.mgt.eis.impl.SessionDaoImpl")) {
                    return;
                }

                Long count = TableUtils.longAdderIncrementGetSum(rowKey, columnKey);

                if (count % countUpperLimit == 0) {
                    log.error(StringUtils.join("调用较频繁, rowKey: ", rowKey, ", columnKey: ", columnKey, ", count: ", count));
                }
            }
        });
    }

    @Override
    public Object get(String cacheName, Object key) {
        if (log.isDebugEnabled()) {
            log.debug(StringUtils.join(Cache.ID, "\r\nget(cacheName:", cacheName, ", key:", key, ")"));
        }

        org.springframework.cache.Cache.ValueWrapper valueWrapper = getCacheValueWrapper(cacheName, key);

        return (valueWrapper == null ? null : valueWrapper.get());
    }

    @Override
    public <T> T get(String cacheName, Object key, Func0<T> func) {
        if (key == null || StringUtils.isBlank(cacheName)) {
            return null;
        }

        return this.get(cacheName, cacheManagerList.size(), key, func);
    }

    @Override
    public <T> T get(String cacheName, int maxLevelSize, Object key, Func0<T> func) {
        if (key == null || StringUtils.isBlank(cacheName)) {
            return null;
        }

        if (func == null) {
            return null;
        }

        List<? extends org.springframework.cache.Cache> caches = getCaches(cacheName, maxLevelSize);

        if (org.apache.commons.collections.CollectionUtils.isEmpty(caches)) {
            return null;
        }

        Object newKey = newKey(key);

        if (maxLevelSize > 1) {
            this.log(cacheName, newKey);
        }

        List<org.springframework.cache.Cache> nullValueCaches = Lists.newArrayList();

        for (org.springframework.cache.Cache cache : caches) {
            org.springframework.cache.Cache.ValueWrapper valueWrapper = cache.get(newKey);

            if (valueWrapper == null) {
                nullValueCaches.add(cache);

                continue;
            }

            nullValueCachePut(newKey, valueWrapper, nullValueCaches);

            return (T) valueWrapper.get();
        }

        T value = func.func();

        for (org.springframework.cache.Cache cache : caches) {
            cache.put(newKey, value);
        }

        return value;
    }

    @Override
    public org.springframework.cache.Cache.ValueWrapper getCacheValueWrapper(String cacheName, Object key) {
        if (key == null || StringUtils.isBlank(cacheName)) {
            return null;
        }

        List<? extends org.springframework.cache.Cache> caches = getCaches(cacheName);

        if (org.apache.commons.collections.CollectionUtils.isEmpty(caches)) {
            return null;
        }

        Object newKey = newKey(key);

        org.springframework.cache.Cache.ValueWrapper valueWrapper;

        List<org.springframework.cache.Cache> nullValueCaches = Lists.newArrayList();

        for (org.springframework.cache.Cache cache : caches) {
            valueWrapper = cache.get(newKey);

            if (valueWrapper == null) {
                nullValueCaches.add(cache);

                continue;
            }

            nullValueCachePut(newKey, valueWrapper, nullValueCaches);

            return valueWrapper;
        }

        return null;
    }

    private void nullValueCachePut(Object newKey, org.springframework.cache.Cache.ValueWrapper valueWrapper, List<org.springframework.cache.Cache> nullValueCaches) {
        if (!CollectionUtils.isEmpty(nullValueCaches)) {
            for (org.springframework.cache.Cache nullValueCache : nullValueCaches) {
                Object value = valueWrapper.get();

                nullValueCache.put(newKey, value);
            }
        }
    }

    @Override
    public List<? extends org.springframework.cache.Cache> getCaches(String cacheName) {
        return this.getCaches(cacheName, cacheManagerList.size());
    }

    @Override
    public List<? extends org.springframework.cache.Cache> getCaches(String cacheName, int maxLevelSize) {
        List<org.springframework.cache.Cache> caches = Lists.newArrayList();

        String newCacheName = appGroupName + ":" + StringUtils.replace(cacheName, StringUtils.SPACE, "-");

        for (int i = 0, s = NumberUtils.min(cacheManagerList.size(), maxLevelSize); i < s; i++) {
            CacheManager cacheManager = cacheManagerList.get(i);

            caches.add(cacheManager.getCache(newCacheName));
        }

        return caches;
    }

    @Override
    public void put(String cacheName, Object key, Object value) {
        if (key == null || StringUtils.isBlank(cacheName)) {
            return;
        }

        this.put(cacheName, cacheManagerList.size(), key, value);
    }

    @Override
    public void put(String cacheName, int maxLevelSize, Object key, Object value) {
        if (key == null || StringUtils.isBlank(cacheName)) {
            return;
        }

        if (log.isDebugEnabled()) {
            log.debug(StringUtils.join(Cache.ID, "\r\nput(cacheName:", cacheName, ", key:", key, ", value:", value + ")"));
        }

        List<? extends org.springframework.cache.Cache> caches = this.getCaches(cacheName, maxLevelSize);

        Object newKey = newKey(key);

        for (org.springframework.cache.Cache cache : caches) {
            cache.put(newKey, value);
        }

        if (NumberUtils.min(cacheManagerList.size(), maxLevelSize) > 1) {
            this.log(cacheName, newKey);

            ApplicationContextHolder.getBean(AsyncListenableTaskExecutor.class, this.taskExecutorBeanName).submitListenable(() -> {
                ApplicationContextHolder.action(Action3.class, action3 -> action3.action(Cache.ID, "evict", new Object[]{cacheName, key}), this.pubActionBeanName);
            });
        }
    }

    @Override
    public org.springframework.cache.Cache.ValueWrapper putIfAbsent(String cacheName, Object key, Object value) {
        return this.putIfAbsent(cacheName, cacheManagerList.size(), key, value);
    }

    @Override
    public org.springframework.cache.Cache.ValueWrapper putIfAbsent(String cacheName, int maxLevelSize, Object key, Object value) {
        this.put(cacheName, maxLevelSize, key, value);

        return new SimpleValueWrapper(value == NullValue.INSTANCE ? null : value);
    }
}