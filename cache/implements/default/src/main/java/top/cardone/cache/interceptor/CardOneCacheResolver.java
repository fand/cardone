package top.cardone.cache.interceptor;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.interceptor.AbstractCacheResolver;
import org.springframework.cache.interceptor.CacheOperationInvocationContext;
import org.springframework.core.annotation.AnnotationUtils;

import java.util.Collection;

/**
 * Created by cardo on 2018/4/9 0009.
 */
public class CardOneCacheResolver extends AbstractCacheResolver {
    @Override
    protected Collection<String> getCacheNames(CacheOperationInvocationContext<?> context) {
        if (context.getOperation().getCacheNames().isEmpty()) {
            CacheConfig annotation = AnnotationUtils.findAnnotation(context.getTarget().getClass(), CacheConfig.class);

            if (annotation != null && ArrayUtils.isNotEmpty(annotation.cacheNames())) {
                return Lists.newArrayList(annotation.cacheNames());
            }

            if (ArrayUtils.isNotEmpty(context.getTarget().getClass().getInterfaces())) {
                return Lists.newArrayList(context.getTarget().getClass().getInterfaces()[0].getName());
            }

            return Lists.newArrayList(context.getTarget().getClass().getName());
        }

        return context.getOperation().getCacheNames();
    }
}
