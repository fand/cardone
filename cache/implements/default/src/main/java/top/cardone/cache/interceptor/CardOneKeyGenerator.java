package top.cardone.cache.interceptor;

import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.util.StringUtils;

import java.lang.reflect.Method;

/**
 * Created by cardo on 2018/4/9 0009.
 */
public class CardOneKeyGenerator implements KeyGenerator {
    @Override
    public Object generate(Object target, Method method, Object... params) {
        return org.apache.commons.lang3.StringUtils.join(method.getName(), "(", StringUtils.arrayToCommaDelimitedString(params), ")");
    }
}