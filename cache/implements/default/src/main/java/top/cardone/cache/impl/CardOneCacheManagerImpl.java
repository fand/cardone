package top.cardone.cache.impl;

import com.google.common.collect.Lists;
import lombok.Setter;
import org.springframework.cache.Cache;
import org.springframework.cache.support.AbstractCacheManager;

import java.util.Collection;

/**
 * @author yao hai tao
 */
public class CardOneCacheManagerImpl extends AbstractCacheManager {
    @Setter
    private top.cardone.cache.Cache cache;

    @Override
    protected Cache getMissingCache(String name) {
        return new CardOneCacheImpl(this.cache, name);
    }

    @Override
    protected Collection<? extends Cache> loadCaches() {
        return Lists.newArrayList();
    }
}