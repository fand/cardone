package top.cardone.security.shiro.model;

import lombok.Getter;
import lombok.Setter;

/**
 * @author yao hai tao
 * @date 2016/2/16
 */
public class OAuthRegParams extends OAuthParams {
    @Setter
    @Getter
    private String registrationType;

    @Setter
    @Getter
    private String name = "OAuth V2.0 Demo Application";

    @Setter
    @Getter
    private String url = "http://localhost:8080";

    @Setter
    @Getter
    private String description = "Demo Application of the OAuth V2.0 Protocol";

    @Setter
    @Getter
    private String icon = "http://localhost:8080/demo.png";

    @Setter
    @Getter
    private String registrationEndpoint;
}
