package top.cardone.security.shiro.authc.impl;

import lombok.Getter;
import lombok.Setter;
import org.apache.shiro.authc.AuthenticationToken;

/**
 * @author yao hai tao
 * @date 16-2-2
 */
public class OAuth2TokenImpl implements AuthenticationToken {
    @Setter
    @Getter
    private Object principal;

    @Setter
    @Getter
    private Object credentials;
//
//    @Setter
//    @Getter
//    private String clientId;
//
//    @Setter
//    @Getter
//    private String clientSecret;
}
