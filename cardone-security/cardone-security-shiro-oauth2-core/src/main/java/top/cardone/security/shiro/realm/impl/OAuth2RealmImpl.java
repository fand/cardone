package top.cardone.security.shiro.realm.impl;

import lombok.extern.log4j.Log4j2;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import top.cardone.security.shiro.authc.impl.OAuth2TokenImpl;

/**
 * @author yao hai tao
 * @date 16-2-2
 */
@Log4j2
public class OAuth2RealmImpl extends StatelessRealmImpl {
    public OAuth2RealmImpl() {
        super();
        setAuthenticationTokenClass(OAuth2TokenImpl.class);
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        return new SimpleAuthenticationInfo(token.getPrincipal(), token.getCredentials(), getName());
    }
}