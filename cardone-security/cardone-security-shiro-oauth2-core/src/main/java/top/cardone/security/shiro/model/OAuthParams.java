package top.cardone.security.shiro.model;

/**
 * @author yao hai tao
 * @date 2016/2/16
 */
public class OAuthParams implements java.io.Serializable {
    @lombok.Getter
    @lombok.Setter
    private String clientId;

    @lombok.Getter
    @lombok.Setter
    private String clientSecret;

    @lombok.Getter
    @lombok.Setter
    private String redirectUri;

    @lombok.Getter
    @lombok.Setter
    private String authzEndpoint;

    @lombok.Getter
    @lombok.Setter
    private String tokenEndpoint;

    @lombok.Getter
    @lombok.Setter
    private String authzCode;

    @lombok.Getter
    @lombok.Setter
    private String accessToken;

    @lombok.Getter
    @lombok.Setter
    private Long expiresIn;

    @lombok.Getter
    @lombok.Setter
    private String refreshToken;

    @lombok.Getter
    @lombok.Setter
    private String scope;

    @lombok.Getter
    @lombok.Setter
    private String state;

    @lombok.Getter
    @lombok.Setter
    private String resourceUrl;

    @lombok.Getter
    @lombok.Setter
    private String resource;

    @lombok.Getter
    @lombok.Setter
    private String application;

    @lombok.Getter
    @lombok.Setter
    private String requestType;

    @lombok.Getter
    @lombok.Setter
    private String requestMethod;

    @lombok.Getter
    @lombok.Setter
    private String idToken;

    @lombok.Getter
    @lombok.Setter
    private String header;

    @lombok.Getter
    @lombok.Setter
    private String claimsSet;

    @lombok.Getter
    @lombok.Setter
    private String jwt;

    @lombok.Getter
    @lombok.Setter
    private boolean idTokenValid;
}
