package top.cardone.security.shiro.authc.impl;

import lombok.Getter;
import lombok.Setter;
import org.apache.shiro.authc.UsernamePasswordToken;

/**
 * @author yao hai tao
 * @date 2016/2/4
 */
public class UsernamePasswordTokenImpl extends UsernamePasswordToken {
    @Setter
    @Getter
    private String validationCode;

    @Setter
    @Getter
    private String serviceValidationCode;

    @Setter
    @Getter
    private Boolean validation;
}
