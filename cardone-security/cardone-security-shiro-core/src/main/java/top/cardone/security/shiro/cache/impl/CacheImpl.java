package top.cardone.security.shiro.cache.impl;

import com.google.common.collect.Sets;
import top.cardone.cache.Cache;
import top.cardone.context.ApplicationContextHolder;

import java.io.Serializable;
import java.util.Collection;
import java.util.Set;

/**
 * @author yao hai tao
 * @date 16-2-2
 */
public class CacheImpl<K extends Serializable, V extends Serializable> implements org.apache.shiro.cache.Cache<K, V> {
    private String name;

    public CacheImpl(String name) {
        this.name = name;
    }

    @Override
    public V get(K key) {
        return (V) ApplicationContextHolder.getBean(Cache.class).get(name, key);
    }

    @Override
    public V put(K key, V value) {
        ApplicationContextHolder.getBean(Cache.class).put(name, key, value);

        return value;
    }

    @Override
    public V remove(K key) {
        V v = this.get(key);

        ApplicationContextHolder.getBean(Cache.class).evict(name, key);

        return v;
    }

    @Override
    public void clear() {
        ApplicationContextHolder.getBean(Cache.class).clear(name);
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public Set<K> keys() {
        return Sets.newHashSet();
    }

    @Override
    public Collection<V> values() {
        return Sets.newHashSet();
    }
}