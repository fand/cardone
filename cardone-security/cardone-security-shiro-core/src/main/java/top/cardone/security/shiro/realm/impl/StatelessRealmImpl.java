package top.cardone.security.shiro.realm.impl;

import com.google.common.collect.Sets;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.credential.PasswordService;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.Permission;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.util.CollectionUtils;
import top.cardone.context.ApplicationContextHolder;
import top.cardone.core.util.func.Func1;
import top.cardone.security.shiro.authc.impl.StatelessTokenImpl;

import java.util.Collection;

/**
 * @author yao hai tao
 * @date 16-2-2
 */
@Log4j2
public class StatelessRealmImpl extends AuthorizingRealm implements AuthorizationInfo {
    @Setter
    @Getter
    protected String readListRoleFuncName = StringUtils.EMPTY;

    @Setter
    @Getter
    protected String readListPermissionFuncName = StringUtils.EMPTY;

    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof StatelessTokenImpl;
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        String principal = (String) principals.getPrimaryPrincipal();

        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();

        if (StringUtils.isNotBlank(readListRoleFuncName)) {
            Collection<String> roles = (Collection<String>) ApplicationContextHolder.func(Func1.class, func -> func.func(principal), readListRoleFuncName);

            if (!CollectionUtils.isEmpty(roles)) {
                authorizationInfo.setRoles(Sets.newHashSet(roles));
            }
        }

        if (StringUtils.isNotBlank(readListPermissionFuncName)) {
            Collection<String> permissions = (Collection<String>) ApplicationContextHolder.func(Func1.class, func -> func.func(principal), readListPermissionFuncName);

            if (!CollectionUtils.isEmpty(permissions)) {
                authorizationInfo.setStringPermissions(Sets.newHashSet(permissions));
            }
        }

        return authorizationInfo;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        String credentials = ApplicationContextHolder.getBean(PasswordService.class).encryptPassword(token.getPrincipal());

        return new SimpleAuthenticationInfo(token.getPrincipal(), credentials, getName());
    }

    @Override
    public Collection<String> getRoles() {
        AuthorizationInfo info = getAuthorizationInfo(SecurityUtils.getSubject().getPrincipals());

        return info.getRoles();
    }

    @Override
    public Collection<String> getStringPermissions() {
        AuthorizationInfo info = getAuthorizationInfo(SecurityUtils.getSubject().getPrincipals());

        return info.getStringPermissions();
    }

    @Override
    public Collection<Permission> getObjectPermissions() {
        AuthorizationInfo info = getAuthorizationInfo(SecurityUtils.getSubject().getPrincipals());

        return info.getObjectPermissions();
    }
}