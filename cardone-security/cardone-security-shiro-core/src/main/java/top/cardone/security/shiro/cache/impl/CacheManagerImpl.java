package top.cardone.security.shiro.cache.impl;

import org.apache.shiro.cache.AbstractCacheManager;
import org.apache.shiro.cache.Cache;

import java.io.Serializable;

/**
 * @author yao hai tao
 * @date 16-2-2
 */
public class CacheManagerImpl extends AbstractCacheManager {
    @Override
    protected Cache createCache(String name) {
        return new CacheImpl<String, Serializable>(name);
    }
}