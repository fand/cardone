package top.cardone.security.shiro.web.filter.authc.impl;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authc.AuthenticationToken;
import top.cardone.security.shiro.authc.impl.OAuth2TokenImpl;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

/**
 * @author yao hai tao
 * @date 2016/2/4
 */
@Log4j2
public class OAuth2FilterImpl extends StatelessAuthcFilterImpl {
    @Override
    protected AuthenticationToken createToken(ServletRequest request, ServletResponse response) throws Exception {
        HttpServletRequest httpRequest = (HttpServletRequest) request;

        String credentials = httpRequest.getHeader(credentialsParam);

        if (StringUtils.isBlank(credentials)) {
            credentials = request.getParameter(credentialsParam);
        }

        String principal = httpRequest.getHeader(principalParam);

        if (StringUtils.isBlank(principal)) {
            principal = request.getParameter(principalParam);
        }

        OAuth2TokenImpl oAuth2Token = new OAuth2TokenImpl();

        oAuth2Token.setCredentials(credentials);

        oAuth2Token.setPrincipal(principal);

        return oAuth2Token;
    }
}
