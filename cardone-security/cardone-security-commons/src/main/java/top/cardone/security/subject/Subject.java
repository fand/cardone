package top.cardone.security.subject;

import java.util.Collection;
import java.util.Map;

public interface Subject {
    Object getPrincipal();

    Object getDetails(String key);

    Map<String, Object> getDetails();

    <T> T getDetails(String key, Class<T> requiredType);

    boolean isPermitted(String permission);

    boolean isPermittedAll(String... permissions);

    boolean hasRole(String roleIdentifier);

    boolean hasAllRoles(String... roleIdentifiers);

    boolean isAuthenticated();

    void logout();

    Collection<String> getAuthorities();
}

