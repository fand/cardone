package top.cardone.security;

/**
 * @author yao hai tao
 * @date 16-2-2
 */
public interface Descipher {
    /**
     * 加密
     *
     * @param salt
     * @param text
     * @return
     * @throws Exception
     */
    String encrypt(String salt, String text);

    /**
     * 加密
     *
     * @param text
     * @return
     * @throws Exception
     */
    String encrypt(String text);

    /**
     * 解密
     *
     * @param salt
     * @param text
     * @return
     * @throws Exception
     */
    String decrypt(String salt, String text);

    /**
     * 解密
     *
     * @param text
     * @return
     * @throws Exception
     */
    String decrypt(String text);
}
