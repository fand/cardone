package top.cardone.security.utils;

import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;
import top.cardone.security.subject.Subject;

import java.util.Collection;
import java.util.Map;

public class SecurityUtils {
    @Setter
    @Getter
    private static Subject subject = new Subject() {
        @Override
        public Object getPrincipal() {
            return null;
        }

        @Override
        public Object getDetails(String key) {
            return null;
        }

        @Override
        public Map<String, Object> getDetails() {
            return null;
        }

        @Override
        public <T> T getDetails(String key, Class<T> requiredType) {
            return null;
        }

        @Override
        public boolean isPermitted(String permission) {
            return false;
        }

        @Override
        public boolean isPermittedAll(String... permissions) {
            return false;
        }

        @Override
        public boolean hasRole(String roleIdentifier) {
            return false;
        }

        @Override
        public boolean hasAllRoles(String... roleIdentifiers) {
            return false;
        }

        @Override
        public boolean isAuthenticated() {
            return false;
        }

        @Override
        public void logout() {

        }

        @Override
        public Collection<String> getAuthorities() {
            return Lists.newArrayList();
        }
    };
}
