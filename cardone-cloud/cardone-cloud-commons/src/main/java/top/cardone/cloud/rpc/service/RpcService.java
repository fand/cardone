package top.cardone.cloud.rpc.service;

public interface RpcService {
    void action(String name);

    void action(String name, Object o1);

    void action(String name, Object o1, Object o2);

    void action(String name, Object o1, Object o2, Object o3);

    void action(String name, Object o1, Object o2, Object o3, Object o4);

    Object func(String name);

    Object func(String name, Object o1);

    Object func(String name, Object o1, Object o2);

    Object func(String name, Object o1, Object o2, Object o3);

    Object func(String name, Object o1, Object o2, Object o3, Object o4);
}
