package top.cardone.cloud.dubbo.rpc.service.impl;

import org.apache.dubbo.config.annotation.DubboService;
import top.cardone.cloud.rpc.service.RpcService;
import top.cardone.context.ApplicationContextHolder;
import top.cardone.core.util.action.*;
import top.cardone.core.util.func.*;

@DubboService(protocol = "dubbo")
public class RpcServiceImpl implements RpcService {
    @Override
    public void action(String name) {
        ApplicationContextHolder.getBean(Action0.class, name).action();
    }

    @Override
    public void action(String name, Object o1) {
        ApplicationContextHolder.getBean(Action1.class, name).action(o1);
    }

    @Override
    public void action(String name, Object o1, Object o2) {
        ApplicationContextHolder.getBean(Action2.class, name).action(o1, o2);
    }

    @Override
    public void action(String name, Object o1, Object o2, Object o3) {
        ApplicationContextHolder.getBean(Action3.class, name).action(o1, o2, o3);
    }

    @Override
    public void action(String name, Object o1, Object o2, Object o3, Object o4) {
        ApplicationContextHolder.getBean(Action4.class, name).action(o1, o2, o3, o4);
    }

    @Override
    public Object func(String name) {
        return ApplicationContextHolder.getBean(Func0.class, name).func();
    }

    @Override
    public Object func(String name, Object o1) {
        return ApplicationContextHolder.getBean(Func1.class, name).func(o1);
    }

    @Override
    public Object func(String name, Object o1, Object o2) {
        return ApplicationContextHolder.getBean(Func2.class, name).func(o1, o2);
    }

    @Override
    public Object func(String name, Object o1, Object o2, Object o3) {
        return ApplicationContextHolder.getBean(Func3.class, name).func(o1, o2, o3);
    }

    @Override
    public Object func(String name, Object o1, Object o2, Object o3, Object o4) {
        return ApplicationContextHolder.getBean(Func4.class, name).func(o1, o2, o3, o4);
    }
}
