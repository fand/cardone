package top.cardone.data.support;

import com.google.common.collect.Table;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import top.cardone.core.util.func.Func4;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

/**
 * @author yao hai tao
 * @date 2016/4/22
 */
public interface ExcelSupport extends java.io.Serializable {
    /**
     * 读取文件名
     *
     * @param filePathName   文件名
     * @param configTableMap excel配置集合
     * @param configTableMap
     * @throws InvalidFormatException
     * @throws IOException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    void readFilePathName(String filePathName, Map<Object, Table<String, String, Object>> configTableMap) throws InvalidFormatException, IOException, InstantiationException, IllegalAccessException;

    /**
     * 读取文件
     *
     * @param file           文件
     * @param configTableMap excel配置集合
     * @throws InvalidFormatException
     * @throws IOException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    void readFile(File file, Map<Object, Table<String, String, Object>> configTableMap) throws InvalidFormatException, IOException, InstantiationException, IllegalAccessException;

    /**
     * 读取工作薄
     *
     * @param workbook       工作薄
     * @param configTableMap excel配置集合
     * @return
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    void readWorkbook(Workbook workbook, Map<Object, Table<String, String, Object>> configTableMap) throws InstantiationException, IllegalAccessException;

    /**
     * 读取工作薄
     *
     * @param workbook    工作薄
     * @param configTable excel配置
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    void readWorkbook(Workbook workbook, Object sheetIndexObject, Table<String, String, Object> configTable) throws InstantiationException, IllegalAccessException;

    /**
     * 读取Sheet
     *
     * @param sheet       Sheet
     * @param configTable excel配置
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    void readSheet(Sheet sheet, Table<String, String, Object> configTable) throws InstantiationException, IllegalAccessException;

    /**
     * 读取行
     *
     * @param row          行
     * @param columns      列定义
     * @param readCellFunc 读取列定义
     * @return
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    Map<String, Object> readRow(Row row, Map<String, Object> columns, Func4<Cell, String, Integer, Map<String, Object>, Row> readCellFunc) throws InstantiationException, IllegalAccessException;

    /**
     * 读取文件流
     *
     * @param is             InputStream
     * @param configTableMap excel配置集合
     * @return
     * @throws InvalidFormatException
     * @throws IOException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    void readInputStream(InputStream is, Map<Object, Table<String, String, Object>> configTableMap) throws InvalidFormatException, IOException, InstantiationException, IllegalAccessException;

    /**
     * 写入文件
     *
     * @param configTableMap excel配置集合
     * @throws InvalidFormatException
     * @throws IOException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    void writeFile(String templateFilePath, String outFilePath, Map<Object, Table<String, String, Object>> configTableMap) throws InvalidFormatException, IOException, InstantiationException, IllegalAccessException;

    /**
     * 写入工作薄
     *
     * @param workbook       工作薄
     * @param configTableMap excel配置集合
     * @return
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    void writeWorkbook(Workbook workbook, Map<Object, Table<String, String, Object>> configTableMap) throws InstantiationException, IllegalAccessException;

    /**
     * 写入工作薄
     *
     * @param workbook    工作薄
     * @param configTable excel配置
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    void writeWorkbook(Workbook workbook, Object sheetIndexObject, Table<String, String, Object> configTable) throws InstantiationException, IllegalAccessException;

    /**
     * 写入Sheet
     *
     * @param sheet       Sheet
     * @param configTable excel配置
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    void writeSheet(Sheet sheet, Table<String, String, Object> configTable) throws InstantiationException, IllegalAccessException;

    /**
     * 写入流
     *
     * @param response
     * @param downFilename     下载文件名
     * @param templateFilePath 模板文件路径
     * @param configTableMap   excel配置集合
     * @throws Exception
     */
    void writeHttpServletResponse(HttpServletResponse response, String downFilename, String templateFilePath, Map<Object, Table<String, String, Object>> configTableMap) throws Exception;

    /**
     * 写入流
     *
     * @param response
     * @param downFilename            下载文件名
     * @param templateFileInputStream 模板文件流
     * @param configTableMap          excel配置集合
     * @throws Exception
     */
    void writeHttpServletResponse(HttpServletResponse response, String downFilename, InputStream templateFileInputStream, Map<Object, Table<String, String, Object>> configTableMap) throws Exception;

    /**
     * 功能：拷贝sheet
     * 实际调用     copySheet(targetSheet, sourceSheet, targetWork, sourceWork, true)
     *
     * @param targetSheet
     * @param sourceSheet
     * @param targetWork
     * @param sourceWork
     * @param endRowIndex
     */
    void copySheet(Sheet targetSheet, Sheet sourceSheet, Workbook targetWork, Workbook sourceWork, int endRowIndex) throws Exception;

    /**
     * 功能：拷贝sheet
     *
     * @param targetSheet
     * @param sourceSheet
     * @param targetWork
     * @param sourceWork
     * @param endRowIndex
     * @param copyStyle   boolean 是否拷贝样式
     */
    void copySheet(Sheet targetSheet, Sheet sourceSheet, Workbook targetWork, Workbook sourceWork, int endRowIndex, boolean copyStyle) throws Exception;

    /**
     * 功能：拷贝row
     *
     * @param targetRow
     * @param sourceRow
     * @param targetWork
     * @param sourceWork
     * @param styleMap
     */
    void copyRow(Row targetRow, Row sourceRow, Workbook targetWork, Workbook sourceWork, Map<String, CellStyle> styleMap) throws Exception;

    /**
     * 功能：拷贝cell，依据styleMap是否为空判断是否拷贝单元格样式
     *
     * @param targetCell 不能为空
     * @param sourceCell 不能为空
     * @param targetWork 不能为空
     * @param sourceWork 不能为空
     * @param styleMap   可以为空
     */
    void copyCell(Cell targetCell, Cell sourceCell, Workbook targetWork, Workbook sourceWork, Map<String, CellStyle> styleMap);

    /**
     * 功能：复制原有sheet的合并单元格到新创建的sheet
     *
     * @param targetSheet
     * @param sourceSheet
     */
    void mergerRegion(Sheet targetSheet, Sheet sourceSheet) throws Exception;

    /**
     * 写入文件流
     *
     * @param is             InputStream
     * @param configTableMap excel配置集合
     * @return
     * @throws InvalidFormatException
     * @throws IOException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    void writeInputStream(InputStream is, Map<Object, Table<String, String, Object>> configTableMap) throws InvalidFormatException, IOException, InstantiationException, IllegalAccessException;
}
