package top.cardone.data.service;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;

import java.util.Map;

/**
 * 分页 service
 *
 * @author yao hai tao
 */
public interface PageService extends SimpleService {
    /**
     * 分页
     *
     * @param page 对象
     * @return 分页对象
     */
    Page<Map<String, Object>> page(Object page);

    @Cacheable
    default Page<Map<String, Object>> pageCache(Object page) {
        return this.page(page);
    }

    Page<Map<String, Object>> pageByFuncId(String funcId, Object page);

    @Cacheable
    default Page<Map<String, Object>> pageByFuncIdCache(String funcId, Object page) {
        return this.pageByFuncId(funcId, page);
    }

    /**
     * 分页
     *
     * @param page 对象
     * @return 分页对象
     */
    <P> Page<P> page(Class<P> mappedClass, Object page);

    @Cacheable
    default <P> Page<P> pageCache(Class<P> mappedClass, Object page) {
        return this.page(mappedClass, page);
    }

    <P> Page<P> pageByFuncId(Class<P> mappedClass, String funcId, Object page);

    @Cacheable
    default <P> Page<P> pageByFuncIdCache(Class<P> mappedClass, String funcId, Object page) {
        return this.pageByFuncId(mappedClass, funcId, page);
    }

    @Cacheable
    default Page<Map<String, Object>> pageBySqlFileNameCache(String countSqlFileName, String findListSqlFileName, Object page) {
        return this.pageBySqlFileName(countSqlFileName, findListSqlFileName, page);
    }

    Page<Map<String, Object>> pageBySqlFileName(String countSqlFileName, String findListSqlFileName, Object page);

    @Cacheable
    default <P> Page<P> pageBySqlFileNameCache(Class<P> mappedClass, String countSqlFileName, String findListSqlFileName, Object page) {
        return this.pageBySqlFileName(mappedClass, countSqlFileName, findListSqlFileName, page);
    }

    <P> Page<P> pageBySqlFileName(Class<P> mappedClass, String countSqlFileName, String findListSqlFileName, Object page);
}
