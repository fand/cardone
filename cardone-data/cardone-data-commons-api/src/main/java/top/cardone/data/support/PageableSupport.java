package top.cardone.data.support;

import org.springframework.data.domain.Pageable;

import java.util.Map;

/**
 * @author yao hai tao
 * @date 2015/8/9
 */
public interface PageableSupport extends java.io.Serializable {
    /**
     * 实例化：分页
     *
     * @param paramMap 参数键值对
     * @return 分页
     */
    Pageable newPageable(Map<String, ?> paramMap);
}
