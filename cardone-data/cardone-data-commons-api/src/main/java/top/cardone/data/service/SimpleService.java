package top.cardone.data.service;

import lombok.NonNull;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

/**
 * 简易 service
 * <p>
 *
 * @author yao hai tao
 * @date 2015/8/21
 */
public interface SimpleService extends CrudService {
    /**
     * 查询复杂类型数据集合
     *
     * @param mappedClass 返回类型
     * @param findList    等于那些属性
     * @return 复杂类型数据集合
     */
    <P> List<P> findList(Class<P> mappedClass, Object findList);

    @Cacheable
    default <P> List<P> findListCache(Class<P> mappedClass, Object findList) {
        return this.findList(mappedClass, findList);
    }

    <P> List<P> findListByFuncId(Class<P> mappedClass, String funcId, Object findList);

    @Cacheable
    default <P> List<P> findListByFuncIdCache(Class<P> mappedClass, String funcId, Object findList) {
        return this.findListByFuncId(mappedClass, funcId, findList);
    }

    /**
     * 查询复杂类型数据
     *
     * @param mappedClass 返回类型
     * @param findOne     等于那些属性
     * @return 复杂类型数据
     */
    <P> P findOne(Class<P> mappedClass, Object findOne);

    @Cacheable
    default <P> P findOneCache(Class<P> mappedClass, Object findOne) {
        return this.findOne(mappedClass, findOne);
    }

    <P> P findOneByFuncId(Class<P> mappedClass, String funcId, Object findOne);

    @Cacheable
    default <P> P findOneByFuncIdCache(Class<P> mappedClass, String funcId, Object findOne) {
        return this.findOneByFuncId(mappedClass, funcId, findOne);
    }

    /**
     * 查询简单类型数据集合
     *
     * @return 简单类型数据集合
     */
    <R> List<R> readList(@NonNull Class<R> requiredType, Object readList);

    @Cacheable
    default <R> List<R> readListCache(@NonNull Class<R> requiredType, Object readList) {
        return this.readList(requiredType, readList);
    }

    <R> List<R> readListByFuncId(@NonNull Class<R> requiredType, String funcId, Object readList);

    @Cacheable
    default <R> List<R> readListByFuncIdCache(@NonNull Class<R> requiredType, String funcId, Object readList) {
        return this.readListByFuncId(requiredType, funcId, readList);
    }

    /**
     * 查询简单类型数据
     *
     * @return 简单类型数据
     */
    <R> R readOne(@NonNull Class<R> requiredType, Object readOne);

    @Cacheable
    default <R> R readOneCache(@NonNull Class<R> requiredType, Object readOne) {
        return this.readOne(requiredType, readOne);
    }

    @Cacheable
    default <R> R readOneByFuncIdCache(@NonNull Class<R> requiredType, String funcId, Object readOne) {
        return this.readOneByFuncId(requiredType, funcId, readOne);
    }

    <R> R readOneByFuncId(@NonNull Class<R> requiredType, String funcId, Object readOne);

    @Cacheable
    default <P> List<P> findListBySqlFileNameCache(Class<P> mappedClass, String sqlFileName, Object findList) {
        return this.findListBySqlFileName(mappedClass, sqlFileName, findList);
    }

    <P> List<P> findListBySqlFileName(Class<P> mappedClass, String sqlFileName, Object findList);

    @Cacheable
    default <P> P findOneBySqlFileNameCache(Class<P> mappedClass, String sqlFileName, Object findOne) {
        return this.findOneBySqlFileName(mappedClass, sqlFileName, findOne);
    }

    <P> P findOneBySqlFileName(Class<P> mappedClass, String sqlFileName, Object findOne);

    @Cacheable
    default <R> List<R> readListBySqlFileNameCache(@NonNull Class<R> requiredType, String sqlFileName, Object readList) {
        return this.readListBySqlFileName(requiredType, sqlFileName, readList);
    }

    <R> List<R> readListBySqlFileName(@NonNull Class<R> requiredType, String sqlFileName, Object readList);

    @Cacheable
    default <R> R readOneBySqlFileNameCache(@NonNull Class<R> requiredType, String sqlFileName, Object readOne) {
        return this.readOneBySqlFileName(requiredType, sqlFileName, readOne);
    }

    <R> R readOneBySqlFileName(@NonNull Class<R> requiredType, String sqlFileName, Object readOne);
}
