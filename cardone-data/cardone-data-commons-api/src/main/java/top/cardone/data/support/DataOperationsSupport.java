package top.cardone.data.support;

import top.cardone.core.util.action.Action2;
import top.cardone.core.util.func.Func2;

/**
 * @author yao hai tao
 * @date 2015/8/9
 */
public interface DataOperationsSupport extends java.io.Serializable {
    /**
     * 执行有返回值的
     *
     * @param parseBeanId          解析 bean id
     * @param t1                   解析对象
     * @param t2                   解析参数
     * @param t3BeanId             数据操作执和方法 bean id
     * @param dataOperationsAction 数据操作执和方法
     */
    <R1, T1, T2, T3> void action(String parseBeanId, T1 t1, T2 t2, String t3BeanId, Action2<R1, T3> dataOperationsAction);

    /**
     * 执行有返回值的
     *
     * @param parseBeanId          解析 bean id
     * @param t3BeanId             数据操作执和方法 bean id
     * @param dataOperationsAction 数据操作执和方法
     */
    <R1, T1, T2, T3> void action(String parseBeanId, String t3BeanId, Action2<Func2<R1, T1, T2>, T3> dataOperationsAction);

    /**
     * 执行有返回值的
     *
     * @param parseBeanId          解析 bean id
     * @param t1                   解析对象
     * @param t2BeanId             数据操作执和方法 bean id
     * @param dataOperationsAction 数据操作执和方法
     */
    <R1, T1, T2> void action(String parseBeanId, T1 t1, String t2BeanId, Action2<R1, T2> dataOperationsAction);

    /**
     * 执行有返回值的
     *
     * @param parseBeanId        解析 bean id
     * @param t1                 解析对象
     * @param t2                 解析参数
     * @param t3BeanId           数据操作执和方法 bean id
     * @param dataOperationsFunc 数据操作执和方法
     * @return 返回值
     */
    <R1, R2, T1, T2, T3> R1 func(String parseBeanId, T1 t1, T2 t2, String t3BeanId, Func2<R1, R2, T3> dataOperationsFunc);

    /**
     * 执行有返回值的
     *
     * @param parseBeanId        解析 bean id
     * @param t3BeanId           数据操作执和方法 bean id
     * @param dataOperationsFunc 数据操作执和方法
     * @return 返回值
     */
    <R1, R2, T1, T2, T3> R1 func(String parseBeanId, String t3BeanId, Func2<R1, Func2<R2, T1, T2>, T3> dataOperationsFunc);

    /**
     * 执行有返回值的
     *
     * @param parseBeanId        解析 bean id
     * @param t1                 解析对象
     * @param t2BeanId           数据操作执和方法 bean id
     * @param dataOperationsFunc 数据操作执和方法
     * @return 返回值
     */
    <R1, R2, T1, T2> R1 func(String parseBeanId, T1 t1, String t2BeanId, Func2<R1, R2, T2> dataOperationsFunc);
}
