package top.cardone.data.support;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import top.cardone.core.util.func.Func0;
import top.cardone.core.util.func.Func1;

import java.util.List;
import java.util.Map;

/**
 * @author yao hai tao
 * @date 2015/8/9
 */
public interface PageSupport extends java.io.Serializable {
    /**
     * 实例化：分页
     *
     * @param content  内容
     * @param paramMap 参数键值对
     * @param total    总数
     * @return 分页
     */
    <T> Map<String, Object> newMap(List<T> content, Map<String, ?> paramMap, long total);

    /**
     * 实例化：分页
     *
     * @param content  内容
     * @param paramMap 参数键值对
     * @param total    总数
     * @return 分页
     */
    <T> Page<T> newPage(List<T> content, Map<String, ?> paramMap, long total);

    /**
     * 实例化：分页
     *
     * @param content  内容
     * @param pageable 参数键值对
     * @param total    总数
     * @return 分页
     */
    <T> Page<T> newPage(List<T> content, Pageable pageable, long total);

    /**
     * 实例化：分页
     *
     * @param contentFunc 内容查询方法
     * @param paramMap    参数键值对
     * @param totalFunc   总数查询方法
     * @return 分页
     */
    <T> Page<T> newPage(Func1<List<T>, Pageable> contentFunc, Map<String, ?> paramMap, Func0<Long> totalFunc);

    /**
     * 实例化：分页
     *
     * @param contentFunc 内容查询方法
     * @param pageable    参数键值对
     * @param totalFunc   总数查询方法
     * @return 分页
     */
    <T> Page<T> newPage(Func0<List<T>> contentFunc, Pageable pageable, Func0<Long> totalFunc);
}