package top.cardone.data.jdbc.support;

import org.springframework.jdbc.core.JdbcOperations;
import top.cardone.core.util.action.Action2;
import top.cardone.core.util.func.Func2;

import java.util.Map;

/**
 * @author yao hai tao
 * @date 2015/8/9
 */
public interface JdbcOperationsSupport {
    /**
     * 执行无返回值的
     *
     * @param parseBeanId          解析 bean id
     * @param parseFilePath        解析文件路径
     * @param parseParamMap        解析参数映射
     * @param jdbcOperationsBeanId 数据操作执和方法 bean id
     * @param jdbcOperationsAction 数据操作执和方法
     */
    void action(String parseBeanId, String parseFilePath, Map<String, ?> parseParamMap, String jdbcOperationsBeanId, Action2<String, JdbcOperations> jdbcOperationsAction);

    /**
     * 执行无返回值的
     *
     * @param parseFilePath        解析文件路径
     * @param parseParamMap        解析参数映射
     * @param jdbcOperationsAction 数据操作执和方法
     */
    void action(String parseFilePath, Map<String, ?> parseParamMap, Action2<String, JdbcOperations> jdbcOperationsAction);

    /**
     * 执行无返回值的
     *
     * @param parseBeanId          解析 bean id
     * @param parseFilePath        解析文件路径
     * @param jdbcOperationsBeanId 数据操作执和方法 bean id
     * @param jdbcOperationsAction 数据操作执和方法
     */
    void action(String parseBeanId, String parseFilePath, String jdbcOperationsBeanId, Action2<String, JdbcOperations> jdbcOperationsAction);

    /**
     * 执行无返回值的
     *
     * @param parseBeanId          解析 bean id
     * @param jdbcOperationsBeanId 数据操作执和方法 bean id
     * @param jdbcOperationsAction 数据操作执和方法
     */
    void action(String parseBeanId, String jdbcOperationsBeanId, Action2<Func2<String, String, Map<String, ?>>, JdbcOperations> jdbcOperationsAction);

    /**
     * 执行无返回值的
     *
     * @param parseFilePath        解析文件路径
     * @param jdbcOperationsAction 数据操作执和方法
     */
    void action(String parseFilePath, Action2<String, JdbcOperations> jdbcOperationsAction);

    /**
     * 执行无返回值的
     *
     * @param jdbcOperationsAction 数据操作执和方法
     */
    void action(Action2<Func2<String, String, Map<String, ?>>, JdbcOperations> jdbcOperationsAction);

    /**
     * 执行有返回值的
     *
     * @param parseBeanId          解析 bean id
     * @param parseFilePath        解析文件路径
     * @param parseParamMap        解析参数映射
     * @param jdbcOperationsBeanId 数据操作执和方法 bean id
     * @param jdbcOperationsFunc   数据操作执和方法
     * @return 返回值
     */
    <R1> R1 func(String parseBeanId, String parseFilePath, Map<String, ?> parseParamMap, String jdbcOperationsBeanId, Func2<R1, String, JdbcOperations> jdbcOperationsFunc);

    /**
     * 执行有返回值的
     *
     * @param parseFilePath      解析文件路径
     * @param parseParamMap      解析参数映射
     * @param jdbcOperationsFunc 数据操作执和方法
     * @return 返回值
     */
    <R1> R1 func(String parseFilePath, Map<String, ?> parseParamMap, Func2<R1, String, JdbcOperations> jdbcOperationsFunc);

    /**
     * 执行有返回值的
     *
     * @param parseBeanId          解析 bean id
     * @param parseFilePath        解析文件路径
     * @param jdbcOperationsBeanId 数据操作执和方法 bean id
     * @param jdbcOperationsFunc   数据操作执和方法
     * @return 返回值
     */
    <R1> R1 func(String parseBeanId, String parseFilePath, String jdbcOperationsBeanId, Func2<R1, String, JdbcOperations> jdbcOperationsFunc);

    /**
     * 执行有返回值的
     *
     * @param parseBeanId          解析 bean id
     * @param jdbcOperationsBeanId 数据操作执和方法 bean id
     * @param jdbcOperationsFunc   数据操作执和方法
     * @return 返回值
     */
    <R1> R1 func(String parseBeanId, String jdbcOperationsBeanId, Func2<R1, Func2<String, String, Map<String, ?>>, JdbcOperations> jdbcOperationsFunc);

    /**
     * 执行有返回值的
     *
     * @param parseFilePath      解析文件路径
     * @param jdbcOperationsFunc 数据操作执和方法
     * @return 返回值
     */
    <R1> R1 func(String parseFilePath, Func2<R1, String, JdbcOperations> jdbcOperationsFunc);

    /**
     * 执行有返回值的
     *
     * @param jdbcOperationsFunc 数据操作执和方法
     * @return 返回值
     */
    <R1> R1 func(Func2<R1, Func2<String, String, Map<String, ?>>, JdbcOperations> jdbcOperationsFunc);
}
