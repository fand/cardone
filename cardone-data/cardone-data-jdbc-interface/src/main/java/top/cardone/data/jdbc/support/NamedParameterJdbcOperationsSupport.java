package top.cardone.data.jdbc.support;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import top.cardone.core.util.action.Action2;
import top.cardone.core.util.func.Func2;

import java.util.Map;

/**
 * @author yao hai tao
 * @date 2015/8/9
 */
public interface NamedParameterJdbcOperationsSupport {
    NamedParameterJdbcOperations getNamedParameterJdbcOperations(String namedParameterJdbcOperationsBeanId);

    String parseSql(String parseBeanId, String filePath, Map<String, ?> model);

    /**
     * 执行无返回值的
     *
     * @param parseBeanId                        解析 bean id
     * @param parseFilePath                      解析文件路径
     * @param parseParamMap                      解析参数映射
     * @param namedParameterJdbcOperationsBeanId 数据操作执和方法 bean id
     * @param namedParameterJdbcOperationsAction 数据操作执和方法
     */
    void action(String parseBeanId, String parseFilePath, Map<String, ?> parseParamMap, String namedParameterJdbcOperationsBeanId, Action2<String, NamedParameterJdbcOperations> namedParameterJdbcOperationsAction);

    /**
     * 执行无返回值的
     *
     * @param parseFilePath                      解析文件路径
     * @param parseParamMap                      解析参数映射
     * @param namedParameterJdbcOperationsAction 数据操作执和方法
     */
    void action(String parseFilePath, Map<String, ?> parseParamMap, Action2<String, NamedParameterJdbcOperations> namedParameterJdbcOperationsAction);

    /**
     * 执行无返回值的
     *
     * @param parseBeanId                        解析 bean id
     * @param parseFilePath                      解析文件路径
     * @param namedParameterJdbcOperationsBeanId 数据操作执和方法 bean id
     * @param namedParameterJdbcOperationsAction 数据操作执和方法
     */
    void action(String parseBeanId, String parseFilePath, String namedParameterJdbcOperationsBeanId, Action2<String, NamedParameterJdbcOperations> namedParameterJdbcOperationsAction);

    /**
     * 执行无返回值的
     *
     * @param parseBeanId                        解析 bean id
     * @param namedParameterJdbcOperationsBeanId 数据操作执和方法 bean id
     * @param namedParameterJdbcOperationsAction 数据操作执和方法
     */
    void action(String parseBeanId, String namedParameterJdbcOperationsBeanId, Action2<Func2<String, String, Map<String, ?>>, NamedParameterJdbcOperations> namedParameterJdbcOperationsAction);

    /**
     * 执行无返回值的
     *
     * @param parseFilePath                      解析文件路径
     * @param namedParameterJdbcOperationsAction 数据操作执和方法
     */
    void action(String parseFilePath, Action2<String, NamedParameterJdbcOperations> namedParameterJdbcOperationsAction);

    /**
     * 执行无返回值的
     *
     * @param namedParameterJdbcOperationsAction 数据操作执和方法
     */
    void action(Action2<Func2<String, String, Map<String, ?>>, NamedParameterJdbcOperations> namedParameterJdbcOperationsAction);

    /**
     * 执行有返回值的
     *
     * @param parseBeanId                        解析 bean id
     * @param parseFilePath                      解析文件路径
     * @param parseParamMap                      解析参数映射
     * @param namedParameterJdbcOperationsBeanId 数据操作执和方法 bean id
     * @param namedParameterJdbcOperationsFunc   数据操作执和方法
     * @return 返回值
     */
    <R1> R1 func(String parseBeanId, String parseFilePath, Map<String, ?> parseParamMap, String namedParameterJdbcOperationsBeanId, Func2<R1, String, NamedParameterJdbcOperations> namedParameterJdbcOperationsFunc);

    /**
     * 执行有返回值的
     *
     * @param parseFilePath                    解析文件路径
     * @param parseParamMap                    解析参数映射
     * @param namedParameterJdbcOperationsFunc 数据操作执和方法
     * @return 返回值
     */
    <R1> R1 func(String parseFilePath, Map<String, ?> parseParamMap, Func2<R1, String, NamedParameterJdbcOperations> namedParameterJdbcOperationsFunc);

    /**
     * 执行有返回值的
     *
     * @param parseBeanId                        解析 bean id
     * @param parseFilePath                      解析文件路径
     * @param namedParameterJdbcOperationsBeanId 数据操作执和方法 bean id
     * @param namedParameterJdbcOperationsFunc   数据操作执和方法
     * @return 返回值
     */
    <R1> R1 func(String parseBeanId, String parseFilePath, String namedParameterJdbcOperationsBeanId, Func2<R1, String, NamedParameterJdbcOperations> namedParameterJdbcOperationsFunc);

    /**
     * 执行有返回值的
     *
     * @param parseBeanId                        解析 bean id
     * @param namedParameterJdbcOperationsBeanId 数据操作执和方法 bean id
     * @param namedParameterJdbcOperationsFunc   数据操作执和方法
     * @return 返回值
     */
    <R1> R1 func(String parseBeanId, String namedParameterJdbcOperationsBeanId, Func2<R1, Func2<String, String, Map<String, ?>>, NamedParameterJdbcOperations> namedParameterJdbcOperationsFunc);

    /**
     * 执行有返回值的
     *
     * @param parseFilePath                    解析文件路径
     * @param namedParameterJdbcOperationsFunc 数据操作执和方法
     * @return 返回值
     */
    <R1> R1 func(String parseFilePath, Func2<R1, String, NamedParameterJdbcOperations> namedParameterJdbcOperationsFunc);

    /**
     * 执行有返回值的
     *
     * @param namedParameterJdbcOperationsFunc 数据操作执和方法
     * @return 返回值
     */
    <R1> R1 func(Func2<R1, Func2<String, String, Map<String, ?>>, NamedParameterJdbcOperations> namedParameterJdbcOperationsFunc);
}