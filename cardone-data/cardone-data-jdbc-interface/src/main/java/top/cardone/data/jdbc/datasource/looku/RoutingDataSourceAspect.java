package top.cardone.data.jdbc.datasource.looku;

import org.aspectj.lang.ProceedingJoinPoint;

/**
 * @author yao hai tao
 */
public interface RoutingDataSourceAspect {
    /**
     * 执行设直目标数据源
     *
     * @param joinPoint
     * @return
     * @throws Throwable
     */
    Object postProcessBeforeSetTargetDataSource(final ProceedingJoinPoint joinPoint) throws Throwable;
}
