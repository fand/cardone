package top.cardone.data.jdbc.datasource.looku;

/**
 * @author yao hai tao
 */
public interface RoutingDataSource {
    ThreadLocal<Object> KEY_THREAD_LOCAL = ThreadLocal.withInitial(() -> null);
}