package top.cardone;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.ImportResource;

/**
 * @author yao hai tao
 */
@ImportResource("applicationContext.xml")
public class CardOneApplication implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication.run(CardOneApplication.class, args);
    }

    @Override
    public void run(String... args) {
        System.out.println("test...");
    }
}