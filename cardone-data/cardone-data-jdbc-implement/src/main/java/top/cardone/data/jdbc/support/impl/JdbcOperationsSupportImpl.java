package top.cardone.data.jdbc.support.impl;

import org.springframework.jdbc.core.JdbcOperations;
import top.cardone.context.ApplicationContextHolder;
import top.cardone.context.util.StringUtils;
import top.cardone.core.util.action.Action2;
import top.cardone.core.util.func.Func2;
import top.cardone.data.jdbc.support.JdbcOperationsSupport;
import top.cardone.data.support.DataOperationsSupport;

import java.util.Map;

/**
 * @author yao hai tao
 * @date 2015/8/9
 */
public class JdbcOperationsSupportImpl implements JdbcOperationsSupport {
    /**
     * 默认数据操作执和方法 bean id
     */
    @lombok.Setter
    private String jdbcOperationsBeanId = JdbcOperations.class.getName();
    /**
     * 默认解析 bean id
     */
    @lombok.Setter
    private String parseBeanId = "parseSqlFunc";

    @Override
    public void action(String parseBeanId, String parseFilePath, Map<String, ?> parseParamMap, String jdbcOperationsBeanId, Action2<String, JdbcOperations> jdbcOperationsAction) {
        String newParseBeanId = StringUtils.defaultIfBlank(parseBeanId, this.parseBeanId);
        String newJdbcOperationsBeanId = StringUtils.defaultIfBlank(jdbcOperationsBeanId, this.jdbcOperationsBeanId);

        ApplicationContextHolder.getBean(DataOperationsSupport.class).action(newParseBeanId, parseFilePath, parseParamMap, newJdbcOperationsBeanId, jdbcOperationsAction);
    }

    @Override
    public void action(String parseFilePath, Map<String, ?> parseParamMap, Action2<String, JdbcOperations> jdbcOperationsAction) {
        ApplicationContextHolder.getBean(DataOperationsSupport.class).action(parseBeanId, parseFilePath, parseParamMap, jdbcOperationsBeanId, jdbcOperationsAction);
    }

    @Override
    public void action(String parseBeanId, String parseFilePath, String jdbcOperationsBeanId, Action2<String, JdbcOperations> jdbcOperationsAction) {
        String newParseBeanId = StringUtils.defaultIfBlank(parseBeanId, this.parseBeanId);
        String newJdbcOperationsBeanId = StringUtils.defaultIfBlank(jdbcOperationsBeanId, this.jdbcOperationsBeanId);

        ApplicationContextHolder.getBean(DataOperationsSupport.class).action(newParseBeanId, parseFilePath, newJdbcOperationsBeanId, jdbcOperationsAction);
    }

    @Override
    public void action(String parseBeanId, String jdbcOperationsBeanId, Action2<Func2<String, String, Map<String, ?>>, JdbcOperations> jdbcOperationsAction) {
        String newParseBeanId = StringUtils.defaultIfBlank(parseBeanId, this.parseBeanId);
        String newJdbcOperationsBeanId = StringUtils.defaultIfBlank(jdbcOperationsBeanId, this.jdbcOperationsBeanId);

        ApplicationContextHolder.getBean(DataOperationsSupport.class).action(newParseBeanId, newJdbcOperationsBeanId, jdbcOperationsAction);
    }

    @Override
    public void action(String parseFilePath, Action2<String, JdbcOperations> jdbcOperationsAction) {
        ApplicationContextHolder.getBean(DataOperationsSupport.class).action(parseBeanId, parseFilePath, jdbcOperationsBeanId, jdbcOperationsAction);
    }

    @Override
    public void action(Action2<Func2<String, String, Map<String, ?>>, JdbcOperations> jdbcOperationsAction) {
        ApplicationContextHolder.getBean(DataOperationsSupport.class).action(parseBeanId, jdbcOperationsBeanId, jdbcOperationsAction);
    }

    @Override
    public <R1> R1 func(String parseBeanId, String parseFilePath, Map<String, ?> parseParamMap, String jdbcOperationsBeanId, Func2<R1, String, JdbcOperations> jdbcOperationsFunc) {
        String newParseBeanId = StringUtils.defaultIfBlank(parseBeanId, this.parseBeanId);
        String newJdbcOperationsBeanId = StringUtils.defaultIfBlank(jdbcOperationsBeanId, this.jdbcOperationsBeanId);

        return ApplicationContextHolder.getBean(DataOperationsSupport.class).func(newParseBeanId, parseFilePath, parseParamMap, newJdbcOperationsBeanId, jdbcOperationsFunc);
    }

    @Override
    public <R1> R1 func(String parseFilePath, Map<String, ?> parseParamMap, Func2<R1, String, JdbcOperations> jdbcOperationsFunc) {
        return ApplicationContextHolder.getBean(DataOperationsSupport.class).func(parseBeanId, parseFilePath, parseParamMap, jdbcOperationsBeanId, jdbcOperationsFunc);
    }

    @Override
    public <R1> R1 func(String parseBeanId, String parseFilePath, String jdbcOperationsBeanId, Func2<R1, String, JdbcOperations> jdbcOperationsFunc) {
        String newParseBeanId = StringUtils.defaultIfBlank(parseBeanId, this.parseBeanId);
        String newJdbcOperationsBeanId = StringUtils.defaultIfBlank(jdbcOperationsBeanId, this.jdbcOperationsBeanId);

        return ApplicationContextHolder.getBean(DataOperationsSupport.class).func(newParseBeanId, parseFilePath, newJdbcOperationsBeanId, jdbcOperationsFunc);
    }

    @Override
    public <R1> R1 func(String parseBeanId, String jdbcOperationsBeanId, Func2<R1, Func2<String, String, Map<String, ?>>, JdbcOperations> jdbcOperationsFunc) {
        String newParseBeanId = StringUtils.defaultIfBlank(parseBeanId, this.parseBeanId);
        String newJdbcOperationsBeanId = StringUtils.defaultIfBlank(jdbcOperationsBeanId, this.jdbcOperationsBeanId);

        return ApplicationContextHolder.getBean(DataOperationsSupport.class).func(newParseBeanId, newJdbcOperationsBeanId, jdbcOperationsFunc);
    }

    @Override
    public <R1> R1 func(String parseFilePath, Func2<R1, String, JdbcOperations> jdbcOperationsFunc) {
        return ApplicationContextHolder.getBean(DataOperationsSupport.class).func(parseBeanId, parseFilePath, jdbcOperationsBeanId, jdbcOperationsFunc);
    }

    @Override
    public <R1> R1 func(Func2<R1, Func2<String, String, Map<String, ?>>, JdbcOperations> jdbcOperationsFunc) {
        return ApplicationContextHolder.getBean(DataOperationsSupport.class).func(parseBeanId, jdbcOperationsBeanId, jdbcOperationsFunc);
    }
}
