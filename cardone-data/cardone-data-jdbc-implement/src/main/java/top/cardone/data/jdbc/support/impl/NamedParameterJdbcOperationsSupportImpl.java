package top.cardone.data.jdbc.support.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import top.cardone.context.ApplicationContextHolder;
import top.cardone.core.util.action.Action2;
import top.cardone.core.util.func.Func2;
import top.cardone.data.jdbc.support.NamedParameterJdbcOperationsSupport;
import top.cardone.data.support.DataOperationsSupport;

import java.util.Map;

/**
 * @author yao hai tao
 * @date 2015/8/9
 */
public class NamedParameterJdbcOperationsSupportImpl implements NamedParameterJdbcOperationsSupport {
    /**
     * 默认数据操作执和方法 bean id
     */
    @lombok.Getter
    @lombok.Setter
    private String namedParameterJdbcOperationsBeanId = NamedParameterJdbcOperations.class.getName();
    /**
     * 默认解析 bean id
     */
    @lombok.Getter
    @lombok.Setter
    private String parseBeanId = "parseSqlFunc";

    /**
     * 执行无返回值的
     *
     * @param parseBeanId                        解析 bean id
     * @param parseFilePath                      解析文件路径
     * @param parseParamMap                      解析参数映射
     * @param namedParameterJdbcOperationsBeanId 数据操作执和方法 bean id
     * @param namedParameterJdbcOperationsAction 数据操作执和方法
     */
    @Override
    public void action(String parseBeanId, String parseFilePath, Map<String, ?> parseParamMap, String namedParameterJdbcOperationsBeanId, Action2<String, NamedParameterJdbcOperations> namedParameterJdbcOperationsAction) {
        String newParseBeanId = StringUtils.defaultIfBlank(parseBeanId, this.parseBeanId);
        String newNamedParameterJdbcOperationsBeanId = StringUtils.defaultIfBlank(namedParameterJdbcOperationsBeanId, this.namedParameterJdbcOperationsBeanId);

        ApplicationContextHolder.getBean(DataOperationsSupport.class).action(newParseBeanId, parseFilePath, parseParamMap, newNamedParameterJdbcOperationsBeanId, namedParameterJdbcOperationsAction);
    }

    /**
     * 执行无返回值的
     *
     * @param parseFilePath                      解析文件路径
     * @param parseParamMap                      解析参数映射
     * @param namedParameterJdbcOperationsAction 数据操作执和方法
     */
    @Override
    public void action(String parseFilePath, Map<String, ?> parseParamMap, Action2<String, NamedParameterJdbcOperations> namedParameterJdbcOperationsAction) {
        ApplicationContextHolder.getBean(DataOperationsSupport.class).action(parseBeanId, parseFilePath, parseParamMap, namedParameterJdbcOperationsBeanId, namedParameterJdbcOperationsAction);
    }

    /**
     * 执行无返回值的
     *
     * @param parseBeanId                        解析 bean id
     * @param parseFilePath                      解析文件路径
     * @param namedParameterJdbcOperationsBeanId 数据操作执和方法 bean id
     * @param namedParameterJdbcOperationsAction 数据操作执和方法
     */
    @Override
    public void action(String parseBeanId, String parseFilePath, String namedParameterJdbcOperationsBeanId, Action2<String, NamedParameterJdbcOperations> namedParameterJdbcOperationsAction) {
        String newParseBeanId = StringUtils.defaultIfBlank(parseBeanId, this.parseBeanId);
        String newNamedParameterJdbcOperationsBeanId = StringUtils.defaultIfBlank(namedParameterJdbcOperationsBeanId, this.namedParameterJdbcOperationsBeanId);

        ApplicationContextHolder.getBean(DataOperationsSupport.class).action(newParseBeanId, parseFilePath, newNamedParameterJdbcOperationsBeanId, namedParameterJdbcOperationsAction);
    }

    /**
     * 执行无返回值的
     *
     * @param parseBeanId                        解析 bean id
     * @param namedParameterJdbcOperationsBeanId 数据操作执和方法 bean id
     * @param namedParameterJdbcOperationsAction 数据操作执和方法
     */
    @Override
    public void action(String parseBeanId, String namedParameterJdbcOperationsBeanId, Action2<Func2<String, String, Map<String, ?>>, NamedParameterJdbcOperations> namedParameterJdbcOperationsAction) {
        String newParseBeanId = StringUtils.defaultIfBlank(parseBeanId, this.parseBeanId);
        String newNamedParameterJdbcOperationsBeanId = StringUtils.defaultIfBlank(namedParameterJdbcOperationsBeanId, this.namedParameterJdbcOperationsBeanId);

        ApplicationContextHolder.getBean(DataOperationsSupport.class).action(newParseBeanId, newNamedParameterJdbcOperationsBeanId, namedParameterJdbcOperationsAction);
    }

    /**
     * 执行无返回值的
     *
     * @param parseFilePath                      解析文件路径
     * @param namedParameterJdbcOperationsAction 数据操作执和方法
     */
    @Override
    public void action(String parseFilePath, Action2<String, NamedParameterJdbcOperations> namedParameterJdbcOperationsAction) {
        ApplicationContextHolder.getBean(DataOperationsSupport.class).action(parseBeanId, parseFilePath, namedParameterJdbcOperationsBeanId, namedParameterJdbcOperationsAction);
    }

    /**
     * 执行无返回值的
     *
     * @param namedParameterJdbcOperationsAction 数据操作执和方法
     */
    @Override
    public void action(Action2<Func2<String, String, Map<String, ?>>, NamedParameterJdbcOperations> namedParameterJdbcOperationsAction) {
        ApplicationContextHolder.getBean(DataOperationsSupport.class).action(parseBeanId, namedParameterJdbcOperationsBeanId, namedParameterJdbcOperationsAction);
    }

    /**
     * 执行有返回值的
     *
     * @param parseBeanId                        解析 bean id
     * @param parseFilePath                      解析文件路径
     * @param parseParamMap                      解析参数映射
     * @param namedParameterJdbcOperationsBeanId 数据操作执和方法 bean id
     * @param namedParameterJdbcOperationsFunc   数据操作执和方法
     * @return 返回值
     */
    @Override
    public <R1> R1 func(String parseBeanId, String parseFilePath, Map<String, ?> parseParamMap, String namedParameterJdbcOperationsBeanId, Func2<R1, String, NamedParameterJdbcOperations> namedParameterJdbcOperationsFunc) {
        String newParseBeanId = StringUtils.defaultIfBlank(parseBeanId, this.parseBeanId);
        String newNamedParameterJdbcOperationsBeanId = StringUtils.defaultIfBlank(namedParameterJdbcOperationsBeanId, this.namedParameterJdbcOperationsBeanId);

        return ApplicationContextHolder.getBean(DataOperationsSupport.class).func(newParseBeanId, parseFilePath, parseParamMap, newNamedParameterJdbcOperationsBeanId, namedParameterJdbcOperationsFunc);
    }

    /**
     * 执行有返回值的
     *
     * @param parseFilePath                    解析文件路径
     * @param parseParamMap                    解析参数映射
     * @param namedParameterJdbcOperationsFunc 数据操作执和方法
     * @return 返回值
     */
    @Override
    public <R1> R1 func(String parseFilePath, Map<String, ?> parseParamMap, Func2<R1, String, NamedParameterJdbcOperations> namedParameterJdbcOperationsFunc) {
        return ApplicationContextHolder.getBean(DataOperationsSupport.class).func(parseBeanId, parseFilePath, parseParamMap, namedParameterJdbcOperationsBeanId, namedParameterJdbcOperationsFunc);
    }

    /**
     * 执行有返回值的
     *
     * @param parseBeanId                        解析 bean id
     * @param parseFilePath                      解析文件路径
     * @param namedParameterJdbcOperationsBeanId 数据操作执和方法 bean id
     * @param namedParameterJdbcOperationsFunc   数据操作执和方法
     * @return 返回值
     */
    @Override
    public <R1> R1 func(String parseBeanId, String parseFilePath, String namedParameterJdbcOperationsBeanId, Func2<R1, String, NamedParameterJdbcOperations> namedParameterJdbcOperationsFunc) {
        String newParseBeanId = StringUtils.defaultIfBlank(parseBeanId, this.parseBeanId);
        String newNamedParameterJdbcOperationsBeanId = StringUtils.defaultIfBlank(namedParameterJdbcOperationsBeanId, this.namedParameterJdbcOperationsBeanId);

        return ApplicationContextHolder.getBean(DataOperationsSupport.class).func(newParseBeanId, parseFilePath, newNamedParameterJdbcOperationsBeanId, namedParameterJdbcOperationsFunc);
    }

    /**
     * 执行有返回值的
     *
     * @param parseBeanId                        解析 bean id
     * @param namedParameterJdbcOperationsBeanId 数据操作执和方法 bean id
     * @param namedParameterJdbcOperationsFunc   数据操作执和方法
     * @return 返回值
     */
    @Override
    public <R1> R1 func(String parseBeanId, String namedParameterJdbcOperationsBeanId, Func2<R1, Func2<String, String, Map<String, ?>>, NamedParameterJdbcOperations> namedParameterJdbcOperationsFunc) {
        String newParseBeanId = StringUtils.defaultIfBlank(parseBeanId, this.parseBeanId);
        String newNamedParameterJdbcOperationsBeanId = StringUtils.defaultIfBlank(namedParameterJdbcOperationsBeanId, this.namedParameterJdbcOperationsBeanId);

        return ApplicationContextHolder.getBean(DataOperationsSupport.class).func(newParseBeanId, newNamedParameterJdbcOperationsBeanId, namedParameterJdbcOperationsFunc);
    }

    /**
     * 执行有返回值的
     *
     * @param parseFilePath                    解析文件路径
     * @param namedParameterJdbcOperationsFunc 数据操作执和方法
     * @return 返回值
     */
    @Override
    public <R1> R1 func(String parseFilePath, Func2<R1, String, NamedParameterJdbcOperations> namedParameterJdbcOperationsFunc) {
        return ApplicationContextHolder.getBean(DataOperationsSupport.class).func(parseBeanId, parseFilePath, namedParameterJdbcOperationsBeanId, namedParameterJdbcOperationsFunc);
    }

    /**
     * 执行有返回值的
     *
     * @param namedParameterJdbcOperationsFunc 数据操作执和方法
     * @return 返回值
     */
    @Override
    public <R1> R1 func(Func2<R1, Func2<String, String, Map<String, ?>>, NamedParameterJdbcOperations> namedParameterJdbcOperationsFunc) {
        return ApplicationContextHolder.getBean(DataOperationsSupport.class).func(parseBeanId, namedParameterJdbcOperationsBeanId, namedParameterJdbcOperationsFunc);
    }

    @Override
    public NamedParameterJdbcOperations getNamedParameterJdbcOperations(String namedParameterJdbcOperationsBeanId) {
        String newNamedParameterJdbcOperationsBeanId = StringUtils.defaultIfBlank(namedParameterJdbcOperationsBeanId, this.namedParameterJdbcOperationsBeanId);

        return ApplicationContextHolder.getBean(NamedParameterJdbcOperations.class, newNamedParameterJdbcOperationsBeanId);
    }

    @Override
    public String parseSql(String parseBeanId, String filePath, Map<String, ?> model) {
        String newParseBeanId = StringUtils.defaultIfBlank(parseBeanId, this.parseBeanId);

        return (String) ApplicationContextHolder.getBean(Func2.class, newParseBeanId).func(filePath, model);
    }
}