package top.cardone.data.jdbc.func;

import top.cardone.context.ApplicationContextHolder;
import top.cardone.core.util.func.Func1;
import top.cardone.core.util.func.Func2;
import top.cardone.data.jdbc.datasource.looku.RoutingDataSource;
import top.cardone.template.support.TemplateSupport;

import java.util.Map;
import java.util.Objects;

/**
 * @author yao hai tao
 */
public class SqlParseFunc implements Func1<String, String>, Func2<String, String, Map<String, ?>> {
    @lombok.Setter
    private String configurationKey = "configuration1";
    @lombok.Setter
    private String templateSupportBeanId = TemplateSupport.class.getName();

    @Override
    public String func(String templateString) {
        String newConfigurationKey = Objects.toString(RoutingDataSource.KEY_THREAD_LOCAL.get(), configurationKey);

        return ApplicationContextHolder.func(TemplateSupport.class, templateSupport -> templateSupport.process(newConfigurationKey, templateString, null), templateSupportBeanId);
    }

    @Override
    public String func(String filePath, Map<String, ?> model) {
        String newConfigurationKey = Objects.toString(RoutingDataSource.KEY_THREAD_LOCAL.get(), configurationKey);

        return ApplicationContextHolder.func(TemplateSupport.class, templateSupport -> templateSupport.process(newConfigurationKey, filePath, model), templateSupportBeanId);
    }
}
