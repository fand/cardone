package top.cardone.data.jdbc.datasource.looku.impl;

import lombok.extern.log4j.Log4j2;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;
import top.cardone.data.jdbc.datasource.looku.RoutingDataSource;

/**
 * @author yao hai tao
 */
@Log4j2
public class RoutingDataSourceImpl extends AbstractRoutingDataSource implements RoutingDataSource {
    @Override
    protected Object determineCurrentLookupKey() {
        return RoutingDataSource.KEY_THREAD_LOCAL.get();
    }
}