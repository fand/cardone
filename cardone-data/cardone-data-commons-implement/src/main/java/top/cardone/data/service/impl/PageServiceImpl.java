package top.cardone.data.service.impl;

import org.springframework.data.domain.Page;
import top.cardone.context.ApplicationContextHolder;
import top.cardone.core.util.func.Func1;
import top.cardone.core.util.func.Func2;
import top.cardone.data.dao.PageDao;
import top.cardone.data.service.PageService;

import java.util.Map;

/**
 * @author yao hai tao
 * @date 2015/8/24
 */
public class PageServiceImpl<Dao extends PageDao> extends SimpleServiceImpl<Dao> implements PageService {
    @Override
    public Page<Map<String, Object>> page(Object page) {
        return this.dao.pageBySqlFileName("page.count", "page.find", page);
    }

    @Override
    public Page<Map<String, Object>> pageByFuncId(String funcId, Object page) {
        return (Page<Map<String, Object>>) ApplicationContextHolder.getBean(Func1.class, funcId).func(page);
    }

    @Override
    public <P> Page<P> page(Class<P> mappedClass, Object page) {
        return this.dao.pageBySqlFileName(mappedClass, "page.count", "page.find", page);
    }

    @Override
    public <P> Page<P> pageByFuncId(Class<P> mappedClass, String funcId, Object page) {
        return (Page<P>) ApplicationContextHolder.getBean(Func2.class, funcId).func(mappedClass, page);
    }

    @Override
    public Page<Map<String, Object>> pageBySqlFileName(String countSqlFileName, String findListSqlFileName, Object page) {
        return this.dao.pageBySqlFileName(countSqlFileName, findListSqlFileName, page);
    }

    @Override
    public <P> Page<P> pageBySqlFileName(Class<P> mappedClass, String countSqlFileName, String findListSqlFileName, Object page) {
        return this.dao.pageBySqlFileName(mappedClass, countSqlFileName, findListSqlFileName, page);
    }
}
