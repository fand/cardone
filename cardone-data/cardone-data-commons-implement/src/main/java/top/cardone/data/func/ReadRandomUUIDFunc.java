package top.cardone.data.func;

import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import top.cardone.context.util.MapUtils;
import top.cardone.core.util.func.Func0;
import top.cardone.core.util.func.Func1;

import java.util.Date;
import java.util.Map;
import java.util.UUID;

/**
 * @author yao hai tao
 * @date 16-3-28
 */
public class ReadRandomUUIDFunc implements Func0<String>, Func1<String, Map<String, Object>> {
    @Setter
    private String pkPrefixName;

    private String dateFormat = "yyyyMM";

    @Override
    public String func() {
        return DateFormatUtils.format(new Date(), dateFormat) + "-" + UUID.randomUUID();
    }

    @Override
    public String func(Map<String, Object> stringObjectMap) {
        if (StringUtils.isBlank(pkPrefixName)) {
            return this.func();
        }

        if(stringObjectMap == null){
            return this.func();
        }

        String pkPrefix = MapUtils.getString(stringObjectMap, pkPrefixName);

        if (StringUtils.isBlank(pkPrefix)) {
            pkPrefix = DateFormatUtils.format(new Date(), dateFormat);

            stringObjectMap.put(pkPrefixName, pkPrefix);
        }

        return pkPrefix + "-" + UUID.randomUUID();
    }
}
