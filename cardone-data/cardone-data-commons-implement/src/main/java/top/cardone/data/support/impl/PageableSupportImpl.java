package top.cardone.data.support.impl;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.util.StringUtils;
import top.cardone.data.support.PageableSupport;

import java.util.Map;

/**
 * @author yao hai tao
 * @date 2015/8/9
 */
public class PageableSupportImpl implements PageableSupport {
    /**
     * 排序集合
     */
    @lombok.Setter
    private String directionParam = "direction";

    /**
     * 页面代码参数名
     */
    @lombok.Setter
    private String pageParam = "pageNumber";

    /**
     * 属性集合
     */
    @lombok.Setter
    private String propertiesParam = "properties";

    /**
     * 分页行数大小
     */
    @lombok.Setter
    private int size = 10;

    /**
     * 分页行数大小参数名
     */
    @lombok.Setter
    private String sizeParam = "pageSize";

    /**
     * 分页行数最大大小
     */
    @lombok.Setter
    private int maxSize = 10000;

    /**
     * 分页行数最大大小参数名
     */
    @lombok.Setter
    private String maxSizeParam = "pageSize";

    /**
     * 分页行数最小大小
     */
    @lombok.Setter
    private int minSize = 1;

    /**
     * 分页行数最小大小参数名
     */
    @lombok.Setter
    private String minSizeParam = "pageSize";

    /**
     * 修正分页参数
     */
    @lombok.Setter
    private int fixPage = -1;

    @Override
    public Pageable newPageable(Map<String, ?> paramMap) {
        int page = Math.max(MapUtils.getIntValue(paramMap, this.pageParam, 1), 1) + fixPage;

        int size = MapUtils.getIntValue(paramMap, this.sizeParam, this.size);
        int maxSize = MapUtils.getIntValue(paramMap, this.maxSizeParam, this.maxSize);
        int minSize = MapUtils.getIntValue(paramMap, this.minSizeParam, this.minSize);

        size = NumberUtils.max(size, minSize);
        size = NumberUtils.min(size, maxSize);

        String propertiesString = MapUtils.getString(paramMap, this.propertiesParam);

        if (ObjectUtils.isEmpty(propertiesString)) {
            return PageRequest.of(page, size);
        }

        String[] properties = propertiesString.split(",");

        if (ArrayUtils.isEmpty(properties)) {
            return PageRequest.of(page, size);
        }

        String directionString = MapUtils.getString(paramMap, this.directionParam);

        Sort.Direction directio = null;

        if (org.apache.commons.lang3.StringUtils.isNotBlank(directionString)) {
            directio = Sort.Direction.fromString(directionString);
        }

        if (directio == null) {
            directio = Sort.Direction.DESC;
        }

        return PageRequest.of(page, size, directio, properties);
    }
}
