package top.cardone.data.action;

import com.google.common.base.Charsets;
import com.google.common.collect.Lists;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.Resource;
import org.springframework.util.CollectionUtils;
import top.cardone.context.ApplicationContextHolder;
import top.cardone.context.util.DateUtils;
import top.cardone.core.util.action.Action0;
import top.cardone.core.util.action.Action1;
import top.cardone.data.service.CrudService;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author yao hai tao
 * @date 2016/4/18
 */
@Log4j2
public class InitDataAction implements Action0 {
    @Setter
    private Resource[] jsonFiles;

    @Setter
    private Class<? extends CrudService> serviceBeanType;

    @Setter
    private boolean reinit = false;

    @Setter
    private String creationDate = null;

    @Setter
    private List<Action1<List<Map<String, Object>>>> postActions;

    @Setter
    private List<String> dataProcessingActionNames;

    @Override
    public void action() {
        if (ArrayUtils.isEmpty(jsonFiles)) {
            return;
        }

        Date creation = DateUtils.parseDate(creationDate);

        for (Resource jsonFile : jsonFiles) {
            if (!jsonFile.exists()) {
                continue;
            }

            try {
                if (creation != null) {
                    BasicFileAttributes basicFileAttributes = Files.readAttributes(jsonFile.getFile().toPath(), BasicFileAttributes.class);

                    if (basicFileAttributes.lastModifiedTime().toMillis() < creation.getTime()) {
                        continue;
                    }
                }

                String json = FileUtils.readFileToString(jsonFile.getFile(), Charsets.UTF_8);

                if (StringUtils.isBlank(json)) {
                    continue;
                }

                List<Map<String, Object>> saveMapList = ApplicationContextHolder.getBean(Gson.class).fromJson(json, new TypeToken<List<LinkedHashMap<String, Object>>>() {
                }.getType());

                if (!CollectionUtils.isEmpty(dataProcessingActionNames)) {
                    for (String dataProcessingActionName : dataProcessingActionNames) {
                        try {
                            ApplicationContextHolder.action(Action1.class, aciton -> aciton.action(saveMapList), dataProcessingActionName);
                        } catch (Exception e) {
                            log.error(e);
                        }
                    }
                }

                if (reinit) {
                    ApplicationContextHolder.func(serviceBeanType, service -> service.saveList(Lists.newArrayList(saveMapList)));
                } else {
                    ApplicationContextHolder.func(serviceBeanType, service -> service.insertListByNotExists(Lists.newArrayList(saveMapList)));
                }

                if (!CollectionUtils.isEmpty(postActions)) {
                    for (Action1 postAction : postActions) {
                        try {
                            postAction.action(saveMapList);
                        } catch (Exception e) {
                            log.error(e);
                        }
                    }
                }
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        }
    }
}