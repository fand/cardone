package top.cardone.data.service.impl;

import lombok.NonNull;
import top.cardone.context.ApplicationContextHolder;
import top.cardone.core.util.func.Func2;
import top.cardone.data.dao.SimpleDao;
import top.cardone.data.service.SimpleService;

import java.util.List;

/**
 * @author yao hai tao
 * @date 2015/8/24
 */
public class SimpleServiceImpl<Dao extends SimpleDao> extends CrudServiceImpl<Dao> implements SimpleService {
    @Override
    public <P> List<P> findList(Class<P> mappedClass, Object findList) {
        return this.dao.findList(mappedClass, findList);
    }

    @Override
    public <P> List<P> findListByFuncId(Class<P> mappedClass, String funcId, Object findList) {
        return (List<P>) ApplicationContextHolder.getBean(Func2.class, funcId).func(mappedClass, findList);
    }

    @Override
    public <P> P findOne(Class<P> mappedClass, Object findOne) {
        return this.dao.findOne(mappedClass, findOne);
    }

    @Override
    public <P> P findOneByFuncId(Class<P> mappedClass, String funcId, Object findOne) {
        return (P) ApplicationContextHolder.getBean(Func2.class, funcId).func(mappedClass, findOne);
    }

    @Override
    public <R> List<R> readList(@NonNull Class<R> requiredType, Object readList) {
        return this.dao.readList(requiredType, readList);
    }

    @Override
    public <R> List<R> readListByFuncId(@NonNull Class<R> requiredType, String funcId, Object readList) {
        return (List<R>) ApplicationContextHolder.getBean(Func2.class, funcId).func(requiredType, readList);
    }

    @Override
    public <R> R readOne(@NonNull Class<R> requiredType, Object readOne) {
        return this.dao.readOne(requiredType, readOne);
    }

    @Override
    public <R> R readOneByFuncId(@NonNull Class<R> requiredType, String funcId, Object readOne) {
        return (R) ApplicationContextHolder.getBean(Func2.class, funcId).func(requiredType, readOne);
    }

    @Override
    public <P> List<P> findListBySqlFileName(Class<P> mappedClass, String sqlFileName, Object findList) {
        return this.dao.findListBySqlFileName(mappedClass, sqlFileName, findList);
    }

    @Override
    public <P> P findOneBySqlFileName(Class<P> mappedClass, String sqlFileName, Object findOne) {
        return this.dao.findOneBySqlFileName(mappedClass, sqlFileName, findOne);
    }

    @Override
    public <R> List<R> readListBySqlFileName(Class<R> requiredType, String sqlFileName, Object readList) {
        return this.dao.readListBySqlFileName(requiredType, sqlFileName, readList);
    }

    @Override
    public <R> R readOneBySqlFileName(Class<R> requiredType, String sqlFileName, Object readOne) {
        return this.dao.readOneBySqlFileName(requiredType, sqlFileName, readOne);
    }
}
