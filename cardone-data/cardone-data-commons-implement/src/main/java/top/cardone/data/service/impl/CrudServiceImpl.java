package top.cardone.data.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Transactional;
import top.cardone.context.ApplicationContextHolder;
import top.cardone.core.util.action.Action1;
import top.cardone.core.util.func.Func1;
import top.cardone.data.dao.CrudDao;
import top.cardone.data.service.CrudService;

import java.util.List;
import java.util.Map;

/**
 * @author yao hai tao
 * @date 2015/8/24
 */
public class CrudServiceImpl<Dao extends CrudDao> implements CrudService {
    @lombok.Setter
    @lombok.Getter
    @Autowired(required = false)
    protected Dao dao;

    @Override
    @Transactional
    public int delete(Object delete) {
        return this.dao.delete(delete);
    }

    @Override
    @Transactional
    public int deleteByFuncId(String funcId, Object delete) {
        return (int) ApplicationContextHolder.getBean(Func1.class, funcId).func(delete);
    }

    @Override
    @Transactional
    public int deleteAll() {
        return this.dao.deleteAll();
    }

    @Override
    @Transactional
    public int deleteByIds(Object ids) {
        return this.dao.updateBySqlFileName("deleteByIds", ids);
    }

    @Override
    @Transactional
    public int[] deleteList(List<Object> deleteList) {
        return this.dao.deleteList(deleteList);
    }

    @Override
    @Transactional
    public int[] deleteListByFuncId(String funcId, List<Object> deleteList) {
        return (int[]) ApplicationContextHolder.getBean(Func1.class, funcId).func(deleteList);
    }

    @Override
    public List<Map<String, Object>> findList(Object findList) {
        return this.dao.findList(findList);
    }

    @Override
    public List<Map<String, Object>> findListByFuncId(String funcId, Object findList) {
        return (List<Map<String, Object>>) ApplicationContextHolder.getBean(Func1.class, funcId).func(findList);
    }

    @Override
    public Map<String, Object> findOne(Object findOne) {
        return this.dao.findOne(findOne);
    }

    @Override
    public Map<String, Object> findOneByFuncId(String funcId, Object findOne) {
        return (Map<String, Object>) ApplicationContextHolder.getBean(Func1.class, funcId).func(findOne);
    }

    @Override
    @Transactional
    public int insert(Object insert) {
        return this.dao.insert(insert);
    }

    @Override
    @Transactional
    public int insertByFuncId(String funcId, Object insert) {
        return (int) ApplicationContextHolder.getBean(Func1.class, funcId).func(insert);
    }

    @Override
    @Transactional
    public int insertByNotExists(Object insert) {
        return this.dao.insertByNotExists(insert);
    }

    @Override
    @Transactional
    public int[] insertList(List<Object> insertList) {
        return this.dao.insertList(insertList);
    }

    @Override
    @Transactional
    public int[] insertListByFuncId(String funcId, List<Object> insertList) {
        return (int[]) ApplicationContextHolder.getBean(Func1.class, funcId).func(insertList);
    }

    @Override
    @Transactional
    public int[] insertListByNotExists(List<Object> insertList) {
        return this.dao.insertListByNotExists(insertList);
    }

    @Override
    public List<Object> readList(Object readList) {
        return this.dao.readList(readList);
    }

    @Override
    public List<Object> readListByFuncId(String funcId, Object readList) {
        return (List<Object>) ApplicationContextHolder.getBean(Func1.class, funcId).func(readList);
    }

    @Override
    public Object readOne(Object readOne) {
        return this.dao.readOne(readOne);
    }

    @Override
    public Object readOneByFuncId(String funcId, Object readOne) {
        return ApplicationContextHolder.getBean(Func1.class, funcId).func(readOne);
    }

    @Override
    @Transactional
    public int save(Object save) {
        return this.dao.save(save);
    }

    @Override
    @Transactional
    public int saveByFuncId(String funcId, Object save) {
        return (int) ApplicationContextHolder.getBean(Func1.class, funcId).func(save);
    }

    @Override
    @Transactional
    public int update(Object update) {
        return this.dao.update(update);
    }

    @Override
    @Transactional
    public int updateByFuncId(String funcId, Object update) {
        return (int) ApplicationContextHolder.getBean(Func1.class, funcId).func(update);
    }

    @Override
    @Transactional
    public int[] updateList(List<Object> updateList) {
        return this.dao.updateList(updateList);
    }

    @Override
    @Transactional
    public int[] updateListByFuncId(String funcId, List<Object> updateList) {
        return (int[]) ApplicationContextHolder.getBean(Func1.class, funcId).func(updateList);
    }

    @Override
    @Transactional
    public int[] saveList(List<Object> saveList) {
        return this.dao.saveList(saveList);
    }

    @Override
    @Transactional
    public int[] saveListByFuncId(String funcId, List<Object> saveList) {
        return (int[]) ApplicationContextHolder.getBean(Func1.class, funcId).func(saveList);
    }

    @Override
    public List<Map<String, Object>> findListByKeyword(Map<String, Object> findList) {
        return this.dao.findListBySqlFileName("findListByKeyword", findList);
    }

    @Override
    @Transactional
    public void execute(String sql) {
        this.dao.execute(sql);
    }

    @Override
    @Transactional
    public int[] batchUpdate(String... sql) {
        return this.dao.batchUpdate(sql);
    }

    @Override
    @Transactional
    public int insertOnConflict(Object insert) {
        return this.dao.insertOnConflict(insert);
    }

    @Override
    @Transactional
    public int[] insertListOnConflict(List<Object> insertList) {
        return this.dao.insertListOnConflict(insertList);
    }

    @Override
    @Transactional
    public int saveOnConflict(Object save) {
        return this.dao.saveOnConflict(save);
    }

    @Override
    @Transactional
    public int[] saveListOnConflict(List<Object> saveList) {
        return this.dao.saveListOnConflict(saveList);
    }

    @Override
    public int executeQueryByFuncId(String funcId, Object param) {
        return this.dao.executeQueryByFuncId(funcId, param);
    }

    @Override
    public int deleteBySqlFileName(String sqlFileName, Object delete) {
        return this.dao.deleteBySqlFileName(sqlFileName, delete);
    }

    @Override
    public List<Map<String, Object>> findListBySqlFileName(String sqlFileName, Object findList) {
        return this.dao.findListBySqlFileName(sqlFileName, findList);
    }

    @Override
    public Map<String, Object> findOneBySqlFileName(String sqlFileName, Object findOne) {
        return this.dao.findOneBySqlFileName(sqlFileName, findOne);
    }

    @Override
    public int executeQueryBySqlFileName(String sqlFileName, Object param, Action1<Map<String, Object>> action) throws DataAccessException {
        return this.dao.executeQueryBySqlFileName(sqlFileName, param, action);
    }

    @Override
    public <T> int executeQueryBySqlFileName(String sqlFileName, Object param, Class<T> elementType, Action1<T> action) throws DataAccessException {
        return this.dao.executeQueryBySqlFileName(sqlFileName, param, elementType, action);
    }

    @Override
    public List<Object> readListBySqlFileName(String sqlFileName, Object readList) {
        return this.dao.readListBySqlFileName(sqlFileName, readList);
    }

    @Override
    public Object readOneBySqlFileName(String sqlFileName, Object readOne) {
        return this.dao.readOneBySqlFileName(sqlFileName, readOne);
    }

    @Override
    @Transactional
    public int saveBySqlFileName(String updateSqlFileName, String insertSqlFileName, Object save) {
        return this.dao.saveBySqlFileName(updateSqlFileName, insertSqlFileName, save);
    }

    @Override
    public int updateBySqlFileName(String sqlFileName, Object update) {
        return this.dao.updateBySqlFileName(sqlFileName, update);
    }

    @Override
    @Transactional
    public int[] updateListBySqlFileName(String sqlFileName, List<Map<String, Object>> updateList) {
        return this.dao.updateListBySqlFileName(sqlFileName, updateList);
    }
}
