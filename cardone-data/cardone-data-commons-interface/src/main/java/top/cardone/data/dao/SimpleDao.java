package top.cardone.data.dao;

import lombok.NonNull;
import top.cardone.core.util.func.Func3;

import java.util.List;
import java.util.Map;

/**
 * @author yao hai tao
 * @date on 2015/8/21
 */
public interface SimpleDao extends CrudDao {
    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param findList    等于那些属性
     * @return 对象集合
     */
    <P> List<P> findList(Class<P> mappedClass, Object findList);

    <P> List<P> findListByFunc(Class<P> mappedClass, Func3 func, Object findList);

    <P> List<P> findListByFuncId(Class<P> mappedClass, String funcId, Object findList);

    <P> List<P> findList(Class<P> mappedClass, String sqlFilePath, Map<String, ?> findList);

    <P> List<P> findListBySqlFileName(Class<P> mappedClass, String sqlFileName, Object findList);

    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param findOne     等于那些属性
     * @return 返回对象
     */
    <P> P findOne(Class<P> mappedClass, Object findOne);

    <P> P findOneByFunc(Class<P> mappedClass, Func3 func, Object findOne);

    <P> P findOneByFuncId(Class<P> mappedClass, String funcId, Object findOne);

    <P> P findOne(Class<P> mappedClass, String sqlFilePath, Map<String, ?> findOne);

    <P> P findOneBySqlFileName(Class<P> mappedClass, String sqlFileName, Object findOne);

    /**
     * 查询
     *
     * @return 返回数据
     */
    <R> List<R> readList(@NonNull Class<R> requiredType, Object readList);

    <R> List<R> readListByFunc(@NonNull Class<R> requiredType, Func3 func, Object readList);

    <R> List<R> readListByFuncId(@NonNull Class<R> requiredType, String funcId, Object readList);

    <R> List<R> readList(@NonNull Class<R> requiredType, String sqlFilePath, Map<String, ?> readList);

    <R> List<R> readListBySqlFileName(@NonNull Class<R> requiredType, String sqlFileName, Object readList);

    /**
     * 查询
     *
     * @return 返回数据
     */
    <R> R readOne(@NonNull Class<R> requiredType, Object readOne);

    <R> R readOneByFunc(@NonNull Class<R> requiredType, Func3 func, Object readOne);

    <R> R readOneByFuncId(@NonNull Class<R> requiredType, String funcId, Object readOne);

    <R> R readOne(@NonNull Class<R> requiredType, String sqlFilePath, Map<String, ?> readOne);

    <R> R readOneBySqlFileName(@NonNull Class<R> requiredType, String sqlFileName, Object readOne);
}