package top.cardone.data.dao;

import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Transactional;
import top.cardone.core.util.action.Action1;
import top.cardone.core.util.func.Func2;

import java.util.List;
import java.util.Map;

/**
 * @author yao hai tao
 * @date 2015/8/21
 */
public interface CrudDao {
    /**
     * 删除
     *
     * @param delete
     * @return 影响行数
     */
    int delete(Object delete);

    int deleteByFunc(Func2 func, Object delete);

    int deleteByFuncId(String funcId, Object delete);

    int deleteBySqlFileName(String sqlFileName, Object delete);

    /**
     * 删除
     *
     * @return 影响行数
     */
    int deleteAll();

    /**
     * 删除
     *
     * @param deleteList
     * @return 影响行数
     */
    int[] deleteList(List<Object> deleteList);

    int[] deleteListBySqlFileName(String sqlFileName, List<Object> deleteList);

    int[] deleteListByFunc(Func2 func, List<Object> deleteList);

    int[] deleteListByFuncId(String funcId, List<Object> deleteList);

    /**
     * 查询
     *
     * @param findList 等于那些属性
     * @return 对象集合
     */
    List<Map<String, Object>> findList(Object findList);

    List<Map<String, Object>> findListByFunc(Func2 func, Object findList);

    List<Map<String, Object>> findListByFuncId(String funcId, Object findList);

    List<Map<String, Object>> findList(String sqlFilePath);

    List<Map<String, Object>> findList(String sqlFilePath, Map<String, ?> findList);

    List<Map<String, Object>> findListBySqlFileName(String sqlFileName, Object findList);

    /**
     * 查询
     *
     * @param findOne 等于那些属性
     * @return 返回对象
     */
    Map<String, Object> findOne(Object findOne);

    Map<String, Object> findOneByFunc(Func2 func, Object findOne);

    Map<String, Object> findOneByFuncId(String funcId, Object findOne);

    Map<String, Object> findOne(String sqlFilePath, Map<String, ?> findOne);

    Map<String, Object> findOneBySqlFileName(String sqlFileName, Object findOne);

    String[] readListCodeForConfigTable(Map<String, Object> objectMa);

    String getNamedParameterJdbcOperationsBeanId();

    String getParseBeanId();

    String[] readListPkForConfigTable(Map<String, Object> objectMa);

    String[] readListWhereForConfigTable(Map<String, Object> objectMap);

    /**
     * 插入
     *
     * @param insert 对象
     * @return 影响行数
     */
    int insert(Object insert);

    int insertByFunc(Func2 func, Object insert);

    int insertByFuncId(String funcId, Object insert);

    /**
     * 插入
     *
     * @param insert 对象
     * @return 影响行数
     */
    int insertByNotExists(Object insert);

    /**
     * 插入
     *
     * @param insert 对象
     * @return 影响行数
     */
    int insertOnConflict(Object insert);

    /**
     * 保存
     *
     * @param save 对象
     * @return 影响行数
     */
    int saveOnConflict(Object save);

    int executeQueryBySqlFileName(String sqlFileName, Object param, Action1<Map<String, Object>> action) throws DataAccessException;

    <T> int executeQueryBySqlFileName(String sqlFileName, Object param, Class<T> elementType, Action1<T> action) throws DataAccessException;

    int executeQueryByFunc(Func2 func, Object param);

    int executeQueryByFuncId(String funcId, Object param);

    /**
     * 插入
     *
     * @param insertList 对象
     * @return 影响行数
     */
    int[] insertList(List<Object> insertList);

    int[] insertListByFunc(Func2 func, List<Object> insertList);

    int[] insertListByFuncId(String funcId, List<Object> insertList);

    /**
     * 插入
     *
     * @param insertList 对象
     * @return 影响行数
     */
    int[] insertListOnConflict(List<Object> insertList);

    /**
     * 插入
     *
     * @param insertList 对象
     * @return 影响行数
     */
    int[] insertListByNotExists(List<Object> insertList);

    /**
     * 查询
     *
     * @param readList
     * @return 返回数据
     */
    List<Object> readList(Object readList);

    List<Object> readListByFunc(Func2 func, Object readList);

    List<Object> readListByFuncId(String funcId, Object readList);

    List<Object> readList(String sqlFilePath, Map<String, ?> readList);

    List<Object> readListBySqlFileName(String sqlFileName, Object readList);

    /**
     * 查询
     *
     * @param readOne
     * @return 返回数据
     */
    Object readOne(Object readOne);

    Object readOneByFunc(Func2 func, Object readOne);

    Object readOneByFuncId(String funcId, Object readOne);

    Object readOne(String sqlFilePath, Map<String, ?> readOne);

    Object readOneBySqlFileName(String sqlFileName, Object readOne);

    /**
     * 保存
     *
     * @param save 保存对象
     * @return 插入后数据库值
     */
    int save(Object save);

    int saveByFunc(Func2 func, Object save);

    int saveByFuncId(String funcId, Object save);

    int save(String updateSqlFilePath, String insertSqlFilePath, Object save);

    int saveBySqlFileName(String updateSqlFileName, String insertSqlFileName, Object save);

    /**
     * 更新
     *
     * @param update 对象
     * @return 影响行数
     */
    int update(Object update);

    int updateByFunc(Func2 func, Object update);

    int updateByFuncId(String funcId, Object update);

    int update(String sqlFilePath, Map<String, ?> update);

    int updateBySqlFileName(String sqlFileName, Object update);

    /**
     * 更新
     *
     * @param updateList 对象
     * @return 影响行数
     */
    int[] updateList(List<Object> updateList);

    int[] updateListByFunc(Func2 func, List<Object> updateList);

    int[] updateListByFuncId(String funcId, List<Object> updateList);

    /**
     * 保存
     *
     * @param saveList 对象
     * @return 影响行数
     */
    int[] saveListOnConflict(List<Object> saveList);

    /**
     * 保存
     *
     * @param saveList 对象
     * @return 影响行数
     */
    int[] saveList(List<Object> saveList);

    int[] saveListByFunc(Func2 func, List<Object> saveList);

    int[] saveListByFuncId(String funcId, List<Object> saveList);

    int[] updateList(String sqlFilePath, List<Map<String, Object>> updateList);

    int[] updateListBySqlFileName(String sqlFileName, List<Map<String, Object>> updateList);

    void execute(String sql);

    /**
     * 执行sql
     *
     * @param sql
     */
    int[] batchUpdate(String... sql);
}