package top.cardone.data.dao;

import org.springframework.data.domain.Page;
import top.cardone.core.util.func.Func2;
import top.cardone.core.util.func.Func3;

import java.util.Map;

/**
 * @author yao hai tao
 * @date 2015/8/21
 */
public interface PageDao extends SimpleDao {
    Page<Map<String, Object>> pageByFunc(Func2 func, Object page);

    Page<Map<String, Object>> pageByFuncId(String funcId, Object page);

    Page<Map<String, Object>> page(String countSqlFilePath, String findListSqlFilePath, Map<String, ?> page);

    Page<Map<String, Object>> pageBySqlFileName(String countSqlFileName, String findListSqlFileName, Object page);

    <P> Page<P> pageByFunc(Class<P> mappedClass, Func3 func, Object page);

    <P> Page<P> pageByFuncId(Class<P> mappedClass, String funcId, Object page);

    <P> Page<P> page(Class<P> mappedClass, String countSqlFilePath, String findListSqlFilePath, Map<String, ?> page);

    <P> Page<P> pageBySqlFileName(Class<P> mappedClass, String countSqlFileName, String findListSqlFileName, Object page);
}