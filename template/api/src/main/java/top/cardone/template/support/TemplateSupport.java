package top.cardone.template.support;

/**
 * 模板支持
 *
 * @author yao hai tao
 */
public interface TemplateSupport {
    /**
     * 默认解析模板
     *
     * @param filePath 文件路径
     * @param model    数据
     * @return 解析后字符
     */
    String process(final String filePath, final Object model);

    /**
     * 默认解析模板
     *
     * @param configurationKey 配置键名
     * @param filePath         文件路径
     * @param model            数据
     * @return 解析后字符
     */
    String process(String configurationKey, final String filePath, final Object model);

    /**
     * 获取模板字符串
     *
     * @param configurationKey 配置键名
     * @param filePath         文件路径
     * @return 模板字符串
     */
    String toString(String configurationKey, final String filePath);

    /**
     * 获取模板字符串
     *
     * @param filePath 文件路径
     * @return 模板字符串
     */
    String toString(String filePath);

    /**
     * 解析模板
     *
     * @param templateString 模板字符
     * @param model          数据
     * @return 解析后字符
     */
    String processString(final String templateString, final Object model);

    /**
     * 解析模板
     *
     * @param configurationKey 配置键名
     * @param templateString   模板字符
     * @param model            数据
     * @return 解析后字符
     */
    String processString(String configurationKey, final String templateString, final Object model);
}