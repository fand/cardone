package top.cardone.mapper;

import java.util.Map;

/**
 * @author yao hai tao
 * @date 2016/10/2
 */
public interface BeanMapper {
    <T> T getObjectByFilePathCache(Class<T> cls, String filePath);

    <T> T getObjectByFilePath(Class<T> cls, String filePath);

    <T> T getObject(Class<T> cls, String jsonString, Map<String, Object> map);

    <T> T getObject(Class<T> cls, Object data);

    <T> T getObject(Class<T> cls, Map<String, Object> map, Map<String, Object> mapperMap);
}