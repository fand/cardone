/*==============================================================*/
/* DBMS name:      PostgreSQL 9.x                               */
/* Created on:     2017/3/23 11:29:37                           */
/*==============================================================*/


drop table t_work_task;

drop table t_work_task_business_type;

drop table t_work_task_media_resource;

drop table t_work_task_read;

drop table t_wt_terminal_use_record;

/*==============================================================*/
/* Table: t_work_task                                           */
/*==============================================================*/
create table t_work_task (
   WORK_TASK_ID         VARCHAR(255)         not null,
   WORK_TASK_BUSINESS_TYPE_CODE VARCHAR(255)         null default NULL,
   ORG_CODE             VARCHAR(255)         null default NULL,
   DEPARTMENT_CODE      VARCHAR(255)         null default NULL,
   BEGIN_DATE           DATE                 null default NULL,
   END_DATE             DATE                 null default NULL,
   CREATED_BY_CODE      VARCHAR(255)         null default NULL,
   CREATED_DATE         DATE                 null default NULL,
   LAST_MODIFIED_BY_CODE VARCHAR(255)         null default NULL,
   LAST_MODIFIED_DATE   DATE                 null default NULL,
   STATE_CODE           VARCHAR(255)         null default NULL,
   DATA_STATE_CODE      VARCHAR(255)         null default NULL,
   VERSION_             INT4                 null default 0,
   WF_ID                VARCHAR(255)         null default NULL,
   ROLE_CODES           VARCHAR(1023)        null default NULL,
   PERMISSION_CODES     VARCHAR(1023)        null default NULL,
   SYSTEM_INFO_CODE     VARCHAR(255)         null default NULL,
   SITE_CODE            VARCHAR(255)         null default NULL,
   WORK_TASK_NO         VARCHAR(255)         not null,
   OFFICER_NO           VARCHAR(255)         null default NULL,
   TIME_                DATE                 null default NULL,
   PLACE                VARCHAR(255)         null default NULL,
   EVENT_CODE           VARCHAR(255)         null default NULL,
   BATCH_NO             VARCHAR(255)         null default NULL,
   LE_OBJECT_NO         VARCHAR(1023)        null default NULL,
   LE_OBJECT_NAME       VARCHAR(1023)        null default NULL,
   IMPORTANCE_CODE      VARCHAR(255)         null default NULL,
   SUGGEST_STORAGE_TERM INT2                 null default NULL,
   RS_EVENT_NO          VARCHAR(255)         null default NULL,
   RS_EVENT             VARCHAR(1023)        null default NULL,
   RELATION_POLICE_CODE VARCHAR(1023)        null default NULL,
   RELATION_POLICE_NAME VARCHAR(1023)        null default NULL,
   RELATION_POLICE      VARCHAR(1023)        null default NULL,
   REMARK               VARCHAR(1023)        null default NULL,
   COMMENT              VARCHAR(1023)        null default NULL,
   CONTENT              VARCHAR(1023)        null default NULL,
   RESOURCE_SUM         INT4                 null default 0,
   constraint PK_T_WORK_TASK primary key (WORK_TASK_ID)
);

comment on table t_work_task is
't_work_task';

comment on column t_work_task.WORK_TASK_ID is
'工作单标识';

comment on column t_work_task.WORK_TASK_BUSINESS_TYPE_CODE is
'工作单业务类型编号';

comment on column t_work_task.ORG_CODE is
'组织代码';

comment on column t_work_task.DEPARTMENT_CODE is
'部门代码';

comment on column t_work_task.BEGIN_DATE is
'开始时间';

comment on column t_work_task.END_DATE is
'结束时间';

comment on column t_work_task.CREATED_BY_CODE is
'创建人代码';

comment on column t_work_task.CREATED_DATE is
'创建时间';

comment on column t_work_task.LAST_MODIFIED_BY_CODE is
'最后修改人代码';

comment on column t_work_task.LAST_MODIFIED_DATE is
'最后修改时间';

comment on column t_work_task.STATE_CODE is
'状态代码';

comment on column t_work_task.DATA_STATE_CODE is
'数据状态代码';

comment on column t_work_task.VERSION_ is
'版本';

comment on column t_work_task.WF_ID is
'工作流标识';

comment on column t_work_task.ROLE_CODES is
'角色代码集合';

comment on column t_work_task.PERMISSION_CODES is
'许可代码集合';

comment on column t_work_task.SYSTEM_INFO_CODE is
'系统信息代码';

comment on column t_work_task.SITE_CODE is
'站点代码';

comment on column t_work_task.WORK_TASK_NO is
'工作单号';

comment on column t_work_task.OFFICER_NO is
'警员号';

comment on column t_work_task.TIME_ is
'时间';

comment on column t_work_task.PLACE is
'地点';

comment on column t_work_task.EVENT_CODE is
'事件编号';

comment on column t_work_task.BATCH_NO is
'批次号';

comment on column t_work_task.LE_OBJECT_NO is
'执法对象号';

comment on column t_work_task.LE_OBJECT_NAME is
'执法对象名称';

comment on column t_work_task.IMPORTANCE_CODE is
'重要程度';

comment on column t_work_task.SUGGEST_STORAGE_TERM is
'建议保存期限';

comment on column t_work_task.RS_EVENT_NO is
'关联源事件号';

comment on column t_work_task.RS_EVENT is
'关联源事件';

comment on column t_work_task.RELATION_POLICE_CODE is
'关联警员编号';

comment on column t_work_task.RELATION_POLICE_NAME is
'关联警员姓名';

comment on column t_work_task.RELATION_POLICE is
'关联警员';

comment on column t_work_task.REMARK is
'说明';

comment on column t_work_task.COMMENT is
'批注';

comment on column t_work_task.CONTENT is
'内容';

comment on column t_work_task.RESOURCE_SUM is
'资源总数';

/*==============================================================*/
/* Table: t_work_task_business_type                             */
/*==============================================================*/
create table t_work_task_business_type (
   WORK_TASK_BUSINESS_TYPE_ID VARCHAR(255)         not null,
   ORG_CODE             VARCHAR(255)         null default NULL,
   DEPARTMENT_CODE      VARCHAR(255)         null default NULL,
   BEGIN_DATE           DATE                 null default NULL,
   END_DATE             DATE                 null default NULL,
   CREATED_BY_CODE      VARCHAR(255)         null default NULL,
   CREATED_DATE         DATE                 null default NULL,
   LAST_MODIFIED_BY_CODE VARCHAR(255)         null default NULL,
   LAST_MODIFIED_DATE   DATE                 null default NULL,
   STATE_CODE           VARCHAR(255)         null default NULL,
   DATA_STATE_CODE      VARCHAR(255)         null default NULL,
   VERSION_             INT4                 null default 0,
   WF_ID                VARCHAR(255)         null default NULL,
   ROLE_CODES           VARCHAR(1023)        null default NULL,
   PERMISSION_CODES     VARCHAR(1023)        null default NULL,
   SYSTEM_INFO_CODE     VARCHAR(255)         null default NULL,
   SITE_CODE            VARCHAR(255)         null default NULL,
   WORK_TASK_BUSINESS_TYPE_CODE VARCHAR(255)         not null,
   VALID_TIME           INT2                 null default NULL,
   BIND_TOLERANCE_TIME  INT2                 null default NULL,
   NAME                 VARCHAR(255)         null default NULL,
   REMARK               VARCHAR(1023)        null default NULL,
   constraint PK_T_WORK_TASK_BUSINESS_TYPE primary key (WORK_TASK_BUSINESS_TYPE_ID)
);

comment on table t_work_task_business_type is
't_work_task_business_type';

comment on column t_work_task_business_type.WORK_TASK_BUSINESS_TYPE_ID is
'工作单业务类型标识';

comment on column t_work_task_business_type.ORG_CODE is
'组织代码';

comment on column t_work_task_business_type.DEPARTMENT_CODE is
'部门代码';

comment on column t_work_task_business_type.BEGIN_DATE is
'开始时间';

comment on column t_work_task_business_type.END_DATE is
'结束时间';

comment on column t_work_task_business_type.CREATED_BY_CODE is
'创建人代码';

comment on column t_work_task_business_type.CREATED_DATE is
'创建时间';

comment on column t_work_task_business_type.LAST_MODIFIED_BY_CODE is
'最后修改人代码';

comment on column t_work_task_business_type.LAST_MODIFIED_DATE is
'最后修改时间';

comment on column t_work_task_business_type.STATE_CODE is
'状态代码';

comment on column t_work_task_business_type.DATA_STATE_CODE is
'数据状态代码';

comment on column t_work_task_business_type.VERSION_ is
'版本';

comment on column t_work_task_business_type.WF_ID is
'工作流标识';

comment on column t_work_task_business_type.ROLE_CODES is
'角色代码集合';

comment on column t_work_task_business_type.PERMISSION_CODES is
'许可代码集合';

comment on column t_work_task_business_type.SYSTEM_INFO_CODE is
'系统信息代码';

comment on column t_work_task_business_type.SITE_CODE is
'站点代码';

comment on column t_work_task_business_type.WORK_TASK_BUSINESS_TYPE_CODE is
'工作单业务类型编号';

comment on column t_work_task_business_type.VALID_TIME is
'有效时间(天)';

comment on column t_work_task_business_type.BIND_TOLERANCE_TIME is
'绑定容差时间';

comment on column t_work_task_business_type.NAME is
'名称';

comment on column t_work_task_business_type.REMARK is
'说明';

/*==============================================================*/
/* Table: t_work_task_media_resource                            */
/*==============================================================*/
create table t_work_task_media_resource (
   WORK_TASK_NO         VARCHAR(255)         not null,
   WORK_TASK_BUSINESS_TYPE_CODE VARCHAR(255)         not null,
   MEDIA_RESOURCE_ID    VARCHAR(255)         not null,
   ORG_CODE             VARCHAR(255)         null default NULL,
   DEPARTMENT_CODE      VARCHAR(255)         null default NULL,
   BEGIN_DATE           DATE                 null default NULL,
   END_DATE             DATE                 null default NULL,
   CREATED_BY_CODE      VARCHAR(255)         null default NULL,
   CREATED_DATE         DATE                 null default NULL,
   LAST_MODIFIED_BY_CODE VARCHAR(255)         null default NULL,
   LAST_MODIFIED_DATE   DATE                 null default NULL,
   STATE_CODE           VARCHAR(255)         null default NULL,
   DATA_STATE_CODE      VARCHAR(255)         null default NULL,
   VERSION_             INT4                 null default 0,
   WF_ID                VARCHAR(255)         null default NULL,
   ROLE_CODES           VARCHAR(1023)        null default NULL,
   PERMISSION_CODES     VARCHAR(1023)        null default NULL,
   SYSTEM_INFO_CODE     VARCHAR(255)         null default NULL,
   SITE_CODE            VARCHAR(255)         null default NULL,
   constraint PK_T_WORK_TASK_MEDIA_RESOURCE primary key (WORK_TASK_NO, WORK_TASK_BUSINESS_TYPE_CODE, MEDIA_RESOURCE_ID)
);

comment on table t_work_task_media_resource is
't_work_task_media_resource';

comment on column t_work_task_media_resource.WORK_TASK_NO is
'工作单号';

comment on column t_work_task_media_resource.WORK_TASK_BUSINESS_TYPE_CODE is
'工作单业务类型编号';

comment on column t_work_task_media_resource.MEDIA_RESOURCE_ID is
'媒体资源标识';

comment on column t_work_task_media_resource.ORG_CODE is
'组织代码';

comment on column t_work_task_media_resource.DEPARTMENT_CODE is
'部门代码';

comment on column t_work_task_media_resource.BEGIN_DATE is
'开始时间';

comment on column t_work_task_media_resource.END_DATE is
'结束时间';

comment on column t_work_task_media_resource.CREATED_BY_CODE is
'创建人代码';

comment on column t_work_task_media_resource.CREATED_DATE is
'创建时间';

comment on column t_work_task_media_resource.LAST_MODIFIED_BY_CODE is
'最后修改人代码';

comment on column t_work_task_media_resource.LAST_MODIFIED_DATE is
'最后修改时间';

comment on column t_work_task_media_resource.STATE_CODE is
'状态代码';

comment on column t_work_task_media_resource.DATA_STATE_CODE is
'数据状态代码';

comment on column t_work_task_media_resource.VERSION_ is
'版本';

comment on column t_work_task_media_resource.WF_ID is
'工作流标识';

comment on column t_work_task_media_resource.ROLE_CODES is
'角色代码集合';

comment on column t_work_task_media_resource.PERMISSION_CODES is
'许可代码集合';

comment on column t_work_task_media_resource.SYSTEM_INFO_CODE is
'系统信息代码';

comment on column t_work_task_media_resource.SITE_CODE is
'站点代码';

/*==============================================================*/
/* Table: t_work_task_read                                      */
/*==============================================================*/
create table t_work_task_read (
   WORK_TASK_ID         VARCHAR(255)         not null,
   WORK_TASK_BUSINESS_TYPE_CODE VARCHAR(255)         null default NULL,
   ORG_CODE             VARCHAR(255)         null default NULL,
   DEPARTMENT_CODE      VARCHAR(255)         null default NULL,
   BEGIN_DATE           DATE                 not null default '0000-00-00 00:00:00',
   END_DATE             DATE                 null default NULL,
   CREATED_BY_CODE      VARCHAR(255)         null default NULL,
   CREATED_DATE         DATE                 null default NULL,
   LAST_MODIFIED_BY_CODE VARCHAR(255)         null default NULL,
   LAST_MODIFIED_DATE   DATE                 null default NULL,
   STATE_CODE           VARCHAR(255)         null default NULL,
   DATA_STATE_CODE      VARCHAR(255)         null default NULL,
   VERSION_             INT4                 null default 0,
   WF_ID                VARCHAR(255)         null default NULL,
   ROLE_CODES           VARCHAR(1023)        null default NULL,
   PERMISSION_CODES     VARCHAR(1023)        null default NULL,
   SYSTEM_INFO_CODE     VARCHAR(255)         null default NULL,
   SITE_CODE            VARCHAR(255)         null default NULL,
   WORK_TASK_NO         VARCHAR(255)         not null,
   OFFICER_NO           VARCHAR(255)         null default NULL,
   TIME_                DATE                 null default NULL,
   PLACE                VARCHAR(255)         null default NULL,
   EVENT_CODE           VARCHAR(255)         null default NULL,
   BATCH_NO             VARCHAR(255)         null default NULL,
   LE_OBJECT_NO         VARCHAR(255)         null default NULL,
   LE_OBJECT_NAME       VARCHAR(255)         null default NULL,
   IMPORTANCE_CODE      VARCHAR(255)         null default NULL,
   SUGGEST_STORAGE_TERM INT2                 null default NULL,
   RS_EVENT_NO          VARCHAR(255)         null default NULL,
   RS_EVENT             VARCHAR(1023)        null default NULL,
   RELATION_POLICE_CODE VARCHAR(1023)        null default NULL,
   RELATION_POLICE_NAME VARCHAR(1023)        null default NULL,
   RELATION_POLICE      VARCHAR(1023)        null default NULL,
   REMARK               VARCHAR(1023)        null default NULL,
   COMMENT              VARCHAR(1023)        null default NULL,
   CONTENT              VARCHAR(1023)        null default NULL,
   RESOURCE_SUM         INT4                 null default 0,
   constraint PK_T_WORK_TASK_READ primary key (WORK_TASK_ID, BEGIN_DATE)
);

comment on table t_work_task_read is
't_work_task_read';

comment on column t_work_task_read.WORK_TASK_ID is
'工作单标识';

comment on column t_work_task_read.WORK_TASK_BUSINESS_TYPE_CODE is
'工作单业务类型编号';

comment on column t_work_task_read.ORG_CODE is
'组织代码';

comment on column t_work_task_read.DEPARTMENT_CODE is
'部门代码';

comment on column t_work_task_read.BEGIN_DATE is
'开始时间';

comment on column t_work_task_read.END_DATE is
'结束时间';

comment on column t_work_task_read.CREATED_BY_CODE is
'创建人代码';

comment on column t_work_task_read.CREATED_DATE is
'创建时间';

comment on column t_work_task_read.LAST_MODIFIED_BY_CODE is
'最后修改人代码';

comment on column t_work_task_read.LAST_MODIFIED_DATE is
'最后修改时间';

comment on column t_work_task_read.STATE_CODE is
'状态代码';

comment on column t_work_task_read.DATA_STATE_CODE is
'数据状态代码';

comment on column t_work_task_read.VERSION_ is
'版本';

comment on column t_work_task_read.WF_ID is
'工作流标识';

comment on column t_work_task_read.ROLE_CODES is
'角色代码集合';

comment on column t_work_task_read.PERMISSION_CODES is
'许可代码集合';

comment on column t_work_task_read.SYSTEM_INFO_CODE is
'系统信息代码';

comment on column t_work_task_read.SITE_CODE is
'站点代码';

comment on column t_work_task_read.WORK_TASK_NO is
'工作单号';

comment on column t_work_task_read.OFFICER_NO is
'警员号';

comment on column t_work_task_read.TIME_ is
'时间';

comment on column t_work_task_read.PLACE is
'地点';

comment on column t_work_task_read.EVENT_CODE is
'事件编号';

comment on column t_work_task_read.BATCH_NO is
'批次号';

comment on column t_work_task_read.LE_OBJECT_NO is
'执法对象号';

comment on column t_work_task_read.LE_OBJECT_NAME is
'执法对象名称';

comment on column t_work_task_read.IMPORTANCE_CODE is
'重要程度';

comment on column t_work_task_read.SUGGEST_STORAGE_TERM is
'建议保存期限';

comment on column t_work_task_read.RS_EVENT_NO is
'关联源事件号';

comment on column t_work_task_read.RS_EVENT is
'关联源事件';

comment on column t_work_task_read.RELATION_POLICE_CODE is
'关联警员编号';

comment on column t_work_task_read.RELATION_POLICE_NAME is
'关联警员姓名';

comment on column t_work_task_read.RELATION_POLICE is
'关联警员';

comment on column t_work_task_read.REMARK is
'说明';

comment on column t_work_task_read.COMMENT is
'批注';

comment on column t_work_task_read.CONTENT is
'内容';

comment on column t_work_task_read.RESOURCE_SUM is
'资源总数';

/*==============================================================*/
/* Table: t_wt_terminal_use_record                              */
/*==============================================================*/
create table t_wt_terminal_use_record (
   WORK_TASK_NO         VARCHAR(255)         not null,
   TERMINAL_USE_RECORD_ID VARCHAR(255)         not null,
   ORG_CODE             VARCHAR(255)         null default NULL,
   DEPARTMENT_CODE      VARCHAR(255)         null default NULL,
   BEGIN_DATE           DATE                 null default NULL,
   END_DATE             DATE                 null default NULL,
   CREATED_BY_CODE      VARCHAR(255)         null default NULL,
   CREATED_DATE         DATE                 null default NULL,
   LAST_MODIFIED_BY_CODE VARCHAR(255)         null default NULL,
   LAST_MODIFIED_DATE   DATE                 null default NULL,
   STATE_CODE           VARCHAR(255)         null default NULL,
   DATA_STATE_CODE      VARCHAR(255)         null default NULL,
   VERSION_             INT4                 null default 0,
   WF_ID                VARCHAR(255)         null default NULL,
   ROLE_CODES           VARCHAR(1023)        null default NULL,
   PERMISSION_CODES     VARCHAR(1023)        null default NULL,
   SYSTEM_INFO_CODE     VARCHAR(255)         null default NULL,
   SITE_CODE            VARCHAR(255)         null default NULL,
   RELATION_POLICE_CODE VARCHAR(1023)        null default NULL,
   constraint PK_T_WT_TERMINAL_USE_RECORD primary key (WORK_TASK_NO, TERMINAL_USE_RECORD_ID)
);

comment on table t_wt_terminal_use_record is
't_wt_terminal_use_record';

comment on column t_wt_terminal_use_record.WORK_TASK_NO is
'工作单号';

comment on column t_wt_terminal_use_record.TERMINAL_USE_RECORD_ID is
'终端使用记录标识';

comment on column t_wt_terminal_use_record.ORG_CODE is
'组织代码';

comment on column t_wt_terminal_use_record.DEPARTMENT_CODE is
'部门代码';

comment on column t_wt_terminal_use_record.BEGIN_DATE is
'开始时间';

comment on column t_wt_terminal_use_record.END_DATE is
'结束时间';

comment on column t_wt_terminal_use_record.CREATED_BY_CODE is
'创建人代码';

comment on column t_wt_terminal_use_record.CREATED_DATE is
'创建时间';

comment on column t_wt_terminal_use_record.LAST_MODIFIED_BY_CODE is
'最后修改人代码';

comment on column t_wt_terminal_use_record.LAST_MODIFIED_DATE is
'最后修改时间';

comment on column t_wt_terminal_use_record.STATE_CODE is
'状态代码';

comment on column t_wt_terminal_use_record.DATA_STATE_CODE is
'数据状态代码';

comment on column t_wt_terminal_use_record.VERSION_ is
'版本';

comment on column t_wt_terminal_use_record.WF_ID is
'工作流标识';

comment on column t_wt_terminal_use_record.ROLE_CODES is
'角色代码集合';

comment on column t_wt_terminal_use_record.PERMISSION_CODES is
'许可代码集合';

comment on column t_wt_terminal_use_record.SYSTEM_INFO_CODE is
'系统信息代码';

comment on column t_wt_terminal_use_record.SITE_CODE is
'站点代码';

comment on column t_wt_terminal_use_record.RELATION_POLICE_CODE is
'关联警员编号';

