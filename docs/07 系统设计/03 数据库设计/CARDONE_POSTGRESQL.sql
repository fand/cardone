/*==============================================================*/
/* DBMS name:      PostgreSQL 9.x                               */
/* Created on:     2017/8/3 星期四 19:18:18                        */
/*==============================================================*/


drop table c1_area;

drop table c1_article;

drop table c1_city;

drop table c1_department;

drop table c1_dictionary;

drop table c1_dictionary_item;

drop table c1_dictionary_type;

drop table c1_error_info;

drop table c1_i18n_info;

drop table c1_navigation;

drop table c1_notice;

drop table c1_oauth_consumer;

drop table c1_open_user;

drop table c1_operate_log;

drop table c1_org;

drop table c1_permission;

drop table c1_province;

drop table c1_region;

drop table c1_role;

drop table c1_role_permission;

drop table c1_site;

drop table c1_system_info;

drop table c1_token_info;

drop table c1_user;

drop table c1_user_address;

drop table c1_user_department;

drop table c1_user_group;

drop table c1_user_group_permission;

drop table c1_user_group_role;

drop table c1_user_group_user;

drop table c1_user_org;

drop table c1_user_permission;

drop table c1_user_role;

drop table c1_wf_variable;

drop table c1_wf_variable_user;

/*==============================================================*/
/* Table: c1_area                                               */
/*==============================================================*/
create table c1_area (
   area_id              varchar(255)         not null default '',
   city_id              varchar(255)         null default '',
   province_id          varchar(255)         null default '',
   begin_date           date                 null default current_timestamp,
   end_date             date                 null default to_timestamp('9999-01-01 00:00:00', 'yyyy-mm-dd hh24:mi:ss'),
   created_by_id        varchar(255)         null default '',
   created_by_code      varchar(255)         null default '',
   created_date         date                 null default current_timestamp,
   last_modified_by_id  varchar(255)         null default '',
   last_modified_by_code varchar(255)         null default '',
   last_modified_date   date                 null default current_timestamp,
   flag_code            varchar(255)         null default 'init',
   batch_no             varchar(255)         null default '',
   flag_object_code     varchar(255)         null default '',
   state_code           varchar(255)         null default '1',
   data_state_code      varchar(255)         null default '1',
   order_by_            int8                 null default 0,
   json_data            json                 null,
   version_             int4                 null default 1,
   system_info_code     varchar(255)         null default '',
   site_code            varchar(255)         null default '',
   org_code             varchar(255)         null default '',
   department_code      varchar(255)         null default '',
   personal_code        varchar(255)         null default '',
   personal_id          varchar(255)         null default '',
   area_code            varchar(255)         null default '',
   city_code            varchar(255)         not null default '',
   province_code        varchar(255)         not null default '',
   name                 varchar(255)         not null default '',
   country_code         varchar(255)         null default '',
   constraint pk_c1_area primary key (area_id)
);

comment on table c1_area is
'地区';

comment on column c1_area.area_id is
'地区标识';

comment on column c1_area.city_id is
'城市标识';

comment on column c1_area.province_id is
'省份标识';

comment on column c1_area.begin_date is
'开始日期';

comment on column c1_area.end_date is
'结束日期';

comment on column c1_area.created_by_id is
'创建人标识';

comment on column c1_area.created_by_code is
'创建人编号';

comment on column c1_area.created_date is
'创建日期';

comment on column c1_area.last_modified_by_id is
'最后修改人标识';

comment on column c1_area.last_modified_by_code is
'最后修改人编号';

comment on column c1_area.last_modified_date is
'最后修改日期';

comment on column c1_area.flag_code is
'标记编号(数据字典：工作流、同步、生成、录入、审批)';

comment on column c1_area.batch_no is
'批次编号';

comment on column c1_area.flag_object_code is
'标识对象编号(工作流：工作流标识、同步：批次号、生成：操作者编号、录入：操作者编号、审批：操作者编号)';

comment on column c1_area.state_code is
'状态编号(数据字典)';

comment on column c1_area.data_state_code is
'数据状态编号(数据字典)';

comment on column c1_area.order_by_ is
'排序';

comment on column c1_area.json_data is
'json数据';

comment on column c1_area.version_ is
'版本';

comment on column c1_area.system_info_code is
'系统信息编号';

comment on column c1_area.site_code is
'站点编号';

comment on column c1_area.org_code is
'组织编号';

comment on column c1_area.department_code is
'部门编号';

comment on column c1_area.personal_code is
'个人编号';

comment on column c1_area.personal_id is
'个人标识';

comment on column c1_area.area_code is
'地区编号';

comment on column c1_area.city_code is
'城市编号';

comment on column c1_area.province_code is
'省份编号';

comment on column c1_area.name is
'名称';

comment on column c1_area.country_code is
'国家编号(数据字典)';

/*==============================================================*/
/* Table: c1_article                                            */
/*==============================================================*/
create table c1_article (
   article_id           varchar(255)         not null default '',
   check_person_id      varchar(255)         null default '',
   author_person_id     varchar(255)         null default '',
   begin_date           date                 null default current_timestamp,
   end_date             date                 null default to_timestamp('9999-01-01 00:00:00', 'yyyy-mm-dd hh24:mi:ss'),
   created_by_id        varchar(255)         null default '',
   created_by_code      varchar(255)         null default '',
   created_date         date                 null default current_timestamp,
   last_modified_by_id  varchar(255)         null default '',
   last_modified_by_code varchar(255)         null default '',
   last_modified_date   date                 null default current_timestamp,
   flag_code            varchar(255)         null default 'init',
   batch_no             varchar(255)         null default '',
   flag_object_code     varchar(255)         null default '',
   state_code           varchar(255)         null default '1',
   data_state_code      varchar(255)         null default '1',
   order_by_            int8                 null default 0,
   json_data            json                 null,
   version_             int4                 null default 1,
   system_info_code     varchar(255)         null default '',
   site_code            varchar(255)         null default '',
   org_code             varchar(255)         null default '',
   department_code      varchar(255)         null default '',
   personal_code        varchar(255)         null default '',
   personal_id          varchar(255)         null default '',
   title                varchar(255)         not null default '',
   content              varchar(4095)        null default '',
   release_date         date                 null default current_timestamp,
   picture_url          varchar(255)         null default '',
   intro                text                 null default '',
   recom                char(1)              null default '0',
   sources_code         varchar(255)         null default '',
   org_ids              varchar(1023)        null default '',
   org_codes            varchar(1023)        null default '',
   department_ids       varchar(1023)        null default '',
   department_codes     varchar(1023)        null default '',
   check_person_code    varchar(255)         null,
   author_person_code   varchar(255)         null,
   constraint pk_c1_article primary key (article_id)
);

comment on table c1_article is
'文章';

comment on column c1_article.article_id is
'文章标识';

comment on column c1_article.check_person_id is
'审核人标识';

comment on column c1_article.author_person_id is
'作者标识';

comment on column c1_article.begin_date is
'开始日期';

comment on column c1_article.end_date is
'结束日期';

comment on column c1_article.created_by_id is
'创建人标识';

comment on column c1_article.created_by_code is
'创建人编号';

comment on column c1_article.created_date is
'创建日期';

comment on column c1_article.last_modified_by_id is
'最后修改人标识';

comment on column c1_article.last_modified_by_code is
'最后修改人编号';

comment on column c1_article.last_modified_date is
'最后修改日期';

comment on column c1_article.flag_code is
'标记编号(数据字典：工作流、同步、生成、录入、审批)';

comment on column c1_article.batch_no is
'批次编号';

comment on column c1_article.flag_object_code is
'标识对象编号(工作流：工作流标识、同步：批次号、生成：操作者编号、录入：操作者编号、审批：操作者编号)';

comment on column c1_article.state_code is
'状态编号(数据字典)';

comment on column c1_article.data_state_code is
'数据状态编号(数据字典)';

comment on column c1_article.order_by_ is
'排序';

comment on column c1_article.json_data is
'json数据';

comment on column c1_article.version_ is
'版本';

comment on column c1_article.system_info_code is
'系统信息编号';

comment on column c1_article.site_code is
'站点编号';

comment on column c1_article.org_code is
'组织编号';

comment on column c1_article.department_code is
'部门编号';

comment on column c1_article.personal_code is
'个人编号';

comment on column c1_article.personal_id is
'个人标识';

comment on column c1_article.title is
'标题';

comment on column c1_article.content is
'正文';

comment on column c1_article.release_date is
'发布日期';

comment on column c1_article.picture_url is
'图片路径';

comment on column c1_article.intro is
'简介';

comment on column c1_article.recom is
'推荐';

comment on column c1_article.sources_code is
'来源代码';

comment on column c1_article.org_ids is
'组织标识集合';

comment on column c1_article.org_codes is
'组织编号集合';

comment on column c1_article.department_ids is
'部门标识集合';

comment on column c1_article.department_codes is
'部门编号集合';

comment on column c1_article.check_person_code is
'审核人代码';

comment on column c1_article.author_person_code is
'作者代码';

/*==============================================================*/
/* Table: c1_city                                               */
/*==============================================================*/
create table c1_city (
   city_id              varchar(255)         not null default '',
   province_id          varchar(255)         null default '',
   begin_date           date                 null default current_timestamp,
   end_date             date                 null default to_timestamp('9999-01-01 00:00:00', 'yyyy-mm-dd hh24:mi:ss'),
   created_by_id        varchar(255)         null default '',
   created_by_code      varchar(255)         null default '',
   created_date         date                 null default current_timestamp,
   last_modified_by_id  varchar(255)         null default '',
   last_modified_by_code varchar(255)         null default '',
   last_modified_date   date                 null default current_timestamp,
   flag_code            varchar(255)         null default 'init',
   batch_no             varchar(255)         null default '',
   flag_object_code     varchar(255)         null default '',
   state_code           varchar(255)         null default '1',
   data_state_code      varchar(255)         null default '1',
   order_by_            int8                 null default 0,
   json_data            json                 null,
   version_             int4                 null default 1,
   system_info_code     varchar(255)         null default '',
   site_code            varchar(255)         null default '',
   org_code             varchar(255)         null default '',
   department_code      varchar(255)         null default '',
   personal_code        varchar(255)         null default '',
   personal_id          varchar(255)         null default '',
   city_code            varchar(255)         not null default '',
   province_code        varchar(255)         not null default '',
   name                 varchar(255)         not null default '',
   country_code         varchar(255)         null default '',
   constraint pk_c1_city primary key (city_id)
);

comment on table c1_city is
'城市';

comment on column c1_city.city_id is
'城市标识';

comment on column c1_city.province_id is
'省份标识';

comment on column c1_city.begin_date is
'开始日期';

comment on column c1_city.end_date is
'结束日期';

comment on column c1_city.created_by_id is
'创建人标识';

comment on column c1_city.created_by_code is
'创建人编号';

comment on column c1_city.created_date is
'创建日期';

comment on column c1_city.last_modified_by_id is
'最后修改人标识';

comment on column c1_city.last_modified_by_code is
'最后修改人编号';

comment on column c1_city.last_modified_date is
'最后修改日期';

comment on column c1_city.flag_code is
'标记编号(数据字典：工作流、同步、生成、录入、审批)';

comment on column c1_city.batch_no is
'批次编号';

comment on column c1_city.flag_object_code is
'标识对象编号(工作流：工作流标识、同步：批次号、生成：操作者编号、录入：操作者编号、审批：操作者编号)';

comment on column c1_city.state_code is
'状态编号(数据字典)';

comment on column c1_city.data_state_code is
'数据状态编号(数据字典)';

comment on column c1_city.order_by_ is
'排序';

comment on column c1_city.json_data is
'json数据';

comment on column c1_city.version_ is
'版本';

comment on column c1_city.system_info_code is
'系统信息编号';

comment on column c1_city.site_code is
'站点编号';

comment on column c1_city.org_code is
'组织编号';

comment on column c1_city.department_code is
'部门编号';

comment on column c1_city.personal_code is
'个人编号';

comment on column c1_city.personal_id is
'个人标识';

comment on column c1_city.city_code is
'城市编号';

comment on column c1_city.province_code is
'省份编号';

comment on column c1_city.name is
'名称';

comment on column c1_city.country_code is
'国家编号(数据字典)';

/*==============================================================*/
/* Table: c1_department                                         */
/*==============================================================*/
create table c1_department (
   department_id        varchar(255)         not null default '',
   org_id               varchar(255)         null default '',
   begin_date           date                 null default current_timestamp,
   end_date             date                 null default to_timestamp('9999-01-01 00:00:00', 'yyyy-mm-dd hh24:mi:ss'),
   created_by_id        varchar(255)         null default '',
   created_by_code      varchar(255)         null default '',
   created_date         date                 null default current_timestamp,
   last_modified_by_id  varchar(255)         null default '',
   last_modified_by_code varchar(255)         null default '',
   last_modified_date   date                 null default current_timestamp,
   flag_code            varchar(255)         null default 'init',
   batch_no             varchar(255)         null default '',
   flag_object_code     varchar(255)         null default '',
   state_code           varchar(255)         null default '1',
   data_state_code      varchar(255)         null default '1',
   order_by_            int8                 null default 0,
   json_data            json                 null,
   version_             int4                 null default 1,
   system_info_code     varchar(255)         null default '',
   site_code            varchar(255)         null default '',
   org_code             varchar(255)         null default '',
   department_code      varchar(255)         null default '',
   personal_code        varchar(255)         null default '',
   personal_id          varchar(255)         null default '',
   name                 varchar(255)         not null default '',
   parent_id            varchar(255)         null default '',
   parent_code          varchar(255)         null default '',
   parent_tree_id       varchar(1023)        null default '',
   parent_tree_code     varchar(1023)        null default '',
   parent_tree_name     varchar(1023)        null default '',
   province_code        varchar(255)         null default '',
   city_code            varchar(255)         null default '',
   area_code            varchar(255)         null default '',
   region_code          varchar(255)         null default '',
   address              varchar(255)         null default '',
   constraint pk_c1_department primary key (department_id)
);

comment on table c1_department is
'部门';

comment on column c1_department.department_id is
'部门标识';

comment on column c1_department.org_id is
'组织标识';

comment on column c1_department.begin_date is
'开始日期';

comment on column c1_department.end_date is
'结束日期';

comment on column c1_department.created_by_id is
'创建人标识';

comment on column c1_department.created_by_code is
'创建人编号';

comment on column c1_department.created_date is
'创建日期';

comment on column c1_department.last_modified_by_id is
'最后修改人标识';

comment on column c1_department.last_modified_by_code is
'最后修改人编号';

comment on column c1_department.last_modified_date is
'最后修改日期';

comment on column c1_department.flag_code is
'标记编号(数据字典：工作流、同步、生成、录入、审批)';

comment on column c1_department.batch_no is
'批次编号';

comment on column c1_department.flag_object_code is
'标识对象编号(工作流：工作流标识、同步：批次号、生成：操作者编号、录入：操作者编号、审批：操作者编号)';

comment on column c1_department.state_code is
'状态编号(数据字典)';

comment on column c1_department.data_state_code is
'数据状态编号(数据字典)';

comment on column c1_department.order_by_ is
'排序';

comment on column c1_department.json_data is
'json数据';

comment on column c1_department.version_ is
'版本';

comment on column c1_department.system_info_code is
'系统信息编号';

comment on column c1_department.site_code is
'站点编号';

comment on column c1_department.org_code is
'组织编号';

comment on column c1_department.department_code is
'部门编号';

comment on column c1_department.personal_code is
'个人编号';

comment on column c1_department.personal_id is
'个人标识';

comment on column c1_department.name is
'名称';

comment on column c1_department.parent_id is
'父级标识';

comment on column c1_department.parent_code is
'父级编号';

comment on column c1_department.parent_tree_id is
'父级树标识';

comment on column c1_department.parent_tree_code is
'父级树编号';

comment on column c1_department.parent_tree_name is
'父级树名称';

comment on column c1_department.province_code is
'省份编号';

comment on column c1_department.city_code is
'城市编号';

comment on column c1_department.area_code is
'地区编号';

comment on column c1_department.region_code is
'区域编号';

comment on column c1_department.address is
'地址';

/*==============================================================*/
/* Table: c1_dictionary                                         */
/*==============================================================*/
create table c1_dictionary (
   dictionary_id        varchar(255)         not null default '',
   site_id              varchar(255)         null default '',
   dictionary_type_id   varchar(255)         null default '',
   begin_date           date                 null default current_timestamp,
   end_date             date                 null default to_timestamp('9999-01-01 00:00:00', 'yyyy-mm-dd hh24:mi:ss'),
   created_by_id        varchar(255)         null default '',
   created_by_code      varchar(255)         null default '',
   created_date         date                 null default current_timestamp,
   last_modified_by_id  varchar(255)         null default '',
   last_modified_by_code varchar(255)         null default '',
   last_modified_date   date                 null default current_timestamp,
   flag_code            varchar(255)         null default 'init',
   batch_no             varchar(255)         null default '',
   flag_object_code     varchar(255)         null default '',
   state_code           varchar(255)         null default '1',
   data_state_code      varchar(255)         null default '1',
   order_by_            int8                 null default 0,
   json_data            json                 null,
   version_             int4                 null default 1,
   system_info_code     varchar(255)         null default '',
   site_code            varchar(255)         null default '',
   org_code             varchar(255)         null default '',
   department_code      varchar(255)         null default '',
   personal_code        varchar(255)         null default '',
   personal_id          varchar(255)         null default '',
   dictionary_code      varchar(255)         not null default '',
   name                 varchar(255)         null default '',
   value_               varchar(1023)        null default '',
   remark               varchar(511)         null default '',
   explain_             varchar(511)         null default '',
   dictionary_type_code varchar(255)         null default '',
   constraint pk_c1_dictionary primary key (dictionary_id)
);

comment on table c1_dictionary is
'字典';

comment on column c1_dictionary.dictionary_id is
'字典标识';

comment on column c1_dictionary.site_id is
'站点标识';

comment on column c1_dictionary.dictionary_type_id is
'字典类别标识';

comment on column c1_dictionary.begin_date is
'开始日期';

comment on column c1_dictionary.end_date is
'结束日期';

comment on column c1_dictionary.created_by_id is
'创建人标识';

comment on column c1_dictionary.created_by_code is
'创建人编号';

comment on column c1_dictionary.created_date is
'创建日期';

comment on column c1_dictionary.last_modified_by_id is
'最后修改人标识';

comment on column c1_dictionary.last_modified_by_code is
'最后修改人编号';

comment on column c1_dictionary.last_modified_date is
'最后修改日期';

comment on column c1_dictionary.flag_code is
'标记编号(数据字典：工作流、同步、生成、录入、审批)';

comment on column c1_dictionary.batch_no is
'批次编号';

comment on column c1_dictionary.flag_object_code is
'标识对象编号(工作流：工作流标识、同步：批次号、生成：操作者编号、录入：操作者编号、审批：操作者编号)';

comment on column c1_dictionary.state_code is
'状态编号(数据字典)';

comment on column c1_dictionary.data_state_code is
'数据状态编号(数据字典)';

comment on column c1_dictionary.order_by_ is
'排序';

comment on column c1_dictionary.json_data is
'json数据';

comment on column c1_dictionary.version_ is
'版本';

comment on column c1_dictionary.system_info_code is
'系统信息编号';

comment on column c1_dictionary.site_code is
'站点编号';

comment on column c1_dictionary.org_code is
'组织编号';

comment on column c1_dictionary.department_code is
'部门编号';

comment on column c1_dictionary.personal_code is
'个人编号';

comment on column c1_dictionary.personal_id is
'个人标识';

comment on column c1_dictionary.dictionary_code is
'字典编号';

comment on column c1_dictionary.name is
'名称';

comment on column c1_dictionary.value_ is
'值';

comment on column c1_dictionary.remark is
'备注';

comment on column c1_dictionary.explain_ is
'解释';

comment on column c1_dictionary.dictionary_type_code is
'字典类别编号';

/*==============================================================*/
/* Table: c1_dictionary_item                                    */
/*==============================================================*/
create table c1_dictionary_item (
   dictionary_item_id   varchar(255)         not null default '',
   dictionary_id        varchar(255)         null default '',
   dictionary_type_id   varchar(255)         null default '',
   begin_date           date                 null default current_timestamp,
   end_date             date                 null default to_timestamp('9999-01-01 00:00:00', 'yyyy-mm-dd hh24:mi:ss'),
   created_by_id        varchar(255)         null default '',
   created_by_code      varchar(255)         null default '',
   created_date         date                 null default current_timestamp,
   last_modified_by_id  varchar(255)         null default '',
   last_modified_by_code varchar(255)         null default '',
   last_modified_date   date                 null default current_timestamp,
   flag_code            varchar(255)         null default 'init',
   batch_no             varchar(255)         null default '',
   flag_object_code     varchar(255)         null default '',
   state_code           varchar(255)         null default '1',
   data_state_code      varchar(255)         null default '1',
   order_by_            int8                 null default 0,
   json_data            json                 null,
   version_             int4                 null default 1,
   system_info_code     varchar(255)         null default '',
   site_code            varchar(255)         null default '',
   org_code             varchar(255)         null default '',
   department_code      varchar(255)         null default '',
   personal_code        varchar(255)         null default '',
   personal_id          varchar(255)         null default '',
   dictionary_item_code varchar(255)         null default '',
   name                 varchar(255)         null default '',
   value_               varchar(1023)        null default '',
   remark               varchar(511)         null default '',
   explain_             varchar(511)         null default '',
   dictionary_code      varchar(255)         null default '',
   dictionary_type_code varchar(255)         null default '',
   constraint pk_c1_dictionary_item primary key (dictionary_item_id)
);

comment on table c1_dictionary_item is
'字典项';

comment on column c1_dictionary_item.dictionary_item_id is
'字典项标识';

comment on column c1_dictionary_item.dictionary_id is
'字典标识';

comment on column c1_dictionary_item.dictionary_type_id is
'字典类别标识';

comment on column c1_dictionary_item.begin_date is
'开始日期';

comment on column c1_dictionary_item.end_date is
'结束日期';

comment on column c1_dictionary_item.created_by_id is
'创建人标识';

comment on column c1_dictionary_item.created_by_code is
'创建人编号';

comment on column c1_dictionary_item.created_date is
'创建日期';

comment on column c1_dictionary_item.last_modified_by_id is
'最后修改人标识';

comment on column c1_dictionary_item.last_modified_by_code is
'最后修改人编号';

comment on column c1_dictionary_item.last_modified_date is
'最后修改日期';

comment on column c1_dictionary_item.flag_code is
'标记编号(数据字典：工作流、同步、生成、录入、审批)';

comment on column c1_dictionary_item.batch_no is
'批次编号';

comment on column c1_dictionary_item.flag_object_code is
'标识对象编号(工作流：工作流标识、同步：批次号、生成：操作者编号、录入：操作者编号、审批：操作者编号)';

comment on column c1_dictionary_item.state_code is
'状态编号(数据字典)';

comment on column c1_dictionary_item.data_state_code is
'数据状态编号(数据字典)';

comment on column c1_dictionary_item.order_by_ is
'排序';

comment on column c1_dictionary_item.json_data is
'json数据';

comment on column c1_dictionary_item.version_ is
'版本';

comment on column c1_dictionary_item.system_info_code is
'系统信息编号';

comment on column c1_dictionary_item.site_code is
'站点编号';

comment on column c1_dictionary_item.org_code is
'组织编号';

comment on column c1_dictionary_item.department_code is
'部门编号';

comment on column c1_dictionary_item.personal_code is
'个人编号';

comment on column c1_dictionary_item.personal_id is
'个人标识';

comment on column c1_dictionary_item.dictionary_item_code is
'字典项代码';

comment on column c1_dictionary_item.name is
'名称';

comment on column c1_dictionary_item.value_ is
'值';

comment on column c1_dictionary_item.remark is
'备注';

comment on column c1_dictionary_item.explain_ is
'解释';

comment on column c1_dictionary_item.dictionary_code is
'字典编号';

comment on column c1_dictionary_item.dictionary_type_code is
'字典类别编号';

/*==============================================================*/
/* Table: c1_dictionary_type                                    */
/*==============================================================*/
create table c1_dictionary_type (
   dictionary_type_id   varchar(255)         not null default '',
   site_id              varchar(255)         null default '',
   begin_date           date                 null default current_timestamp,
   end_date             date                 null default to_timestamp('9999-01-01 00:00:00', 'yyyy-mm-dd hh24:mi:ss'),
   created_by_id        varchar(255)         null default '',
   created_by_code      varchar(255)         null default '',
   created_date         date                 null default current_timestamp,
   last_modified_by_id  varchar(255)         null default '',
   last_modified_by_code varchar(255)         null default '',
   last_modified_date   date                 null default current_timestamp,
   flag_code            varchar(255)         null default 'init',
   batch_no             varchar(255)         null default '',
   flag_object_code     varchar(255)         null default '',
   state_code           varchar(255)         null default '1',
   data_state_code      varchar(255)         null default '1',
   order_by_            int8                 null default 0,
   json_data            json                 null,
   version_             int4                 null default 1,
   system_info_code     varchar(255)         null default '',
   site_code            varchar(255)         null default '',
   org_code             varchar(255)         null default '',
   department_code      varchar(255)         null default '',
   personal_code        varchar(255)         null default '',
   personal_id          varchar(255)         null default '',
   dictionary_type_code varchar(255)         not null default '',
   name                 varchar(255)         null default '',
   remark               varchar(511)         null default '',
   parent_id            varchar(255)         null default '',
   parent_code          varchar(255)         null default '',
   parent_tree_id       varchar(1023)        null default '',
   parent_tree_code     varchar(1023)        null default '',
   parent_tree_name     varchar(1023)        null default '',
   constraint pk_c1_dictionary_type primary key (dictionary_type_id)
);

comment on table c1_dictionary_type is
'字典类别';

comment on column c1_dictionary_type.dictionary_type_id is
'字典类别标识';

comment on column c1_dictionary_type.site_id is
'站点标识';

comment on column c1_dictionary_type.begin_date is
'开始日期';

comment on column c1_dictionary_type.end_date is
'结束日期';

comment on column c1_dictionary_type.created_by_id is
'创建人标识';

comment on column c1_dictionary_type.created_by_code is
'创建人编号';

comment on column c1_dictionary_type.created_date is
'创建日期';

comment on column c1_dictionary_type.last_modified_by_id is
'最后修改人标识';

comment on column c1_dictionary_type.last_modified_by_code is
'最后修改人编号';

comment on column c1_dictionary_type.last_modified_date is
'最后修改日期';

comment on column c1_dictionary_type.flag_code is
'标记编号(数据字典：工作流、同步、生成、录入、审批)';

comment on column c1_dictionary_type.batch_no is
'批次编号';

comment on column c1_dictionary_type.flag_object_code is
'标识对象编号(工作流：工作流标识、同步：批次号、生成：操作者编号、录入：操作者编号、审批：操作者编号)';

comment on column c1_dictionary_type.state_code is
'状态编号(数据字典)';

comment on column c1_dictionary_type.data_state_code is
'数据状态编号(数据字典)';

comment on column c1_dictionary_type.order_by_ is
'排序';

comment on column c1_dictionary_type.json_data is
'json数据';

comment on column c1_dictionary_type.version_ is
'版本';

comment on column c1_dictionary_type.system_info_code is
'系统信息编号';

comment on column c1_dictionary_type.site_code is
'站点编号';

comment on column c1_dictionary_type.org_code is
'组织编号';

comment on column c1_dictionary_type.department_code is
'部门编号';

comment on column c1_dictionary_type.personal_code is
'个人编号';

comment on column c1_dictionary_type.personal_id is
'个人标识';

comment on column c1_dictionary_type.dictionary_type_code is
'字典类别编号';

comment on column c1_dictionary_type.name is
'名称';

comment on column c1_dictionary_type.remark is
'备注';

comment on column c1_dictionary_type.parent_id is
'父级标识';

comment on column c1_dictionary_type.parent_code is
'父级编号';

comment on column c1_dictionary_type.parent_tree_id is
'父级树标识';

comment on column c1_dictionary_type.parent_tree_code is
'父级树编号';

comment on column c1_dictionary_type.parent_tree_name is
'父级树名称';

/*==============================================================*/
/* Table: c1_error_info                                         */
/*==============================================================*/
create table c1_error_info (
   error_info_id        varchar(255)         not null default '',
   site_id              varchar(255)         null default '',
   begin_date           date                 null default current_timestamp,
   end_date             date                 null default to_timestamp('9999-01-01 00:00:00', 'yyyy-mm-dd hh24:mi:ss'),
   created_by_id        varchar(255)         null default '',
   created_by_code      varchar(255)         null default '',
   created_date         date                 null default current_timestamp,
   last_modified_by_id  varchar(255)         null default '',
   last_modified_by_code varchar(255)         null default '',
   last_modified_date   date                 null default current_timestamp,
   flag_code            varchar(255)         null default 'init',
   batch_no             varchar(255)         null default '',
   flag_object_code     varchar(255)         null default '',
   state_code           varchar(255)         null default '1',
   data_state_code      varchar(255)         null default '1',
   order_by_            int8                 null default 0,
   json_data            json                 null,
   version_             int4                 null default 1,
   system_info_code     varchar(255)         null default '',
   site_code            varchar(255)         null default '',
   org_code             varchar(255)         null default '',
   department_code      varchar(255)         null default '',
   personal_code        varchar(255)         null default '',
   personal_id          varchar(255)         null default '',
   error_info_code      varchar(255)         not null default '',
   type_code            varchar(255)         null default '',
   content              varchar(4095)        null default '',
   object_type_code     varchar(255)         null default '',
   object_code          varchar(255)         null default '',
   object_id            varchar(255)         null default '',
   url                  varchar(255)         null default '',
   constraint pk_c1_error_info primary key (error_info_id)
);

comment on table c1_error_info is
'错误信息';

comment on column c1_error_info.error_info_id is
'错误信息标识';

comment on column c1_error_info.site_id is
'站点标识';

comment on column c1_error_info.begin_date is
'开始日期';

comment on column c1_error_info.end_date is
'结束日期';

comment on column c1_error_info.created_by_id is
'创建人标识';

comment on column c1_error_info.created_by_code is
'创建人编号';

comment on column c1_error_info.created_date is
'创建日期';

comment on column c1_error_info.last_modified_by_id is
'最后修改人标识';

comment on column c1_error_info.last_modified_by_code is
'最后修改人编号';

comment on column c1_error_info.last_modified_date is
'最后修改日期';

comment on column c1_error_info.flag_code is
'标记编号(数据字典：工作流、同步、生成、录入、审批)';

comment on column c1_error_info.batch_no is
'批次编号';

comment on column c1_error_info.flag_object_code is
'标识对象编号(工作流：工作流标识、同步：批次号、生成：操作者编号、录入：操作者编号、审批：操作者编号)';

comment on column c1_error_info.state_code is
'状态编号(数据字典)';

comment on column c1_error_info.data_state_code is
'数据状态编号(数据字典)';

comment on column c1_error_info.order_by_ is
'排序';

comment on column c1_error_info.json_data is
'json数据';

comment on column c1_error_info.version_ is
'版本';

comment on column c1_error_info.system_info_code is
'系统信息编号';

comment on column c1_error_info.site_code is
'站点编号';

comment on column c1_error_info.org_code is
'组织编号';

comment on column c1_error_info.department_code is
'部门编号';

comment on column c1_error_info.personal_code is
'个人编号';

comment on column c1_error_info.personal_id is
'个人标识';

comment on column c1_error_info.error_info_code is
'错误信息编号';

comment on column c1_error_info.type_code is
'类别编号(数据字典)';

comment on column c1_error_info.content is
'正文';

comment on column c1_error_info.object_type_code is
'对象类别编号(数据字典)';

comment on column c1_error_info.object_code is
'对象编号';

comment on column c1_error_info.object_id is
'对象标识';

comment on column c1_error_info.url is
'url';

/*==============================================================*/
/* Table: c1_i18n_info                                          */
/*==============================================================*/
create table c1_i18n_info (
   i18n_info_id         varchar(255)         not null default '',
   site_id              varchar(255)         null default '',
   begin_date           date                 null default current_timestamp,
   end_date             date                 null default to_timestamp('9999-01-01 00:00:00', 'yyyy-mm-dd hh24:mi:ss'),
   created_by_id        varchar(255)         null default '',
   created_by_code      varchar(255)         null default '',
   created_date         date                 null default current_timestamp,
   last_modified_by_id  varchar(255)         null default '',
   last_modified_by_code varchar(255)         null default '',
   last_modified_date   date                 null default current_timestamp,
   flag_code            varchar(255)         null default 'init',
   batch_no             varchar(255)         null default '',
   flag_object_code     varchar(255)         null default '',
   state_code           varchar(255)         null default '1',
   data_state_code      varchar(255)         null default '1',
   order_by_            int8                 null default 0,
   json_data            json                 null,
   version_             int4                 null default 1,
   system_info_code     varchar(255)         null default '',
   site_code            varchar(255)         null default '',
   org_code             varchar(255)         null default '',
   department_code      varchar(255)         null default '',
   personal_code        varchar(255)         null default '',
   personal_id          varchar(255)         null default '',
   i18n_info_code       varchar(255)         not null default '',
   type_code            varchar(255)         null default '',
   content              varchar(4095)        null default '',
   constraint pk_c1_i18n_info primary key (i18n_info_id)
);

comment on table c1_i18n_info is
'国际化信息';

comment on column c1_i18n_info.i18n_info_id is
'国际化信息标识';

comment on column c1_i18n_info.site_id is
'站点标识';

comment on column c1_i18n_info.begin_date is
'开始日期';

comment on column c1_i18n_info.end_date is
'结束日期';

comment on column c1_i18n_info.created_by_id is
'创建人标识';

comment on column c1_i18n_info.created_by_code is
'创建人编号';

comment on column c1_i18n_info.created_date is
'创建日期';

comment on column c1_i18n_info.last_modified_by_id is
'最后修改人标识';

comment on column c1_i18n_info.last_modified_by_code is
'最后修改人编号';

comment on column c1_i18n_info.last_modified_date is
'最后修改日期';

comment on column c1_i18n_info.flag_code is
'标记编号(数据字典：工作流、同步、生成、录入、审批)';

comment on column c1_i18n_info.batch_no is
'批次编号';

comment on column c1_i18n_info.flag_object_code is
'标识对象编号(工作流：工作流标识、同步：批次号、生成：操作者编号、录入：操作者编号、审批：操作者编号)';

comment on column c1_i18n_info.state_code is
'状态编号(数据字典)';

comment on column c1_i18n_info.data_state_code is
'数据状态编号(数据字典)';

comment on column c1_i18n_info.order_by_ is
'排序';

comment on column c1_i18n_info.json_data is
'json数据';

comment on column c1_i18n_info.version_ is
'版本';

comment on column c1_i18n_info.system_info_code is
'系统信息编号';

comment on column c1_i18n_info.site_code is
'站点编号';

comment on column c1_i18n_info.org_code is
'组织编号';

comment on column c1_i18n_info.department_code is
'部门编号';

comment on column c1_i18n_info.personal_code is
'个人编号';

comment on column c1_i18n_info.personal_id is
'个人标识';

comment on column c1_i18n_info.i18n_info_code is
'国际化信息编号';

comment on column c1_i18n_info.type_code is
'类别编号(数据字典)';

comment on column c1_i18n_info.content is
'正文';

/*==============================================================*/
/* Table: c1_navigation                                         */
/*==============================================================*/
create table c1_navigation (
   navigation_id        varchar(255)         not null default '',
   begin_date           date                 null default current_timestamp,
   end_date             date                 null default to_timestamp('9999-01-01 00:00:00', 'yyyy-mm-dd hh24:mi:ss'),
   created_by_id        varchar(255)         null default '',
   created_by_code      varchar(255)         null default '',
   created_date         date                 null default current_timestamp,
   last_modified_by_id  varchar(255)         null default '',
   last_modified_by_code varchar(255)         null default '',
   last_modified_date   date                 null default current_timestamp,
   flag_code            varchar(255)         null default 'init',
   batch_no             varchar(255)         null default '',
   flag_object_code     varchar(255)         null default '',
   state_code           varchar(255)         null default '1',
   data_state_code      varchar(255)         null default '1',
   order_by_            int8                 null default 0,
   json_data            json                 null,
   version_             int4                 null default 1,
   system_info_code     varchar(255)         null default '',
   site_code            varchar(255)         null default '',
   org_code             varchar(255)         null default '',
   department_code      varchar(255)         null default '',
   personal_code        varchar(255)         null default '',
   personal_id          varchar(255)         null default '',
   navigation_code      varchar(255)         not null default '',
   name                 varchar(255)         null default '',
   icon_style           varchar(255)         null default '',
   url                  varchar(255)         null default '',
   target               varchar(255)         null default '',
   data_option          varchar(511)         null default '',
   type_code            varchar(255)         null default '',
   parent_code          varchar(255)         null default '',
   parent_id            varchar(255)         null default '',
   parent_tree_id       varchar(1023)        null default '',
   parent_tree_code     varchar(1023)        null default '',
   parent_tree_name     varchar(1023)        null default '',
   constraint pk_c1_navigation primary key (navigation_id)
);

comment on table c1_navigation is
'导航';

comment on column c1_navigation.navigation_id is
'导航标识';

comment on column c1_navigation.begin_date is
'开始日期';

comment on column c1_navigation.end_date is
'结束日期';

comment on column c1_navigation.created_by_id is
'创建人标识';

comment on column c1_navigation.created_by_code is
'创建人编号';

comment on column c1_navigation.created_date is
'创建日期';

comment on column c1_navigation.last_modified_by_id is
'最后修改人标识';

comment on column c1_navigation.last_modified_by_code is
'最后修改人编号';

comment on column c1_navigation.last_modified_date is
'最后修改日期';

comment on column c1_navigation.flag_code is
'标记编号(数据字典：工作流、同步、生成、录入、审批)';

comment on column c1_navigation.batch_no is
'批次编号';

comment on column c1_navigation.flag_object_code is
'标识对象编号(工作流：工作流标识、同步：批次号、生成：操作者编号、录入：操作者编号、审批：操作者编号)';

comment on column c1_navigation.state_code is
'状态编号(数据字典)';

comment on column c1_navigation.data_state_code is
'数据状态编号(数据字典)';

comment on column c1_navigation.order_by_ is
'排序';

comment on column c1_navigation.json_data is
'json数据';

comment on column c1_navigation.version_ is
'版本';

comment on column c1_navigation.system_info_code is
'系统信息编号';

comment on column c1_navigation.site_code is
'站点编号';

comment on column c1_navigation.org_code is
'组织编号';

comment on column c1_navigation.department_code is
'部门编号';

comment on column c1_navigation.personal_code is
'个人编号';

comment on column c1_navigation.personal_id is
'个人标识';

comment on column c1_navigation.navigation_code is
'导航编号';

comment on column c1_navigation.name is
'名称';

comment on column c1_navigation.icon_style is
'图标样式';

comment on column c1_navigation.url is
'url';

comment on column c1_navigation.target is
'目标';

comment on column c1_navigation.data_option is
'数据选项';

comment on column c1_navigation.type_code is
'类别编号(数据字典)';

comment on column c1_navigation.parent_code is
'父级编号';

comment on column c1_navigation.parent_id is
'父级标识';

comment on column c1_navigation.parent_tree_id is
'父级树标识';

comment on column c1_navigation.parent_tree_code is
'父级树编号';

comment on column c1_navigation.parent_tree_name is
'父级树名称';

/*==============================================================*/
/* Table: c1_notice                                             */
/*==============================================================*/
create table c1_notice (
   notice_id            varchar(255)         not null default '',
   check_person_id      varchar(255)         null default '',
   author_person_id     varchar(255)         null default '',
   begin_date           date                 null default current_timestamp,
   end_date             date                 null default to_timestamp('9999-01-01 00:00:00', 'yyyy-mm-dd hh24:mi:ss'),
   created_by_id        varchar(255)         null default '',
   created_by_code      varchar(255)         null default '',
   created_date         date                 null default current_timestamp,
   last_modified_by_id  varchar(255)         null default '',
   last_modified_by_code varchar(255)         null default '',
   last_modified_date   date                 null default current_timestamp,
   flag_code            varchar(255)         null default 'init',
   batch_no             varchar(255)         null default '',
   flag_object_code     varchar(255)         null default '',
   state_code           varchar(255)         null default '1',
   data_state_code      varchar(255)         null default '1',
   order_by_            int8                 null default 0,
   json_data            json                 null,
   version_             int4                 null default 1,
   system_info_code     varchar(255)         null default '',
   site_code            varchar(255)         null default '',
   org_code             varchar(255)         null default '',
   department_code      varchar(255)         null default '',
   personal_code        varchar(255)         null default '',
   personal_id          varchar(255)         null default '',
   title                varchar(255)         not null default '',
   content              varchar(4095)        null default '',
   type_code            varchar(255)         null default '',
   picture_url          varchar(255)         null default '',
   sources_code         varchar(255)         null default '',
   org_codes            varchar(1023)        null default '',
   org_ids              varchar(1023)        null default '',
   department_codes     varchar(1023)        null default '',
   department_ids       varchar(1023)        null default '',
   check_person_code    varchar(255)         null,
   author_person_code   varchar(255)         null,
   constraint pk_c1_notice primary key (notice_id)
);

comment on table c1_notice is
'公告';

comment on column c1_notice.notice_id is
'公告标识';

comment on column c1_notice.check_person_id is
'审核人标识';

comment on column c1_notice.author_person_id is
'作者标识';

comment on column c1_notice.begin_date is
'开始日期';

comment on column c1_notice.end_date is
'结束日期';

comment on column c1_notice.created_by_id is
'创建人标识';

comment on column c1_notice.created_by_code is
'创建人编号';

comment on column c1_notice.created_date is
'创建日期';

comment on column c1_notice.last_modified_by_id is
'最后修改人标识';

comment on column c1_notice.last_modified_by_code is
'最后修改人编号';

comment on column c1_notice.last_modified_date is
'最后修改日期';

comment on column c1_notice.flag_code is
'标记编号(数据字典：工作流、同步、生成、录入、审批)';

comment on column c1_notice.batch_no is
'批次编号';

comment on column c1_notice.flag_object_code is
'标识对象编号(工作流：工作流标识、同步：批次号、生成：操作者编号、录入：操作者编号、审批：操作者编号)';

comment on column c1_notice.state_code is
'状态编号(数据字典)';

comment on column c1_notice.data_state_code is
'数据状态编号(数据字典)';

comment on column c1_notice.order_by_ is
'排序';

comment on column c1_notice.json_data is
'json数据';

comment on column c1_notice.version_ is
'版本';

comment on column c1_notice.system_info_code is
'系统信息编号';

comment on column c1_notice.site_code is
'站点编号';

comment on column c1_notice.org_code is
'组织编号';

comment on column c1_notice.department_code is
'部门编号';

comment on column c1_notice.personal_code is
'个人编号';

comment on column c1_notice.personal_id is
'个人标识';

comment on column c1_notice.title is
'标题';

comment on column c1_notice.content is
'正文';

comment on column c1_notice.type_code is
'类别编号(数据字典)';

comment on column c1_notice.picture_url is
'图片路径';

comment on column c1_notice.sources_code is
'来源代码';

comment on column c1_notice.org_codes is
'组织编号集合';

comment on column c1_notice.org_ids is
'组织标识集合';

comment on column c1_notice.department_codes is
'部门编号集合';

comment on column c1_notice.department_ids is
'部门标识集合';

comment on column c1_notice.check_person_code is
'审核人代码';

comment on column c1_notice.author_person_code is
'作者代码';

/*==============================================================*/
/* Table: c1_oauth_consumer                                     */
/*==============================================================*/
create table c1_oauth_consumer (
   oauth_consumer_id    varchar(255)         not null default '',
   begin_date           date                 null default current_timestamp,
   end_date             date                 null default to_timestamp('9999-01-01 00:00:00', 'yyyy-mm-dd hh24:mi:ss'),
   created_by_id        varchar(255)         null default '',
   created_by_code      varchar(255)         null default '',
   created_date         date                 null default current_timestamp,
   last_modified_by_id  varchar(255)         null default '',
   last_modified_by_code varchar(255)         null default '',
   last_modified_date   date                 null default current_timestamp,
   flag_code            varchar(255)         null default 'init',
   batch_no             varchar(255)         null default '',
   flag_object_code     varchar(255)         null default '',
   state_code           varchar(255)         null default '1',
   data_state_code      varchar(255)         null default '1',
   order_by_            int8                 null default 0,
   json_data            json                 null,
   version_             int4                 null default 1,
   system_info_code     varchar(255)         null default '',
   site_code            varchar(255)         null default '',
   org_code             varchar(255)         null default '',
   department_code      varchar(255)         null default '',
   personal_code        varchar(255)         null default '',
   personal_id          varchar(255)         null default '',
   client_secret        varchar(255)         null default '',
   constraint pk_c1_oauth_consumer primary key (oauth_consumer_id)
);

comment on table c1_oauth_consumer is
'授权消费';

comment on column c1_oauth_consumer.oauth_consumer_id is
'授权消费标识';

comment on column c1_oauth_consumer.begin_date is
'开始日期';

comment on column c1_oauth_consumer.end_date is
'结束日期';

comment on column c1_oauth_consumer.created_by_id is
'创建人标识';

comment on column c1_oauth_consumer.created_by_code is
'创建人编号';

comment on column c1_oauth_consumer.created_date is
'创建日期';

comment on column c1_oauth_consumer.last_modified_by_id is
'最后修改人标识';

comment on column c1_oauth_consumer.last_modified_by_code is
'最后修改人编号';

comment on column c1_oauth_consumer.last_modified_date is
'最后修改日期';

comment on column c1_oauth_consumer.flag_code is
'标记编号(数据字典：工作流、同步、生成、录入、审批)';

comment on column c1_oauth_consumer.batch_no is
'批次编号';

comment on column c1_oauth_consumer.flag_object_code is
'标识对象编号(工作流：工作流标识、同步：批次号、生成：操作者编号、录入：操作者编号、审批：操作者编号)';

comment on column c1_oauth_consumer.state_code is
'状态编号(数据字典)';

comment on column c1_oauth_consumer.data_state_code is
'数据状态编号(数据字典)';

comment on column c1_oauth_consumer.order_by_ is
'排序';

comment on column c1_oauth_consumer.json_data is
'json数据';

comment on column c1_oauth_consumer.version_ is
'版本';

comment on column c1_oauth_consumer.system_info_code is
'系统信息编号';

comment on column c1_oauth_consumer.site_code is
'站点编号';

comment on column c1_oauth_consumer.org_code is
'组织编号';

comment on column c1_oauth_consumer.department_code is
'部门编号';

comment on column c1_oauth_consumer.personal_code is
'个人编号';

comment on column c1_oauth_consumer.personal_id is
'个人标识';

comment on column c1_oauth_consumer.client_secret is
'客户端密钥';

/*==============================================================*/
/* Table: c1_open_user                                          */
/*==============================================================*/
create table c1_open_user (
   open_user_id         varchar(255)         not null default '',
   user_id              varchar(255)         null default '',
   begin_date           date                 null default current_timestamp,
   end_date             date                 null default to_timestamp('9999-01-01 00:00:00', 'yyyy-mm-dd hh24:mi:ss'),
   created_by_id        varchar(255)         null default '',
   created_by_code      varchar(255)         null default '',
   created_date         date                 null default current_timestamp,
   last_modified_by_id  varchar(255)         null default '',
   last_modified_by_code varchar(255)         null default '',
   last_modified_date   date                 null default current_timestamp,
   flag_code            varchar(255)         null default 'init',
   batch_no             varchar(255)         null default '',
   flag_object_code     varchar(255)         null default '',
   state_code           varchar(255)         null default '1',
   data_state_code      varchar(255)         null default '1',
   order_by_            int8                 null default 0,
   json_data            json                 null,
   version_             int4                 null default 1,
   system_info_code     varchar(255)         null default '',
   site_code            varchar(255)         null default '',
   org_code             varchar(255)         null default '',
   department_code      varchar(255)         null default '',
   personal_code        varchar(255)         null default '',
   personal_id          varchar(255)         null default '',
   client_id            varchar(255)         not null default '',
   user_code            varchar(255)         null default '',
   constraint pk_c1_open_user primary key (open_user_id)
);

comment on table c1_open_user is
'开放用户';

comment on column c1_open_user.open_user_id is
'开放用户标识';

comment on column c1_open_user.user_id is
'用户标识';

comment on column c1_open_user.begin_date is
'开始日期';

comment on column c1_open_user.end_date is
'结束日期';

comment on column c1_open_user.created_by_id is
'创建人标识';

comment on column c1_open_user.created_by_code is
'创建人编号';

comment on column c1_open_user.created_date is
'创建日期';

comment on column c1_open_user.last_modified_by_id is
'最后修改人标识';

comment on column c1_open_user.last_modified_by_code is
'最后修改人编号';

comment on column c1_open_user.last_modified_date is
'最后修改日期';

comment on column c1_open_user.flag_code is
'标记编号(数据字典：工作流、同步、生成、录入、审批)';

comment on column c1_open_user.batch_no is
'批次编号';

comment on column c1_open_user.flag_object_code is
'标识对象编号(工作流：工作流标识、同步：批次号、生成：操作者编号、录入：操作者编号、审批：操作者编号)';

comment on column c1_open_user.state_code is
'状态编号(数据字典)';

comment on column c1_open_user.data_state_code is
'数据状态编号(数据字典)';

comment on column c1_open_user.order_by_ is
'排序';

comment on column c1_open_user.json_data is
'json数据';

comment on column c1_open_user.version_ is
'版本';

comment on column c1_open_user.system_info_code is
'系统信息编号';

comment on column c1_open_user.site_code is
'站点编号';

comment on column c1_open_user.org_code is
'组织编号';

comment on column c1_open_user.department_code is
'部门编号';

comment on column c1_open_user.personal_code is
'个人编号';

comment on column c1_open_user.personal_id is
'个人标识';

comment on column c1_open_user.client_id is
'客户端标识';

comment on column c1_open_user.user_code is
'用户编号';

/*==============================================================*/
/* Table: c1_operate_log                                        */
/*==============================================================*/
create table c1_operate_log (
   operate_log_id       varchar(255)         not null default '',
   begin_date           date                 null default current_timestamp,
   end_date             date                 null default to_timestamp('9999-01-01 00:00:00', 'yyyy-mm-dd hh24:mi:ss'),
   created_by_id        varchar(255)         null default '',
   created_by_code      varchar(255)         null default '',
   created_date         date                 null default current_timestamp,
   last_modified_by_id  varchar(255)         null default '',
   last_modified_by_code varchar(255)         null default '',
   last_modified_date   date                 null default current_timestamp,
   flag_code            varchar(255)         null default 'init',
   batch_no             varchar(255)         null default '',
   flag_object_code     varchar(255)         null default '',
   state_code           varchar(255)         null default '1',
   data_state_code      varchar(255)         null default '1',
   order_by_            int8                 null default 0,
   json_data            json                 null,
   version_             int4                 null default 1,
   system_info_code     varchar(255)         null default '',
   site_code            varchar(255)         null default '',
   org_code             varchar(255)         null default '',
   department_code      varchar(255)         null default '',
   personal_code        varchar(255)         null default '',
   personal_id          varchar(255)         null default '',
   type_code            varchar(255)         not null default '',
   message              varchar(1023)        null default '',
   object_type_code     varchar(255)         null default '',
   object_code          varchar(255)         null default '',
   object_id            varchar(255)         null default '',
   constraint pk_c1_operate_log primary key (operate_log_id)
);

comment on table c1_operate_log is
'操作日志';

comment on column c1_operate_log.operate_log_id is
'操作日志标识';

comment on column c1_operate_log.begin_date is
'开始日期';

comment on column c1_operate_log.end_date is
'结束日期';

comment on column c1_operate_log.created_by_id is
'创建人标识';

comment on column c1_operate_log.created_by_code is
'创建人编号';

comment on column c1_operate_log.created_date is
'创建日期';

comment on column c1_operate_log.last_modified_by_id is
'最后修改人标识';

comment on column c1_operate_log.last_modified_by_code is
'最后修改人编号';

comment on column c1_operate_log.last_modified_date is
'最后修改日期';

comment on column c1_operate_log.flag_code is
'标记编号(数据字典：工作流、同步、生成、录入、审批)';

comment on column c1_operate_log.batch_no is
'批次编号';

comment on column c1_operate_log.flag_object_code is
'标识对象编号(工作流：工作流标识、同步：批次号、生成：操作者编号、录入：操作者编号、审批：操作者编号)';

comment on column c1_operate_log.state_code is
'状态编号(数据字典)';

comment on column c1_operate_log.data_state_code is
'数据状态编号(数据字典)';

comment on column c1_operate_log.order_by_ is
'排序';

comment on column c1_operate_log.json_data is
'json数据';

comment on column c1_operate_log.version_ is
'版本';

comment on column c1_operate_log.system_info_code is
'系统信息编号';

comment on column c1_operate_log.site_code is
'站点编号';

comment on column c1_operate_log.org_code is
'组织编号';

comment on column c1_operate_log.department_code is
'部门编号';

comment on column c1_operate_log.personal_code is
'个人编号';

comment on column c1_operate_log.personal_id is
'个人标识';

comment on column c1_operate_log.type_code is
'类别编号(数据字典)';

comment on column c1_operate_log.message is
'消息';

comment on column c1_operate_log.object_type_code is
'对象类别编号(数据字典)';

comment on column c1_operate_log.object_code is
'对象编号';

comment on column c1_operate_log.object_id is
'对象标识';

/*==============================================================*/
/* Table: c1_org                                                */
/*==============================================================*/
create table c1_org (
   org_id               varchar(255)         not null default '',
   begin_date           date                 null default current_timestamp,
   end_date             date                 null default to_timestamp('9999-01-01 00:00:00', 'yyyy-mm-dd hh24:mi:ss'),
   created_by_id        varchar(255)         null default '',
   created_by_code      varchar(255)         null default '',
   created_date         date                 null default current_timestamp,
   last_modified_by_id  varchar(255)         null default '',
   last_modified_by_code varchar(255)         null default '',
   last_modified_date   date                 null default current_timestamp,
   flag_code            varchar(255)         null default 'init',
   batch_no             varchar(255)         null default '',
   flag_object_code     varchar(255)         null default '',
   state_code           varchar(255)         null default '1',
   data_state_code      varchar(255)         null default '1',
   order_by_            int8                 null default 0,
   json_data            json                 null,
   version_             int4                 null default 1,
   system_info_code     varchar(255)         null default '',
   site_code            varchar(255)         null default '',
   org_code             varchar(255)         null default '',
   department_code      varchar(255)         null default '',
   personal_code        varchar(255)         null default '',
   personal_id          varchar(255)         null default '',
   name                 varchar(255)         null default '',
   parent_id            varchar(255)         null default '',
   parent_code          varchar(255)         null default '',
   parent_tree_id       varchar(1023)        null default '',
   parent_tree_code     varchar(1023)        null default '',
   parent_tree_name     varchar(1023)        null default '',
   province_code        varchar(255)         null default '',
   city_code            varchar(255)         null default '',
   area_code            varchar(255)         null default '',
   region_code          varchar(255)         null default '',
   address              varchar(255)         null default '',
   constraint pk_c1_org primary key (org_id)
);

comment on table c1_org is
'组织';

comment on column c1_org.org_id is
'组织标识';

comment on column c1_org.begin_date is
'开始日期';

comment on column c1_org.end_date is
'结束日期';

comment on column c1_org.created_by_id is
'创建人标识';

comment on column c1_org.created_by_code is
'创建人编号';

comment on column c1_org.created_date is
'创建日期';

comment on column c1_org.last_modified_by_id is
'最后修改人标识';

comment on column c1_org.last_modified_by_code is
'最后修改人编号';

comment on column c1_org.last_modified_date is
'最后修改日期';

comment on column c1_org.flag_code is
'标记编号(数据字典：工作流、同步、生成、录入、审批)';

comment on column c1_org.batch_no is
'批次编号';

comment on column c1_org.flag_object_code is
'标识对象编号(工作流：工作流标识、同步：批次号、生成：操作者编号、录入：操作者编号、审批：操作者编号)';

comment on column c1_org.state_code is
'状态编号(数据字典)';

comment on column c1_org.data_state_code is
'数据状态编号(数据字典)';

comment on column c1_org.order_by_ is
'排序';

comment on column c1_org.json_data is
'json数据';

comment on column c1_org.version_ is
'版本';

comment on column c1_org.system_info_code is
'系统信息编号';

comment on column c1_org.site_code is
'站点编号';

comment on column c1_org.org_code is
'组织编号';

comment on column c1_org.department_code is
'部门编号';

comment on column c1_org.personal_code is
'个人编号';

comment on column c1_org.personal_id is
'个人标识';

comment on column c1_org.name is
'名称';

comment on column c1_org.parent_id is
'父级标识';

comment on column c1_org.parent_code is
'父级编号';

comment on column c1_org.parent_tree_id is
'父级树标识';

comment on column c1_org.parent_tree_code is
'父级树编号';

comment on column c1_org.parent_tree_name is
'父级树名称';

comment on column c1_org.province_code is
'省份编号';

comment on column c1_org.city_code is
'城市编号';

comment on column c1_org.area_code is
'地区编号';

comment on column c1_org.region_code is
'区域编号';

comment on column c1_org.address is
'地址';

/*==============================================================*/
/* Table: c1_permission                                         */
/*==============================================================*/
create table c1_permission (
   permission_id        varchar(255)         not null default '',
   begin_date           date                 null default current_timestamp,
   end_date             date                 null default to_timestamp('9999-01-01 00:00:00', 'yyyy-mm-dd hh24:mi:ss'),
   created_by_id        varchar(255)         null default '',
   created_by_code      varchar(255)         null default '',
   created_date         date                 null default current_timestamp,
   last_modified_by_id  varchar(255)         null default '',
   last_modified_by_code varchar(255)         null default '',
   last_modified_date   date                 null default current_timestamp,
   flag_code            varchar(255)         null default 'init',
   batch_no             varchar(255)         null default '',
   flag_object_code     varchar(255)         null default '',
   state_code           varchar(255)         null default '1',
   data_state_code      varchar(255)         null default '1',
   order_by_            int8                 null default 0,
   json_data            json                 null,
   version_             int4                 null default 1,
   system_info_code     varchar(255)         null default '',
   site_code            varchar(255)         null default '',
   org_code             varchar(255)         null default '',
   department_code      varchar(255)         null default '',
   personal_code        varchar(255)         null default '',
   personal_id          varchar(255)         null default '',
   type_code            varchar(255)         null default '',
   permission_code      varchar(255)         not null default '',
   name                 varchar(255)         null default '',
   parent_id            varchar(255)         null default '',
   parent_code          varchar(255)         null default '',
   parent_tree_id       varchar(1023)        null default '',
   parent_tree_code     varchar(1023)        null default '',
   parent_tree_name     varchar(1023)        null default '',
   constraint pk_c1_permission primary key (permission_id)
);

comment on table c1_permission is
'许可';

comment on column c1_permission.permission_id is
'许可标识';

comment on column c1_permission.begin_date is
'开始日期';

comment on column c1_permission.end_date is
'结束日期';

comment on column c1_permission.created_by_id is
'创建人标识';

comment on column c1_permission.created_by_code is
'创建人编号';

comment on column c1_permission.created_date is
'创建日期';

comment on column c1_permission.last_modified_by_id is
'最后修改人标识';

comment on column c1_permission.last_modified_by_code is
'最后修改人编号';

comment on column c1_permission.last_modified_date is
'最后修改日期';

comment on column c1_permission.flag_code is
'标记编号(数据字典：工作流、同步、生成、录入、审批)';

comment on column c1_permission.batch_no is
'批次编号';

comment on column c1_permission.flag_object_code is
'标识对象编号(工作流：工作流标识、同步：批次号、生成：操作者编号、录入：操作者编号、审批：操作者编号)';

comment on column c1_permission.state_code is
'状态编号(数据字典)';

comment on column c1_permission.data_state_code is
'数据状态编号(数据字典)';

comment on column c1_permission.order_by_ is
'排序';

comment on column c1_permission.json_data is
'json数据';

comment on column c1_permission.version_ is
'版本';

comment on column c1_permission.system_info_code is
'系统信息编号';

comment on column c1_permission.site_code is
'站点编号';

comment on column c1_permission.org_code is
'组织编号';

comment on column c1_permission.department_code is
'部门编号';

comment on column c1_permission.personal_code is
'个人编号';

comment on column c1_permission.personal_id is
'个人标识';

comment on column c1_permission.type_code is
'类别编号(数据字典)';

comment on column c1_permission.permission_code is
'许可编号';

comment on column c1_permission.name is
'名称';

comment on column c1_permission.parent_id is
'父级标识';

comment on column c1_permission.parent_code is
'父级编号';

comment on column c1_permission.parent_tree_id is
'父级树标识';

comment on column c1_permission.parent_tree_code is
'父级树编号';

comment on column c1_permission.parent_tree_name is
'父级树名称';

/*==============================================================*/
/* Table: c1_province                                           */
/*==============================================================*/
create table c1_province (
   province_id          varchar(255)         not null default '',
   begin_date           date                 null default current_timestamp,
   end_date             date                 null default to_timestamp('9999-01-01 00:00:00', 'yyyy-mm-dd hh24:mi:ss'),
   created_by_id        varchar(255)         null default '',
   created_by_code      varchar(255)         null default '',
   created_date         date                 null default current_timestamp,
   last_modified_by_id  varchar(255)         null default '',
   last_modified_by_code varchar(255)         null default '',
   last_modified_date   date                 null default current_timestamp,
   flag_code            varchar(255)         null default 'init',
   batch_no             varchar(255)         null default '',
   flag_object_code     varchar(255)         null default '',
   state_code           varchar(255)         null default '1',
   data_state_code      varchar(255)         null default '1',
   order_by_            int8                 null default 0,
   json_data            json                 null,
   version_             int4                 null default 1,
   system_info_code     varchar(255)         null default '',
   site_code            varchar(255)         null default '',
   org_code             varchar(255)         null default '',
   department_code      varchar(255)         null default '',
   personal_code        varchar(255)         null default '',
   personal_id          varchar(255)         null default '',
   province_code        varchar(255)         not null default '',
   name                 varchar(255)         not null default '',
   country_code         varchar(255)         null default '',
   constraint pk_c1_province primary key (province_id)
);

comment on table c1_province is
'省份';

comment on column c1_province.province_id is
'省份标识';

comment on column c1_province.begin_date is
'开始日期';

comment on column c1_province.end_date is
'结束日期';

comment on column c1_province.created_by_id is
'创建人标识';

comment on column c1_province.created_by_code is
'创建人编号';

comment on column c1_province.created_date is
'创建日期';

comment on column c1_province.last_modified_by_id is
'最后修改人标识';

comment on column c1_province.last_modified_by_code is
'最后修改人编号';

comment on column c1_province.last_modified_date is
'最后修改日期';

comment on column c1_province.flag_code is
'标记编号(数据字典：工作流、同步、生成、录入、审批)';

comment on column c1_province.batch_no is
'批次编号';

comment on column c1_province.flag_object_code is
'标识对象编号(工作流：工作流标识、同步：批次号、生成：操作者编号、录入：操作者编号、审批：操作者编号)';

comment on column c1_province.state_code is
'状态编号(数据字典)';

comment on column c1_province.data_state_code is
'数据状态编号(数据字典)';

comment on column c1_province.order_by_ is
'排序';

comment on column c1_province.json_data is
'json数据';

comment on column c1_province.version_ is
'版本';

comment on column c1_province.system_info_code is
'系统信息编号';

comment on column c1_province.site_code is
'站点编号';

comment on column c1_province.org_code is
'组织编号';

comment on column c1_province.department_code is
'部门编号';

comment on column c1_province.personal_code is
'个人编号';

comment on column c1_province.personal_id is
'个人标识';

comment on column c1_province.province_code is
'省份编号';

comment on column c1_province.name is
'名称';

comment on column c1_province.country_code is
'国家编号(数据字典)';

/*==============================================================*/
/* Table: c1_region                                             */
/*==============================================================*/
create table c1_region (
   region_id            varchar(255)         not null default '',
   province_id          varchar(255)         null default '',
   city_id              varchar(255)         null default '',
   area_id              varchar(255)         null default '',
   begin_date           date                 null default current_timestamp,
   end_date             date                 null default to_timestamp('9999-01-01 00:00:00', 'yyyy-mm-dd hh24:mi:ss'),
   created_by_id        varchar(255)         null default '',
   created_by_code      varchar(255)         null default '',
   created_date         date                 null default current_timestamp,
   last_modified_by_id  varchar(255)         null default '',
   last_modified_by_code varchar(255)         null default '',
   last_modified_date   date                 null default current_timestamp,
   flag_code            varchar(255)         null default 'init',
   batch_no             varchar(255)         null default '',
   flag_object_code     varchar(255)         null default '',
   state_code           varchar(255)         null default '1',
   data_state_code      varchar(255)         null default '1',
   order_by_            int8                 null default 0,
   json_data            json                 null,
   version_             int4                 null default 1,
   system_info_code     varchar(255)         null default '',
   site_code            varchar(255)         null default '',
   org_code             varchar(255)         null default '',
   department_code      varchar(255)         null default '',
   personal_code        varchar(255)         null default '',
   personal_id          varchar(255)         null default '',
   region_code          varchar(255)         not null default '',
   area_code            varchar(255)         null default '',
   city_code            varchar(255)         not null default '',
   province_code        varchar(255)         not null default '',
   name                 varchar(255)         not null default '',
   parent_id            varchar(255)         null default '',
   parent_code          varchar(255)         null default '',
   parent_tree_id       varchar(1023)        null default '',
   parent_tree_code     varchar(1023)        null default '',
   parent_tree_name     varchar(1023)        null default '',
   country_code         varchar(255)         null default '',
   constraint pk_c1_region primary key (region_id)
);

comment on table c1_region is
'区域';

comment on column c1_region.region_id is
'区域标识';

comment on column c1_region.province_id is
'省份标识';

comment on column c1_region.city_id is
'城市标识';

comment on column c1_region.area_id is
'地区标识';

comment on column c1_region.begin_date is
'开始日期';

comment on column c1_region.end_date is
'结束日期';

comment on column c1_region.created_by_id is
'创建人标识';

comment on column c1_region.created_by_code is
'创建人编号';

comment on column c1_region.created_date is
'创建日期';

comment on column c1_region.last_modified_by_id is
'最后修改人标识';

comment on column c1_region.last_modified_by_code is
'最后修改人编号';

comment on column c1_region.last_modified_date is
'最后修改日期';

comment on column c1_region.flag_code is
'标记编号(数据字典：工作流、同步、生成、录入、审批)';

comment on column c1_region.batch_no is
'批次编号';

comment on column c1_region.flag_object_code is
'标识对象编号(工作流：工作流标识、同步：批次号、生成：操作者编号、录入：操作者编号、审批：操作者编号)';

comment on column c1_region.state_code is
'状态编号(数据字典)';

comment on column c1_region.data_state_code is
'数据状态编号(数据字典)';

comment on column c1_region.order_by_ is
'排序';

comment on column c1_region.json_data is
'json数据';

comment on column c1_region.version_ is
'版本';

comment on column c1_region.system_info_code is
'系统信息编号';

comment on column c1_region.site_code is
'站点编号';

comment on column c1_region.org_code is
'组织编号';

comment on column c1_region.department_code is
'部门编号';

comment on column c1_region.personal_code is
'个人编号';

comment on column c1_region.personal_id is
'个人标识';

comment on column c1_region.region_code is
'区域编号';

comment on column c1_region.area_code is
'地区编号';

comment on column c1_region.city_code is
'城市编号';

comment on column c1_region.province_code is
'省份编号';

comment on column c1_region.name is
'名称';

comment on column c1_region.parent_id is
'父级标识';

comment on column c1_region.parent_code is
'父级编号';

comment on column c1_region.parent_tree_id is
'父级树标识';

comment on column c1_region.parent_tree_code is
'父级树编号';

comment on column c1_region.parent_tree_name is
'父级树名称';

comment on column c1_region.country_code is
'国家编号(数据字典)';

/*==============================================================*/
/* Table: c1_role                                               */
/*==============================================================*/
create table c1_role (
   role_id              varchar(255)         not null default '',
   begin_date           date                 null default current_timestamp,
   end_date             date                 null default to_timestamp('9999-01-01 00:00:00', 'yyyy-mm-dd hh24:mi:ss'),
   created_by_id        varchar(255)         null default '',
   created_by_code      varchar(255)         null default '',
   created_date         date                 null default current_timestamp,
   last_modified_by_id  varchar(255)         null default '',
   last_modified_by_code varchar(255)         null default '',
   last_modified_date   date                 null default current_timestamp,
   flag_code            varchar(255)         null default 'init',
   batch_no             varchar(255)         null default '',
   flag_object_code     varchar(255)         null default '',
   state_code           varchar(255)         null default '1',
   data_state_code      varchar(255)         null default '1',
   order_by_            int8                 null default 0,
   json_data            json                 null,
   version_             int4                 null default 1,
   system_info_code     varchar(255)         null default '',
   site_code            varchar(255)         null default '',
   org_code             varchar(255)         null default '',
   department_code      varchar(255)         null default '',
   personal_code        varchar(255)         null default '',
   personal_id          varchar(255)         null default '',
   type_code            varchar(255)         null default '',
   role_code            varchar(255)         not null default '',
   name                 varchar(255)         null default '',
   parent_code          varchar(255)         null default '',
   parent_id            varchar(255)         null default '',
   parent_tree_id       varchar(1023)        null default '',
   parent_tree_code     varchar(1023)        null default '',
   parent_tree_name     varchar(1023)        null default '',
   constraint pk_c1_role primary key (role_id)
);

comment on table c1_role is
'角色';

comment on column c1_role.role_id is
'角色标识';

comment on column c1_role.begin_date is
'开始日期';

comment on column c1_role.end_date is
'结束日期';

comment on column c1_role.created_by_id is
'创建人标识';

comment on column c1_role.created_by_code is
'创建人编号';

comment on column c1_role.created_date is
'创建日期';

comment on column c1_role.last_modified_by_id is
'最后修改人标识';

comment on column c1_role.last_modified_by_code is
'最后修改人编号';

comment on column c1_role.last_modified_date is
'最后修改日期';

comment on column c1_role.flag_code is
'标记编号(数据字典：工作流、同步、生成、录入、审批)';

comment on column c1_role.batch_no is
'批次编号';

comment on column c1_role.flag_object_code is
'标识对象编号(工作流：工作流标识、同步：批次号、生成：操作者编号、录入：操作者编号、审批：操作者编号)';

comment on column c1_role.state_code is
'状态编号(数据字典)';

comment on column c1_role.data_state_code is
'数据状态编号(数据字典)';

comment on column c1_role.order_by_ is
'排序';

comment on column c1_role.json_data is
'json数据';

comment on column c1_role.version_ is
'版本';

comment on column c1_role.system_info_code is
'系统信息编号';

comment on column c1_role.site_code is
'站点编号';

comment on column c1_role.org_code is
'组织编号';

comment on column c1_role.department_code is
'部门编号';

comment on column c1_role.personal_code is
'个人编号';

comment on column c1_role.personal_id is
'个人标识';

comment on column c1_role.type_code is
'类别编号(数据字典)';

comment on column c1_role.role_code is
'角色编号';

comment on column c1_role.name is
'名称';

comment on column c1_role.parent_code is
'父级编号';

comment on column c1_role.parent_id is
'父级标识';

comment on column c1_role.parent_tree_id is
'父级树标识';

comment on column c1_role.parent_tree_code is
'父级树编号';

comment on column c1_role.parent_tree_name is
'父级树名称';

/*==============================================================*/
/* Table: c1_role_permission                                    */
/*==============================================================*/
create table c1_role_permission (
   role_permission_id   varchar(255)         not null default '',
   permission_id        varchar(255)         null default '',
   role_id              varchar(255)         null default '',
   begin_date           date                 null default current_timestamp,
   end_date             date                 null default to_timestamp('9999-01-01 00:00:00', 'yyyy-mm-dd hh24:mi:ss'),
   created_by_id        varchar(255)         null default '',
   created_by_code      varchar(255)         null default '',
   created_date         date                 null default current_timestamp,
   last_modified_by_id  varchar(255)         null default '',
   last_modified_by_code varchar(255)         null default '',
   last_modified_date   date                 null default current_timestamp,
   flag_code            varchar(255)         null default 'init',
   batch_no             varchar(255)         null default '',
   flag_object_code     varchar(255)         null default '',
   state_code           varchar(255)         null default '1',
   data_state_code      varchar(255)         null default '1',
   order_by_            int8                 null default 0,
   json_data            json                 null,
   version_             int4                 null default 1,
   system_info_code     varchar(255)         null default '',
   site_code            varchar(255)         null default '',
   org_code             varchar(255)         null default '',
   department_code      varchar(255)         null default '',
   personal_code        varchar(255)         null default '',
   personal_id          varchar(255)         null default '',
   permission_code      varchar(255)         null default '',
   role_code            varchar(255)         null default '',
   constraint pk_c1_role_permission primary key (role_permission_id)
);

comment on table c1_role_permission is
'角色与许可';

comment on column c1_role_permission.role_permission_id is
'角色与许可标识';

comment on column c1_role_permission.permission_id is
'许可标识';

comment on column c1_role_permission.role_id is
'角色标识';

comment on column c1_role_permission.begin_date is
'开始日期';

comment on column c1_role_permission.end_date is
'结束日期';

comment on column c1_role_permission.created_by_id is
'创建人标识';

comment on column c1_role_permission.created_by_code is
'创建人编号';

comment on column c1_role_permission.created_date is
'创建日期';

comment on column c1_role_permission.last_modified_by_id is
'最后修改人标识';

comment on column c1_role_permission.last_modified_by_code is
'最后修改人编号';

comment on column c1_role_permission.last_modified_date is
'最后修改日期';

comment on column c1_role_permission.flag_code is
'标记编号(数据字典：工作流、同步、生成、录入、审批)';

comment on column c1_role_permission.batch_no is
'批次编号';

comment on column c1_role_permission.flag_object_code is
'标识对象编号(工作流：工作流标识、同步：批次号、生成：操作者编号、录入：操作者编号、审批：操作者编号)';

comment on column c1_role_permission.state_code is
'状态编号(数据字典)';

comment on column c1_role_permission.data_state_code is
'数据状态编号(数据字典)';

comment on column c1_role_permission.order_by_ is
'排序';

comment on column c1_role_permission.json_data is
'json数据';

comment on column c1_role_permission.version_ is
'版本';

comment on column c1_role_permission.system_info_code is
'系统信息编号';

comment on column c1_role_permission.site_code is
'站点编号';

comment on column c1_role_permission.org_code is
'组织编号';

comment on column c1_role_permission.department_code is
'部门编号';

comment on column c1_role_permission.personal_code is
'个人编号';

comment on column c1_role_permission.personal_id is
'个人标识';

comment on column c1_role_permission.permission_code is
'许可编号';

comment on column c1_role_permission.role_code is
'角色编号';

/*==============================================================*/
/* Table: c1_site                                               */
/*==============================================================*/
create table c1_site (
   site_id              varchar(255)         not null default '',
   system_info_id       varchar(255)         null default '',
   begin_date           date                 null default current_timestamp,
   end_date             date                 null default to_timestamp('9999-01-01 00:00:00', 'yyyy-mm-dd hh24:mi:ss'),
   created_by_id        varchar(255)         null default '',
   created_by_code      varchar(255)         null default '',
   created_date         date                 null default current_timestamp,
   last_modified_by_id  varchar(255)         null default '',
   last_modified_by_code varchar(255)         null default '',
   last_modified_date   date                 null default current_timestamp,
   flag_code            varchar(255)         null default 'init',
   batch_no             varchar(255)         null default '',
   flag_object_code     varchar(255)         null default '',
   state_code           varchar(255)         null default '1',
   data_state_code      varchar(255)         null default '1',
   order_by_            int8                 null default 0,
   json_data            json                 null,
   version_             int4                 null default 1,
   system_info_code     varchar(255)         null default '',
   site_code            varchar(255)         null default '',
   org_code             varchar(255)         null default '',
   department_code      varchar(255)         null default '',
   personal_code        varchar(255)         null default '',
   personal_id          varchar(255)         null default '',
   name                 varchar(255)         null default '',
   content              varchar(4095)        null default '',
   explain_             varchar(511)         null default '',
   constraint pk_c1_site primary key (site_id)
);

comment on table c1_site is
'站点';

comment on column c1_site.site_id is
'站点标识';

comment on column c1_site.system_info_id is
'系统信息标识';

comment on column c1_site.begin_date is
'开始日期';

comment on column c1_site.end_date is
'结束日期';

comment on column c1_site.created_by_id is
'创建人标识';

comment on column c1_site.created_by_code is
'创建人编号';

comment on column c1_site.created_date is
'创建日期';

comment on column c1_site.last_modified_by_id is
'最后修改人标识';

comment on column c1_site.last_modified_by_code is
'最后修改人编号';

comment on column c1_site.last_modified_date is
'最后修改日期';

comment on column c1_site.flag_code is
'标记编号(数据字典：工作流、同步、生成、录入、审批)';

comment on column c1_site.batch_no is
'批次编号';

comment on column c1_site.flag_object_code is
'标识对象编号(工作流：工作流标识、同步：批次号、生成：操作者编号、录入：操作者编号、审批：操作者编号)';

comment on column c1_site.state_code is
'状态编号(数据字典)';

comment on column c1_site.data_state_code is
'数据状态编号(数据字典)';

comment on column c1_site.order_by_ is
'排序';

comment on column c1_site.json_data is
'json数据';

comment on column c1_site.version_ is
'版本';

comment on column c1_site.system_info_code is
'系统信息编号';

comment on column c1_site.site_code is
'站点编号';

comment on column c1_site.org_code is
'组织编号';

comment on column c1_site.department_code is
'部门编号';

comment on column c1_site.personal_code is
'个人编号';

comment on column c1_site.personal_id is
'个人标识';

comment on column c1_site.name is
'名称';

comment on column c1_site.content is
'正文';

comment on column c1_site.explain_ is
'解释';

/*==============================================================*/
/* Table: c1_system_info                                        */
/*==============================================================*/
create table c1_system_info (
   system_info_id       varchar(255)         not null default '',
   begin_date           date                 null default current_timestamp,
   end_date             date                 null default to_timestamp('9999-01-01 00:00:00', 'yyyy-mm-dd hh24:mi:ss'),
   created_by_id        varchar(255)         null default '',
   created_by_code      varchar(255)         null default '',
   created_date         date                 null default current_timestamp,
   last_modified_by_id  varchar(255)         null default '',
   last_modified_by_code varchar(255)         null default '',
   last_modified_date   date                 null default current_timestamp,
   flag_code            varchar(255)         null default 'init',
   batch_no             varchar(255)         null default '',
   flag_object_code     varchar(255)         null default '',
   state_code           varchar(255)         null default '1',
   data_state_code      varchar(255)         null default '1',
   order_by_            int8                 null default 0,
   json_data            json                 null,
   version_             int4                 null default 1,
   system_info_code     varchar(255)         null default '',
   site_code            varchar(255)         null default '',
   org_code             varchar(255)         null default '',
   department_code      varchar(255)         null default '',
   personal_code        varchar(255)         null default '',
   personal_id          varchar(255)         null default '',
   name                 varchar(255)         null default '',
   content              varchar(4095)        null default '',
   remark               varchar(511)         null default '',
   constraint pk_c1_system_info primary key (system_info_id)
);

comment on table c1_system_info is
'系统信息';

comment on column c1_system_info.system_info_id is
'系统信息标识';

comment on column c1_system_info.begin_date is
'开始日期';

comment on column c1_system_info.end_date is
'结束日期';

comment on column c1_system_info.created_by_id is
'创建人标识';

comment on column c1_system_info.created_by_code is
'创建人编号';

comment on column c1_system_info.created_date is
'创建日期';

comment on column c1_system_info.last_modified_by_id is
'最后修改人标识';

comment on column c1_system_info.last_modified_by_code is
'最后修改人编号';

comment on column c1_system_info.last_modified_date is
'最后修改日期';

comment on column c1_system_info.flag_code is
'标记编号(数据字典：工作流、同步、生成、录入、审批)';

comment on column c1_system_info.batch_no is
'批次编号';

comment on column c1_system_info.flag_object_code is
'标识对象编号(工作流：工作流标识、同步：批次号、生成：操作者编号、录入：操作者编号、审批：操作者编号)';

comment on column c1_system_info.state_code is
'状态编号(数据字典)';

comment on column c1_system_info.data_state_code is
'数据状态编号(数据字典)';

comment on column c1_system_info.order_by_ is
'排序';

comment on column c1_system_info.json_data is
'json数据';

comment on column c1_system_info.version_ is
'版本';

comment on column c1_system_info.system_info_code is
'系统信息编号';

comment on column c1_system_info.site_code is
'站点编号';

comment on column c1_system_info.org_code is
'组织编号';

comment on column c1_system_info.department_code is
'部门编号';

comment on column c1_system_info.personal_code is
'个人编号';

comment on column c1_system_info.personal_id is
'个人标识';

comment on column c1_system_info.name is
'名称';

comment on column c1_system_info.content is
'正文';

comment on column c1_system_info.remark is
'备注';

/*==============================================================*/
/* Table: c1_token_info                                         */
/*==============================================================*/
create table c1_token_info (
   token_info_id        varchar(255)         not null default '',
   user_id              varchar(255)         null default '',
   begin_date           date                 null default current_timestamp,
   end_date             date                 null default to_timestamp('9999-01-01 00:00:00', 'yyyy-mm-dd hh24:mi:ss'),
   created_by_id        varchar(255)         null default '',
   created_by_code      varchar(255)         null default '',
   created_date         date                 null default current_timestamp,
   last_modified_by_id  varchar(255)         null default '',
   last_modified_by_code varchar(255)         null default '',
   last_modified_date   date                 null default current_timestamp,
   flag_code            varchar(255)         null default 'init',
   batch_no             varchar(255)         null default '',
   flag_object_code     varchar(255)         null default '',
   state_code           varchar(255)         null default '1',
   data_state_code      varchar(255)         null default '1',
   order_by_            int8                 null default 0,
   json_data            json                 null,
   version_             int4                 null default 1,
   system_info_code     varchar(255)         null default '',
   site_code            varchar(255)         null default '',
   org_code             varchar(255)         null default '',
   department_code      varchar(255)         null default '',
   personal_code        varchar(255)         null default '',
   personal_id          varchar(255)         null default '',
   token_info_code      varchar(255)         not null default '',
   client_id            varchar(255)         null default '',
   scope                varchar(511)         null default '',
   user_code            varchar(255)         null default '',
   constraint pk_c1_token_info primary key (token_info_id)
);

comment on table c1_token_info is
'令牌信息';

comment on column c1_token_info.token_info_id is
'令牌信息标识';

comment on column c1_token_info.user_id is
'用户标识';

comment on column c1_token_info.begin_date is
'开始日期';

comment on column c1_token_info.end_date is
'结束日期';

comment on column c1_token_info.created_by_id is
'创建人标识';

comment on column c1_token_info.created_by_code is
'创建人编号';

comment on column c1_token_info.created_date is
'创建日期';

comment on column c1_token_info.last_modified_by_id is
'最后修改人标识';

comment on column c1_token_info.last_modified_by_code is
'最后修改人编号';

comment on column c1_token_info.last_modified_date is
'最后修改日期';

comment on column c1_token_info.flag_code is
'标记编号(数据字典：工作流、同步、生成、录入、审批)';

comment on column c1_token_info.batch_no is
'批次编号';

comment on column c1_token_info.flag_object_code is
'标识对象编号(工作流：工作流标识、同步：批次号、生成：操作者编号、录入：操作者编号、审批：操作者编号)';

comment on column c1_token_info.state_code is
'状态编号(数据字典)';

comment on column c1_token_info.data_state_code is
'数据状态编号(数据字典)';

comment on column c1_token_info.order_by_ is
'排序';

comment on column c1_token_info.json_data is
'json数据';

comment on column c1_token_info.version_ is
'版本';

comment on column c1_token_info.system_info_code is
'系统信息编号';

comment on column c1_token_info.site_code is
'站点编号';

comment on column c1_token_info.org_code is
'组织编号';

comment on column c1_token_info.department_code is
'部门编号';

comment on column c1_token_info.personal_code is
'个人编号';

comment on column c1_token_info.personal_id is
'个人标识';

comment on column c1_token_info.token_info_code is
'令牌信息编号';

comment on column c1_token_info.client_id is
'客户端标识';

comment on column c1_token_info.scope is
'范围';

comment on column c1_token_info.user_code is
'用户编号';

/*==============================================================*/
/* Table: c1_user                                               */
/*==============================================================*/
create table c1_user (
   user_id              varchar(255)         not null default '',
   department_id        varchar(255)         null default '',
   begin_date           date                 null default current_timestamp,
   end_date             date                 null default to_timestamp('9999-01-01 00:00:00', 'yyyy-mm-dd hh24:mi:ss'),
   created_by_id        varchar(255)         null default '',
   created_by_code      varchar(255)         null default '',
   created_date         date                 null default current_timestamp,
   last_modified_by_id  varchar(255)         null default '',
   last_modified_by_code varchar(255)         null default '',
   last_modified_date   date                 null default current_timestamp,
   flag_code            varchar(255)         null default 'init',
   batch_no             varchar(255)         null default '',
   flag_object_code     varchar(255)         null default '',
   state_code           varchar(255)         null default '1',
   data_state_code      varchar(255)         null default '1',
   order_by_            int8                 null default 0,
   json_data            json                 null,
   version_             int4                 null default 1,
   system_info_code     varchar(255)         null default '',
   site_code            varchar(255)         null default '',
   org_code             varchar(255)         null default '',
   department_code      varchar(255)         null default '',
   personal_code        varchar(255)         null default '',
   personal_id          varchar(255)         null default '',
   user_code            varchar(255)         not null default '',
   first_name           varchar(255)         null default '',
   last_name            varchar(255)         null default '',
   name                 varchar(255)         null default '',
   password_            varchar(255)         not null default '',
   password_salt        varchar(255)         null default '',
   email                varchar(255)         null default '',
   age                  int4                 null default 0,
   intro                text                 null default '',
   mobile_phone         varchar(255)         null default '',
   locus                varchar(255)         null default '',
   company_name         varchar(255)         null default '',
   contact              varchar(255)         null default '',
   telephone            varchar(255)         null default '',
   birthday             date                 null default current_timestamp,
   call_name            varchar(255)         null default '',
   profession_code      varchar(255)         null default '',
   marry_state_code     varchar(255)         null default '',
   folk_code            varchar(255)         null default '',
   diploma_code         varchar(255)         null default '',
   country_code         varchar(255)         null default '',
   province_code        varchar(255)         null default '',
   city_code            varchar(255)         null default '',
   area_code            varchar(255)         null default '',
   region_code          varchar(255)         null default '',
   address              varchar(255)         null default '',
   sex_code             varchar(255)         null default '',
   qq                   varchar(255)         null default '',
   id_card_code         varchar(255)         null default '',
   id_card_no           varchar(255)         null default '',
   private_password     varchar(255)         null default '',
   private_password_salt varchar(255)         null default '',
   portrait_url         varchar(255)         null default '',
   remark               varchar(511)         null default '',
   alias_name           varchar(255)         null default '',
   name_pinyin          varchar(255)         null default '',
   constraint pk_c1_user primary key (user_id)
);

comment on table c1_user is
'用户';

comment on column c1_user.user_id is
'用户标识';

comment on column c1_user.department_id is
'部门标识';

comment on column c1_user.begin_date is
'开始日期';

comment on column c1_user.end_date is
'结束日期';

comment on column c1_user.created_by_id is
'创建人标识';

comment on column c1_user.created_by_code is
'创建人编号';

comment on column c1_user.created_date is
'创建日期';

comment on column c1_user.last_modified_by_id is
'最后修改人标识';

comment on column c1_user.last_modified_by_code is
'最后修改人编号';

comment on column c1_user.last_modified_date is
'最后修改日期';

comment on column c1_user.flag_code is
'标记编号(数据字典：工作流、同步、生成、录入、审批)';

comment on column c1_user.batch_no is
'批次编号';

comment on column c1_user.flag_object_code is
'标识对象编号(工作流：工作流标识、同步：批次号、生成：操作者编号、录入：操作者编号、审批：操作者编号)';

comment on column c1_user.state_code is
'状态编号(数据字典)';

comment on column c1_user.data_state_code is
'数据状态编号(数据字典)';

comment on column c1_user.order_by_ is
'排序';

comment on column c1_user.json_data is
'json数据';

comment on column c1_user.version_ is
'版本';

comment on column c1_user.system_info_code is
'系统信息编号';

comment on column c1_user.site_code is
'站点编号';

comment on column c1_user.org_code is
'组织编号';

comment on column c1_user.department_code is
'部门编号';

comment on column c1_user.personal_code is
'个人编号';

comment on column c1_user.personal_id is
'个人标识';

comment on column c1_user.user_code is
'用户编号';

comment on column c1_user.first_name is
'名字';

comment on column c1_user.last_name is
'姓氏';

comment on column c1_user.name is
'名称';

comment on column c1_user.password_ is
'密码';

comment on column c1_user.password_salt is
'密码盐';

comment on column c1_user.email is
'邮箱';

comment on column c1_user.age is
'年龄';

comment on column c1_user.intro is
'简介';

comment on column c1_user.mobile_phone is
'手机';

comment on column c1_user.locus is
'现居住地';

comment on column c1_user.company_name is
'工作单位';

comment on column c1_user.contact is
'联系人';

comment on column c1_user.telephone is
'联系电话';

comment on column c1_user.birthday is
'出生日期';

comment on column c1_user.call_name is
'昵称';

comment on column c1_user.profession_code is
'职业编号';

comment on column c1_user.marry_state_code is
'婚姻状态编号(数据字典)';

comment on column c1_user.folk_code is
'民族编号(数据字典)';

comment on column c1_user.diploma_code is
'学历/文凭编号(数据字典)';

comment on column c1_user.country_code is
'国家编号(数据字典)';

comment on column c1_user.province_code is
'省份编号';

comment on column c1_user.city_code is
'城市编号';

comment on column c1_user.area_code is
'地区编号';

comment on column c1_user.region_code is
'区域编号';

comment on column c1_user.address is
'地址';

comment on column c1_user.sex_code is
'性别编号(数据字典)';

comment on column c1_user.qq is
'qq号码';

comment on column c1_user.id_card_code is
'证件类型编号';

comment on column c1_user.id_card_no is
'证件号';

comment on column c1_user.private_password is
'隐私密码';

comment on column c1_user.private_password_salt is
'隐私密码盐';

comment on column c1_user.portrait_url is
'肖像路径';

comment on column c1_user.remark is
'备注';

comment on column c1_user.alias_name is
'别名';

comment on column c1_user.name_pinyin is
'拼音';

/*==============================================================*/
/* Table: c1_user_address                                       */
/*==============================================================*/
create table c1_user_address (
   user_address_id      varchar(255)         not null default '',
   city_id              varchar(255)         null default '',
   area_id              varchar(255)         null default '',
   region_id            varchar(255)         null default '',
   user_id              varchar(255)         null default '',
   province_id          varchar(255)         null default '',
   begin_date           date                 null default current_timestamp,
   end_date             date                 null default to_timestamp('9999-01-01 00:00:00', 'yyyy-mm-dd hh24:mi:ss'),
   created_by_id        varchar(255)         null default '',
   created_by_code      varchar(255)         null default '',
   created_date         date                 null default current_timestamp,
   last_modified_by_id  varchar(255)         null default '',
   last_modified_by_code varchar(255)         null default '',
   last_modified_date   date                 null default current_timestamp,
   flag_code            varchar(255)         null default 'init',
   batch_no             varchar(255)         null default '',
   flag_object_code     varchar(255)         null default '',
   state_code           varchar(255)         null default '1',
   data_state_code      varchar(255)         null default '1',
   order_by_            int8                 null default 0,
   json_data            json                 null,
   version_             int4                 null default 1,
   system_info_code     varchar(255)         null default '',
   site_code            varchar(255)         null default '',
   org_code             varchar(255)         null default '',
   department_code      varchar(255)         null default '',
   personal_code        varchar(255)         null default '',
   personal_id          varchar(255)         null default '',
   type_code            varchar(255)         null default '',
   is_default           varchar(255)         null default '0',
   country_code         varchar(255)         null default '',
   address              varchar(255)         null default '',
   user_code            varchar(255)         null default '',
   region_code          varchar(255)         not null default '',
   area_code            varchar(255)         null default '',
   city_code            varchar(255)         not null default '',
   province_code        varchar(255)         not null default '',
   constraint pk_c1_user_address primary key (user_address_id)
);

comment on table c1_user_address is
'用户与地址';

comment on column c1_user_address.user_address_id is
'用户与地址标识';

comment on column c1_user_address.city_id is
'城市标识';

comment on column c1_user_address.area_id is
'地区标识';

comment on column c1_user_address.region_id is
'区域标识';

comment on column c1_user_address.user_id is
'用户标识';

comment on column c1_user_address.province_id is
'省份标识';

comment on column c1_user_address.begin_date is
'开始日期';

comment on column c1_user_address.end_date is
'结束日期';

comment on column c1_user_address.created_by_id is
'创建人标识';

comment on column c1_user_address.created_by_code is
'创建人编号';

comment on column c1_user_address.created_date is
'创建日期';

comment on column c1_user_address.last_modified_by_id is
'最后修改人标识';

comment on column c1_user_address.last_modified_by_code is
'最后修改人编号';

comment on column c1_user_address.last_modified_date is
'最后修改日期';

comment on column c1_user_address.flag_code is
'标记编号(数据字典：工作流、同步、生成、录入、审批)';

comment on column c1_user_address.batch_no is
'批次编号';

comment on column c1_user_address.flag_object_code is
'标识对象编号(工作流：工作流标识、同步：批次号、生成：操作者编号、录入：操作者编号、审批：操作者编号)';

comment on column c1_user_address.state_code is
'状态编号(数据字典)';

comment on column c1_user_address.data_state_code is
'数据状态编号(数据字典)';

comment on column c1_user_address.order_by_ is
'排序';

comment on column c1_user_address.json_data is
'json数据';

comment on column c1_user_address.version_ is
'版本';

comment on column c1_user_address.system_info_code is
'系统信息编号';

comment on column c1_user_address.site_code is
'站点编号';

comment on column c1_user_address.org_code is
'组织编号';

comment on column c1_user_address.department_code is
'部门编号';

comment on column c1_user_address.personal_code is
'个人编号';

comment on column c1_user_address.personal_id is
'个人标识';

comment on column c1_user_address.type_code is
'类别编号(数据字典)';

comment on column c1_user_address.is_default is
'是否默认(数据字典)';

comment on column c1_user_address.country_code is
'国家编号(数据字典)';

comment on column c1_user_address.address is
'地址';

comment on column c1_user_address.user_code is
'用户编号';

comment on column c1_user_address.region_code is
'区域编号';

comment on column c1_user_address.area_code is
'地区编号';

comment on column c1_user_address.city_code is
'城市编号';

comment on column c1_user_address.province_code is
'省份编号';

/*==============================================================*/
/* Table: c1_user_department                                    */
/*==============================================================*/
create table c1_user_department (
   user_department_id   varchar(255)         not null default '',
   department_id        varchar(255)         null default '',
   user_id              varchar(255)         null default '',
   begin_date           date                 null default current_timestamp,
   end_date             date                 null default to_timestamp('9999-01-01 00:00:00', 'yyyy-mm-dd hh24:mi:ss'),
   created_by_id        varchar(255)         null default '',
   created_by_code      varchar(255)         null default '',
   created_date         date                 null default current_timestamp,
   last_modified_by_id  varchar(255)         null default '',
   last_modified_by_code varchar(255)         null default '',
   last_modified_date   date                 null default current_timestamp,
   flag_code            varchar(255)         null default 'init',
   batch_no             varchar(255)         null default '',
   flag_object_code     varchar(255)         null default '',
   state_code           varchar(255)         null default '1',
   data_state_code      varchar(255)         null default '1',
   order_by_            int8                 null default 0,
   json_data            json                 null,
   version_             int4                 null default 1,
   system_info_code     varchar(255)         null default '',
   site_code            varchar(255)         null default '',
   org_code             varchar(255)         null default '',
   c1__department_code  varchar(255)         null default '',
   personal_code        varchar(255)         null default '',
   personal_id          varchar(255)         null default '',
   user_code            varchar(255)         null default '',
   department_code      varchar(255)         null default '',
   constraint pk_c1_user_department primary key (user_department_id)
);

comment on table c1_user_department is
'用户与部门';

comment on column c1_user_department.user_department_id is
'用户与部门标识';

comment on column c1_user_department.department_id is
'部门标识';

comment on column c1_user_department.user_id is
'用户标识';

comment on column c1_user_department.begin_date is
'开始日期';

comment on column c1_user_department.end_date is
'结束日期';

comment on column c1_user_department.created_by_id is
'创建人标识';

comment on column c1_user_department.created_by_code is
'创建人编号';

comment on column c1_user_department.created_date is
'创建日期';

comment on column c1_user_department.last_modified_by_id is
'最后修改人标识';

comment on column c1_user_department.last_modified_by_code is
'最后修改人编号';

comment on column c1_user_department.last_modified_date is
'最后修改日期';

comment on column c1_user_department.flag_code is
'标记编号(数据字典：工作流、同步、生成、录入、审批)';

comment on column c1_user_department.batch_no is
'批次编号';

comment on column c1_user_department.flag_object_code is
'标识对象编号(工作流：工作流标识、同步：批次号、生成：操作者编号、录入：操作者编号、审批：操作者编号)';

comment on column c1_user_department.state_code is
'状态编号(数据字典)';

comment on column c1_user_department.data_state_code is
'数据状态编号(数据字典)';

comment on column c1_user_department.order_by_ is
'排序';

comment on column c1_user_department.json_data is
'json数据';

comment on column c1_user_department.version_ is
'版本';

comment on column c1_user_department.system_info_code is
'系统信息编号';

comment on column c1_user_department.site_code is
'站点编号';

comment on column c1_user_department.org_code is
'组织编号';

comment on column c1_user_department.c1__department_code is
'父表_部门编号';

comment on column c1_user_department.personal_code is
'个人编号';

comment on column c1_user_department.personal_id is
'个人标识';

comment on column c1_user_department.user_code is
'用户编号';

comment on column c1_user_department.department_code is
'部门编号';

/*==============================================================*/
/* Table: c1_user_group                                         */
/*==============================================================*/
create table c1_user_group (
   user_group_id        varchar(255)         not null default '',
   begin_date           date                 null default current_timestamp,
   end_date             date                 null default to_timestamp('9999-01-01 00:00:00', 'yyyy-mm-dd hh24:mi:ss'),
   created_by_id        varchar(255)         null default '',
   created_by_code      varchar(255)         null default '',
   created_date         date                 null default current_timestamp,
   last_modified_by_id  varchar(255)         null default '',
   last_modified_by_code varchar(255)         null default '',
   last_modified_date   date                 null default current_timestamp,
   flag_code            varchar(255)         null default 'init',
   batch_no             varchar(255)         null default '',
   flag_object_code     varchar(255)         null default '',
   state_code           varchar(255)         null default '1',
   data_state_code      varchar(255)         null default '1',
   order_by_            int8                 null default 0,
   json_data            json                 null,
   version_             int4                 null default 1,
   system_info_code     varchar(255)         null default '',
   site_code            varchar(255)         null default '',
   org_code             varchar(255)         null default '',
   department_code      varchar(255)         null default '',
   personal_code        varchar(255)         null default '',
   personal_id          varchar(255)         null default '',
   type_code            varchar(255)         null default '',
   user_group_code      varchar(255)         not null default '',
   name                 varchar(255)         null default '',
   parent_id            varchar(255)         null default '',
   parent_code          varchar(255)         null default '',
   parent_tree_id       varchar(1023)        null default '',
   parent_tree_code     varchar(1023)        null default '',
   parent_tree_name     varchar(1023)        null default '',
   constraint pk_c1_user_group primary key (user_group_id)
);

comment on table c1_user_group is
'用户组';

comment on column c1_user_group.user_group_id is
'用户组标识';

comment on column c1_user_group.begin_date is
'开始日期';

comment on column c1_user_group.end_date is
'结束日期';

comment on column c1_user_group.created_by_id is
'创建人标识';

comment on column c1_user_group.created_by_code is
'创建人编号';

comment on column c1_user_group.created_date is
'创建日期';

comment on column c1_user_group.last_modified_by_id is
'最后修改人标识';

comment on column c1_user_group.last_modified_by_code is
'最后修改人编号';

comment on column c1_user_group.last_modified_date is
'最后修改日期';

comment on column c1_user_group.flag_code is
'标记编号(数据字典：工作流、同步、生成、录入、审批)';

comment on column c1_user_group.batch_no is
'批次编号';

comment on column c1_user_group.flag_object_code is
'标识对象编号(工作流：工作流标识、同步：批次号、生成：操作者编号、录入：操作者编号、审批：操作者编号)';

comment on column c1_user_group.state_code is
'状态编号(数据字典)';

comment on column c1_user_group.data_state_code is
'数据状态编号(数据字典)';

comment on column c1_user_group.order_by_ is
'排序';

comment on column c1_user_group.json_data is
'json数据';

comment on column c1_user_group.version_ is
'版本';

comment on column c1_user_group.system_info_code is
'系统信息编号';

comment on column c1_user_group.site_code is
'站点编号';

comment on column c1_user_group.org_code is
'组织编号';

comment on column c1_user_group.department_code is
'部门编号';

comment on column c1_user_group.personal_code is
'个人编号';

comment on column c1_user_group.personal_id is
'个人标识';

comment on column c1_user_group.type_code is
'类别编号(数据字典)';

comment on column c1_user_group.user_group_code is
'用户组编号';

comment on column c1_user_group.name is
'名称';

comment on column c1_user_group.parent_id is
'父级标识';

comment on column c1_user_group.parent_code is
'父级编号';

comment on column c1_user_group.parent_tree_id is
'父级树标识';

comment on column c1_user_group.parent_tree_code is
'父级树编号';

comment on column c1_user_group.parent_tree_name is
'父级树名称';

/*==============================================================*/
/* Table: c1_user_group_permission                              */
/*==============================================================*/
create table c1_user_group_permission (
   user_group_permission_id varchar(255)         not null default '',
   user_group_id        varchar(255)         null default '',
   permission_id        varchar(255)         null default '',
   begin_date           date                 null default current_timestamp,
   end_date             date                 null default to_timestamp('9999-01-01 00:00:00', 'yyyy-mm-dd hh24:mi:ss'),
   created_by_id        varchar(255)         null default '',
   created_by_code      varchar(255)         null default '',
   created_date         date                 null default current_timestamp,
   last_modified_by_id  varchar(255)         null default '',
   last_modified_by_code varchar(255)         null default '',
   last_modified_date   date                 null default current_timestamp,
   flag_code            varchar(255)         null default 'init',
   batch_no             varchar(255)         null default '',
   flag_object_code     varchar(255)         null default '',
   state_code           varchar(255)         null default '1',
   data_state_code      varchar(255)         null default '1',
   order_by_            int8                 null default 0,
   json_data            json                 null,
   version_             int4                 null default 1,
   system_info_code     varchar(255)         null default '',
   site_code            varchar(255)         null default '',
   org_code             varchar(255)         null default '',
   department_code      varchar(255)         null default '',
   personal_code        varchar(255)         null default '',
   personal_id          varchar(255)         null default '',
   permission_code      varchar(255)         null default '',
   user_group_code      varchar(255)         null default '',
   constraint pk_c1_user_group_permission primary key (user_group_permission_id)
);

comment on table c1_user_group_permission is
'用户组与许可';

comment on column c1_user_group_permission.user_group_permission_id is
'用户组与许可标识';

comment on column c1_user_group_permission.user_group_id is
'用户组标识';

comment on column c1_user_group_permission.permission_id is
'许可标识';

comment on column c1_user_group_permission.begin_date is
'开始日期';

comment on column c1_user_group_permission.end_date is
'结束日期';

comment on column c1_user_group_permission.created_by_id is
'创建人标识';

comment on column c1_user_group_permission.created_by_code is
'创建人编号';

comment on column c1_user_group_permission.created_date is
'创建日期';

comment on column c1_user_group_permission.last_modified_by_id is
'最后修改人标识';

comment on column c1_user_group_permission.last_modified_by_code is
'最后修改人编号';

comment on column c1_user_group_permission.last_modified_date is
'最后修改日期';

comment on column c1_user_group_permission.flag_code is
'标记编号(数据字典：工作流、同步、生成、录入、审批)';

comment on column c1_user_group_permission.batch_no is
'批次编号';

comment on column c1_user_group_permission.flag_object_code is
'标识对象编号(工作流：工作流标识、同步：批次号、生成：操作者编号、录入：操作者编号、审批：操作者编号)';

comment on column c1_user_group_permission.state_code is
'状态编号(数据字典)';

comment on column c1_user_group_permission.data_state_code is
'数据状态编号(数据字典)';

comment on column c1_user_group_permission.order_by_ is
'排序';

comment on column c1_user_group_permission.json_data is
'json数据';

comment on column c1_user_group_permission.version_ is
'版本';

comment on column c1_user_group_permission.system_info_code is
'系统信息编号';

comment on column c1_user_group_permission.site_code is
'站点编号';

comment on column c1_user_group_permission.org_code is
'组织编号';

comment on column c1_user_group_permission.department_code is
'部门编号';

comment on column c1_user_group_permission.personal_code is
'个人编号';

comment on column c1_user_group_permission.personal_id is
'个人标识';

comment on column c1_user_group_permission.permission_code is
'许可编号';

comment on column c1_user_group_permission.user_group_code is
'用户组编号';

/*==============================================================*/
/* Table: c1_user_group_role                                    */
/*==============================================================*/
create table c1_user_group_role (
   user_group_role_id   varchar(255)         not null default '',
   role_id              varchar(255)         null default '',
   user_group_id        varchar(255)         null default '',
   begin_date           date                 null default current_timestamp,
   end_date             date                 null default to_timestamp('9999-01-01 00:00:00', 'yyyy-mm-dd hh24:mi:ss'),
   created_by_id        varchar(255)         null default '',
   created_by_code      varchar(255)         null default '',
   created_date         date                 null default current_timestamp,
   last_modified_by_id  varchar(255)         null default '',
   last_modified_by_code varchar(255)         null default '',
   last_modified_date   date                 null default current_timestamp,
   flag_code            varchar(255)         null default 'init',
   batch_no             varchar(255)         null default '',
   flag_object_code     varchar(255)         null default '',
   state_code           varchar(255)         null default '1',
   data_state_code      varchar(255)         null default '1',
   order_by_            int8                 null default 0,
   json_data            json                 null,
   version_             int4                 null default 1,
   system_info_code     varchar(255)         null default '',
   site_code            varchar(255)         null default '',
   org_code             varchar(255)         null default '',
   department_code      varchar(255)         null default '',
   personal_code        varchar(255)         null default '',
   personal_id          varchar(255)         null default '',
   role_code            varchar(255)         null default '',
   user_group_code      varchar(255)         null default '',
   constraint pk_c1_user_group_role primary key (user_group_role_id)
);

comment on table c1_user_group_role is
'用户组与角色';

comment on column c1_user_group_role.user_group_role_id is
'用户组与角色标识';

comment on column c1_user_group_role.role_id is
'角色标识';

comment on column c1_user_group_role.user_group_id is
'用户组标识';

comment on column c1_user_group_role.begin_date is
'开始日期';

comment on column c1_user_group_role.end_date is
'结束日期';

comment on column c1_user_group_role.created_by_id is
'创建人标识';

comment on column c1_user_group_role.created_by_code is
'创建人编号';

comment on column c1_user_group_role.created_date is
'创建日期';

comment on column c1_user_group_role.last_modified_by_id is
'最后修改人标识';

comment on column c1_user_group_role.last_modified_by_code is
'最后修改人编号';

comment on column c1_user_group_role.last_modified_date is
'最后修改日期';

comment on column c1_user_group_role.flag_code is
'标记编号(数据字典：工作流、同步、生成、录入、审批)';

comment on column c1_user_group_role.batch_no is
'批次编号';

comment on column c1_user_group_role.flag_object_code is
'标识对象编号(工作流：工作流标识、同步：批次号、生成：操作者编号、录入：操作者编号、审批：操作者编号)';

comment on column c1_user_group_role.state_code is
'状态编号(数据字典)';

comment on column c1_user_group_role.data_state_code is
'数据状态编号(数据字典)';

comment on column c1_user_group_role.order_by_ is
'排序';

comment on column c1_user_group_role.json_data is
'json数据';

comment on column c1_user_group_role.version_ is
'版本';

comment on column c1_user_group_role.system_info_code is
'系统信息编号';

comment on column c1_user_group_role.site_code is
'站点编号';

comment on column c1_user_group_role.org_code is
'组织编号';

comment on column c1_user_group_role.department_code is
'部门编号';

comment on column c1_user_group_role.personal_code is
'个人编号';

comment on column c1_user_group_role.personal_id is
'个人标识';

comment on column c1_user_group_role.role_code is
'角色编号';

comment on column c1_user_group_role.user_group_code is
'用户组编号';

/*==============================================================*/
/* Table: c1_user_group_user                                    */
/*==============================================================*/
create table c1_user_group_user (
   user_group_user_id   varchar(255)         not null default '',
   user_group_id        varchar(255)         null default '',
   user_id              varchar(255)         null default '',
   begin_date           date                 null default current_timestamp,
   end_date             date                 null default to_timestamp('9999-01-01 00:00:00', 'yyyy-mm-dd hh24:mi:ss'),
   created_by_id        varchar(255)         null default '',
   created_by_code      varchar(255)         null default '',
   created_date         date                 null default current_timestamp,
   last_modified_by_id  varchar(255)         null default '',
   last_modified_by_code varchar(255)         null default '',
   last_modified_date   date                 null default current_timestamp,
   flag_code            varchar(255)         null default 'init',
   batch_no             varchar(255)         null default '',
   flag_object_code     varchar(255)         null default '',
   state_code           varchar(255)         null default '1',
   data_state_code      varchar(255)         null default '1',
   order_by_            int8                 null default 0,
   json_data            json                 null,
   version_             int4                 null default 1,
   system_info_code     varchar(255)         null default '',
   site_code            varchar(255)         null default '',
   org_code             varchar(255)         null default '',
   department_code      varchar(255)         null default '',
   personal_code        varchar(255)         null default '',
   personal_id          varchar(255)         null default '',
   user_code            varchar(255)         null default '',
   user_group_code      varchar(255)         null default '',
   constraint pk_c1_user_group_user primary key (user_group_user_id)
);

comment on table c1_user_group_user is
'用户组与用户';

comment on column c1_user_group_user.user_group_user_id is
'用户组与用户标识';

comment on column c1_user_group_user.user_group_id is
'用户组标识';

comment on column c1_user_group_user.user_id is
'用户标识';

comment on column c1_user_group_user.begin_date is
'开始日期';

comment on column c1_user_group_user.end_date is
'结束日期';

comment on column c1_user_group_user.created_by_id is
'创建人标识';

comment on column c1_user_group_user.created_by_code is
'创建人编号';

comment on column c1_user_group_user.created_date is
'创建日期';

comment on column c1_user_group_user.last_modified_by_id is
'最后修改人标识';

comment on column c1_user_group_user.last_modified_by_code is
'最后修改人编号';

comment on column c1_user_group_user.last_modified_date is
'最后修改日期';

comment on column c1_user_group_user.flag_code is
'标记编号(数据字典：工作流、同步、生成、录入、审批)';

comment on column c1_user_group_user.batch_no is
'批次编号';

comment on column c1_user_group_user.flag_object_code is
'标识对象编号(工作流：工作流标识、同步：批次号、生成：操作者编号、录入：操作者编号、审批：操作者编号)';

comment on column c1_user_group_user.state_code is
'状态编号(数据字典)';

comment on column c1_user_group_user.data_state_code is
'数据状态编号(数据字典)';

comment on column c1_user_group_user.order_by_ is
'排序';

comment on column c1_user_group_user.json_data is
'json数据';

comment on column c1_user_group_user.version_ is
'版本';

comment on column c1_user_group_user.system_info_code is
'系统信息编号';

comment on column c1_user_group_user.site_code is
'站点编号';

comment on column c1_user_group_user.org_code is
'组织编号';

comment on column c1_user_group_user.department_code is
'部门编号';

comment on column c1_user_group_user.personal_code is
'个人编号';

comment on column c1_user_group_user.personal_id is
'个人标识';

comment on column c1_user_group_user.user_code is
'用户编号';

comment on column c1_user_group_user.user_group_code is
'用户组编号';

/*==============================================================*/
/* Table: c1_user_org                                           */
/*==============================================================*/
create table c1_user_org (
   user_org_id          varchar(255)         not null default '',
   user_id              varchar(255)         null default '',
   org_id               varchar(255)         null default '',
   begin_date           date                 null default current_timestamp,
   end_date             date                 null default to_timestamp('9999-01-01 00:00:00', 'yyyy-mm-dd hh24:mi:ss'),
   created_by_id        varchar(255)         null default '',
   created_by_code      varchar(255)         null default '',
   created_date         date                 null default current_timestamp,
   last_modified_by_id  varchar(255)         null default '',
   last_modified_by_code varchar(255)         null default '',
   last_modified_date   date                 null default current_timestamp,
   flag_code            varchar(255)         null default 'init',
   batch_no             varchar(255)         null default '',
   flag_object_code     varchar(255)         null default '',
   state_code           varchar(255)         null default '1',
   data_state_code      varchar(255)         null default '1',
   order_by_            int8                 null default 0,
   json_data            json                 null,
   version_             int4                 null default 1,
   system_info_code     varchar(255)         null default '',
   site_code            varchar(255)         null default '',
   c1__org_code         varchar(255)         null default '',
   department_code      varchar(255)         null default '',
   personal_code        varchar(255)         null default '',
   personal_id          varchar(255)         null default '',
   org_code             varchar(255)         null default '',
   user_code            varchar(255)         null default '',
   constraint pk_c1_user_org primary key (user_org_id)
);

comment on table c1_user_org is
'用户与组织';

comment on column c1_user_org.user_org_id is
'用户与组织标识';

comment on column c1_user_org.user_id is
'用户标识';

comment on column c1_user_org.org_id is
'组织标识';

comment on column c1_user_org.begin_date is
'开始日期';

comment on column c1_user_org.end_date is
'结束日期';

comment on column c1_user_org.created_by_id is
'创建人标识';

comment on column c1_user_org.created_by_code is
'创建人编号';

comment on column c1_user_org.created_date is
'创建日期';

comment on column c1_user_org.last_modified_by_id is
'最后修改人标识';

comment on column c1_user_org.last_modified_by_code is
'最后修改人编号';

comment on column c1_user_org.last_modified_date is
'最后修改日期';

comment on column c1_user_org.flag_code is
'标记编号(数据字典：工作流、同步、生成、录入、审批)';

comment on column c1_user_org.batch_no is
'批次编号';

comment on column c1_user_org.flag_object_code is
'标识对象编号(工作流：工作流标识、同步：批次号、生成：操作者编号、录入：操作者编号、审批：操作者编号)';

comment on column c1_user_org.state_code is
'状态编号(数据字典)';

comment on column c1_user_org.data_state_code is
'数据状态编号(数据字典)';

comment on column c1_user_org.order_by_ is
'排序';

comment on column c1_user_org.json_data is
'json数据';

comment on column c1_user_org.version_ is
'版本';

comment on column c1_user_org.system_info_code is
'系统信息编号';

comment on column c1_user_org.site_code is
'站点编号';

comment on column c1_user_org.c1__org_code is
'父表_组织编号';

comment on column c1_user_org.department_code is
'部门编号';

comment on column c1_user_org.personal_code is
'个人编号';

comment on column c1_user_org.personal_id is
'个人标识';

comment on column c1_user_org.org_code is
'组织编号';

comment on column c1_user_org.user_code is
'用户编号';

/*==============================================================*/
/* Table: c1_user_permission                                    */
/*==============================================================*/
create table c1_user_permission (
   user_permission_id   varchar(255)         not null default '',
   permission_id        varchar(255)         null default '',
   user_id              varchar(255)         null default '',
   begin_date           date                 null default current_timestamp,
   end_date             date                 null default to_timestamp('9999-01-01 00:00:00', 'yyyy-mm-dd hh24:mi:ss'),
   created_by_id        varchar(255)         null default '',
   created_by_code      varchar(255)         null default '',
   created_date         date                 null default current_timestamp,
   last_modified_by_id  varchar(255)         null default '',
   last_modified_by_code varchar(255)         null default '',
   last_modified_date   date                 null default current_timestamp,
   flag_code            varchar(255)         null default 'init',
   batch_no             varchar(255)         null default '',
   flag_object_code     varchar(255)         null default '',
   state_code           varchar(255)         null default '1',
   data_state_code      varchar(255)         null default '1',
   order_by_            int8                 null default 0,
   json_data            json                 null,
   version_             int4                 null default 1,
   system_info_code     varchar(255)         null default '',
   site_code            varchar(255)         null default '',
   org_code             varchar(255)         null default '',
   department_code      varchar(255)         null default '',
   personal_code        varchar(255)         null default '',
   personal_id          varchar(255)         null default '',
   user_code            varchar(255)         null default '',
   permission_code      varchar(255)         null default '',
   constraint pk_c1_user_permission primary key (user_permission_id)
);

comment on table c1_user_permission is
'用户与许可';

comment on column c1_user_permission.user_permission_id is
'用户与许可标识';

comment on column c1_user_permission.permission_id is
'许可标识';

comment on column c1_user_permission.user_id is
'用户标识';

comment on column c1_user_permission.begin_date is
'开始日期';

comment on column c1_user_permission.end_date is
'结束日期';

comment on column c1_user_permission.created_by_id is
'创建人标识';

comment on column c1_user_permission.created_by_code is
'创建人编号';

comment on column c1_user_permission.created_date is
'创建日期';

comment on column c1_user_permission.last_modified_by_id is
'最后修改人标识';

comment on column c1_user_permission.last_modified_by_code is
'最后修改人编号';

comment on column c1_user_permission.last_modified_date is
'最后修改日期';

comment on column c1_user_permission.flag_code is
'标记编号(数据字典：工作流、同步、生成、录入、审批)';

comment on column c1_user_permission.batch_no is
'批次编号';

comment on column c1_user_permission.flag_object_code is
'标识对象编号(工作流：工作流标识、同步：批次号、生成：操作者编号、录入：操作者编号、审批：操作者编号)';

comment on column c1_user_permission.state_code is
'状态编号(数据字典)';

comment on column c1_user_permission.data_state_code is
'数据状态编号(数据字典)';

comment on column c1_user_permission.order_by_ is
'排序';

comment on column c1_user_permission.json_data is
'json数据';

comment on column c1_user_permission.version_ is
'版本';

comment on column c1_user_permission.system_info_code is
'系统信息编号';

comment on column c1_user_permission.site_code is
'站点编号';

comment on column c1_user_permission.org_code is
'组织编号';

comment on column c1_user_permission.department_code is
'部门编号';

comment on column c1_user_permission.personal_code is
'个人编号';

comment on column c1_user_permission.personal_id is
'个人标识';

comment on column c1_user_permission.user_code is
'用户编号';

comment on column c1_user_permission.permission_code is
'许可编号';

/*==============================================================*/
/* Table: c1_user_role                                          */
/*==============================================================*/
create table c1_user_role (
   user_role_id         varchar(255)         not null default '',
   user_id              varchar(255)         null default '',
   role_id              varchar(255)         null default '',
   begin_date           date                 null default current_timestamp,
   end_date             date                 null default to_timestamp('9999-01-01 00:00:00', 'yyyy-mm-dd hh24:mi:ss'),
   created_by_id        varchar(255)         null default '',
   created_by_code      varchar(255)         null default '',
   created_date         date                 null default current_timestamp,
   last_modified_by_id  varchar(255)         null default '',
   last_modified_by_code varchar(255)         null default '',
   last_modified_date   date                 null default current_timestamp,
   flag_code            varchar(255)         null default 'init',
   batch_no             varchar(255)         null default '',
   flag_object_code     varchar(255)         null default '',
   state_code           varchar(255)         null default '1',
   data_state_code      varchar(255)         null default '1',
   order_by_            int8                 null default 0,
   json_data            json                 null,
   version_             int4                 null default 1,
   system_info_code     varchar(255)         null default '',
   site_code            varchar(255)         null default '',
   org_code             varchar(255)         null default '',
   department_code      varchar(255)         null default '',
   personal_code        varchar(255)         null default '',
   personal_id          varchar(255)         null default '',
   user_code            varchar(255)         null default '',
   role_code            varchar(255)         null default '',
   constraint pk_c1_user_role primary key (user_role_id)
);

comment on table c1_user_role is
'用户与角色';

comment on column c1_user_role.user_role_id is
'用户与角色标识';

comment on column c1_user_role.user_id is
'用户标识';

comment on column c1_user_role.role_id is
'角色标识';

comment on column c1_user_role.begin_date is
'开始日期';

comment on column c1_user_role.end_date is
'结束日期';

comment on column c1_user_role.created_by_id is
'创建人标识';

comment on column c1_user_role.created_by_code is
'创建人编号';

comment on column c1_user_role.created_date is
'创建日期';

comment on column c1_user_role.last_modified_by_id is
'最后修改人标识';

comment on column c1_user_role.last_modified_by_code is
'最后修改人编号';

comment on column c1_user_role.last_modified_date is
'最后修改日期';

comment on column c1_user_role.flag_code is
'标记编号(数据字典：工作流、同步、生成、录入、审批)';

comment on column c1_user_role.batch_no is
'批次编号';

comment on column c1_user_role.flag_object_code is
'标识对象编号(工作流：工作流标识、同步：批次号、生成：操作者编号、录入：操作者编号、审批：操作者编号)';

comment on column c1_user_role.state_code is
'状态编号(数据字典)';

comment on column c1_user_role.data_state_code is
'数据状态编号(数据字典)';

comment on column c1_user_role.order_by_ is
'排序';

comment on column c1_user_role.json_data is
'json数据';

comment on column c1_user_role.version_ is
'版本';

comment on column c1_user_role.system_info_code is
'系统信息编号';

comment on column c1_user_role.site_code is
'站点编号';

comment on column c1_user_role.org_code is
'组织编号';

comment on column c1_user_role.department_code is
'部门编号';

comment on column c1_user_role.personal_code is
'个人编号';

comment on column c1_user_role.personal_id is
'个人标识';

comment on column c1_user_role.user_code is
'用户编号';

comment on column c1_user_role.role_code is
'角色编号';

/*==============================================================*/
/* Table: c1_wf_variable                                        */
/*==============================================================*/
create table c1_wf_variable (
   wf_variable_id       varchar(255)         not null default '',
   begin_date           date                 null default current_timestamp,
   end_date             date                 null default to_timestamp('9999-01-01 00:00:00', 'yyyy-mm-dd hh24:mi:ss'),
   created_by_id        varchar(255)         null default '',
   created_by_code      varchar(255)         null default '',
   created_date         date                 null default current_timestamp,
   last_modified_by_id  varchar(255)         null default '',
   last_modified_by_code varchar(255)         null default '',
   last_modified_date   date                 null default current_timestamp,
   flag_code            varchar(255)         null default 'init',
   batch_no             varchar(255)         null default '',
   flag_object_code     varchar(255)         null default '',
   state_code           varchar(255)         null default '1',
   data_state_code      varchar(255)         null default '1',
   order_by_            int8                 null default 0,
   json_data            json                 null,
   version_             int4                 null default 1,
   system_info_code     varchar(255)         null default '',
   site_code            varchar(255)         null default '',
   org_code             varchar(255)         null default '',
   department_code      varchar(255)         null default '',
   personal_code        varchar(255)         null default '',
   personal_id          varchar(255)         null default '',
   wf_key               varchar(255)         null default '',
   content              varchar(4095)        null default '',
   constraint pk_c1_wf_variable primary key (wf_variable_id)
);

comment on table c1_wf_variable is
'工作流变量';

comment on column c1_wf_variable.wf_variable_id is
'工作流变量标识';

comment on column c1_wf_variable.begin_date is
'开始日期';

comment on column c1_wf_variable.end_date is
'结束日期';

comment on column c1_wf_variable.created_by_id is
'创建人标识';

comment on column c1_wf_variable.created_by_code is
'创建人编号';

comment on column c1_wf_variable.created_date is
'创建日期';

comment on column c1_wf_variable.last_modified_by_id is
'最后修改人标识';

comment on column c1_wf_variable.last_modified_by_code is
'最后修改人编号';

comment on column c1_wf_variable.last_modified_date is
'最后修改日期';

comment on column c1_wf_variable.flag_code is
'标记编号(数据字典：工作流、同步、生成、录入、审批)';

comment on column c1_wf_variable.batch_no is
'批次编号';

comment on column c1_wf_variable.flag_object_code is
'标识对象编号(工作流：工作流标识、同步：批次号、生成：操作者编号、录入：操作者编号、审批：操作者编号)';

comment on column c1_wf_variable.state_code is
'状态编号(数据字典)';

comment on column c1_wf_variable.data_state_code is
'数据状态编号(数据字典)';

comment on column c1_wf_variable.order_by_ is
'排序';

comment on column c1_wf_variable.json_data is
'json数据';

comment on column c1_wf_variable.version_ is
'版本';

comment on column c1_wf_variable.system_info_code is
'系统信息编号';

comment on column c1_wf_variable.site_code is
'站点编号';

comment on column c1_wf_variable.org_code is
'组织编号';

comment on column c1_wf_variable.department_code is
'部门编号';

comment on column c1_wf_variable.personal_code is
'个人编号';

comment on column c1_wf_variable.personal_id is
'个人标识';

comment on column c1_wf_variable.wf_key is
'工作流键';

comment on column c1_wf_variable.content is
'正文';

/*==============================================================*/
/* Table: c1_wf_variable_user                                   */
/*==============================================================*/
create table c1_wf_variable_user (
   wf_variable_user_id  varchar(255)         not null default '',
   user_id              varchar(255)         null default '',
   begin_date           date                 null default current_timestamp,
   end_date             date                 null default to_timestamp('9999-01-01 00:00:00', 'yyyy-mm-dd hh24:mi:ss'),
   created_by_id        varchar(255)         null default '',
   created_by_code      varchar(255)         null default '',
   created_date         date                 null default current_timestamp,
   last_modified_by_id  varchar(255)         null default '',
   last_modified_by_code varchar(255)         null default '',
   last_modified_date   date                 null default current_timestamp,
   flag_code            varchar(255)         null default 'init',
   batch_no             varchar(255)         null default '',
   flag_object_code     varchar(255)         null default '',
   state_code           varchar(255)         null default '1',
   data_state_code      varchar(255)         null default '1',
   order_by_            int8                 null default 0,
   json_data            json                 null,
   version_             int4                 null default 1,
   system_info_code     varchar(255)         null default '',
   site_code            varchar(255)         null default '',
   org_code             varchar(255)         null default '',
   department_code      varchar(255)         null default '',
   personal_code        varchar(255)         null default '',
   personal_id          varchar(255)         null default '',
   wf_key               varchar(255)         not null default '',
   user_code            varchar(255)         null default '',
   constraint pk_c1_wf_variable_user primary key (wf_variable_user_id)
);

comment on table c1_wf_variable_user is
'工作流变量与用户';

comment on column c1_wf_variable_user.wf_variable_user_id is
'工作流变量与用户标识';

comment on column c1_wf_variable_user.user_id is
'用户标识';

comment on column c1_wf_variable_user.begin_date is
'开始日期';

comment on column c1_wf_variable_user.end_date is
'结束日期';

comment on column c1_wf_variable_user.created_by_id is
'创建人标识';

comment on column c1_wf_variable_user.created_by_code is
'创建人编号';

comment on column c1_wf_variable_user.created_date is
'创建日期';

comment on column c1_wf_variable_user.last_modified_by_id is
'最后修改人标识';

comment on column c1_wf_variable_user.last_modified_by_code is
'最后修改人编号';

comment on column c1_wf_variable_user.last_modified_date is
'最后修改日期';

comment on column c1_wf_variable_user.flag_code is
'标记编号(数据字典：工作流、同步、生成、录入、审批)';

comment on column c1_wf_variable_user.batch_no is
'批次编号';

comment on column c1_wf_variable_user.flag_object_code is
'标识对象编号(工作流：工作流标识、同步：批次号、生成：操作者编号、录入：操作者编号、审批：操作者编号)';

comment on column c1_wf_variable_user.state_code is
'状态编号(数据字典)';

comment on column c1_wf_variable_user.data_state_code is
'数据状态编号(数据字典)';

comment on column c1_wf_variable_user.order_by_ is
'排序';

comment on column c1_wf_variable_user.json_data is
'json数据';

comment on column c1_wf_variable_user.version_ is
'版本';

comment on column c1_wf_variable_user.system_info_code is
'系统信息编号';

comment on column c1_wf_variable_user.site_code is
'站点编号';

comment on column c1_wf_variable_user.org_code is
'组织编号';

comment on column c1_wf_variable_user.department_code is
'部门编号';

comment on column c1_wf_variable_user.personal_code is
'个人编号';

comment on column c1_wf_variable_user.personal_id is
'个人标识';

comment on column c1_wf_variable_user.wf_key is
'工作流键';

comment on column c1_wf_variable_user.user_code is
'用户编号';

