﻿DROP TABLE IF EXISTS C1_DICTIONARY;

DROP TABLE IF EXISTS C1_DICTIONARY_TYPE;

DROP TABLE IF EXISTS C1_ERROR_INFO;

DROP TABLE IF EXISTS C1_I18N_INFO;

DROP TABLE IF EXISTS C1_SITE;

DROP TABLE IF EXISTS C1_SYSTEM_INFO;

/*==============================================================*/
/* Table: C1_DICTIONARY                                         */
/*==============================================================*/
CREATE TABLE C1_DICTIONARY
(
   DICTIONARY_ID        VARCHAR(36) NOT NULL COMMENT '字典标识',
   SYSTEM_INFO_CODE     VARCHAR(36) COMMENT '系统信息代码',
   SITE_CODE            VARCHAR(36) COMMENT '站点编号',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_CODE      VARCHAR(36) COMMENT '创建人代码',
   CREATED_DATE         DATETIME COMMENT '创建时间',
   LAST_MODIFIED_BY_CODE VARCHAR(36) COMMENT '最后修改人代码',
   LAST_MODIFIED_DATE   DATETIME COMMENT '最后修改时间',
   STATE_CODE           VARCHAR(36) COMMENT '状态代码',
   DATA_STATE_CODE      VARCHAR(36) COMMENT '数据状态代码',
   VERSION_             INT DEFAULT 0 COMMENT '版本',
   WF_ID                VARCHAR(36) COMMENT '工作流标识',
   ROLE_CODES           VARCHAR(1024) COMMENT '角色代码集合',
   PERMISSION_CODES     VARCHAR(1024) COMMENT '许可代码集合',
   ORG_CODE             VARCHAR(36) COMMENT '组织代码',
   DEPARTMENT_CODE      VARCHAR(36) COMMENT '部门代码',
   DICTIONARY_CODE      VARCHAR(36) NOT NULL COMMENT '字典代码',
   NAME                 VARCHAR(256) COMMENT '名称',
   VALUE_               VARCHAR(1024) COMMENT '值',
   ORDER_BY_               BIGINT DEFAULT 0 COMMENT '排序',
   REMARK               VARCHAR(512) COMMENT '说明',
   EXPLAIN_             VARCHAR(512) COMMENT '解释',
   DICTIONARY_TYPE_CODE VARCHAR(36) NOT NULL COMMENT '字典类别代码',
   PRIMARY KEY (DICTIONARY_ID)
);

ALTER TABLE C1_DICTIONARY COMMENT '字典';

/*==============================================================*/
/* Table: C1_DICTIONARY_TYPE                                    */
/*==============================================================*/
CREATE TABLE C1_DICTIONARY_TYPE
(
   DICTIONARY_TYPE_ID   VARCHAR(36) NOT NULL COMMENT '字典类别标识',
   SYSTEM_INFO_CODE     VARCHAR(36) COMMENT '系统信息代码',
   SITE_CODE            VARCHAR(36) COMMENT '站点编号',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_CODE      VARCHAR(36) COMMENT '创建人代码',
   CREATED_DATE         DATETIME COMMENT '创建时间',
   LAST_MODIFIED_BY_CODE VARCHAR(36) COMMENT '最后修改人代码',
   LAST_MODIFIED_DATE   DATETIME COMMENT '最后修改时间',
   STATE_CODE           VARCHAR(36) COMMENT '状态代码',
   DATA_STATE_CODE      VARCHAR(36) COMMENT '数据状态代码',
   VERSION_             INT DEFAULT 0 COMMENT '版本',
   WF_ID                VARCHAR(36) COMMENT '工作流标识',
   ROLE_CODES           VARCHAR(1024) COMMENT '角色代码集合',
   PERMISSION_CODES     VARCHAR(1024) COMMENT '许可代码集合',
   ORG_CODE             VARCHAR(36) COMMENT '组织代码',
   DEPARTMENT_CODE      VARCHAR(36) COMMENT '部门代码',
   DICTIONARY_TYPE_CODE VARCHAR(36) NOT NULL COMMENT '字典类别代码',
   NAME                 VARCHAR(256) COMMENT '名称',
   REMARK               VARCHAR(512) COMMENT '说明',
   ORDER_BY_               BIGINT DEFAULT 0 COMMENT '排序',
   PARENT_CODE          VARCHAR(36) COMMENT '父级代码',
   PARENT_TREE_CODE     VARCHAR(1024) COMMENT '父级树代码',
   PARENT_TREE_NAME     VARCHAR(1024) COMMENT '父级树代名称',
   PRIMARY KEY (DICTIONARY_TYPE_ID)
);

ALTER TABLE C1_DICTIONARY_TYPE COMMENT '字典类别';

/*==============================================================*/
/* Table: C1_ERROR_INFO                                         */
/*==============================================================*/
CREATE TABLE C1_ERROR_INFO
(
   ERROR_INFO_ID        VARCHAR(36) NOT NULL COMMENT '错误信息标识',
   SYSTEM_INFO_CODE     VARCHAR(36) COMMENT '系统信息代码',
   SITE_CODE            VARCHAR(36) COMMENT '站点编号',
   ORG_CODE             VARCHAR(36) COMMENT '组织代码',
   DEPARTMENT_CODE      VARCHAR(36) COMMENT '部门代码',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_CODE      VARCHAR(36) COMMENT '创建人代码',
   CREATED_DATE         DATETIME COMMENT '创建时间',
   LAST_MODIFIED_BY_CODE VARCHAR(36) COMMENT '最后修改人代码',
   LAST_MODIFIED_DATE   DATETIME COMMENT '最后修改时间',
   STATE_CODE           VARCHAR(36) COMMENT '状态代码',
   DATA_STATE_CODE      VARCHAR(36) COMMENT '数据状态代码',
   VERSION_             INT DEFAULT 0 COMMENT '版本',
   WF_ID                VARCHAR(36) COMMENT '工作流标识',
   ROLE_CODES           VARCHAR(1024) COMMENT '角色代码集合',
   PERMISSION_CODES     VARCHAR(1024) COMMENT '许可代码集合',
   ERROR_INFO_CODE      VARCHAR(36) NOT NULL COMMENT '错误信息代码',
   TYPE_CODE            VARCHAR(36) NOT NULL COMMENT '类别代码',
   CONTENT              VARCHAR(4000) COMMENT '正文',
   PRIMARY KEY (ERROR_INFO_ID)
);

ALTER TABLE C1_ERROR_INFO COMMENT '错误信息';

/*==============================================================*/
/* Table: C1_I18N_INFO                                          */
/*==============================================================*/
CREATE TABLE C1_I18N_INFO
(
   I18N_INFO_ID         VARCHAR(36) NOT NULL COMMENT '国际化信息标识',
   SYSTEM_INFO_CODE     VARCHAR(36) COMMENT '系统信息代码',
   SITE_CODE            VARCHAR(36) COMMENT '站点编号',
   ORG_CODE             VARCHAR(36) COMMENT '组织代码',
   DEPARTMENT_CODE      VARCHAR(36) COMMENT '部门代码',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_CODE      VARCHAR(36) COMMENT '创建人代码',
   CREATED_DATE         DATETIME COMMENT '创建时间',
   LAST_MODIFIED_BY_CODE VARCHAR(36) COMMENT '最后修改人代码',
   LAST_MODIFIED_DATE   DATETIME COMMENT '最后修改时间',
   STATE_CODE           VARCHAR(36) COMMENT '状态代码',
   DATA_STATE_CODE      VARCHAR(36) COMMENT '数据状态代码',
   VERSION_             INT DEFAULT 0 COMMENT '版本',
   WF_ID                VARCHAR(36) COMMENT '工作流标识',
   ROLE_CODES           VARCHAR(1024) COMMENT '角色代码集合',
   PERMISSION_CODES     VARCHAR(1024) COMMENT '许可代码集合',
   I18N_INFO_CODE       VARCHAR(36) NOT NULL COMMENT '国际化信息代码',
   TYPE_CODE            VARCHAR(36) NOT NULL COMMENT '类别代码',
   CONTENT              VARCHAR(4000) COMMENT '正文',
   PRIMARY KEY (I18N_INFO_ID)
);

ALTER TABLE C1_I18N_INFO COMMENT '国际化信息';

/*==============================================================*/
/* Table: C1_SITE                                               */
/*==============================================================*/
CREATE TABLE C1_SITE
(
   SITE_ID              VARCHAR(36) NOT NULL COMMENT '站点标识',
   ORG_CODE             VARCHAR(36) COMMENT '组织代码',
   DEPARTMENT_CODE      VARCHAR(36) COMMENT '部门代码',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_CODE      VARCHAR(36) COMMENT '创建人代码',
   CREATED_DATE         DATETIME COMMENT '创建时间',
   LAST_MODIFIED_BY_CODE VARCHAR(36) COMMENT '最后修改人代码',
   LAST_MODIFIED_DATE   DATETIME COMMENT '最后修改时间',
   STATE_CODE           VARCHAR(36) COMMENT '状态代码',
   DATA_STATE_CODE      VARCHAR(36) COMMENT '数据状态代码',
   VERSION_             INT DEFAULT 0 COMMENT '版本',
   WF_ID                VARCHAR(36) COMMENT '工作流标识',
   ROLE_CODES           VARCHAR(1024) COMMENT '角色代码集合',
   PERMISSION_CODES     VARCHAR(1024) COMMENT '许可代码集合',
   SYSTEM_INFO_CODE     VARCHAR(36) COMMENT '系统信息代码',
   SITE_CODE            VARCHAR(36) COMMENT '站点编号',
   NAME                 VARCHAR(256) COMMENT '名称',
   CONTENT              VARCHAR(4000) COMMENT '正文',
   EXPLAIN_             VARCHAR(512) COMMENT '解释',
   PRIMARY KEY (SITE_ID)
);

ALTER TABLE C1_SITE COMMENT '站点';

/*==============================================================*/
/* Table: C1_SYSTEM_INFO                                        */
/*==============================================================*/
CREATE TABLE C1_SYSTEM_INFO
(
   SYSTEM_INFO_ID       VARCHAR(36) NOT NULL COMMENT '系统信息标识',
   ORG_CODE             VARCHAR(36) COMMENT '组织代码',
   DEPARTMENT_CODE      VARCHAR(36) COMMENT '部门代码',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_CODE      VARCHAR(36) COMMENT '创建人代码',
   CREATED_DATE         DATETIME COMMENT '创建时间',
   LAST_MODIFIED_BY_CODE VARCHAR(36) COMMENT '最后修改人代码',
   LAST_MODIFIED_DATE   DATETIME COMMENT '最后修改时间',
   STATE_CODE           VARCHAR(36) COMMENT '状态代码',
   DATA_STATE_CODE      VARCHAR(36) COMMENT '数据状态代码',
   VERSION_             INT DEFAULT 0 COMMENT '版本',
   WF_ID                VARCHAR(36) COMMENT '工作流标识',
   ROLE_CODES           VARCHAR(1024) COMMENT '角色代码集合',
   PERMISSION_CODES     VARCHAR(1024) COMMENT '许可代码集合',
   SYSTEM_INFO_CODE     VARCHAR(36) NOT NULL COMMENT '系统信息代码',
   NAME                 VARCHAR(256) COMMENT '名称',
   CONTENT              VARCHAR(4000) COMMENT '正文',
   REMARK               VARCHAR(512) COMMENT '说明',
   PRIMARY KEY (SYSTEM_INFO_ID)
);

ALTER TABLE C1_SYSTEM_INFO COMMENT '系统信息';

