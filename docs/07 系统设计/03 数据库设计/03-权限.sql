﻿DROP TABLE IF EXISTS C1_PERMISSION;

DROP TABLE IF EXISTS C1_DEPARTMENT_ROLE;

DROP TABLE IF EXISTS C1_NAVIGATION;

DROP TABLE IF EXISTS C1_OAUTH_CONSUMER;

DROP TABLE IF EXISTS C1_ORG_ROLE;

DROP TABLE IF EXISTS C1_ROLE;

DROP TABLE IF EXISTS C1_ROLE_PERMISSION;

DROP TABLE IF EXISTS C1_TOKEN_INFO;

DROP TABLE IF EXISTS C1_USER_PERMISSION;

DROP TABLE IF EXISTS C1_USER_GROUP;

DROP TABLE IF EXISTS C1_USER_GROUP_ROLE;

DROP TABLE IF EXISTS C1_USER_GROUP_USER;

DROP TABLE IF EXISTS C1_USER_ROLE;

/*==============================================================*/
/* Table: C1_PERMISSION                                         */
/*==============================================================*/
CREATE TABLE C1_PERMISSION
(
   PERMISSION_ID        VARCHAR(36) NOT NULL COMMENT '许可标识',
   SYSTEM_INFO_CODE     VARCHAR(36) COMMENT '系统信息代码',
   SITE_CODE            VARCHAR(36) COMMENT '站点代码',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_CODE      VARCHAR(36) COMMENT '创建人代码',
   CREATED_DATE         DATETIME COMMENT '创建时间',
   LAST_MODIFIED_BY_CODE VARCHAR(36) COMMENT '最后修改人代码',
   LAST_MODIFIED_DATE   DATETIME COMMENT '最后修改时间',
   STATE_CODE           VARCHAR(36) COMMENT '状态代码',
   DATA_STATE_CODE      VARCHAR(36) COMMENT '数据状态代码',
   VERSION_             INT DEFAULT 0 COMMENT '版本',
   WF_ID                VARCHAR(36) COMMENT '工作流标识',
   ROLE_CODES           VARCHAR(1024) COMMENT '角色代码集合',
   PERMISSION_CODES     VARCHAR(1024) COMMENT '许可代码集合',
   ORG_CODE             VARCHAR(36) COMMENT '组织代码',
   DEPARTMENT_CODE      VARCHAR(36) COMMENT '部门代码',
   PERMISSION_CODE      VARCHAR(36) NOT NULL COMMENT '许可代码',
   PARENT_CODE          VARCHAR(36) COMMENT '父级代码',
   PARENT_TREE_CODE     VARCHAR(1024) COMMENT '父级树代码',
   PARENT_TREE_NAME     VARCHAR(1024) COMMENT '父级树名称',
   PRIMARY KEY (PERMISSION_ID)
);

ALTER TABLE C1_PERMISSION COMMENT '许可';

/*==============================================================*/
/* Table: C1_DEPARTMENT_ROLE                                    */
/*==============================================================*/
CREATE TABLE C1_DEPARTMENT_ROLE
(
   DEPARTMENT_ROLE_ID   VARCHAR(36) NOT NULL COMMENT '部门与角色标识',
   SYSTEM_INFO_CODE     VARCHAR(36) COMMENT '系统信息代码',
   SITE_CODE            VARCHAR(36) COMMENT '站点代码',
   ORG_CODE             VARCHAR(36) COMMENT '组织代码',
   DEPARTMENT_CODE      VARCHAR(36) COMMENT '部门代码',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_CODE      VARCHAR(36) COMMENT '创建人代码',
   CREATED_DATE         DATETIME COMMENT '创建时间',
   LAST_MODIFIED_BY_CODE VARCHAR(36) COMMENT '最后修改人代码',
   LAST_MODIFIED_DATE   DATETIME COMMENT '最后修改时间',
   STATE_CODE           VARCHAR(36) COMMENT '状态代码',
   DATA_STATE_CODE      VARCHAR(36) COMMENT '数据状态代码',
   VERSION_             INT DEFAULT 0 COMMENT '版本',
   WF_ID                VARCHAR(36) COMMENT '工作流标识',
   ROLE_CODES           VARCHAR(1024) COMMENT '角色代码集合',
   PERMISSION_CODES     VARCHAR(1024) COMMENT '许可代码集合',
   ROLE_CODE            VARCHAR(36) NOT NULL COMMENT '角色代码',
   PRIMARY KEY (DEPARTMENT_ROLE_ID)
);

ALTER TABLE C1_DEPARTMENT_ROLE COMMENT '部门与角色';

/*==============================================================*/
/* Table: C1_NAVIGATION                                         */
/*==============================================================*/
CREATE TABLE C1_NAVIGATION
(
   NAVIGATION_ID        VARCHAR(36) NOT NULL COMMENT '导航标识',
   SYSTEM_INFO_CODE     VARCHAR(36) COMMENT '系统信息代码',
   SITE_CODE            VARCHAR(36) COMMENT '站点代码',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_CODE      VARCHAR(36) COMMENT '创建人代码',
   CREATED_DATE         DATETIME COMMENT '创建时间',
   LAST_MODIFIED_BY_CODE VARCHAR(36) COMMENT '最后修改人代码',
   LAST_MODIFIED_DATE   DATETIME COMMENT '最后修改时间',
   STATE_CODE           VARCHAR(36) COMMENT '状态代码',
   DATA_STATE_CODE      VARCHAR(36) COMMENT '数据状态代码',
   VERSION_             INT DEFAULT 0 COMMENT '版本',
   WF_ID                VARCHAR(36) COMMENT '工作流标识',
   ROLE_CODES           VARCHAR(1024) COMMENT '角色代码集合',
   PERMISSION_CODES     VARCHAR(1024) COMMENT '许可代码集合',
   ORG_CODE             VARCHAR(36) COMMENT '组织代码',
   DEPARTMENT_CODE      VARCHAR(36) COMMENT '部门代码',
   NAVIGATION_CODE      VARCHAR(36) NOT NULL COMMENT '导航代码',
   NAME                 VARCHAR(256) COMMENT '名称',
   ORDER_BY_               BIGINT DEFAULT 0 COMMENT '排序',
   ICON_STYLE           VARCHAR(256) COMMENT '图标样式',
   URL                  VARCHAR(256) COMMENT 'URL',
   TARGET               VARCHAR(256) COMMENT '目标',
   DATA_OPTION          VARCHAR(512) COMMENT '数据选项',
   TYPE_CODE            VARCHAR(36) COMMENT '类别代码',
   PARENT_CODE          VARCHAR(36) COMMENT '父级代码',
   PARENT_TREE_CODE     VARCHAR(1024) COMMENT '父级树代码',
   PARENT_TREE_NAME     VARCHAR(1024) COMMENT '父级树名称',
   PRIMARY KEY (NAVIGATION_ID)
);

ALTER TABLE C1_NAVIGATION COMMENT '导航';

/*==============================================================*/
/* Table: C1_OAUTH_CONSUMER                                     */
/*==============================================================*/
CREATE TABLE C1_OAUTH_CONSUMER
(
   OAUTH_CONSUMER_ID    VARCHAR(36) NOT NULL COMMENT '授权消费标识',
   SYSTEM_INFO_CODE     VARCHAR(36) COMMENT '系统信息代码',
   SITE_CODE            VARCHAR(36) COMMENT '站点代码',
   ORG_CODE             VARCHAR(36) COMMENT '组织代码',
   DEPARTMENT_CODE      VARCHAR(36) COMMENT '部门代码',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_CODE      VARCHAR(36) COMMENT '创建人代码',
   CREATED_DATE         DATETIME COMMENT '创建时间',
   LAST_MODIFIED_BY_CODE VARCHAR(36) COMMENT '最后修改人代码',
   LAST_MODIFIED_DATE   DATETIME COMMENT '最后修改时间',
   STATE_CODE           VARCHAR(36) COMMENT '状态代码',
   DATA_STATE_CODE      VARCHAR(36) COMMENT '数据状态代码',
   VERSION_             INT DEFAULT 0 COMMENT '版本',
   WF_ID                VARCHAR(36) COMMENT '工作流标识',
   ROLE_CODES           VARCHAR(1024) COMMENT '角色代码集合',
   PERMISSION_CODES     VARCHAR(1024) COMMENT '许可代码集合',
   CLIENT_ID            VARCHAR(64) COMMENT '客户端标识',
   CLIENT_SECRET        VARCHAR(64) COMMENT '客户端密钥',
   PRIMARY KEY (OAUTH_CONSUMER_ID)
);

ALTER TABLE C1_OAUTH_CONSUMER COMMENT '授权消费';

/*==============================================================*/
/* Table: C1_ORG_ROLE                                           */
/*==============================================================*/
CREATE TABLE C1_ORG_ROLE
(
   ORG_ROLE_ID          VARCHAR(36) NOT NULL COMMENT '组织与角色标识',
   SYSTEM_INFO_CODE     VARCHAR(36) COMMENT '系统信息代码',
   SITE_CODE            VARCHAR(36) COMMENT '站点代码',
   ORG_CODE             VARCHAR(36) COMMENT '组织代码',
   DEPARTMENT_CODE      VARCHAR(36) COMMENT '部门代码',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_CODE      VARCHAR(36) COMMENT '创建人代码',
   CREATED_DATE         DATETIME COMMENT '创建时间',
   LAST_MODIFIED_BY_CODE VARCHAR(36) COMMENT '最后修改人代码',
   LAST_MODIFIED_DATE   DATETIME COMMENT '最后修改时间',
   STATE_CODE           VARCHAR(36) COMMENT '状态代码',
   DATA_STATE_CODE      VARCHAR(36) COMMENT '数据状态代码',
   VERSION_             INT DEFAULT 0 COMMENT '版本',
   WF_ID                VARCHAR(36) COMMENT '工作流标识',
   ROLE_CODES           VARCHAR(1024) COMMENT '角色代码集合',
   PERMISSION_CODES     VARCHAR(1024) COMMENT '许可代码集合',
   ROLE_CODE            VARCHAR(36) NOT NULL COMMENT '角色代码',
   PRIMARY KEY (ORG_ROLE_ID)
);

ALTER TABLE C1_ORG_ROLE COMMENT '组织与角色';

/*==============================================================*/
/* Table: C1_ROLE                                               */
/*==============================================================*/
CREATE TABLE C1_ROLE
(
   ROLE_ID              VARCHAR(36) NOT NULL COMMENT '角色标识',
   SYSTEM_INFO_CODE     VARCHAR(36) COMMENT '系统信息代码',
   SITE_CODE            VARCHAR(36) COMMENT '站点代码',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_CODE      VARCHAR(36) COMMENT '创建人代码',
   CREATED_DATE         DATETIME COMMENT '创建时间',
   LAST_MODIFIED_BY_CODE VARCHAR(36) COMMENT '最后修改人代码',
   LAST_MODIFIED_DATE   DATETIME COMMENT '最后修改时间',
   STATE_CODE           VARCHAR(36) COMMENT '状态代码',
   DATA_STATE_CODE      VARCHAR(36) COMMENT '数据状态代码',
   VERSION_             INT DEFAULT 0 COMMENT '版本',
   WF_ID                VARCHAR(36) COMMENT '工作流标识',
   ROLE_CODES           VARCHAR(1024) COMMENT '角色代码集合',
   PERMISSION_CODES     VARCHAR(1024) COMMENT '许可代码集合',
   ORG_CODE             VARCHAR(36) COMMENT '组织代码',
   DEPARTMENT_CODE      VARCHAR(36) COMMENT '部门代码',
   ROLE_CODE            VARCHAR(36) NOT NULL COMMENT '角色代码',
   NAME                 VARCHAR(256) COMMENT '名称',
   PRIMARY KEY (ROLE_ID)
);

ALTER TABLE C1_ROLE COMMENT '角色';

/*==============================================================*/
/* Table: C1_ROLE_PERMISSION                                    */
/*==============================================================*/
CREATE TABLE C1_ROLE_PERMISSION
(
   ROLE_PERMISSION_ID   VARCHAR(36) NOT NULL COMMENT '角色与许可标识',
   SYSTEM_INFO_CODE     VARCHAR(36) COMMENT '系统信息代码',
   SITE_CODE            VARCHAR(36) COMMENT '站点代码',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_CODE      VARCHAR(36) COMMENT '创建人代码',
   CREATED_DATE         DATETIME COMMENT '创建时间',
   LAST_MODIFIED_BY_CODE VARCHAR(36) COMMENT '最后修改人代码',
   LAST_MODIFIED_DATE   DATETIME COMMENT '最后修改时间',
   STATE_CODE           VARCHAR(36) COMMENT '状态代码',
   DATA_STATE_CODE      VARCHAR(36) COMMENT '数据状态代码',
   VERSION_             INT DEFAULT 0 COMMENT '版本',
   WF_ID                VARCHAR(36) COMMENT '工作流标识',
   ROLE_CODES           VARCHAR(1024) COMMENT '角色代码集合',
   PERMISSION_CODES     VARCHAR(1024) COMMENT '许可代码集合',
   ORG_CODE             VARCHAR(36) COMMENT '组织代码',
   DEPARTMENT_CODE      VARCHAR(36) COMMENT '部门代码',
   ROLE_CODE            VARCHAR(36) NOT NULL COMMENT '角色代码',
   PERMISSION_CODE      VARCHAR(36) NOT NULL COMMENT '许可代码',
   PRIMARY KEY (ROLE_PERMISSION_ID)
);

ALTER TABLE C1_ROLE_PERMISSION COMMENT '角色与许可';

/*==============================================================*/
/* Table: C1_TOKEN_INFO                                         */
/*==============================================================*/
CREATE TABLE C1_TOKEN_INFO
(
   TOKEN_INFO_ID        VARCHAR(36) NOT NULL COMMENT '令牌信息标识',
   SYSTEM_INFO_CODE     VARCHAR(36) COMMENT '系统信息代码',
   SITE_CODE            VARCHAR(36) COMMENT '站点代码',
   ORG_CODE             VARCHAR(36) COMMENT '组织代码',
   DEPARTMENT_CODE      VARCHAR(36) COMMENT '部门代码',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_CODE      VARCHAR(36) COMMENT '创建人代码',
   CREATED_DATE         DATETIME COMMENT '创建时间',
   LAST_MODIFIED_BY_CODE VARCHAR(36) COMMENT '最后修改人代码',
   LAST_MODIFIED_DATE   DATETIME COMMENT '最后修改时间',
   STATE_CODE           VARCHAR(36) COMMENT '状态代码',
   DATA_STATE_CODE      VARCHAR(36) COMMENT '数据状态代码',
   VERSION_             INT DEFAULT 0 COMMENT '版本',
   WF_ID                VARCHAR(36) COMMENT '工作流标识',
   ROLE_CODES           VARCHAR(1024) COMMENT '角色代码集合',
   PERMISSION_CODES     VARCHAR(1024) COMMENT '许可代码集合',
   USER_CODE            VARCHAR(36) COMMENT '用户代码',
   TOKEN_INFO_CODE      VARCHAR(64) NOT NULL COMMENT '令牌信息代码',
   CLIENT_ID            VARCHAR(64) COMMENT '客户端标识',
   SCOPE                VARCHAR(512) COMMENT '范围',
   PRIMARY KEY (TOKEN_INFO_ID)
);

ALTER TABLE C1_TOKEN_INFO COMMENT '令牌信息';

/*==============================================================*/
/* Table: C1_USER_PERMISSION                                    */
/*==============================================================*/
CREATE TABLE C1_USER_PERMISSION
(
   USER_PERMISSION_ID   VARCHAR(36) NOT NULL COMMENT '用户与许可标识',
   SYSTEM_INFO_CODE     VARCHAR(36) COMMENT '系统信息代码',
   SITE_CODE            VARCHAR(36) COMMENT '站点代码',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_CODE      VARCHAR(36) COMMENT '创建人代码',
   CREATED_DATE         DATETIME COMMENT '创建时间',
   LAST_MODIFIED_BY_CODE VARCHAR(36) COMMENT '最后修改人代码',
   LAST_MODIFIED_DATE   DATETIME COMMENT '最后修改时间',
   STATE_CODE           VARCHAR(36) COMMENT '状态代码',
   DATA_STATE_CODE      VARCHAR(36) COMMENT '数据状态代码',
   VERSION_             INT DEFAULT 0 COMMENT '版本',
   WF_ID                VARCHAR(36) COMMENT '工作流标识',
   ROLE_CODES           VARCHAR(1024) COMMENT '角色代码集合',
   PERMISSION_CODES     VARCHAR(1024) COMMENT '许可代码集合',
   ORG_CODE             VARCHAR(36) COMMENT '组织代码',
   DEPARTMENT_CODE      VARCHAR(36) COMMENT '部门代码',
   USER_CODE            VARCHAR(36) NOT NULL COMMENT '用户代码',
   PERMISSION_CODE      VARCHAR(36) NOT NULL COMMENT '许可代码',
   FLAG_CODE            VARCHAR(36) COMMENT '标记代码',
   PRIMARY KEY (USER_PERMISSION_ID)
);

ALTER TABLE C1_USER_PERMISSION COMMENT '用户与许可';

/*==============================================================*/
/* Table: C1_USER_GROUP                                         */
/*==============================================================*/
CREATE TABLE C1_USER_GROUP
(
   USER_GROUP_ID        VARCHAR(36) NOT NULL COMMENT '用户组标识',
   SYSTEM_INFO_CODE     VARCHAR(36) COMMENT '系统信息代码',
   SITE_CODE            VARCHAR(36) COMMENT '站点代码',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_CODE      VARCHAR(36) COMMENT '创建人代码',
   CREATED_DATE         DATETIME COMMENT '创建时间',
   LAST_MODIFIED_BY_CODE VARCHAR(36) COMMENT '最后修改人代码',
   LAST_MODIFIED_DATE   DATETIME COMMENT '最后修改时间',
   STATE_CODE           VARCHAR(36) COMMENT '状态代码',
   DATA_STATE_CODE      VARCHAR(36) COMMENT '数据状态代码',
   VERSION_             INT DEFAULT 0 COMMENT '版本',
   WF_ID                VARCHAR(36) COMMENT '工作流标识',
   ROLE_CODES           VARCHAR(1024) COMMENT '角色代码集合',
   PERMISSION_CODES     VARCHAR(1024) COMMENT '许可代码集合',
   ORG_CODE             VARCHAR(36) COMMENT '组织代码',
   DEPARTMENT_CODE      VARCHAR(36) COMMENT '部门代码',
   USER_GROUP_CODE      VARCHAR(36) NOT NULL COMMENT '用户组代码',
   NAME                 VARCHAR(256) COMMENT '名称',
   ORDER_BY_               BIGINT DEFAULT 0 COMMENT '排序',
   PARENT_CODE          VARCHAR(36) COMMENT '父级代码',
   PARENT_TREE_CODE     VARCHAR(1024) COMMENT '父级树代码',
   PARENT_TREE_NAME     VARCHAR(1024) COMMENT '父级树名称',
   PRIMARY KEY (USER_GROUP_ID)
);

ALTER TABLE C1_USER_GROUP COMMENT '用户组';

/*==============================================================*/
/* Table: C1_USER_GROUP_ROLE                                    */
/*==============================================================*/
CREATE TABLE C1_USER_GROUP_ROLE
(
   USER_GROUP_ROLE_ID   VARCHAR(36) NOT NULL COMMENT '用户组与角色标识',
   SYSTEM_INFO_CODE     VARCHAR(36) COMMENT '系统信息代码',
   SITE_CODE            VARCHAR(36) COMMENT '站点代码',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_CODE      VARCHAR(36) COMMENT '创建人代码',
   CREATED_DATE         DATETIME COMMENT '创建时间',
   LAST_MODIFIED_BY_CODE VARCHAR(36) COMMENT '最后修改人代码',
   LAST_MODIFIED_DATE   DATETIME COMMENT '最后修改时间',
   STATE_CODE           VARCHAR(36) COMMENT '状态代码',
   DATA_STATE_CODE      VARCHAR(36) COMMENT '数据状态代码',
   VERSION_             INT DEFAULT 0 COMMENT '版本',
   WF_ID                VARCHAR(36) COMMENT '工作流标识',
   ROLE_CODES           VARCHAR(1024) COMMENT '角色代码集合',
   PERMISSION_CODES     VARCHAR(1024) COMMENT '许可代码集合',
   ORG_CODE             VARCHAR(36) COMMENT '组织代码',
   DEPARTMENT_CODE      VARCHAR(36) COMMENT '部门代码',
   USER_GROUP_CODE      VARCHAR(36) NOT NULL COMMENT '用户组代码',
   ROLE_CODE            VARCHAR(36) NOT NULL COMMENT '角色代码',
   PRIMARY KEY (USER_GROUP_ROLE_ID)
);

ALTER TABLE C1_USER_GROUP_ROLE COMMENT '用户组与角色';

/*==============================================================*/
/* Table: C1_USER_GROUP_USER                                    */
/*==============================================================*/
CREATE TABLE C1_USER_GROUP_USER
(
   USER_GROUP_USER_ID   VARCHAR(36) NOT NULL COMMENT '用户组与用户标识',
   SYSTEM_INFO_CODE     VARCHAR(36) COMMENT '系统信息代码',
   SITE_CODE            VARCHAR(36) COMMENT '站点代码',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_CODE      VARCHAR(36) COMMENT '创建人代码',
   CREATED_DATE         DATETIME COMMENT '创建时间',
   LAST_MODIFIED_BY_CODE VARCHAR(36) COMMENT '最后修改人代码',
   LAST_MODIFIED_DATE   DATETIME COMMENT '最后修改时间',
   STATE_CODE           VARCHAR(36) COMMENT '状态代码',
   DATA_STATE_CODE      VARCHAR(36) COMMENT '数据状态代码',
   VERSION_             INT DEFAULT 0 COMMENT '版本',
   WF_ID                VARCHAR(36) COMMENT '工作流标识',
   ROLE_CODES           VARCHAR(1024) COMMENT '角色代码集合',
   PERMISSION_CODES     VARCHAR(1024) COMMENT '许可代码集合',
   ORG_CODE             VARCHAR(36) COMMENT '组织代码',
   DEPARTMENT_CODE      VARCHAR(36) COMMENT '部门代码',
   USER_GROUP_CODE      VARCHAR(36) NOT NULL COMMENT '用户组代码',
   USER_CODE            VARCHAR(36) NOT NULL COMMENT '用户代码',
   PRIMARY KEY (USER_GROUP_USER_ID)
);

ALTER TABLE C1_USER_GROUP_USER COMMENT '用户组与用户';

/*==============================================================*/
/* Table: C1_USER_ROLE                                          */
/*==============================================================*/
CREATE TABLE C1_USER_ROLE
(
   USER_ROLE_ID         VARCHAR(36) NOT NULL COMMENT '用户与角色标识',
   SYSTEM_INFO_CODE     VARCHAR(36) COMMENT '系统信息代码',
   SITE_CODE            VARCHAR(36) COMMENT '站点代码',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_CODE      VARCHAR(36) COMMENT '创建人代码',
   CREATED_DATE         DATETIME COMMENT '创建时间',
   LAST_MODIFIED_BY_CODE VARCHAR(36) COMMENT '最后修改人代码',
   LAST_MODIFIED_DATE   DATETIME COMMENT '最后修改时间',
   STATE_CODE           VARCHAR(36) COMMENT '状态代码',
   DATA_STATE_CODE      VARCHAR(36) COMMENT '数据状态代码',
   VERSION_             INT DEFAULT 0 COMMENT '版本',
   WF_ID                VARCHAR(36) COMMENT '工作流标识',
   ROLE_CODES           VARCHAR(1024) COMMENT '角色代码集合',
   PERMISSION_CODES     VARCHAR(1024) COMMENT '许可代码集合',
   ORG_CODE             VARCHAR(36) COMMENT '组织代码',
   DEPARTMENT_CODE      VARCHAR(36) COMMENT '部门代码',
   USER_CODE            VARCHAR(36) NOT NULL COMMENT '用户代码',
   ROLE_CODE            VARCHAR(36) NOT NULL COMMENT '角色代码',
   FLAG_CODE            VARCHAR(36) COMMENT '标记代码',
   PRIMARY KEY (USER_ROLE_ID)
);

ALTER TABLE C1_USER_ROLE COMMENT '用户与角色';

