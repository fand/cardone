﻿DROP TABLE IF EXISTS C1_OPERATE_LOG;

/*==============================================================*/
/* Table: C1_OPERATE_LOG                                        */
/*==============================================================*/
CREATE TABLE C1_OPERATE_LOG
(
   OPERATE_LOG_ID       VARCHAR(36) NOT NULL COMMENT '操作日志标识',
   SYSTEM_INFO_CODE     VARCHAR(36) COMMENT '系统信息代码',
   SITE_CODE            VARCHAR(36) COMMENT '站点代码',
   ORG_CODE             VARCHAR(36) COMMENT '组织代码',
   DEPARTMENT_CODE      VARCHAR(36) COMMENT '部门代码',
   OPERATE_LOG_CODE     VARCHAR(36) NOT NULL COMMENT '操作日志代码',
   TYPE_CODE            VARCHAR(36) NOT NULL COMMENT '类别代码',
   MESSAGE              VARCHAR(512) COMMENT '消息',
   CREATED_BY_CODE      VARCHAR(36) COMMENT '创建人代码',
   CREATED_DATE         DATETIME COMMENT '创建时间',
   PRIMARY KEY (OPERATE_LOG_ID)
);

ALTER TABLE C1_OPERATE_LOG COMMENT '操作日志';

