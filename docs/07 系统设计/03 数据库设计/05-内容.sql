﻿DROP TABLE IF EXISTS C1_ARTICLE;

DROP TABLE IF EXISTS C1_NOTICE;

/*==============================================================*/
/* Table: C1_ARTICLE                                            */
/*==============================================================*/
CREATE TABLE C1_ARTICLE
(
   ARTICLE_ID           VARCHAR(36) NOT NULL COMMENT '文章标识',
   SYSTEM_INFO_CODE     VARCHAR(36) COMMENT '系统信息代码',
   SITE_CODE            VARCHAR(36) COMMENT '站点代码',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_CODE      VARCHAR(36) COMMENT '创建人代码',
   CREATED_DATE         DATETIME COMMENT '创建时间',
   LAST_MODIFIED_BY_CODE VARCHAR(36) COMMENT '最后修改人代码',
   LAST_MODIFIED_DATE   DATETIME COMMENT '最后修改时间',
   STATE_CODE           VARCHAR(36) COMMENT '状态代码',
   DATA_STATE_CODE      VARCHAR(36) COMMENT '数据状态代码',
   VERSION_             INT DEFAULT 0 COMMENT '版本',
   WF_ID                VARCHAR(36) COMMENT '工作流标识',
   ROLE_CODES           VARCHAR(1024) COMMENT '角色代码集合',
   PERMISSION_CODES     VARCHAR(1024) COMMENT '许可代码集合',
   ORG_CODE             VARCHAR(36) COMMENT '组织代码',
   DEPARTMENT_CODE      VARCHAR(36) COMMENT '部门代码',
   TITLE                VARCHAR(256) NOT NULL COMMENT '标题',
   CONTENT              VARCHAR(4000) COMMENT '正文',
   RELEASE_DATE         DATETIME COMMENT '发布日期',
   TYPE_CODE            VARCHAR(36) COMMENT '类别代码',
   PICTURE_URL          VARCHAR(256) COMMENT '图片路径',
   INTRO                TEXT COMMENT '简介',
   RECOM                CHAR(1) COMMENT '推荐',
   PRIMARY KEY (ARTICLE_ID)
);

ALTER TABLE C1_ARTICLE COMMENT '文章';

/*==============================================================*/
/* Table: C1_NOTICE                                             */
/*==============================================================*/
CREATE TABLE C1_NOTICE
(
   NOTICE_ID            VARCHAR(36) NOT NULL COMMENT '公告标识',
   SYSTEM_INFO_CODE     VARCHAR(36) COMMENT '系统信息代码',
   SITE_CODE            VARCHAR(36) COMMENT '站点代码',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_CODE      VARCHAR(36) COMMENT '创建人代码',
   CREATED_DATE         DATETIME COMMENT '创建时间',
   LAST_MODIFIED_BY_CODE VARCHAR(36) COMMENT '最后修改人代码',
   LAST_MODIFIED_DATE   DATETIME COMMENT '最后修改时间',
   STATE_CODE           VARCHAR(36) COMMENT '状态代码',
   DATA_STATE_CODE      VARCHAR(36) COMMENT '数据状态代码',
   VERSION_             INT DEFAULT 0 COMMENT '版本',
   WF_ID                VARCHAR(36) COMMENT '工作流标识',
   ROLE_CODES           VARCHAR(1024) COMMENT '角色代码集合',
   PERMISSION_CODES     VARCHAR(1024) COMMENT '许可代码集合',
   ORG_CODE             VARCHAR(36) COMMENT '组织代码',
   DEPARTMENT_CODE      VARCHAR(36) COMMENT '部门代码',
   TITLE                VARCHAR(256) NOT NULL COMMENT '标题',
   CONTENT              VARCHAR(4000) COMMENT '正文',
   RELEASE_DATE         DATETIME COMMENT '发布日期',
   TYPE_CODE            VARCHAR(36) COMMENT '类别代码',
   PICTURE_URL          VARCHAR(256) COMMENT '图片路径',
   PRIMARY KEY (NOTICE_ID)
);

ALTER TABLE C1_NOTICE COMMENT '公告';

