CREATE TABLE "public"."act_evt_log" (
"log_nr_" int4 DEFAULT nextval('act_evt_log_log_nr__seq'::regclass) NOT NULL,
"type_" varchar(64) COLLATE "default",
"proc_def_id_" varchar(64) COLLATE "default",
"proc_inst_id_" varchar(64) COLLATE "default",
"execution_id_" varchar(64) COLLATE "default",
"task_id_" varchar(64) COLLATE "default",
"time_stamp_" timestamp(6) NOT NULL,
"user_id_" varchar(255) COLLATE "default",
"data_" bytea,
"lock_owner_" varchar(255) COLLATE "default",
"lock_time_" timestamp(6),
"is_processed_" int2 DEFAULT 0,
CONSTRAINT "act_evt_log_pkey" PRIMARY KEY ("log_nr_")
)
WITH (OIDS=FALSE);

CREATE TABLE "public"."act_ge_bytearray" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"rev_" int4,
"name_" varchar(255) COLLATE "default",
"deployment_id_" varchar(64) COLLATE "default",
"bytes_" bytea,
"generated_" bool,
CONSTRAINT "act_ge_bytearray_pkey" PRIMARY KEY ("id_")
)
WITH (OIDS=FALSE);

CREATE TABLE "public"."act_ge_property" (
"name_" varchar(64) COLLATE "default" NOT NULL,
"value_" varchar(300) COLLATE "default",
"rev_" int4,
CONSTRAINT "act_ge_property_pkey" PRIMARY KEY ("name_")
)
WITH (OIDS=FALSE);

CREATE TABLE "public"."act_hi_actinst" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"proc_def_id_" varchar(64) COLLATE "default" NOT NULL,
"proc_inst_id_" varchar(64) COLLATE "default" NOT NULL,
"execution_id_" varchar(64) COLLATE "default" NOT NULL,
"act_id_" varchar(255) COLLATE "default" NOT NULL,
"task_id_" varchar(64) COLLATE "default",
"call_proc_inst_id_" varchar(64) COLLATE "default",
"act_name_" varchar(255) COLLATE "default",
"act_type_" varchar(255) COLLATE "default" NOT NULL,
"assignee_" varchar(255) COLLATE "default",
"start_time_" timestamp(6) NOT NULL,
"end_time_" timestamp(6),
"duration_" int8,
"delete_reason_" varchar(4000) COLLATE "default",
"tenant_id_" varchar(255) COLLATE "default" DEFAULT ''::character varying,
CONSTRAINT "act_hi_actinst_pkey" PRIMARY KEY ("id_")
)
WITH (OIDS=FALSE);

CREATE TABLE "public"."act_hi_attachment" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"rev_" int4,
"user_id_" varchar(255) COLLATE "default",
"name_" varchar(255) COLLATE "default",
"description_" varchar(4000) COLLATE "default",
"type_" varchar(255) COLLATE "default",
"task_id_" varchar(64) COLLATE "default",
"proc_inst_id_" varchar(64) COLLATE "default",
"url_" varchar(4000) COLLATE "default",
"content_id_" varchar(64) COLLATE "default",
"time_" timestamp(6),
CONSTRAINT "act_hi_attachment_pkey" PRIMARY KEY ("id_")
)
WITH (OIDS=FALSE);

CREATE TABLE "public"."act_hi_comment" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"type_" varchar(255) COLLATE "default",
"time_" timestamp(6) NOT NULL,
"user_id_" varchar(255) COLLATE "default",
"task_id_" varchar(64) COLLATE "default",
"proc_inst_id_" varchar(64) COLLATE "default",
"action_" varchar(255) COLLATE "default",
"message_" varchar(4000) COLLATE "default",
"full_msg_" bytea,
CONSTRAINT "act_hi_comment_pkey" PRIMARY KEY ("id_")
)
WITH (OIDS=FALSE);

CREATE TABLE "public"."act_hi_detail" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"type_" varchar(255) COLLATE "default" NOT NULL,
"proc_inst_id_" varchar(64) COLLATE "default",
"execution_id_" varchar(64) COLLATE "default",
"task_id_" varchar(64) COLLATE "default",
"act_inst_id_" varchar(64) COLLATE "default",
"name_" varchar(255) COLLATE "default" NOT NULL,
"var_type_" varchar(64) COLLATE "default",
"rev_" int4,
"time_" timestamp(6) NOT NULL,
"bytearray_id_" varchar(64) COLLATE "default",
"double_" float8,
"long_" int8,
"text_" varchar(4000) COLLATE "default",
"text2_" varchar(4000) COLLATE "default",
CONSTRAINT "act_hi_detail_pkey" PRIMARY KEY ("id_")
)
WITH (OIDS=FALSE);

CREATE TABLE "public"."act_hi_identitylink" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"group_id_" varchar(255) COLLATE "default",
"type_" varchar(255) COLLATE "default",
"user_id_" varchar(255) COLLATE "default",
"task_id_" varchar(64) COLLATE "default",
"proc_inst_id_" varchar(64) COLLATE "default",
CONSTRAINT "act_hi_identitylink_pkey" PRIMARY KEY ("id_")
)
WITH (OIDS=FALSE);

CREATE TABLE "public"."act_hi_procinst" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"proc_inst_id_" varchar(64) COLLATE "default" NOT NULL,
"business_key_" varchar(255) COLLATE "default",
"proc_def_id_" varchar(64) COLLATE "default" NOT NULL,
"start_time_" timestamp(6) NOT NULL,
"end_time_" timestamp(6),
"duration_" int8,
"start_user_id_" varchar(255) COLLATE "default",
"start_act_id_" varchar(255) COLLATE "default",
"end_act_id_" varchar(255) COLLATE "default",
"super_process_instance_id_" varchar(64) COLLATE "default",
"delete_reason_" varchar(4000) COLLATE "default",
"tenant_id_" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"name_" varchar(255) COLLATE "default",
CONSTRAINT "act_hi_procinst_pkey" PRIMARY KEY ("id_"),
CONSTRAINT "act_hi_procinst_proc_inst_id__key" UNIQUE ("proc_inst_id_")
)
WITH (OIDS=FALSE);

CREATE TABLE "public"."act_hi_taskinst" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"proc_def_id_" varchar(64) COLLATE "default",
"task_def_key_" varchar(255) COLLATE "default",
"proc_inst_id_" varchar(64) COLLATE "default",
"execution_id_" varchar(64) COLLATE "default",
"name_" varchar(255) COLLATE "default",
"parent_task_id_" varchar(64) COLLATE "default",
"description_" varchar(4000) COLLATE "default",
"owner_" varchar(255) COLLATE "default",
"assignee_" varchar(255) COLLATE "default",
"start_time_" timestamp(6) NOT NULL,
"claim_time_" timestamp(6),
"end_time_" timestamp(6),
"duration_" int8,
"delete_reason_" varchar(4000) COLLATE "default",
"priority_" int4,
"due_date_" timestamp(6),
"form_key_" varchar(255) COLLATE "default",
"category_" varchar(255) COLLATE "default",
"tenant_id_" varchar(255) COLLATE "default" DEFAULT ''::character varying,
CONSTRAINT "act_hi_taskinst_pkey" PRIMARY KEY ("id_")
)
WITH (OIDS=FALSE);

CREATE TABLE "public"."act_hi_varinst" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"proc_inst_id_" varchar(64) COLLATE "default",
"execution_id_" varchar(64) COLLATE "default",
"task_id_" varchar(64) COLLATE "default",
"name_" varchar(255) COLLATE "default" NOT NULL,
"var_type_" varchar(100) COLLATE "default",
"rev_" int4,
"bytearray_id_" varchar(64) COLLATE "default",
"double_" float8,
"long_" int8,
"text_" varchar(4000) COLLATE "default",
"text2_" varchar(4000) COLLATE "default",
"create_time_" timestamp(6),
"last_updated_time_" timestamp(6),
CONSTRAINT "act_hi_varinst_pkey" PRIMARY KEY ("id_")
)
WITH (OIDS=FALSE);

CREATE TABLE "public"."act_id_group" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"rev_" int4,
"name_" varchar(255) COLLATE "default",
"type_" varchar(255) COLLATE "default",
CONSTRAINT "act_id_group_pkey" PRIMARY KEY ("id_")
)
WITH (OIDS=FALSE);

CREATE TABLE "public"."act_id_info" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"rev_" int4,
"user_id_" varchar(64) COLLATE "default",
"type_" varchar(64) COLLATE "default",
"key_" varchar(255) COLLATE "default",
"value_" varchar(255) COLLATE "default",
"password_" bytea,
"parent_id_" varchar(255) COLLATE "default",
CONSTRAINT "act_id_info_pkey" PRIMARY KEY ("id_")
)
WITH (OIDS=FALSE);

CREATE TABLE "public"."act_id_membership" (
"user_id_" varchar(64) COLLATE "default" NOT NULL,
"group_id_" varchar(64) COLLATE "default" NOT NULL,
CONSTRAINT "act_id_membership_pkey" PRIMARY KEY ("user_id_", "group_id_")
)
WITH (OIDS=FALSE);

CREATE TABLE "public"."act_id_user" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"rev_" int4,
"first_" varchar(255) COLLATE "default",
"last_" varchar(255) COLLATE "default",
"email_" varchar(255) COLLATE "default",
"pwd_" varchar(255) COLLATE "default",
"picture_id_" varchar(64) COLLATE "default",
CONSTRAINT "act_id_user_pkey" PRIMARY KEY ("id_")
)
WITH (OIDS=FALSE);

CREATE TABLE "public"."act_procdef_info" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"proc_def_id_" varchar(64) COLLATE "default" NOT NULL,
"rev_" int4,
"info_json_id_" varchar(64) COLLATE "default",
CONSTRAINT "act_procdef_info_pkey" PRIMARY KEY ("id_"),
CONSTRAINT "act_uniq_info_procdef" UNIQUE ("proc_def_id_")
)
WITH (OIDS=FALSE);

CREATE TABLE "public"."act_re_deployment" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"name_" varchar(255) COLLATE "default",
"category_" varchar(255) COLLATE "default",
"key_" varchar(255) COLLATE "default",
"tenant_id_" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"deploy_time_" timestamp(6),
"engine_version_" varchar(255) COLLATE "default",
CONSTRAINT "act_re_deployment_pkey" PRIMARY KEY ("id_")
)
WITH (OIDS=FALSE);

CREATE TABLE "public"."act_re_model" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"rev_" int4,
"name_" varchar(255) COLLATE "default",
"key_" varchar(255) COLLATE "default",
"category_" varchar(255) COLLATE "default",
"create_time_" timestamp(6),
"last_update_time_" timestamp(6),
"version_" int4,
"meta_info_" varchar(4000) COLLATE "default",
"deployment_id_" varchar(64) COLLATE "default",
"editor_source_value_id_" varchar(64) COLLATE "default",
"editor_source_extra_value_id_" varchar(64) COLLATE "default",
"tenant_id_" varchar(255) COLLATE "default" DEFAULT ''::character varying,
CONSTRAINT "act_re_model_pkey" PRIMARY KEY ("id_")
)
WITH (OIDS=FALSE);

CREATE TABLE "public"."act_re_procdef" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"rev_" int4,
"category_" varchar(255) COLLATE "default",
"name_" varchar(255) COLLATE "default",
"key_" varchar(255) COLLATE "default" NOT NULL,
"version_" int4 NOT NULL,
"deployment_id_" varchar(64) COLLATE "default",
"resource_name_" varchar(4000) COLLATE "default",
"dgrm_resource_name_" varchar(4000) COLLATE "default",
"description_" varchar(4000) COLLATE "default",
"has_start_form_key_" bool,
"has_graphical_notation_" bool,
"suspension_state_" int4,
"tenant_id_" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"engine_version_" varchar(255) COLLATE "default",
CONSTRAINT "act_re_procdef_pkey" PRIMARY KEY ("id_"),
CONSTRAINT "act_uniq_procdef" UNIQUE ("key_", "version_", "tenant_id_")
)
WITH (OIDS=FALSE);

CREATE TABLE "public"."act_ru_deadletter_job" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"rev_" int4,
"type_" varchar(255) COLLATE "default" NOT NULL,
"exclusive_" bool,
"execution_id_" varchar(64) COLLATE "default",
"process_instance_id_" varchar(64) COLLATE "default",
"proc_def_id_" varchar(64) COLLATE "default",
"exception_stack_id_" varchar(64) COLLATE "default",
"exception_msg_" varchar(4000) COLLATE "default",
"duedate_" timestamp(6),
"repeat_" varchar(255) COLLATE "default",
"handler_type_" varchar(255) COLLATE "default",
"handler_cfg_" varchar(4000) COLLATE "default",
"tenant_id_" varchar(255) COLLATE "default" DEFAULT ''::character varying,
CONSTRAINT "act_ru_deadletter_job_pkey" PRIMARY KEY ("id_")
)
WITH (OIDS=FALSE);

CREATE TABLE "public"."act_ru_event_subscr" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"rev_" int4,
"event_type_" varchar(255) COLLATE "default" NOT NULL,
"event_name_" varchar(255) COLLATE "default",
"execution_id_" varchar(64) COLLATE "default",
"proc_inst_id_" varchar(64) COLLATE "default",
"activity_id_" varchar(64) COLLATE "default",
"configuration_" varchar(255) COLLATE "default",
"created_" timestamp(6) NOT NULL,
"proc_def_id_" varchar(64) COLLATE "default",
"tenant_id_" varchar(255) COLLATE "default" DEFAULT ''::character varying,
CONSTRAINT "act_ru_event_subscr_pkey" PRIMARY KEY ("id_")
)
WITH (OIDS=FALSE);

CREATE TABLE "public"."act_ru_execution" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"rev_" int4,
"proc_inst_id_" varchar(64) COLLATE "default",
"business_key_" varchar(255) COLLATE "default",
"parent_id_" varchar(64) COLLATE "default",
"proc_def_id_" varchar(64) COLLATE "default",
"super_exec_" varchar(64) COLLATE "default",
"root_proc_inst_id_" varchar(64) COLLATE "default",
"act_id_" varchar(255) COLLATE "default",
"is_active_" bool,
"is_concurrent_" bool,
"is_scope_" bool,
"is_event_scope_" bool,
"is_mi_root_" bool,
"suspension_state_" int4,
"cached_ent_state_" int4,
"tenant_id_" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"name_" varchar(255) COLLATE "default",
"start_time_" timestamp(6),
"start_user_id_" varchar(255) COLLATE "default",
"lock_time_" timestamp(6),
"is_count_enabled_" bool,
"evt_subscr_count_" int4,
"task_count_" int4,
"job_count_" int4,
"timer_job_count_" int4,
"susp_job_count_" int4,
"deadletter_job_count_" int4,
"var_count_" int4,
"id_link_count_" int4,
CONSTRAINT "act_ru_execution_pkey" PRIMARY KEY ("id_")
)
WITH (OIDS=FALSE);

CREATE TABLE "public"."act_ru_identitylink" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"rev_" int4,
"group_id_" varchar(255) COLLATE "default",
"type_" varchar(255) COLLATE "default",
"user_id_" varchar(255) COLLATE "default",
"task_id_" varchar(64) COLLATE "default",
"proc_inst_id_" varchar(64) COLLATE "default",
"proc_def_id_" varchar(64) COLLATE "default",
CONSTRAINT "act_ru_identitylink_pkey" PRIMARY KEY ("id_")
)
WITH (OIDS=FALSE);

CREATE TABLE "public"."act_ru_job" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"rev_" int4,
"type_" varchar(255) COLLATE "default" NOT NULL,
"lock_exp_time_" timestamp(6),
"lock_owner_" varchar(255) COLLATE "default",
"exclusive_" bool,
"execution_id_" varchar(64) COLLATE "default",
"process_instance_id_" varchar(64) COLLATE "default",
"proc_def_id_" varchar(64) COLLATE "default",
"retries_" int4,
"exception_stack_id_" varchar(64) COLLATE "default",
"exception_msg_" varchar(4000) COLLATE "default",
"duedate_" timestamp(6),
"repeat_" varchar(255) COLLATE "default",
"handler_type_" varchar(255) COLLATE "default",
"handler_cfg_" varchar(4000) COLLATE "default",
"tenant_id_" varchar(255) COLLATE "default" DEFAULT ''::character varying,
CONSTRAINT "act_ru_job_pkey" PRIMARY KEY ("id_")
)
WITH (OIDS=FALSE);

CREATE TABLE "public"."act_ru_suspended_job" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"rev_" int4,
"type_" varchar(255) COLLATE "default" NOT NULL,
"exclusive_" bool,
"execution_id_" varchar(64) COLLATE "default",
"process_instance_id_" varchar(64) COLLATE "default",
"proc_def_id_" varchar(64) COLLATE "default",
"retries_" int4,
"exception_stack_id_" varchar(64) COLLATE "default",
"exception_msg_" varchar(4000) COLLATE "default",
"duedate_" timestamp(6),
"repeat_" varchar(255) COLLATE "default",
"handler_type_" varchar(255) COLLATE "default",
"handler_cfg_" varchar(4000) COLLATE "default",
"tenant_id_" varchar(255) COLLATE "default" DEFAULT ''::character varying,
CONSTRAINT "act_ru_suspended_job_pkey" PRIMARY KEY ("id_")
)
WITH (OIDS=FALSE);

CREATE TABLE "public"."act_ru_task" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"rev_" int4,
"execution_id_" varchar(64) COLLATE "default",
"proc_inst_id_" varchar(64) COLLATE "default",
"proc_def_id_" varchar(64) COLLATE "default",
"name_" varchar(255) COLLATE "default",
"parent_task_id_" varchar(64) COLLATE "default",
"description_" varchar(4000) COLLATE "default",
"task_def_key_" varchar(255) COLLATE "default",
"owner_" varchar(255) COLLATE "default",
"assignee_" varchar(255) COLLATE "default",
"delegation_" varchar(64) COLLATE "default",
"priority_" int4,
"create_time_" timestamp(6),
"due_date_" timestamp(6),
"category_" varchar(255) COLLATE "default",
"suspension_state_" int4,
"tenant_id_" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"form_key_" varchar(255) COLLATE "default",
"claim_time_" timestamp(6),
CONSTRAINT "act_ru_task_pkey" PRIMARY KEY ("id_")
)
WITH (OIDS=FALSE);

CREATE TABLE "public"."act_ru_timer_job" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"rev_" int4,
"type_" varchar(255) COLLATE "default" NOT NULL,
"lock_exp_time_" timestamp(6),
"lock_owner_" varchar(255) COLLATE "default",
"exclusive_" bool,
"execution_id_" varchar(64) COLLATE "default",
"process_instance_id_" varchar(64) COLLATE "default",
"proc_def_id_" varchar(64) COLLATE "default",
"retries_" int4,
"exception_stack_id_" varchar(64) COLLATE "default",
"exception_msg_" varchar(4000) COLLATE "default",
"duedate_" timestamp(6),
"repeat_" varchar(255) COLLATE "default",
"handler_type_" varchar(255) COLLATE "default",
"handler_cfg_" varchar(4000) COLLATE "default",
"tenant_id_" varchar(255) COLLATE "default" DEFAULT ''::character varying,
CONSTRAINT "act_ru_timer_job_pkey" PRIMARY KEY ("id_")
)
WITH (OIDS=FALSE);

CREATE TABLE "public"."act_ru_variable" (
"id_" varchar(64) COLLATE "default" NOT NULL,
"rev_" int4,
"type_" varchar(255) COLLATE "default" NOT NULL,
"name_" varchar(255) COLLATE "default" NOT NULL,
"execution_id_" varchar(64) COLLATE "default",
"proc_inst_id_" varchar(64) COLLATE "default",
"task_id_" varchar(64) COLLATE "default",
"bytearray_id_" varchar(64) COLLATE "default",
"double_" float8,
"long_" int8,
"text_" varchar(4000) COLLATE "default",
"text2_" varchar(4000) COLLATE "default",
CONSTRAINT "act_ru_variable_pkey" PRIMARY KEY ("id_")
)
WITH (OIDS=FALSE);

ALTER TABLE "public"."c1_area" ALTER COLUMN "city_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_area" ALTER COLUMN "province_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_area" ALTER COLUMN "begin_date" SET DEFAULT now();

ALTER TABLE "public"."c1_area" ALTER COLUMN "created_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_area" ALTER COLUMN "created_date" SET DEFAULT now();

ALTER TABLE "public"."c1_area" ALTER COLUMN "last_modified_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_area" ALTER COLUMN "last_modified_date" SET DEFAULT now();

ALTER TABLE "public"."c1_area" ALTER COLUMN "flag_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_area" ALTER COLUMN "batch_no" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_area" ALTER COLUMN "flag_object_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_area" ALTER COLUMN "state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_area" ALTER COLUMN "data_state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_area" ALTER COLUMN "json_data" TYPE json;

ALTER TABLE "public"."c1_area" ALTER COLUMN "system_info_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_area" ALTER COLUMN "site_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_area" ALTER COLUMN "org_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_area" ALTER COLUMN "department_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_area" ALTER COLUMN "personal_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_area" ALTER COLUMN "area_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_area" ALTER COLUMN "name" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_area" ALTER COLUMN "country_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_area" ALTER COLUMN "created_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_area" ALTER COLUMN "last_modified_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_area" ALTER COLUMN "personal_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_article" ALTER COLUMN "check_person_code" DROP DEFAULT;

ALTER TABLE "public"."c1_article" ALTER COLUMN "author_person_code" DROP DEFAULT;

ALTER TABLE "public"."c1_article" ALTER COLUMN "begin_date" SET DEFAULT now();

ALTER TABLE "public"."c1_article" ALTER COLUMN "created_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_article" ALTER COLUMN "created_date" SET DEFAULT now();

ALTER TABLE "public"."c1_article" ALTER COLUMN "last_modified_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_article" ALTER COLUMN "last_modified_date" SET DEFAULT now();

ALTER TABLE "public"."c1_article" ALTER COLUMN "flag_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_article" ALTER COLUMN "batch_no" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_article" ALTER COLUMN "flag_object_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_article" ALTER COLUMN "state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_article" ALTER COLUMN "data_state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_article" ALTER COLUMN "json_data" TYPE json;

ALTER TABLE "public"."c1_article" ALTER COLUMN "system_info_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_article" ALTER COLUMN "site_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_article" ALTER COLUMN "org_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_article" ALTER COLUMN "department_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_article" ALTER COLUMN "personal_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_article" ALTER COLUMN "title" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_article" ALTER COLUMN "content" TYPE varchar(4095) COLLATE "default";

ALTER TABLE "public"."c1_article" ALTER COLUMN "release_date" SET DEFAULT now();

ALTER TABLE "public"."c1_article" ALTER COLUMN "intro" TYPE text COLLATE "default", ALTER COLUMN "intro" SET DEFAULT ''::text;

ALTER TABLE "public"."c1_article" ALTER COLUMN "recom" TYPE char(1) COLLATE "default", ALTER COLUMN "recom" SET DEFAULT '0'::bpchar;

ALTER TABLE "public"."c1_article" ALTER COLUMN "created_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_article" ALTER COLUMN "last_modified_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_article" ALTER COLUMN "personal_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_city" ALTER COLUMN "province_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_city" ALTER COLUMN "begin_date" SET DEFAULT now();

ALTER TABLE "public"."c1_city" ALTER COLUMN "created_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_city" ALTER COLUMN "created_date" SET DEFAULT now();

ALTER TABLE "public"."c1_city" ALTER COLUMN "last_modified_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_city" ALTER COLUMN "last_modified_date" SET DEFAULT now();

ALTER TABLE "public"."c1_city" ALTER COLUMN "flag_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_city" ALTER COLUMN "batch_no" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_city" ALTER COLUMN "flag_object_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_city" ALTER COLUMN "state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_city" ALTER COLUMN "data_state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_city" ALTER COLUMN "json_data" TYPE json;

ALTER TABLE "public"."c1_city" ALTER COLUMN "system_info_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_city" ALTER COLUMN "site_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_city" ALTER COLUMN "org_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_city" ALTER COLUMN "department_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_city" ALTER COLUMN "personal_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_city" ALTER COLUMN "city_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_city" ALTER COLUMN "name" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_city" ALTER COLUMN "country_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_city" ALTER COLUMN "created_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_city" ALTER COLUMN "last_modified_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_city" ALTER COLUMN "personal_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_department" ALTER COLUMN "begin_date" SET DEFAULT now();

ALTER TABLE "public"."c1_department" ALTER COLUMN "created_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_department" ALTER COLUMN "created_date" SET DEFAULT now();

ALTER TABLE "public"."c1_department" ALTER COLUMN "last_modified_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_department" ALTER COLUMN "last_modified_date" TYPE timestamp(3), ALTER COLUMN "last_modified_date" SET DEFAULT now();

ALTER TABLE "public"."c1_department" ALTER COLUMN "flag_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_department" ALTER COLUMN "batch_no" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_department" ALTER COLUMN "flag_object_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_department" ALTER COLUMN "state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_department" ALTER COLUMN "data_state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_department" ALTER COLUMN "system_info_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_department" ALTER COLUMN "site_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_department" ALTER COLUMN "org_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_department" ALTER COLUMN "department_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_department" ALTER COLUMN "personal_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_department" ALTER COLUMN "name" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_department" ALTER COLUMN "parent_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_department" ALTER COLUMN "parent_tree_code" TYPE varchar(1023) COLLATE "default";

ALTER TABLE "public"."c1_department" ALTER COLUMN "parent_tree_name" TYPE varchar(1023) COLLATE "default";

ALTER TABLE "public"."c1_department" ALTER COLUMN "province_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_department" ALTER COLUMN "city_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_department" ALTER COLUMN "area_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_department" ALTER COLUMN "region_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_department" ALTER COLUMN "address" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_department" ALTER COLUMN "org_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_department" ALTER COLUMN "created_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_department" ALTER COLUMN "last_modified_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_department" ALTER COLUMN "personal_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_department" ALTER COLUMN "parent_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_department" ALTER COLUMN "parent_tree_id" TYPE varchar(1023) COLLATE "default";

ALTER TABLE "public"."c1_department" ALTER COLUMN "source_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_department" ALTER COLUMN "third_party_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_department" ALTER COLUMN "compatible_department_code_1" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_department" ALTER COLUMN "compatible_department_code_2" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_department" ALTER COLUMN "compatible_parent_code_1" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_department" ALTER COLUMN "compatible_parent_code_2" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_department" ALTER COLUMN "sync_return_code" TYPE varchar(128) COLLATE "default", ALTER COLUMN "sync_return_code" SET DEFAULT 'wait'::character varying;

COMMENT ON COLUMN "public"."c1_department"."sync_return_code" IS '';

COMMENT ON COLUMN "public"."c1_department"."sync_return_message" IS '';

COMMENT ON COLUMN "public"."c1_department"."last_sync_time" IS '';

ALTER TABLE "public"."c1_dictionary" ALTER COLUMN "dictionary_type_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary" ALTER COLUMN "begin_date" SET DEFAULT now();

ALTER TABLE "public"."c1_dictionary" ALTER COLUMN "created_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary" ALTER COLUMN "created_date" SET DEFAULT now();

ALTER TABLE "public"."c1_dictionary" ALTER COLUMN "last_modified_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary" ALTER COLUMN "last_modified_date" SET DEFAULT now();

ALTER TABLE "public"."c1_dictionary" ALTER COLUMN "flag_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary" ALTER COLUMN "batch_no" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary" ALTER COLUMN "flag_object_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary" ALTER COLUMN "state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary" ALTER COLUMN "data_state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary" ALTER COLUMN "json_data" TYPE json;

ALTER TABLE "public"."c1_dictionary" ALTER COLUMN "system_info_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary" ALTER COLUMN "site_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary" ALTER COLUMN "org_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary" ALTER COLUMN "department_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary" ALTER COLUMN "personal_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary" ALTER COLUMN "dictionary_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary" ALTER COLUMN "name" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary" ALTER COLUMN "value_" TYPE varchar(1023) COLLATE "default";

ALTER TABLE "public"."c1_dictionary" ALTER COLUMN "remark" TYPE varchar(511) COLLATE "default";

ALTER TABLE "public"."c1_dictionary" ALTER COLUMN "explain_" TYPE varchar(511) COLLATE "default";

ALTER TABLE "public"."c1_dictionary" ALTER COLUMN "site_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary" ALTER COLUMN "created_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary" ALTER COLUMN "last_modified_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary" ALTER COLUMN "personal_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_item" ALTER COLUMN "dictionary_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_item" ALTER COLUMN "dictionary_type_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_item" ALTER COLUMN "begin_date" SET DEFAULT now();

ALTER TABLE "public"."c1_dictionary_item" ALTER COLUMN "created_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_item" ALTER COLUMN "created_date" SET DEFAULT now();

ALTER TABLE "public"."c1_dictionary_item" ALTER COLUMN "last_modified_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_item" ALTER COLUMN "last_modified_date" SET DEFAULT now();

ALTER TABLE "public"."c1_dictionary_item" ALTER COLUMN "flag_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_item" ALTER COLUMN "batch_no" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_item" ALTER COLUMN "flag_object_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_item" ALTER COLUMN "state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_item" ALTER COLUMN "data_state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_item" ALTER COLUMN "json_data" TYPE json;

ALTER TABLE "public"."c1_dictionary_item" ALTER COLUMN "system_info_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_item" ALTER COLUMN "site_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_item" ALTER COLUMN "org_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_item" ALTER COLUMN "department_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_item" ALTER COLUMN "personal_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_item" ALTER COLUMN "name" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_item" ALTER COLUMN "value_" TYPE varchar(1023) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_item" ALTER COLUMN "remark" TYPE varchar(511) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_item" ALTER COLUMN "explain_" TYPE varchar(511) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_item" ALTER COLUMN "created_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_item" ALTER COLUMN "last_modified_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_item" ALTER COLUMN "personal_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_type" ALTER COLUMN "begin_date" SET DEFAULT now();

ALTER TABLE "public"."c1_dictionary_type" ALTER COLUMN "created_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_type" ALTER COLUMN "created_date" SET DEFAULT now();

ALTER TABLE "public"."c1_dictionary_type" ALTER COLUMN "last_modified_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_type" ALTER COLUMN "last_modified_date" SET DEFAULT now();

ALTER TABLE "public"."c1_dictionary_type" ALTER COLUMN "flag_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_type" ALTER COLUMN "batch_no" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_type" ALTER COLUMN "flag_object_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_type" ALTER COLUMN "state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_type" ALTER COLUMN "data_state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_type" ALTER COLUMN "json_data" TYPE json;

ALTER TABLE "public"."c1_dictionary_type" ALTER COLUMN "system_info_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_type" ALTER COLUMN "site_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_type" ALTER COLUMN "org_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_type" ALTER COLUMN "department_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_type" ALTER COLUMN "personal_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_type" ALTER COLUMN "dictionary_type_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_type" ALTER COLUMN "name" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_type" ALTER COLUMN "remark" TYPE varchar(511) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_type" ALTER COLUMN "parent_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_type" ALTER COLUMN "parent_tree_code" TYPE varchar(1023) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_type" ALTER COLUMN "parent_tree_name" TYPE varchar(1023) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_type" ALTER COLUMN "site_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_type" ALTER COLUMN "created_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_type" ALTER COLUMN "last_modified_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_type" ALTER COLUMN "personal_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_type" ALTER COLUMN "parent_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_dictionary_type" ALTER COLUMN "parent_tree_id" TYPE varchar(1023) COLLATE "default";

ALTER TABLE "public"."c1_error_info" ALTER COLUMN "begin_date" SET DEFAULT now();

ALTER TABLE "public"."c1_error_info" ALTER COLUMN "created_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_error_info" ALTER COLUMN "created_date" SET DEFAULT now();

ALTER TABLE "public"."c1_error_info" ALTER COLUMN "last_modified_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_error_info" ALTER COLUMN "last_modified_date" SET DEFAULT now();

ALTER TABLE "public"."c1_error_info" ALTER COLUMN "flag_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_error_info" ALTER COLUMN "batch_no" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_error_info" ALTER COLUMN "flag_object_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_error_info" ALTER COLUMN "state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_error_info" ALTER COLUMN "data_state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_error_info" ALTER COLUMN "json_data" TYPE json;

ALTER TABLE "public"."c1_error_info" ALTER COLUMN "system_info_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_error_info" ALTER COLUMN "site_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_error_info" ALTER COLUMN "org_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_error_info" ALTER COLUMN "department_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_error_info" ALTER COLUMN "personal_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_error_info" ALTER COLUMN "error_info_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_error_info" ALTER COLUMN "type_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_error_info" ALTER COLUMN "content" TYPE varchar(4095) COLLATE "default";

ALTER TABLE "public"."c1_error_info" ALTER COLUMN "object_type_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_error_info" ALTER COLUMN "object_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_error_info" ALTER COLUMN "object_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_error_info" ALTER COLUMN "url" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_error_info" ALTER COLUMN "site_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_error_info" ALTER COLUMN "created_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_error_info" ALTER COLUMN "last_modified_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_error_info" ALTER COLUMN "personal_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_i18n_info" ALTER COLUMN "begin_date" SET DEFAULT now();

ALTER TABLE "public"."c1_i18n_info" ALTER COLUMN "created_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_i18n_info" ALTER COLUMN "created_date" SET DEFAULT now();

ALTER TABLE "public"."c1_i18n_info" ALTER COLUMN "last_modified_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_i18n_info" ALTER COLUMN "last_modified_date" SET DEFAULT now();

ALTER TABLE "public"."c1_i18n_info" ALTER COLUMN "flag_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_i18n_info" ALTER COLUMN "batch_no" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_i18n_info" ALTER COLUMN "flag_object_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_i18n_info" ALTER COLUMN "state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_i18n_info" ALTER COLUMN "data_state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_i18n_info" ALTER COLUMN "json_data" TYPE json;

ALTER TABLE "public"."c1_i18n_info" ALTER COLUMN "system_info_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_i18n_info" ALTER COLUMN "site_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_i18n_info" ALTER COLUMN "org_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_i18n_info" ALTER COLUMN "department_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_i18n_info" ALTER COLUMN "personal_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_i18n_info" ALTER COLUMN "i18n_info_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_i18n_info" ALTER COLUMN "type_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_i18n_info" ALTER COLUMN "content" TYPE varchar(4095) COLLATE "default";

ALTER TABLE "public"."c1_i18n_info" ALTER COLUMN "site_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_i18n_info" ALTER COLUMN "created_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_i18n_info" ALTER COLUMN "last_modified_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_i18n_info" ALTER COLUMN "personal_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_i18n_info" ALTER COLUMN "language" TYPE varchar(255) COLLATE "default", ALTER COLUMN "language" SET DEFAULT 'en'::character varying;

ALTER TABLE "public"."c1_navigation" ALTER COLUMN "begin_date" SET DEFAULT now();

ALTER TABLE "public"."c1_navigation" ALTER COLUMN "created_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_navigation" ALTER COLUMN "created_date" SET DEFAULT now();

ALTER TABLE "public"."c1_navigation" ALTER COLUMN "last_modified_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_navigation" ALTER COLUMN "last_modified_date" SET DEFAULT now();

ALTER TABLE "public"."c1_navigation" ALTER COLUMN "flag_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_navigation" ALTER COLUMN "batch_no" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_navigation" ALTER COLUMN "flag_object_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_navigation" ALTER COLUMN "state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_navigation" ALTER COLUMN "data_state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_navigation" ALTER COLUMN "json_data" TYPE json;

ALTER TABLE "public"."c1_navigation" ALTER COLUMN "system_info_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_navigation" ALTER COLUMN "site_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_navigation" ALTER COLUMN "org_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_navigation" ALTER COLUMN "department_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_navigation" ALTER COLUMN "personal_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_navigation" ALTER COLUMN "navigation_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_navigation" ALTER COLUMN "name" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_navigation" ALTER COLUMN "icon_style" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_navigation" ALTER COLUMN "url" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_navigation" ALTER COLUMN "target" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_navigation" ALTER COLUMN "data_option" TYPE varchar(511) COLLATE "default";

ALTER TABLE "public"."c1_navigation" ALTER COLUMN "type_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_navigation" ALTER COLUMN "parent_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_navigation" ALTER COLUMN "parent_tree_code" TYPE varchar(1023) COLLATE "default";

ALTER TABLE "public"."c1_navigation" ALTER COLUMN "parent_tree_name" TYPE varchar(1023) COLLATE "default";

ALTER TABLE "public"."c1_navigation" ALTER COLUMN "created_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_navigation" ALTER COLUMN "last_modified_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_navigation" ALTER COLUMN "personal_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_navigation" ALTER COLUMN "parent_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_navigation" ALTER COLUMN "parent_tree_id" TYPE varchar(1023) COLLATE "default";

ALTER TABLE "public"."c1_notice" ALTER COLUMN "check_person_code" DROP DEFAULT;

ALTER TABLE "public"."c1_notice" ALTER COLUMN "author_person_code" DROP DEFAULT;

ALTER TABLE "public"."c1_notice" ALTER COLUMN "begin_date" SET DEFAULT now();

ALTER TABLE "public"."c1_notice" ALTER COLUMN "created_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_notice" ALTER COLUMN "created_date" SET DEFAULT now();

ALTER TABLE "public"."c1_notice" ALTER COLUMN "last_modified_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_notice" ALTER COLUMN "last_modified_date" SET DEFAULT now();

ALTER TABLE "public"."c1_notice" ALTER COLUMN "flag_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_notice" ALTER COLUMN "batch_no" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_notice" ALTER COLUMN "flag_object_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_notice" ALTER COLUMN "state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_notice" ALTER COLUMN "data_state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_notice" ALTER COLUMN "json_data" TYPE json;

ALTER TABLE "public"."c1_notice" ALTER COLUMN "system_info_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_notice" ALTER COLUMN "site_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_notice" ALTER COLUMN "org_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_notice" ALTER COLUMN "department_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_notice" ALTER COLUMN "personal_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_notice" ALTER COLUMN "title" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_notice" ALTER COLUMN "content" TYPE varchar(4095) COLLATE "default";

ALTER TABLE "public"."c1_notice" ALTER COLUMN "type_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_notice" ALTER COLUMN "created_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_notice" ALTER COLUMN "last_modified_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_notice" ALTER COLUMN "personal_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_oauth_consumer" ALTER COLUMN "begin_date" SET DEFAULT now();

ALTER TABLE "public"."c1_oauth_consumer" ALTER COLUMN "created_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_oauth_consumer" ALTER COLUMN "created_date" SET DEFAULT now();

ALTER TABLE "public"."c1_oauth_consumer" ALTER COLUMN "last_modified_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_oauth_consumer" ALTER COLUMN "last_modified_date" SET DEFAULT now();

ALTER TABLE "public"."c1_oauth_consumer" ALTER COLUMN "flag_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_oauth_consumer" ALTER COLUMN "batch_no" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_oauth_consumer" ALTER COLUMN "flag_object_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_oauth_consumer" ALTER COLUMN "state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_oauth_consumer" ALTER COLUMN "data_state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_oauth_consumer" ALTER COLUMN "json_data" TYPE json;

ALTER TABLE "public"."c1_oauth_consumer" ALTER COLUMN "system_info_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_oauth_consumer" ALTER COLUMN "site_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_oauth_consumer" ALTER COLUMN "org_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_oauth_consumer" ALTER COLUMN "department_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_oauth_consumer" ALTER COLUMN "personal_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_oauth_consumer" ALTER COLUMN "client_secret" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_oauth_consumer" ALTER COLUMN "created_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_oauth_consumer" ALTER COLUMN "last_modified_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_oauth_consumer" ALTER COLUMN "personal_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_open_user" ALTER COLUMN "user_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_open_user" ALTER COLUMN "begin_date" SET DEFAULT now();

ALTER TABLE "public"."c1_open_user" ALTER COLUMN "created_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_open_user" ALTER COLUMN "created_date" SET DEFAULT now();

ALTER TABLE "public"."c1_open_user" ALTER COLUMN "last_modified_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_open_user" ALTER COLUMN "last_modified_date" SET DEFAULT now();

ALTER TABLE "public"."c1_open_user" ALTER COLUMN "flag_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_open_user" ALTER COLUMN "batch_no" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_open_user" ALTER COLUMN "flag_object_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_open_user" ALTER COLUMN "state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_open_user" ALTER COLUMN "data_state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_open_user" ALTER COLUMN "json_data" TYPE json;

ALTER TABLE "public"."c1_open_user" ALTER COLUMN "system_info_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_open_user" ALTER COLUMN "site_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_open_user" ALTER COLUMN "org_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_open_user" ALTER COLUMN "department_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_open_user" ALTER COLUMN "personal_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_open_user" ALTER COLUMN "client_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_open_user" ALTER COLUMN "created_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_open_user" ALTER COLUMN "last_modified_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_open_user" ALTER COLUMN "personal_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_operate_log" ALTER COLUMN "begin_date" SET DEFAULT now();

ALTER TABLE "public"."c1_operate_log" ALTER COLUMN "created_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_operate_log" ALTER COLUMN "created_date" SET DEFAULT now();

ALTER TABLE "public"."c1_operate_log" ALTER COLUMN "last_modified_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_operate_log" ALTER COLUMN "last_modified_date" SET DEFAULT now();

ALTER TABLE "public"."c1_operate_log" ALTER COLUMN "flag_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_operate_log" ALTER COLUMN "batch_no" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_operate_log" ALTER COLUMN "flag_object_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_operate_log" ALTER COLUMN "state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_operate_log" ALTER COLUMN "data_state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_operate_log" ALTER COLUMN "json_data" TYPE json;

ALTER TABLE "public"."c1_operate_log" ALTER COLUMN "system_info_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_operate_log" ALTER COLUMN "site_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_operate_log" ALTER COLUMN "org_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_operate_log" ALTER COLUMN "department_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_operate_log" ALTER COLUMN "personal_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_operate_log" ALTER COLUMN "type_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_operate_log" ALTER COLUMN "message" TYPE varchar(1023) COLLATE "default";

ALTER TABLE "public"."c1_operate_log" ALTER COLUMN "object_type_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_operate_log" ALTER COLUMN "object_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_operate_log" ALTER COLUMN "object_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_operate_log" ALTER COLUMN "created_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_operate_log" ALTER COLUMN "last_modified_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_operate_log" ALTER COLUMN "personal_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_org" ALTER COLUMN "org_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_org" ALTER COLUMN "begin_date" SET DEFAULT now();

ALTER TABLE "public"."c1_org" ALTER COLUMN "created_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_org" ALTER COLUMN "created_date" SET DEFAULT now();

ALTER TABLE "public"."c1_org" ALTER COLUMN "last_modified_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_org" ALTER COLUMN "last_modified_date" SET DEFAULT now();

ALTER TABLE "public"."c1_org" ALTER COLUMN "flag_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_org" ALTER COLUMN "batch_no" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_org" ALTER COLUMN "flag_object_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_org" ALTER COLUMN "state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_org" ALTER COLUMN "data_state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_org" ALTER COLUMN "json_data" TYPE json;

ALTER TABLE "public"."c1_org" ALTER COLUMN "system_info_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_org" ALTER COLUMN "site_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_org" ALTER COLUMN "org_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_org" ALTER COLUMN "department_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_org" ALTER COLUMN "personal_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_org" ALTER COLUMN "name" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_org" ALTER COLUMN "parent_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_org" ALTER COLUMN "parent_tree_code" TYPE varchar(1023) COLLATE "default";

ALTER TABLE "public"."c1_org" ALTER COLUMN "parent_tree_name" TYPE varchar(1023) COLLATE "default";

ALTER TABLE "public"."c1_org" ALTER COLUMN "province_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_org" ALTER COLUMN "city_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_org" ALTER COLUMN "area_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_org" ALTER COLUMN "region_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_org" ALTER COLUMN "address" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_org" ALTER COLUMN "created_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_org" ALTER COLUMN "last_modified_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_org" ALTER COLUMN "personal_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_org" ALTER COLUMN "parent_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_org" ALTER COLUMN "parent_tree_id" TYPE varchar(1023) COLLATE "default";

ALTER TABLE "public"."c1_permission" ALTER COLUMN "begin_date" SET DEFAULT now();

ALTER TABLE "public"."c1_permission" ALTER COLUMN "created_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_permission" ALTER COLUMN "created_date" SET DEFAULT now();

ALTER TABLE "public"."c1_permission" ALTER COLUMN "last_modified_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_permission" ALTER COLUMN "last_modified_date" SET DEFAULT now();

ALTER TABLE "public"."c1_permission" ALTER COLUMN "flag_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_permission" ALTER COLUMN "batch_no" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_permission" ALTER COLUMN "flag_object_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_permission" ALTER COLUMN "state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_permission" ALTER COLUMN "data_state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_permission" ALTER COLUMN "json_data" TYPE json;

ALTER TABLE "public"."c1_permission" ALTER COLUMN "system_info_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_permission" ALTER COLUMN "site_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_permission" ALTER COLUMN "org_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_permission" ALTER COLUMN "department_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_permission" ALTER COLUMN "personal_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_permission" ALTER COLUMN "type_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_permission" ALTER COLUMN "permission_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_permission" ALTER COLUMN "name" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_permission" ALTER COLUMN "parent_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_permission" ALTER COLUMN "parent_tree_code" TYPE varchar(1023) COLLATE "default";

ALTER TABLE "public"."c1_permission" ALTER COLUMN "parent_tree_name" TYPE varchar(1023) COLLATE "default";

ALTER TABLE "public"."c1_permission" ALTER COLUMN "created_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_permission" ALTER COLUMN "last_modified_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_permission" ALTER COLUMN "personal_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_permission" ALTER COLUMN "parent_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_permission" ALTER COLUMN "parent_tree_id" TYPE varchar(1023) COLLATE "default";

ALTER TABLE "public"."c1_province" ALTER COLUMN "begin_date" SET DEFAULT now();

ALTER TABLE "public"."c1_province" ALTER COLUMN "created_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_province" ALTER COLUMN "created_date" SET DEFAULT now();

ALTER TABLE "public"."c1_province" ALTER COLUMN "last_modified_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_province" ALTER COLUMN "last_modified_date" SET DEFAULT now();

ALTER TABLE "public"."c1_province" ALTER COLUMN "flag_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_province" ALTER COLUMN "batch_no" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_province" ALTER COLUMN "flag_object_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_province" ALTER COLUMN "state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_province" ALTER COLUMN "data_state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_province" ALTER COLUMN "json_data" TYPE json;

ALTER TABLE "public"."c1_province" ALTER COLUMN "system_info_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_province" ALTER COLUMN "site_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_province" ALTER COLUMN "org_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_province" ALTER COLUMN "department_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_province" ALTER COLUMN "personal_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_province" ALTER COLUMN "province_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_province" ALTER COLUMN "name" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_province" ALTER COLUMN "country_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_province" ALTER COLUMN "created_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_province" ALTER COLUMN "last_modified_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_province" ALTER COLUMN "personal_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_region" ALTER COLUMN "province_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_region" ALTER COLUMN "city_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_region" ALTER COLUMN "area_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_region" ALTER COLUMN "begin_date" SET DEFAULT now();

ALTER TABLE "public"."c1_region" ALTER COLUMN "created_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_region" ALTER COLUMN "created_date" SET DEFAULT now();

ALTER TABLE "public"."c1_region" ALTER COLUMN "last_modified_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_region" ALTER COLUMN "last_modified_date" SET DEFAULT now();

ALTER TABLE "public"."c1_region" ALTER COLUMN "flag_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_region" ALTER COLUMN "batch_no" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_region" ALTER COLUMN "flag_object_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_region" ALTER COLUMN "state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_region" ALTER COLUMN "data_state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_region" ALTER COLUMN "json_data" TYPE json;

ALTER TABLE "public"."c1_region" ALTER COLUMN "system_info_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_region" ALTER COLUMN "site_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_region" ALTER COLUMN "org_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_region" ALTER COLUMN "department_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_region" ALTER COLUMN "personal_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_region" ALTER COLUMN "region_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_region" ALTER COLUMN "name" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_region" ALTER COLUMN "parent_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_region" ALTER COLUMN "parent_tree_code" TYPE varchar(1023) COLLATE "default";

ALTER TABLE "public"."c1_region" ALTER COLUMN "parent_tree_name" TYPE varchar(1023) COLLATE "default";

ALTER TABLE "public"."c1_region" ALTER COLUMN "country_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_region" ALTER COLUMN "created_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_region" ALTER COLUMN "last_modified_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_region" ALTER COLUMN "personal_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_region" ALTER COLUMN "parent_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_region" ALTER COLUMN "parent_tree_id" TYPE varchar(1023) COLLATE "default";

ALTER TABLE "public"."c1_role" ALTER COLUMN "begin_date" SET DEFAULT now();

ALTER TABLE "public"."c1_role" ALTER COLUMN "created_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_role" ALTER COLUMN "created_date" SET DEFAULT now();

ALTER TABLE "public"."c1_role" ALTER COLUMN "last_modified_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_role" ALTER COLUMN "last_modified_date" SET DEFAULT now();

ALTER TABLE "public"."c1_role" ALTER COLUMN "flag_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_role" ALTER COLUMN "batch_no" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_role" ALTER COLUMN "flag_object_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_role" ALTER COLUMN "state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_role" ALTER COLUMN "data_state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_role" ALTER COLUMN "json_data" TYPE json;

ALTER TABLE "public"."c1_role" ALTER COLUMN "system_info_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_role" ALTER COLUMN "site_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_role" ALTER COLUMN "org_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_role" ALTER COLUMN "department_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_role" ALTER COLUMN "personal_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_role" ALTER COLUMN "type_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_role" ALTER COLUMN "role_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_role" ALTER COLUMN "name" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_role" ALTER COLUMN "parent_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_role" ALTER COLUMN "parent_tree_code" TYPE varchar(1023) COLLATE "default";

ALTER TABLE "public"."c1_role" ALTER COLUMN "parent_tree_name" TYPE varchar(1023) COLLATE "default";

ALTER TABLE "public"."c1_role" ALTER COLUMN "created_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_role" ALTER COLUMN "last_modified_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_role" ALTER COLUMN "personal_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_role" ALTER COLUMN "parent_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_role" ALTER COLUMN "parent_tree_id" TYPE varchar(1023) COLLATE "default";

ALTER TABLE "public"."c1_role_permission" ALTER COLUMN "permission_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_role_permission" ALTER COLUMN "role_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_role_permission" ALTER COLUMN "begin_date" SET DEFAULT now();

ALTER TABLE "public"."c1_role_permission" ALTER COLUMN "created_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_role_permission" ALTER COLUMN "created_date" SET DEFAULT now();

ALTER TABLE "public"."c1_role_permission" ALTER COLUMN "last_modified_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_role_permission" ALTER COLUMN "last_modified_date" SET DEFAULT now();

ALTER TABLE "public"."c1_role_permission" ALTER COLUMN "flag_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_role_permission" ALTER COLUMN "batch_no" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_role_permission" ALTER COLUMN "flag_object_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_role_permission" ALTER COLUMN "state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_role_permission" ALTER COLUMN "data_state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_role_permission" ALTER COLUMN "json_data" TYPE json;

ALTER TABLE "public"."c1_role_permission" ALTER COLUMN "system_info_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_role_permission" ALTER COLUMN "site_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_role_permission" ALTER COLUMN "org_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_role_permission" ALTER COLUMN "department_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_role_permission" ALTER COLUMN "personal_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_role_permission" ALTER COLUMN "created_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_role_permission" ALTER COLUMN "last_modified_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_role_permission" ALTER COLUMN "personal_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_site" ALTER COLUMN "site_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_site" ALTER COLUMN "begin_date" SET DEFAULT now();

ALTER TABLE "public"."c1_site" ALTER COLUMN "created_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_site" ALTER COLUMN "created_date" SET DEFAULT now();

ALTER TABLE "public"."c1_site" ALTER COLUMN "last_modified_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_site" ALTER COLUMN "last_modified_date" SET DEFAULT now();

ALTER TABLE "public"."c1_site" ALTER COLUMN "flag_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_site" ALTER COLUMN "batch_no" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_site" ALTER COLUMN "flag_object_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_site" ALTER COLUMN "state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_site" ALTER COLUMN "data_state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_site" ALTER COLUMN "system_info_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_site" ALTER COLUMN "site_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_site" ALTER COLUMN "org_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_site" ALTER COLUMN "department_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_site" ALTER COLUMN "personal_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_site" ALTER COLUMN "name" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_site" ALTER COLUMN "content" TYPE varchar(4095) COLLATE "default";

ALTER TABLE "public"."c1_site" ALTER COLUMN "explain_" TYPE varchar(511) COLLATE "default";

ALTER TABLE "public"."c1_site" ALTER COLUMN "system_info_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_site" ALTER COLUMN "created_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_site" ALTER COLUMN "last_modified_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_site" ALTER COLUMN "personal_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_site" ALTER COLUMN "remark" TYPE varchar(511) COLLATE "default";

ALTER TABLE "public"."c1_site" ALTER COLUMN "parent_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_site" ALTER COLUMN "parent_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_site" ALTER COLUMN "parent_tree_id" TYPE varchar(1023) COLLATE "default";

ALTER TABLE "public"."c1_site" ALTER COLUMN "parent_tree_code" TYPE varchar(1023) COLLATE "default";

ALTER TABLE "public"."c1_site" ALTER COLUMN "parent_tree_name" TYPE varchar(1023) COLLATE "default";

ALTER TABLE "public"."c1_site" ALTER COLUMN "url" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_site" ALTER COLUMN "ftp_ip" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_site" ALTER COLUMN "ftp_port" TYPE varchar(255) COLLATE "default", ALTER COLUMN "ftp_port" SET DEFAULT '21'::character varying;

ALTER TABLE "public"."c1_site" ALTER COLUMN "ftp_username" TYPE varchar(255) COLLATE "default", ALTER COLUMN "ftp_username" SET DEFAULT 'admin'::character varying;

ALTER TABLE "public"."c1_site" ALTER COLUMN "ftp_password" TYPE varchar(255) COLLATE "default", ALTER COLUMN "ftp_password" SET DEFAULT '888888'::character varying;

COMMENT ON COLUMN "public"."c1_site"."ftp_password" IS 'FTP_PASSWORD';

ALTER TABLE "public"."c1_site" ALTER COLUMN "longitude" TYPE varchar(511) COLLATE "default";

ALTER TABLE "public"."c1_site" ALTER COLUMN "latitude" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_site" ALTER COLUMN "postal_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_system_info" ALTER COLUMN "system_info_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_system_info" ALTER COLUMN "begin_date" SET DEFAULT now();

ALTER TABLE "public"."c1_system_info" ALTER COLUMN "created_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_system_info" ALTER COLUMN "created_date" SET DEFAULT now();

ALTER TABLE "public"."c1_system_info" ALTER COLUMN "last_modified_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_system_info" ALTER COLUMN "last_modified_date" SET DEFAULT now();

ALTER TABLE "public"."c1_system_info" ALTER COLUMN "flag_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_system_info" ALTER COLUMN "batch_no" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_system_info" ALTER COLUMN "flag_object_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_system_info" ALTER COLUMN "state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_system_info" ALTER COLUMN "data_state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_system_info" ALTER COLUMN "json_data" TYPE json;

ALTER TABLE "public"."c1_system_info" ALTER COLUMN "system_info_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_system_info" ALTER COLUMN "site_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_system_info" ALTER COLUMN "org_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_system_info" ALTER COLUMN "department_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_system_info" ALTER COLUMN "personal_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_system_info" ALTER COLUMN "name" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_system_info" ALTER COLUMN "content" TYPE varchar(4095) COLLATE "default";

ALTER TABLE "public"."c1_system_info" ALTER COLUMN "remark" TYPE varchar(511) COLLATE "default";

ALTER TABLE "public"."c1_system_info" ALTER COLUMN "created_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_system_info" ALTER COLUMN "last_modified_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_system_info" ALTER COLUMN "personal_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_token_info" ALTER COLUMN "user_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_token_info" ALTER COLUMN "begin_date" SET DEFAULT now();

ALTER TABLE "public"."c1_token_info" ALTER COLUMN "created_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_token_info" ALTER COLUMN "created_date" SET DEFAULT now();

ALTER TABLE "public"."c1_token_info" ALTER COLUMN "last_modified_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_token_info" ALTER COLUMN "last_modified_date" SET DEFAULT now();

ALTER TABLE "public"."c1_token_info" ALTER COLUMN "flag_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_token_info" ALTER COLUMN "batch_no" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_token_info" ALTER COLUMN "flag_object_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_token_info" ALTER COLUMN "state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_token_info" ALTER COLUMN "data_state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_token_info" ALTER COLUMN "json_data" TYPE json;

ALTER TABLE "public"."c1_token_info" ALTER COLUMN "system_info_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_token_info" ALTER COLUMN "site_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_token_info" ALTER COLUMN "org_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_token_info" ALTER COLUMN "department_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_token_info" ALTER COLUMN "personal_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_token_info" ALTER COLUMN "token_info_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_token_info" ALTER COLUMN "client_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_token_info" ALTER COLUMN "scope" TYPE varchar(511) COLLATE "default";

ALTER TABLE "public"."c1_token_info" ALTER COLUMN "created_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_token_info" ALTER COLUMN "last_modified_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_token_info" ALTER COLUMN "personal_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "begin_date" SET DEFAULT now();

ALTER TABLE "public"."c1_user" ALTER COLUMN "created_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "created_date" SET DEFAULT now();

ALTER TABLE "public"."c1_user" ALTER COLUMN "last_modified_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "last_modified_date" TYPE timestamp(3), ALTER COLUMN "last_modified_date" SET DEFAULT now();

ALTER TABLE "public"."c1_user" ALTER COLUMN "flag_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "batch_no" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "flag_object_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "data_state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "system_info_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "site_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "org_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "department_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "personal_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "user_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "name" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "password_" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "password_salt" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "email" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "intro" TYPE text COLLATE "default", ALTER COLUMN "intro" SET DEFAULT ''::text;

ALTER TABLE "public"."c1_user" ALTER COLUMN "mobile_phone" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "locus" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "company_name" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "contact" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "telephone" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "birthday" TYPE timestamp(3), ALTER COLUMN "birthday" SET DEFAULT now();

ALTER TABLE "public"."c1_user" ALTER COLUMN "call_name" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "profession_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "marry_state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "folk_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "diploma_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "country_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "province_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "city_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "area_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "region_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "address" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "sex_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "qq" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "id_card_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "id_card_no" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "private_password" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "private_password_salt" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "portrait_url" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "remark" TYPE varchar(511) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "alias_name" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "created_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "last_modified_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "personal_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "source_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "third_party_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "compatible_user_code_1" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "compatible_user_code_2" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "compatible_department_code_1" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "compatible_department_code_2" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "compatible_password_1" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "compatible_password_2" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user" ALTER COLUMN "sync_return_code" TYPE varchar(128) COLLATE "default", ALTER COLUMN "sync_return_code" SET DEFAULT 'wait'::character varying;

COMMENT ON COLUMN "public"."c1_user"."sync_return_code" IS '';

COMMENT ON COLUMN "public"."c1_user"."sync_return_message" IS '';

COMMENT ON COLUMN "public"."c1_user"."last_sync_time" IS '';

ALTER TABLE "public"."c1_user_address" ALTER COLUMN "city_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_address" ALTER COLUMN "area_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_address" ALTER COLUMN "region_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_address" ALTER COLUMN "user_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_address" ALTER COLUMN "province_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_address" ALTER COLUMN "begin_date" SET DEFAULT now();

ALTER TABLE "public"."c1_user_address" ALTER COLUMN "created_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_address" ALTER COLUMN "created_date" SET DEFAULT now();

ALTER TABLE "public"."c1_user_address" ALTER COLUMN "last_modified_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_address" ALTER COLUMN "last_modified_date" SET DEFAULT now();

ALTER TABLE "public"."c1_user_address" ALTER COLUMN "flag_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_address" ALTER COLUMN "batch_no" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_address" ALTER COLUMN "flag_object_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_address" ALTER COLUMN "state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_address" ALTER COLUMN "data_state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_address" ALTER COLUMN "json_data" TYPE json;

ALTER TABLE "public"."c1_user_address" ALTER COLUMN "system_info_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_address" ALTER COLUMN "site_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_address" ALTER COLUMN "org_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_address" ALTER COLUMN "department_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_address" ALTER COLUMN "personal_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_address" ALTER COLUMN "type_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_address" ALTER COLUMN "is_default" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_address" ALTER COLUMN "country_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_address" ALTER COLUMN "address" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_address" ALTER COLUMN "created_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_address" ALTER COLUMN "last_modified_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_address" ALTER COLUMN "personal_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_department" ALTER COLUMN "user_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_department" ALTER COLUMN "begin_date" SET DEFAULT now();

ALTER TABLE "public"."c1_user_department" ALTER COLUMN "created_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_department" ALTER COLUMN "created_date" SET DEFAULT now();

ALTER TABLE "public"."c1_user_department" ALTER COLUMN "last_modified_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_department" ALTER COLUMN "last_modified_date" SET DEFAULT now();

ALTER TABLE "public"."c1_user_department" ALTER COLUMN "flag_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_department" ALTER COLUMN "batch_no" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_department" ALTER COLUMN "flag_object_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_department" ALTER COLUMN "state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_department" ALTER COLUMN "data_state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_department" ALTER COLUMN "json_data" TYPE json;

ALTER TABLE "public"."c1_user_department" ALTER COLUMN "system_info_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_department" ALTER COLUMN "site_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_department" ALTER COLUMN "org_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_department" ALTER COLUMN "department_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_department" ALTER COLUMN "personal_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_department" ALTER COLUMN "created_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_department" ALTER COLUMN "last_modified_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_department" ALTER COLUMN "personal_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group" ALTER COLUMN "begin_date" SET DEFAULT now();

ALTER TABLE "public"."c1_user_group" ALTER COLUMN "created_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group" ALTER COLUMN "created_date" SET DEFAULT now();

ALTER TABLE "public"."c1_user_group" ALTER COLUMN "last_modified_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group" ALTER COLUMN "last_modified_date" SET DEFAULT now();

ALTER TABLE "public"."c1_user_group" ALTER COLUMN "flag_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group" ALTER COLUMN "batch_no" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group" ALTER COLUMN "flag_object_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group" ALTER COLUMN "state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group" ALTER COLUMN "data_state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group" ALTER COLUMN "json_data" TYPE json;

ALTER TABLE "public"."c1_user_group" ALTER COLUMN "system_info_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group" ALTER COLUMN "site_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group" ALTER COLUMN "org_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group" ALTER COLUMN "department_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group" ALTER COLUMN "personal_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group" ALTER COLUMN "type_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group" ALTER COLUMN "user_group_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group" ALTER COLUMN "name" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group" ALTER COLUMN "parent_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group" ALTER COLUMN "parent_tree_code" TYPE varchar(1023) COLLATE "default";

ALTER TABLE "public"."c1_user_group" ALTER COLUMN "parent_tree_name" TYPE varchar(1023) COLLATE "default";

ALTER TABLE "public"."c1_user_group" ALTER COLUMN "created_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group" ALTER COLUMN "last_modified_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group" ALTER COLUMN "personal_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group" ALTER COLUMN "parent_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group" ALTER COLUMN "parent_tree_id" TYPE varchar(1023) COLLATE "default";

ALTER TABLE "public"."c1_user_group_permission" ALTER COLUMN "user_group_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_permission" ALTER COLUMN "permission_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_permission" ALTER COLUMN "begin_date" SET DEFAULT now();

ALTER TABLE "public"."c1_user_group_permission" ALTER COLUMN "created_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_permission" ALTER COLUMN "created_date" SET DEFAULT now();

ALTER TABLE "public"."c1_user_group_permission" ALTER COLUMN "last_modified_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_permission" ALTER COLUMN "last_modified_date" SET DEFAULT now();

ALTER TABLE "public"."c1_user_group_permission" ALTER COLUMN "flag_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_permission" ALTER COLUMN "batch_no" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_permission" ALTER COLUMN "flag_object_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_permission" ALTER COLUMN "state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_permission" ALTER COLUMN "data_state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_permission" ALTER COLUMN "json_data" TYPE json;

ALTER TABLE "public"."c1_user_group_permission" ALTER COLUMN "system_info_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_permission" ALTER COLUMN "site_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_permission" ALTER COLUMN "org_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_permission" ALTER COLUMN "department_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_permission" ALTER COLUMN "personal_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_permission" ALTER COLUMN "created_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_permission" ALTER COLUMN "last_modified_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_permission" ALTER COLUMN "personal_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_role" ALTER COLUMN "role_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_role" ALTER COLUMN "user_group_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_role" ALTER COLUMN "begin_date" SET DEFAULT now();

ALTER TABLE "public"."c1_user_group_role" ALTER COLUMN "created_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_role" ALTER COLUMN "created_date" SET DEFAULT now();

ALTER TABLE "public"."c1_user_group_role" ALTER COLUMN "last_modified_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_role" ALTER COLUMN "last_modified_date" SET DEFAULT now();

ALTER TABLE "public"."c1_user_group_role" ALTER COLUMN "flag_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_role" ALTER COLUMN "batch_no" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_role" ALTER COLUMN "flag_object_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_role" ALTER COLUMN "state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_role" ALTER COLUMN "data_state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_role" ALTER COLUMN "json_data" TYPE json;

ALTER TABLE "public"."c1_user_group_role" ALTER COLUMN "system_info_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_role" ALTER COLUMN "site_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_role" ALTER COLUMN "org_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_role" ALTER COLUMN "department_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_role" ALTER COLUMN "personal_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_role" ALTER COLUMN "created_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_role" ALTER COLUMN "last_modified_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_role" ALTER COLUMN "personal_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_user" ALTER COLUMN "user_group_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_user" ALTER COLUMN "user_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_user" ALTER COLUMN "begin_date" SET DEFAULT now();

ALTER TABLE "public"."c1_user_group_user" ALTER COLUMN "created_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_user" ALTER COLUMN "created_date" SET DEFAULT now();

ALTER TABLE "public"."c1_user_group_user" ALTER COLUMN "last_modified_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_user" ALTER COLUMN "last_modified_date" SET DEFAULT now();

ALTER TABLE "public"."c1_user_group_user" ALTER COLUMN "flag_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_user" ALTER COLUMN "batch_no" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_user" ALTER COLUMN "flag_object_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_user" ALTER COLUMN "state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_user" ALTER COLUMN "data_state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_user" ALTER COLUMN "json_data" TYPE json;

ALTER TABLE "public"."c1_user_group_user" ALTER COLUMN "system_info_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_user" ALTER COLUMN "site_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_user" ALTER COLUMN "org_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_user" ALTER COLUMN "department_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_user" ALTER COLUMN "personal_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_user" ALTER COLUMN "created_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_user" ALTER COLUMN "last_modified_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_group_user" ALTER COLUMN "personal_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_org" ALTER COLUMN "user_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_org" ALTER COLUMN "begin_date" SET DEFAULT now();

ALTER TABLE "public"."c1_user_org" ALTER COLUMN "created_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_org" ALTER COLUMN "created_date" SET DEFAULT now();

ALTER TABLE "public"."c1_user_org" ALTER COLUMN "last_modified_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_org" ALTER COLUMN "last_modified_date" SET DEFAULT now();

ALTER TABLE "public"."c1_user_org" ALTER COLUMN "flag_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_org" ALTER COLUMN "batch_no" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_org" ALTER COLUMN "flag_object_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_org" ALTER COLUMN "state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_org" ALTER COLUMN "data_state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_org" ALTER COLUMN "json_data" TYPE json;

ALTER TABLE "public"."c1_user_org" ALTER COLUMN "system_info_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_org" ALTER COLUMN "site_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_org" ALTER COLUMN "org_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_org" ALTER COLUMN "department_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_org" ALTER COLUMN "personal_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_org" ALTER COLUMN "created_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_org" ALTER COLUMN "last_modified_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_org" ALTER COLUMN "personal_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_org" ALTER COLUMN "org_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_permission" ALTER COLUMN "permission_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_permission" ALTER COLUMN "user_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_permission" ALTER COLUMN "begin_date" SET DEFAULT now();

ALTER TABLE "public"."c1_user_permission" ALTER COLUMN "created_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_permission" ALTER COLUMN "created_date" SET DEFAULT now();

ALTER TABLE "public"."c1_user_permission" ALTER COLUMN "last_modified_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_permission" ALTER COLUMN "last_modified_date" SET DEFAULT now();

ALTER TABLE "public"."c1_user_permission" ALTER COLUMN "flag_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_permission" ALTER COLUMN "batch_no" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_permission" ALTER COLUMN "flag_object_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_permission" ALTER COLUMN "state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_permission" ALTER COLUMN "data_state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_permission" ALTER COLUMN "json_data" TYPE json;

ALTER TABLE "public"."c1_user_permission" ALTER COLUMN "system_info_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_permission" ALTER COLUMN "site_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_permission" ALTER COLUMN "org_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_permission" ALTER COLUMN "department_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_permission" ALTER COLUMN "personal_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_permission" ALTER COLUMN "created_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_permission" ALTER COLUMN "last_modified_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_permission" ALTER COLUMN "personal_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_role" ALTER COLUMN "user_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_role" ALTER COLUMN "role_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_role" ALTER COLUMN "begin_date" SET DEFAULT now();

ALTER TABLE "public"."c1_user_role" ALTER COLUMN "created_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_role" ALTER COLUMN "created_date" SET DEFAULT now();

ALTER TABLE "public"."c1_user_role" ALTER COLUMN "last_modified_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_role" ALTER COLUMN "last_modified_date" SET DEFAULT now();

ALTER TABLE "public"."c1_user_role" ALTER COLUMN "flag_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_role" ALTER COLUMN "batch_no" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_role" ALTER COLUMN "flag_object_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_role" ALTER COLUMN "state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_role" ALTER COLUMN "data_state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_role" ALTER COLUMN "json_data" TYPE json;

ALTER TABLE "public"."c1_user_role" ALTER COLUMN "system_info_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_role" ALTER COLUMN "site_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_role" ALTER COLUMN "org_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_role" ALTER COLUMN "department_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_role" ALTER COLUMN "personal_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_role" ALTER COLUMN "created_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_role" ALTER COLUMN "last_modified_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_user_role" ALTER COLUMN "personal_id" TYPE varchar(255) COLLATE "default";

CREATE TABLE "public"."c1_variable" (
"variable_id" varchar(255) COLLATE "default" NOT NULL,
"begin_date" timestamp(6) DEFAULT CURRENT_TIMESTAMP,
"end_date" timestamp(6) DEFAULT to_timestamp('9999-01-01 00:00:00'::text, 'yyyy-mm-dd hh24:mi:ss'::text),
"created_by_id" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"created_by_code" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"created_date" timestamp(6) DEFAULT CURRENT_TIMESTAMP,
"last_modified_by_id" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"last_modified_by_code" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"last_modified_date" timestamp(6) DEFAULT CURRENT_TIMESTAMP,
"flag_code" varchar(255) COLLATE "default" DEFAULT 'init'::character varying,
"batch_no" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"flag_object_code" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"state_code" varchar(255) COLLATE "default" DEFAULT '1'::character varying,
"data_state_code" varchar(255) COLLATE "default" DEFAULT '1'::character varying,
"order_by_" int8 DEFAULT 0,
"json_data" jsonb,
"version_" int4 DEFAULT 1,
"system_info_code" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"site_code" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"org_code" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"department_code" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"personal_code" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"personal_id" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"variable_code" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"value_" varchar(1023) COLLATE "default" DEFAULT ''::character varying,
CONSTRAINT "pk_c1_variable" PRIMARY KEY ("variable_id")
)
WITH (OIDS=FALSE);

COMMENT ON TABLE "public"."c1_variable" IS '变量';

COMMENT ON COLUMN "public"."c1_variable"."variable_id" IS '变量标识';

COMMENT ON COLUMN "public"."c1_variable"."begin_date" IS '开始日期';

COMMENT ON COLUMN "public"."c1_variable"."end_date" IS '结束日期';

COMMENT ON COLUMN "public"."c1_variable"."created_by_id" IS '创建人标识';

COMMENT ON COLUMN "public"."c1_variable"."created_by_code" IS '创建人编号';

COMMENT ON COLUMN "public"."c1_variable"."created_date" IS '创建日期';

COMMENT ON COLUMN "public"."c1_variable"."last_modified_by_id" IS '最后修改人标识';

COMMENT ON COLUMN "public"."c1_variable"."last_modified_by_code" IS '最后修改人编号';

COMMENT ON COLUMN "public"."c1_variable"."last_modified_date" IS '最后修改日期';

COMMENT ON COLUMN "public"."c1_variable"."flag_code" IS '标记编号(数据字典：工作流、同步、生成、录入、审批)';

COMMENT ON COLUMN "public"."c1_variable"."batch_no" IS '批次编号';

COMMENT ON COLUMN "public"."c1_variable"."flag_object_code" IS '标识对象编号(工作流：工作流标识、同步：批次号、生成：操作者编号、录入：操作者编号、审批：操作者编号)';

COMMENT ON COLUMN "public"."c1_variable"."state_code" IS '状态编号(数据字典)';

COMMENT ON COLUMN "public"."c1_variable"."data_state_code" IS '数据状态编号(数据字典)';

COMMENT ON COLUMN "public"."c1_variable"."order_by_" IS '排序';

COMMENT ON COLUMN "public"."c1_variable"."json_data" IS 'json数据';

COMMENT ON COLUMN "public"."c1_variable"."version_" IS '版本';

COMMENT ON COLUMN "public"."c1_variable"."system_info_code" IS '系统信息编号';

COMMENT ON COLUMN "public"."c1_variable"."site_code" IS '站点编号';

COMMENT ON COLUMN "public"."c1_variable"."org_code" IS '组织编号';

COMMENT ON COLUMN "public"."c1_variable"."department_code" IS '部门编号';

COMMENT ON COLUMN "public"."c1_variable"."personal_code" IS '个人编号';

COMMENT ON COLUMN "public"."c1_variable"."personal_id" IS '个人标识';

COMMENT ON COLUMN "public"."c1_variable"."variable_code" IS '变量编号';

COMMENT ON COLUMN "public"."c1_variable"."value_" IS '值';

CREATE TABLE "public"."c1_wf_task_handle" (
"wf_task_handle_id" varchar(255) COLLATE "default" NOT NULL,
"begin_date" timestamp(6) DEFAULT CURRENT_TIMESTAMP,
"end_date" timestamp(6) DEFAULT to_timestamp('9999-01-01 00:00:00'::text, 'yyyy-mm-dd hh24:mi:ss'::text),
"created_by_id" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"created_by_code" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"created_date" timestamp(6) DEFAULT CURRENT_TIMESTAMP,
"last_modified_by_id" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"last_modified_by_code" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"last_modified_date" timestamp(3) DEFAULT CURRENT_TIMESTAMP,
"flag_code" varchar(255) COLLATE "default" DEFAULT 'init'::character varying,
"batch_no" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"flag_object_code" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"state_code" varchar(255) COLLATE "default" DEFAULT '1'::character varying,
"data_state_code" varchar(255) COLLATE "default" DEFAULT '1'::character varying,
"order_by_" int8 DEFAULT 0,
"json_data" jsonb,
"version_" int4 DEFAULT 1,
"system_info_code" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"site_code" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"org_code" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"department_code" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"personal_code" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"personal_id" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"business_id" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"business_code" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"business_type_code" varchar(255) COLLATE "default" DEFAULT 'other'::character varying,
CONSTRAINT "pk_c1_wf_task_handle" PRIMARY KEY ("wf_task_handle_id")
)
WITH (OIDS=FALSE);

COMMENT ON TABLE "public"."c1_wf_task_handle" IS '工作流任务处理';

COMMENT ON COLUMN "public"."c1_wf_task_handle"."wf_task_handle_id" IS '工作流任务处理标识';

COMMENT ON COLUMN "public"."c1_wf_task_handle"."begin_date" IS '开始日期';

COMMENT ON COLUMN "public"."c1_wf_task_handle"."end_date" IS '结束日期';

COMMENT ON COLUMN "public"."c1_wf_task_handle"."created_by_id" IS '创建人标识';

COMMENT ON COLUMN "public"."c1_wf_task_handle"."created_by_code" IS '创建人编号';

COMMENT ON COLUMN "public"."c1_wf_task_handle"."created_date" IS '创建日期';

COMMENT ON COLUMN "public"."c1_wf_task_handle"."last_modified_by_id" IS '最后修改人标识';

COMMENT ON COLUMN "public"."c1_wf_task_handle"."last_modified_by_code" IS '最后修改人编号';

COMMENT ON COLUMN "public"."c1_wf_task_handle"."last_modified_date" IS '最后修改日期';

COMMENT ON COLUMN "public"."c1_wf_task_handle"."flag_code" IS '标记编号(数据字典：工作流、同步、生成、录入、审批)';

COMMENT ON COLUMN "public"."c1_wf_task_handle"."batch_no" IS '批次编号';

COMMENT ON COLUMN "public"."c1_wf_task_handle"."flag_object_code" IS '标识对象编号(工作流：工作流标识、同步：批次号、生成：操作者编号、录入：操作者编号、审批：操作者编号)';

COMMENT ON COLUMN "public"."c1_wf_task_handle"."state_code" IS '状态编号(数据字典)';

COMMENT ON COLUMN "public"."c1_wf_task_handle"."data_state_code" IS '数据状态编号(数据字典)';

COMMENT ON COLUMN "public"."c1_wf_task_handle"."order_by_" IS '排序';

COMMENT ON COLUMN "public"."c1_wf_task_handle"."json_data" IS 'json数据';

COMMENT ON COLUMN "public"."c1_wf_task_handle"."version_" IS '版本';

COMMENT ON COLUMN "public"."c1_wf_task_handle"."system_info_code" IS '系统信息编号';

COMMENT ON COLUMN "public"."c1_wf_task_handle"."site_code" IS '站点编号';

COMMENT ON COLUMN "public"."c1_wf_task_handle"."org_code" IS '组织编号';

COMMENT ON COLUMN "public"."c1_wf_task_handle"."department_code" IS '部门编号';

COMMENT ON COLUMN "public"."c1_wf_task_handle"."personal_code" IS '个人编号';

COMMENT ON COLUMN "public"."c1_wf_task_handle"."personal_id" IS '个人标识';

COMMENT ON COLUMN "public"."c1_wf_task_handle"."business_id" IS '业务标识';

COMMENT ON COLUMN "public"."c1_wf_task_handle"."business_code" IS '业务编号';

COMMENT ON COLUMN "public"."c1_wf_task_handle"."business_type_code" IS '业务类型';

CREATE TABLE "public"."c1_wf_task_handle_retry" (
"wf_task_handle_retry_id" varchar(255) COLLATE "default" NOT NULL,
"wf_task_handle_id" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"begin_date" timestamp(6) DEFAULT CURRENT_TIMESTAMP,
"end_date" timestamp(6) DEFAULT to_timestamp('9999-01-01 00:00:00'::text, 'yyyy-mm-dd hh24:mi:ss'::text),
"created_by_id" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"created_by_code" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"created_date" timestamp(6) DEFAULT CURRENT_TIMESTAMP,
"last_modified_by_id" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"last_modified_by_code" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"last_modified_date" timestamp(3) DEFAULT CURRENT_TIMESTAMP,
"flag_code" varchar(255) COLLATE "default" DEFAULT 'init'::character varying,
"batch_no" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"flag_object_code" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"state_code" varchar(255) COLLATE "default" DEFAULT '1'::character varying,
"data_state_code" varchar(255) COLLATE "default" DEFAULT '1'::character varying,
"order_by_" int8 DEFAULT 0,
"json_data" jsonb,
"version_" int4 DEFAULT 1,
"system_info_code" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"site_code" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"org_code" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"department_code" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"personal_code" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"personal_id" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"handle_code" varchar(255) COLLATE "default" DEFAULT 'other'::character varying,
"retry_count" int8 DEFAULT 0,
"last_retry_date" timestamp(6) DEFAULT CURRENT_TIMESTAMP,
"error_message" varchar(1000) COLLATE "default" DEFAULT ''::character varying,
"error_date" timestamp(6) DEFAULT CURRENT_TIMESTAMP,
CONSTRAINT "pk_c1_wf_task_handle_retry" PRIMARY KEY ("wf_task_handle_retry_id")
)
WITH (OIDS=FALSE);

COMMENT ON TABLE "public"."c1_wf_task_handle_retry" IS '工作流任务处理重试';

COMMENT ON COLUMN "public"."c1_wf_task_handle_retry"."wf_task_handle_retry_id" IS '工作流任务处理重试标识';

COMMENT ON COLUMN "public"."c1_wf_task_handle_retry"."wf_task_handle_id" IS '工作流任务处理标识';

COMMENT ON COLUMN "public"."c1_wf_task_handle_retry"."begin_date" IS '开始日期';

COMMENT ON COLUMN "public"."c1_wf_task_handle_retry"."end_date" IS '结束日期';

COMMENT ON COLUMN "public"."c1_wf_task_handle_retry"."created_by_id" IS '创建人标识';

COMMENT ON COLUMN "public"."c1_wf_task_handle_retry"."created_by_code" IS '创建人编号';

COMMENT ON COLUMN "public"."c1_wf_task_handle_retry"."created_date" IS '创建日期';

COMMENT ON COLUMN "public"."c1_wf_task_handle_retry"."last_modified_by_id" IS '最后修改人标识';

COMMENT ON COLUMN "public"."c1_wf_task_handle_retry"."last_modified_by_code" IS '最后修改人编号';

COMMENT ON COLUMN "public"."c1_wf_task_handle_retry"."last_modified_date" IS '最后修改日期';

COMMENT ON COLUMN "public"."c1_wf_task_handle_retry"."flag_code" IS '标记编号(数据字典：工作流、同步、生成、录入、审批)';

COMMENT ON COLUMN "public"."c1_wf_task_handle_retry"."batch_no" IS '批次编号';

COMMENT ON COLUMN "public"."c1_wf_task_handle_retry"."flag_object_code" IS '标识对象编号(工作流：工作流标识、同步：批次号、生成：操作者编号、录入：操作者编号、审批：操作者编号)';

COMMENT ON COLUMN "public"."c1_wf_task_handle_retry"."state_code" IS '状态编号(数据字典)';

COMMENT ON COLUMN "public"."c1_wf_task_handle_retry"."data_state_code" IS '数据状态编号(数据字典)';

COMMENT ON COLUMN "public"."c1_wf_task_handle_retry"."order_by_" IS '排序';

COMMENT ON COLUMN "public"."c1_wf_task_handle_retry"."json_data" IS 'json数据';

COMMENT ON COLUMN "public"."c1_wf_task_handle_retry"."version_" IS '版本';

COMMENT ON COLUMN "public"."c1_wf_task_handle_retry"."system_info_code" IS '系统信息编号';

COMMENT ON COLUMN "public"."c1_wf_task_handle_retry"."site_code" IS '站点编号';

COMMENT ON COLUMN "public"."c1_wf_task_handle_retry"."org_code" IS '组织编号';

COMMENT ON COLUMN "public"."c1_wf_task_handle_retry"."department_code" IS '部门编号';

COMMENT ON COLUMN "public"."c1_wf_task_handle_retry"."personal_code" IS '个人编号';

COMMENT ON COLUMN "public"."c1_wf_task_handle_retry"."personal_id" IS '个人标识';

COMMENT ON COLUMN "public"."c1_wf_task_handle_retry"."handle_code" IS '处理类型编号';

COMMENT ON COLUMN "public"."c1_wf_task_handle_retry"."retry_count" IS '重试次数';

COMMENT ON COLUMN "public"."c1_wf_task_handle_retry"."last_retry_date" IS '最近重试时间';

COMMENT ON COLUMN "public"."c1_wf_task_handle_retry"."error_message" IS '错误信息';

COMMENT ON COLUMN "public"."c1_wf_task_handle_retry"."error_date" IS '错误时间';

ALTER TABLE "public"."c1_wf_variable" ALTER COLUMN "begin_date" SET DEFAULT now();

ALTER TABLE "public"."c1_wf_variable" ALTER COLUMN "created_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_wf_variable" ALTER COLUMN "created_date" SET DEFAULT now();

ALTER TABLE "public"."c1_wf_variable" ALTER COLUMN "last_modified_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_wf_variable" ALTER COLUMN "last_modified_date" SET DEFAULT now();

ALTER TABLE "public"."c1_wf_variable" ALTER COLUMN "flag_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_wf_variable" ALTER COLUMN "batch_no" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_wf_variable" ALTER COLUMN "flag_object_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_wf_variable" ALTER COLUMN "state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_wf_variable" ALTER COLUMN "data_state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_wf_variable" ALTER COLUMN "json_data" TYPE json;

ALTER TABLE "public"."c1_wf_variable" ALTER COLUMN "system_info_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_wf_variable" ALTER COLUMN "site_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_wf_variable" ALTER COLUMN "org_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_wf_variable" ALTER COLUMN "department_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_wf_variable" ALTER COLUMN "personal_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_wf_variable" ALTER COLUMN "wf_key" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_wf_variable" ALTER COLUMN "content" TYPE varchar(4095) COLLATE "default";

ALTER TABLE "public"."c1_wf_variable" ALTER COLUMN "created_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_wf_variable" ALTER COLUMN "last_modified_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_wf_variable" ALTER COLUMN "personal_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_wf_variable" ALTER COLUMN "type_code" TYPE varchar(255) COLLATE "default";

COMMENT ON COLUMN "public"."c1_wf_variable"."type_code" IS '类别编号';

ALTER TABLE "public"."c1_wf_variable" ALTER COLUMN "name" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_wf_variable_user" ALTER COLUMN "user_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_wf_variable_user" ALTER COLUMN "begin_date" SET DEFAULT now();

ALTER TABLE "public"."c1_wf_variable_user" ALTER COLUMN "created_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_wf_variable_user" ALTER COLUMN "created_date" SET DEFAULT now();

ALTER TABLE "public"."c1_wf_variable_user" ALTER COLUMN "last_modified_by_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_wf_variable_user" ALTER COLUMN "last_modified_date" SET DEFAULT now();

ALTER TABLE "public"."c1_wf_variable_user" ALTER COLUMN "flag_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_wf_variable_user" ALTER COLUMN "batch_no" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_wf_variable_user" ALTER COLUMN "flag_object_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_wf_variable_user" ALTER COLUMN "state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_wf_variable_user" ALTER COLUMN "data_state_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_wf_variable_user" ALTER COLUMN "json_data" TYPE json;

ALTER TABLE "public"."c1_wf_variable_user" ALTER COLUMN "system_info_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_wf_variable_user" ALTER COLUMN "site_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_wf_variable_user" ALTER COLUMN "org_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_wf_variable_user" ALTER COLUMN "department_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_wf_variable_user" ALTER COLUMN "personal_code" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_wf_variable_user" ALTER COLUMN "wf_key" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_wf_variable_user" ALTER COLUMN "created_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_wf_variable_user" ALTER COLUMN "last_modified_by_id" TYPE varchar(255) COLLATE "default";

ALTER TABLE "public"."c1_wf_variable_user" ALTER COLUMN "personal_id" TYPE varchar(255) COLLATE "default";
