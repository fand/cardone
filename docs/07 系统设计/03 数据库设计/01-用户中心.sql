﻿DROP TABLE IF EXISTS C1_OPEN_USER;

DROP TABLE IF EXISTS C1_DEPARTMENT;

DROP TABLE IF EXISTS C1_ORG;

DROP TABLE IF EXISTS C1_USER;

DROP TABLE IF EXISTS C1_USER_DEPARTMENT;

/*==============================================================*/
/* Table: C1_OPEN_USER                                          */
/*==============================================================*/
CREATE TABLE C1_OPEN_USER
(
   OPEN_USER_ID         VARCHAR(36) NOT NULL COMMENT '开放用户标识',
   ORG_CODE             VARCHAR(36) COMMENT '组织代码',
   DEPARTMENT_CODE      VARCHAR(36) COMMENT '部门代码',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_CODE      VARCHAR(36) COMMENT '创建人代码',
   CREATED_DATE         DATETIME COMMENT '创建时间',
   LAST_MODIFIED_BY_CODE VARCHAR(36) COMMENT '最后修改人代码',
   LAST_MODIFIED_DATE   DATETIME COMMENT '最后修改时间',
   STATE_CODE           VARCHAR(36) COMMENT '状态代码',
   DATA_STATE_CODE      VARCHAR(36) COMMENT '数据状态代码',
   VERSION_             INT DEFAULT 0 COMMENT '版本',
   WF_ID                VARCHAR(36) COMMENT '工作流标识',
   ROLE_CODES           VARCHAR(1024) COMMENT '角色代码集合',
   PERMISSION_CODES     VARCHAR(1024) COMMENT '许可代码集合',
   USER_CODE            VARCHAR(36) NOT NULL COMMENT '用户代码',
   CLIENT_ID            VARCHAR(64) NOT NULL COMMENT '客户端标识',
   PRIMARY KEY (OPEN_USER_ID)
);

ALTER TABLE C1_OPEN_USER COMMENT '开放用户';

/*==============================================================*/
/* Table: C1_DEPARTMENT                                         */
/*==============================================================*/
CREATE TABLE C1_DEPARTMENT
(
   DEPARTMENT_ID        VARCHAR(36) NOT NULL COMMENT '部门标识',
   ORG_CODE             VARCHAR(36) COMMENT '组织代码',
   DEPARTMENT_CODE      VARCHAR(36) COMMENT '部门代码',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_CODE      VARCHAR(36) COMMENT '创建人代码',
   CREATED_DATE         DATETIME COMMENT '创建时间',
   LAST_MODIFIED_BY_CODE VARCHAR(36) COMMENT '最后修改人代码',
   LAST_MODIFIED_DATE   DATETIME COMMENT '最后修改时间',
   STATE_CODE           VARCHAR(36) COMMENT '状态代码',
   DATA_STATE_CODE      VARCHAR(36) COMMENT '数据状态代码',
   VERSION_             INT DEFAULT 0 COMMENT '版本',
   WF_ID                VARCHAR(36) COMMENT '工作流标识',
   ROLE_CODES           VARCHAR(1024) COMMENT '角色代码集合',
   PERMISSION_CODES     VARCHAR(1024) COMMENT '许可代码集合',
   NAME                 VARCHAR(256) NOT NULL COMMENT '名称',
   PARENT_CODE          VARCHAR(36) COMMENT '父级代码',
   PARENT_TREE_CODE     VARCHAR(1024) COMMENT '父级树代码',
   PARENT_TREE_NAME     VARCHAR(1024) COMMENT '父级树名称',
   PRIMARY KEY (DEPARTMENT_ID)
);

ALTER TABLE C1_DEPARTMENT COMMENT '部门';

/*==============================================================*/
/* Table: C1_ORG                                                */
/*==============================================================*/
CREATE TABLE C1_ORG
(
   ORG_ID               VARCHAR(36) NOT NULL COMMENT '组织标识',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_CODE      VARCHAR(36) COMMENT '创建人代码',
   CREATED_DATE         DATETIME COMMENT '创建时间',
   LAST_MODIFIED_BY_CODE VARCHAR(36) COMMENT '最后修改人代码',
   LAST_MODIFIED_DATE   DATETIME COMMENT '最后修改时间',
   STATE_CODE           VARCHAR(36) COMMENT '状态代码',
   DATA_STATE_CODE      VARCHAR(36) COMMENT '数据状态代码',
   VERSION_             INT DEFAULT 0 COMMENT '版本',
   WF_ID                VARCHAR(36) COMMENT '工作流标识',
   ROLE_CODES           VARCHAR(1024) COMMENT '角色代码集合',
   PERMISSION_CODES     VARCHAR(1024) COMMENT '许可代码集合',
   ORG_CODE             VARCHAR(36) NOT NULL COMMENT '组织代码',
   NAME                 VARCHAR(256) COMMENT '名称',
   PARENT_CODE          VARCHAR(36) COMMENT '父级代码',
   PARENT_TREE_CODE     VARCHAR(1024) COMMENT '父级树代码',
   PARENT_TREE_NAME     VARCHAR(1024) COMMENT '父级树名称',
   PRIMARY KEY (ORG_ID)
);

ALTER TABLE C1_ORG COMMENT '组织';

/*==============================================================*/
/* Table: C1_USER                                               */
/*==============================================================*/
CREATE TABLE C1_USER
(
   USER_ID              VARCHAR(36) NOT NULL COMMENT '用户标识',
   ORG_CODE             VARCHAR(36) COMMENT '组织代码',
   DEPARTMENT_CODE      VARCHAR(36) COMMENT '部门代码',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_CODE      VARCHAR(36) COMMENT '创建人代码',
   CREATED_DATE         DATETIME COMMENT '创建时间',
   LAST_MODIFIED_BY_CODE VARCHAR(36) COMMENT '最后修改人代码',
   LAST_MODIFIED_DATE   DATETIME COMMENT '最后修改时间',
   STATE_CODE           VARCHAR(36) COMMENT '状态代码',
   DATA_STATE_CODE      VARCHAR(36) COMMENT '数据状态代码',
   VERSION_             INT DEFAULT 0 COMMENT '版本',
   WF_ID                VARCHAR(36) COMMENT '工作流标识',
   ROLE_CODES           VARCHAR(1024) COMMENT '角色代码集合',
   PERMISSION_CODES     VARCHAR(1024) COMMENT '许可代码集合',
   USER_CODE            VARCHAR(36) NOT NULL COMMENT '用户代码',
   NAME                 VARCHAR(256) COMMENT '名称',
   PASSWORD_            VARCHAR(256) NOT NULL COMMENT '密码',
   PASSWORD_SALT        VARCHAR(256) COMMENT '密码盐',
   EMAIL                VARCHAR(256) COMMENT '邮箱',
   AGE                  INT COMMENT '年龄',
   INTRO                TEXT COMMENT '简介',
   MOBILE_PHONE         VARCHAR(256) COMMENT '手机',
   LOCUS                VARCHAR(256) COMMENT '现居住地',
   COMPANY_NAME         VARCHAR(256) COMMENT '工作单位',
   TELEPHONE            VARCHAR(256) COMMENT '联系电话',
   BIRTHDAY             DATETIME COMMENT '出生日期',
   CALL_NAME            VARCHAR(256) COMMENT '昵称',
   PROFESSION_CODE      VARCHAR(36) COMMENT '职业代码',
   MARRY_STATE_CODE     VARCHAR(36) COMMENT '婚姻状态代码',
   FOLK_CODE            VARCHAR(36) COMMENT '民族代码',
   DIPLOMA_CODE         VARCHAR(36) COMMENT '学历/文凭代码',
   COUNTRY_CODE         VARCHAR(36) COMMENT '国家代码',
   PROVINCE_CODE        VARCHAR(36) COMMENT '省代码',
   CITY_CODE            VARCHAR(36) COMMENT '市代码',
   AREA_CODE            VARCHAR(36) COMMENT '区域代码',
   ADDRESS              VARCHAR(256) COMMENT '地址',
   FLAG_CODE            VARCHAR(36) COMMENT '标记代码',
   SEX_CODE             VARCHAR(36) COMMENT '性别代码',
   QQ                   VARCHAR(256) COMMENT 'QQ号码',
   ID_CARD_NO           VARCHAR(256) COMMENT '身份证号码',
   PRIVATE_PASSWORD     VARCHAR(256) COMMENT '隐私资料查看密码',
   PRIVATE_PASSWORD_SALT VARCHAR(256) COMMENT '隐私密码盐',
   PORTRAIT_URL         VARCHAR(256) COMMENT '肖像路径',
   PRIMARY KEY (USER_ID)
);

ALTER TABLE C1_USER COMMENT '用户';

/*==============================================================*/
/* Table: C1_USER_DEPARTMENT                                    */
/*==============================================================*/
CREATE TABLE C1_USER_DEPARTMENT
(
   USER_DEPARTMENT_ID   VARCHAR(36) NOT NULL COMMENT '用户与部门标识',
   ORG_CODE             VARCHAR(36) COMMENT '组织代码',
   DEPARTMENT_CODE      VARCHAR(36) COMMENT '部门代码',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_CODE      VARCHAR(36) COMMENT '创建人代码',
   CREATED_DATE         DATETIME COMMENT '创建时间',
   LAST_MODIFIED_BY_CODE VARCHAR(36) COMMENT '最后修改人代码',
   LAST_MODIFIED_DATE   DATETIME COMMENT '最后修改时间',
   STATE_CODE           VARCHAR(36) COMMENT '状态代码',
   DATA_STATE_CODE      VARCHAR(36) COMMENT '数据状态代码',
   VERSION_             INT DEFAULT 0 COMMENT '版本',
   WF_ID                VARCHAR(36) COMMENT '工作流标识',
   ROLE_CODES           VARCHAR(1024) COMMENT '角色代码集合',
   PERMISSION_CODES     VARCHAR(1024) COMMENT '许可代码集合',
   USER_CODE            VARCHAR(36) NOT NULL COMMENT '用户代码',
   PRIMARY KEY (USER_DEPARTMENT_ID)
);

ALTER TABLE C1_USER_DEPARTMENT COMMENT '用户与部门';

