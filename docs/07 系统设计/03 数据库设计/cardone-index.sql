

CREATE INDEX "idx_c1_area_area_code" ON "c1_area" USING btree ("area_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_area_area_id" ON "c1_area" USING btree ("area_id" "pg_catalog"."varchar_pattern_ops");

CREATE UNIQUE INDEX "idx_c1_area_business_code" ON "c1_area" USING btree ("province_code", "city_code", "area_code", "end_date");

CREATE INDEX "idx_c1_area_city_code" ON "c1_area" USING btree ("city_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_area_country_code" ON "c1_area" USING btree ("country_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_area_department_code" ON "c1_area" USING btree ("department_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_area_flag_code" ON "c1_area" USING btree ("flag_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_area_last_modified_by_code" ON "c1_area" USING btree ("last_modified_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_area_name" ON "c1_area" USING btree ("name" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_area_province_code" ON "c1_area" USING btree ("province_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_area_site_code" ON "c1_area" USING btree ("site_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_area_system_info_code" ON "c1_area" USING btree ("system_info_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_article_article_id" ON "c1_article" USING btree ("article_id" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_article_author_person_code" ON "c1_article" USING btree ("author_person_code" "pg_catalog"."varchar_pattern_ops");

CREATE UNIQUE INDEX "idx_c1_article_business_code" ON "c1_article" USING btree ("system_info_code", "site_code", "title", "end_date");

CREATE INDEX "idx_c1_article_check_person_code" ON "c1_article" USING btree ("check_person_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_article_created_by_code" ON "c1_article" USING btree ("created_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_article_created_date" ON "c1_article" USING btree ("created_date");

CREATE INDEX "idx_c1_article_department_code" ON "c1_article" USING btree ("department_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_article_department_codes" ON "c1_article" USING btree ("department_codes" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_article_end_date" ON "c1_article" USING btree ("end_date");

CREATE INDEX "idx_c1_article_flag_code" ON "c1_article" USING btree ("flag_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_article_last_modified_by_code" ON "c1_article" USING btree ("last_modified_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_article_recom" ON "c1_article" USING btree ("recom");

CREATE INDEX "idx_c1_article_release_date" ON "c1_article" USING btree ("release_date");

CREATE INDEX "idx_c1_article_site_code" ON "c1_article" USING btree ("site_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_article_sources_code" ON "c1_article" USING btree ("sources_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_article_system_info_code" ON "c1_article" USING btree ("system_info_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_article_title" ON "c1_article" USING btree ("title" "pg_catalog"."varchar_pattern_ops");

CREATE UNIQUE INDEX "idx_c1_city_business_code" ON "c1_city" USING btree ("province_code", "city_code", "end_date");

CREATE INDEX "idx_c1_city_city_code" ON "c1_city" USING btree ("city_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_city_city_id" ON "c1_city" USING btree ("city_id" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_city_country_code" ON "c1_city" USING btree ("country_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_city_created_by_code" ON "c1_city" USING btree ("created_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_city_created_date" ON "c1_city" USING btree ("created_date");

CREATE INDEX "idx_c1_city_department_code" ON "c1_city" USING btree ("department_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_city_province_code" ON "c1_city" USING btree ("province_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_city_site_code" ON "c1_city" USING btree ("site_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_city_system_info_code" ON "c1_city" USING btree ("system_info_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_department_address" ON "c1_department" USING btree ("address" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_department_area_code" ON "c1_department" USING btree ("area_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_department_begin_date" ON "c1_department" USING btree ("begin_date");

CREATE UNIQUE INDEX "idx_c1_department_business_code" ON "c1_department" USING btree ("org_code", "department_code", "end_date");

CREATE INDEX "idx_c1_department_city_code" ON "c1_department" USING btree ("city_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_department_created_by_code" ON "c1_department" USING btree ("created_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_department_created_date" ON "c1_department" USING btree ("created_date");

CREATE INDEX "idx_c1_department_department_code" ON "c1_department" USING btree ("department_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_department_department_id" ON "c1_department" USING btree ("department_id" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_department_end_date" ON "c1_department" USING btree ("end_date");

CREATE INDEX "idx_c1_department_flag_code" ON "c1_department" USING btree ("flag_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_department_last_modified_by_code" ON "c1_department" USING btree ("last_modified_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_department_name" ON "c1_department" USING btree ("name" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_department_parent_code" ON "c1_department" USING btree ("parent_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_department_parent_tree_code" ON "c1_department" USING btree ("parent_tree_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_department_parent_tree_name" ON "c1_department" USING btree ("parent_tree_name" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_department_personal_code" ON "c1_department" USING btree ("personal_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_department_province_code" ON "c1_department" USING btree ("province_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_department_region_code" ON "c1_department" USING btree ("region_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_department_sd" ON "c1_department" USING btree ("state_code", "data_state_code");

CREATE INDEX "idx_c1_department_sddd" ON "c1_department" USING btree ("state_code", "data_state_code", "department_id", "department_code");

CREATE INDEX "idx_c1_department_sds" ON "c1_department" USING btree ("state_code", "data_state_code", "site_code");

CREATE INDEX "idx_c1_department_site_code" ON "c1_department" USING btree ("site_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_department_system_info_code" ON "c1_department" USING btree ("system_info_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_dictionary_begin_date" ON "c1_dictionary" USING btree ("begin_date");

CREATE UNIQUE INDEX "idx_c1_dictionary_business_code" ON "c1_dictionary" USING btree ("system_info_code", "site_code", "org_code", "dictionary_type_code", "dictionary_code", "end_date");

CREATE INDEX "idx_c1_dictionary_created_by_code" ON "c1_dictionary" USING btree ("created_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_dictionary_created_date" ON "c1_dictionary" USING btree ("created_date");

CREATE INDEX "idx_c1_dictionary_department_code" ON "c1_dictionary" USING btree ("department_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_dictionary_dictionary_code" ON "c1_dictionary" USING btree ("dictionary_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_dictionary_dictionary_id" ON "c1_dictionary" USING btree ("dictionary_id" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_dictionary_dictionary_type_code" ON "c1_dictionary" USING btree ("dictionary_type_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_dictionary_end_date" ON "c1_dictionary" USING btree ("end_date");

CREATE INDEX "idx_c1_dictionary_flag_code" ON "c1_dictionary" USING btree ("flag_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_dictionary_item_begin_date" ON "c1_dictionary_item" USING btree ("begin_date");

CREATE UNIQUE INDEX "idx_c1_dictionary_item_business_code" ON "c1_dictionary_item" USING btree ("system_info_code", "site_code", "org_code", "dictionary_type_code", "dictionary_code", "dictionary_item_code", "end_date");

CREATE INDEX "idx_c1_dictionary_item_created_by_code" ON "c1_dictionary_item" USING btree ("created_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_dictionary_item_created_date" ON "c1_dictionary_item" USING btree ("created_date");

CREATE INDEX "idx_c1_dictionary_item_department_code" ON "c1_dictionary_item" USING btree ("department_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_dictionary_item_dictionary_code" ON "c1_dictionary_item" USING btree ("dictionary_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_dictionary_item_dictionary_item_code" ON "c1_dictionary_item" USING btree ("dictionary_item_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_dictionary_item_dictionary_item_id" ON "c1_dictionary_item" USING btree ("dictionary_item_id" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_dictionary_item_dictionary_type_code" ON "c1_dictionary_item" USING btree ("dictionary_type_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_dictionary_item_end_date" ON "c1_dictionary_item" USING btree ("end_date");

CREATE INDEX "idx_c1_dictionary_item_flag_code" ON "c1_dictionary_item" USING btree ("flag_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_dictionary_item_last_modified_by_code" ON "c1_dictionary_item" USING btree ("last_modified_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_dictionary_item_name" ON "c1_dictionary_item" USING btree ("name" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_dictionary_item_site_code" ON "c1_dictionary_item" USING btree ("site_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_dictionary_item_system_info_code" ON "c1_dictionary_item" USING btree ("system_info_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_dictionary_last_modified_by_code" ON "c1_dictionary" USING btree ("last_modified_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_dictionary_name" ON "c1_dictionary" USING btree ("name" "pg_catalog"."varchar_pattern_ops");

CREATE UNIQUE INDEX "idx_c1_dictionary_type_business_code" ON "c1_dictionary_type" USING btree ("system_info_code", "site_code", "org_code", "dictionary_type_code", "end_date");

CREATE INDEX "idx_c1_dictionary_type_created_by_code" ON "c1_dictionary_type" USING btree ("created_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_dictionary_type_created_date" ON "c1_dictionary_type" USING btree ("created_date");

CREATE INDEX "idx_c1_dictionary_type_department_code" ON "c1_dictionary_type" USING btree ("department_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_dictionary_type_dictionary_type_code" ON "c1_dictionary_type" USING btree ("dictionary_type_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_dictionary_type_dictionary_type_id" ON "c1_dictionary_type" USING btree ("dictionary_type_id" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_dictionary_type_end_date" ON "c1_dictionary_type" USING btree ("end_date");

CREATE INDEX "idx_c1_dictionary_type_flag_code" ON "c1_dictionary_type" USING btree ("flag_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_dictionary_type_last_modified_by_code" ON "c1_dictionary_type" USING btree ("last_modified_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_dictionary_type_parent_code" ON "c1_dictionary_type" USING btree ("parent_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_dictionary_type_parent_tree_code" ON "c1_dictionary_type" USING btree ("parent_tree_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_dictionary_type_parent_tree_name" ON "c1_dictionary_type" USING btree ("parent_tree_name" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_dictionary_type_personal_code" ON "c1_dictionary_type" USING btree ("personal_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_dictionary_type_site_code" ON "c1_dictionary_type" USING btree ("site_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_dictionary_type_system_info_code" ON "c1_dictionary_type" USING btree ("system_info_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_error_info_begin_date" ON "c1_error_info" USING btree ("begin_date");

CREATE UNIQUE INDEX "idx_c1_error_info_business_code" ON "c1_error_info" USING btree ("system_info_code", "site_code", "type_code", "url", "error_info_code", "end_date");

CREATE INDEX "idx_c1_error_info_created_by_code" ON "c1_error_info" USING btree ("created_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_error_info_created_date" ON "c1_error_info" USING btree ("created_date");

CREATE INDEX "idx_c1_error_info_department_code" ON "c1_error_info" USING btree ("department_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_error_info_end_date" ON "c1_error_info" USING btree ("end_date");

CREATE INDEX "idx_c1_error_info_error_info_code" ON "c1_error_info" USING btree ("error_info_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_error_info_error_info_id" ON "c1_error_info" USING btree ("error_info_id" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_error_info_flag_code" ON "c1_error_info" USING btree ("flag_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_error_info_last_modified_by_code" ON "c1_error_info" USING btree ("last_modified_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_error_info_object_code" ON "c1_error_info" USING btree ("object_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_error_info_object_id" ON "c1_error_info" USING btree ("object_id" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_error_info_object_type_code" ON "c1_error_info" USING btree ("object_type_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_error_info_site_code" ON "c1_error_info" USING btree ("site_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_error_info_system_info_code" ON "c1_error_info" USING btree ("system_info_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_error_info_type_code" ON "c1_error_info" USING btree ("type_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_error_info_url" ON "c1_error_info" USING btree ("url" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_i18n_info_begin_date" ON "c1_i18n_info" USING btree ("begin_date");

CREATE UNIQUE INDEX "idx_c1_i18n_info_business_code" ON "c1_i18n_info" USING btree ("system_info_code", "site_code", "language", "type_code", "i18n_info_code", "end_date");

CREATE INDEX "idx_c1_i18n_info_created_by_code" ON "c1_i18n_info" USING btree ("created_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_i18n_info_created_date" ON "c1_i18n_info" USING btree ("created_date");

CREATE INDEX "idx_c1_i18n_info_department_code" ON "c1_i18n_info" USING btree ("department_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_i18n_info_end_date" ON "c1_i18n_info" USING btree ("end_date");

CREATE INDEX "idx_c1_i18n_info_flag_code" ON "c1_i18n_info" USING btree ("flag_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_i18n_info_i18n_info_code" ON "c1_i18n_info" USING btree ("i18n_info_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_i18n_info_i18n_info_id" ON "c1_i18n_info" USING btree ("i18n_info_id" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_i18n_info_last_modified_by_code" ON "c1_i18n_info" USING btree ("last_modified_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_i18n_info_system_info_code" ON "c1_i18n_info" USING btree ("system_info_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_i18n_info_type_code" ON "c1_i18n_info" USING btree ("type_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_navigation_begin_date" ON "c1_navigation" USING btree ("begin_date");

CREATE UNIQUE INDEX "idx_c1_navigation_business_code" ON "c1_navigation" USING btree ("system_info_code", "site_code", "navigation_code", "end_date");

CREATE INDEX "idx_c1_navigation_created_by_code" ON "c1_navigation" USING btree ("created_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_navigation_created_date" ON "c1_navigation" USING btree ("created_date");

CREATE INDEX "idx_c1_navigation_data_option" ON "c1_navigation" USING btree ("data_option" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_navigation_department_code" ON "c1_navigation" USING btree ("department_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_navigation_end_date" ON "c1_navigation" USING btree ("end_date");

CREATE INDEX "idx_c1_navigation_flag_code" ON "c1_navigation" USING btree ("flag_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_navigation_last_modified_by_code" ON "c1_navigation" USING btree ("last_modified_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_navigation_name" ON "c1_navigation" USING btree ("name" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_navigation_navigation_code" ON "c1_navigation" USING btree ("navigation_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_navigation_navigation_id" ON "c1_navigation" USING btree ("navigation_id" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_navigation_parent_code" ON "c1_navigation" USING btree ("parent_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_navigation_parent_tree_code" ON "c1_navigation" USING btree ("parent_tree_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_navigation_parent_tree_name" ON "c1_navigation" USING btree ("parent_tree_name" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_navigation_personal_code" ON "c1_navigation" USING btree ("personal_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_navigation_site_code" ON "c1_navigation" USING btree ("site_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_navigation_system_info_code" ON "c1_navigation" USING btree ("system_info_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_navigation_type_code" ON "c1_navigation" USING btree ("type_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_notice_author_person_code" ON "c1_notice" USING btree ("author_person_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_notice_begin_date" ON "c1_notice" USING btree ("begin_date");

CREATE UNIQUE INDEX "idx_c1_notice_business_code" ON "c1_notice" USING btree ("system_info_code", "site_code", "title", "end_date");

CREATE INDEX "idx_c1_notice_check_person_code" ON "c1_notice" USING btree ("check_person_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_notice_created_by_code" ON "c1_notice" USING btree ("created_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_notice_created_date" ON "c1_notice" USING btree ("created_date");

CREATE INDEX "idx_c1_notice_department_code" ON "c1_notice" USING btree ("department_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_notice_department_codes" ON "c1_notice" USING btree ("department_codes" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_notice_end_date" ON "c1_notice" USING btree ("end_date");

CREATE INDEX "idx_c1_notice_flag_code" ON "c1_notice" USING btree ("flag_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_notice_last_modified_by_code" ON "c1_notice" USING btree ("last_modified_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_notice_notice_id" ON "c1_notice" USING btree ("notice_id" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_notice_personal_code" ON "c1_notice" USING btree ("personal_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_notice_site_code" ON "c1_notice" USING btree ("site_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_notice_sources_code" ON "c1_notice" USING btree ("sources_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_notice_system_info_code" ON "c1_notice" USING btree ("system_info_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_notice_title" ON "c1_notice" USING btree ("title" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_notice_type_code" ON "c1_notice" USING btree ("type_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_oauth_consumer_begin_date" ON "c1_oauth_consumer" USING btree ("begin_date");

CREATE UNIQUE INDEX "idx_c1_oauth_consumer_business_code" ON "c1_oauth_consumer" USING btree ("system_info_code", "site_code", "client_secret", "end_date");

CREATE INDEX "idx_c1_oauth_consumer_client_secret" ON "c1_oauth_consumer" USING btree ("client_secret" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_oauth_consumer_created_by_code" ON "c1_oauth_consumer" USING btree ("created_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_oauth_consumer_created_date" ON "c1_oauth_consumer" USING btree ("created_date");

CREATE INDEX "idx_c1_oauth_consumer_department_code" ON "c1_oauth_consumer" USING btree ("department_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_oauth_consumer_end_date" ON "c1_oauth_consumer" USING btree ("end_date");

CREATE INDEX "idx_c1_oauth_consumer_flag_code" ON "c1_oauth_consumer" USING btree ("flag_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_oauth_consumer_last_modified_by_code" ON "c1_oauth_consumer" USING btree ("last_modified_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_oauth_consumer_oauth_consumer_id" ON "c1_oauth_consumer" USING btree ("oauth_consumer_id" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_oauth_consumer_system_info_code" ON "c1_oauth_consumer" USING btree ("system_info_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_open_user_begin_date" ON "c1_open_user" USING btree ("begin_date");

CREATE UNIQUE INDEX "idx_c1_open_user_business_code" ON "c1_open_user" USING btree ("org_code", "user_code", "end_date");

CREATE INDEX "idx_c1_open_user_client_id" ON "c1_open_user" USING btree ("client_id" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_open_user_created_by_code" ON "c1_open_user" USING btree ("created_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_open_user_created_date" ON "c1_open_user" USING btree ("created_date");

CREATE INDEX "idx_c1_open_user_department_code" ON "c1_open_user" USING btree ("department_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_open_user_end_date" ON "c1_open_user" USING btree ("end_date");

CREATE INDEX "idx_c1_open_user_flag_code" ON "c1_open_user" USING btree ("flag_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_open_user_last_modified_by_code" ON "c1_open_user" USING btree ("last_modified_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_open_user_open_user_id" ON "c1_open_user" USING btree ("open_user_id" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_open_user_system_info_code" ON "c1_open_user" USING btree ("system_info_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_open_user_user_code" ON "c1_open_user" USING btree ("user_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_operate_log_created_date" ON "c1_operate_log" USING btree ("created_date");

CREATE INDEX "idx_c1_operate_log_department_code" ON "c1_operate_log" USING btree ("department_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_operate_log_object_code" ON "c1_operate_log" USING btree ("object_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_operate_log_object_type_code" ON "c1_operate_log" USING btree ("object_type_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_operate_log_operate_log_id" ON "c1_operate_log" USING btree ("operate_log_id" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_operate_log_system_info_code" ON "c1_operate_log" USING btree ("system_info_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_operate_log_type_code" ON "c1_operate_log" USING btree ("type_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_org_address" ON "c1_org" USING btree ("address" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_org_area_code" ON "c1_org" USING btree ("area_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_org_begin_date" ON "c1_org" USING btree ("begin_date");

CREATE UNIQUE INDEX "idx_c1_org_business_code" ON "c1_org" USING btree ("org_code", "end_date");

CREATE INDEX "idx_c1_org_city_code" ON "c1_org" USING btree ("city_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_org_created_by_code" ON "c1_org" USING btree ("created_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_org_created_date" ON "c1_org" USING btree ("created_date");

CREATE INDEX "idx_c1_org_department_code" ON "c1_org" USING btree ("department_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_org_end_date" ON "c1_org" USING btree ("end_date");

CREATE INDEX "idx_c1_org_flag_code" ON "c1_org" USING btree ("flag_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_org_last_modified_by_code" ON "c1_org" USING btree ("last_modified_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_org_name" ON "c1_org" USING btree ("name" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_org_org_code" ON "c1_org" USING btree ("org_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_org_org_id" ON "c1_org" USING btree ("org_id" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_org_parent_code" ON "c1_org" USING btree ("parent_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_org_parent_tree_code" ON "c1_org" USING btree ("parent_tree_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_org_parent_tree_name" ON "c1_org" USING btree ("parent_tree_name" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_org_personal_code" ON "c1_org" USING btree ("personal_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_org_province_code" ON "c1_org" USING btree ("province_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_org_region_code" ON "c1_org" USING btree ("region_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_org_site_code" ON "c1_org" USING btree ("site_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_org_system_info_code" ON "c1_org" USING btree ("system_info_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_permission_begin_date" ON "c1_permission" USING btree ("begin_date");

CREATE UNIQUE INDEX "idx_c1_permission_business_code" ON "c1_permission" USING btree ("system_info_code", "site_code", "permission_code", "end_date");

CREATE INDEX "idx_c1_permission_created_by_code" ON "c1_permission" USING btree ("created_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_permission_created_date" ON "c1_permission" USING btree ("created_date");

CREATE INDEX "idx_c1_permission_department_code" ON "c1_permission" USING btree ("department_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_permission_end_date" ON "c1_permission" USING btree ("end_date");

CREATE INDEX "idx_c1_permission_flag_code" ON "c1_permission" USING btree ("flag_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_permission_last_modified_by_code" ON "c1_permission" USING btree ("last_modified_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_permission_name" ON "c1_permission" USING btree ("name" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_permission_parent_code" ON "c1_permission" USING btree ("parent_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_permission_parent_tree_code" ON "c1_permission" USING btree ("parent_tree_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_permission_parent_tree_name" ON "c1_permission" USING btree ("parent_tree_name" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_permission_permission_code" ON "c1_permission" USING btree ("permission_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_permission_permission_id" ON "c1_permission" USING btree ("permission_id" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_permission_personal_code" ON "c1_permission" USING btree ("personal_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_permission_site_code" ON "c1_permission" USING btree ("site_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_permission_system_info_code" ON "c1_permission" USING btree ("system_info_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_permission_type_code" ON "c1_permission" USING btree ("type_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_province_begin_date" ON "c1_province" USING btree ("begin_date");

CREATE UNIQUE INDEX "idx_c1_province_business_code" ON "c1_province" USING btree ("province_code", "end_date");

CREATE INDEX "idx_c1_province_country_code" ON "c1_province" USING btree ("country_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_province_created_by_code" ON "c1_province" USING btree ("created_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_province_created_date" ON "c1_province" USING btree ("created_date");

CREATE INDEX "idx_c1_province_department_code" ON "c1_province" USING btree ("department_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_province_end_date" ON "c1_province" USING btree ("end_date");

CREATE INDEX "idx_c1_province_flag_code" ON "c1_province" USING btree ("flag_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_province_last_modified_by_code" ON "c1_province" USING btree ("last_modified_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_province_name" ON "c1_province" USING btree ("name" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_province_province_code" ON "c1_province" USING btree ("province_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_province_province_id" ON "c1_province" USING btree ("province_id" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_province_site_code" ON "c1_province" USING btree ("site_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_province_system_info_code" ON "c1_province" USING btree ("system_info_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_region_area_code" ON "c1_region" USING btree ("area_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_region_begin_date" ON "c1_region" USING btree ("begin_date");

CREATE UNIQUE INDEX "idx_c1_region_business_code" ON "c1_region" USING btree ("province_code", "city_code", "area_code", "region_code", "end_date");

CREATE INDEX "idx_c1_region_city_code" ON "c1_region" USING btree ("city_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_region_country_code" ON "c1_region" USING btree ("country_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_region_created_by_code" ON "c1_region" USING btree ("created_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_region_created_date" ON "c1_region" USING btree ("created_date");

CREATE INDEX "idx_c1_region_department_code" ON "c1_region" USING btree ("department_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_region_end_date" ON "c1_region" USING btree ("end_date");

CREATE INDEX "idx_c1_region_flag_code" ON "c1_region" USING btree ("flag_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_region_last_modified_by_code" ON "c1_region" USING btree ("last_modified_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_region_name" ON "c1_region" USING btree ("name" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_region_parent_code" ON "c1_region" USING btree ("parent_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_region_parent_tree_code" ON "c1_region" USING btree ("parent_tree_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_region_parent_tree_name" ON "c1_region" USING btree ("parent_tree_name" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_region_personal_code" ON "c1_region" USING btree ("personal_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_region_province_code" ON "c1_region" USING btree ("province_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_region_region_code" ON "c1_region" USING btree ("region_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_region_region_id" ON "c1_region" USING btree ("region_id" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_region_site_code" ON "c1_region" USING btree ("site_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_region_system_info_code" ON "c1_region" USING btree ("system_info_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_role_begin_date" ON "c1_role" USING btree ("begin_date");

CREATE UNIQUE INDEX "idx_c1_role_business_code" ON "c1_role" USING btree ("system_info_code", "site_code", "role_code", "end_date");

CREATE INDEX "idx_c1_role_created_by_code" ON "c1_role" USING btree ("created_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_role_created_date" ON "c1_role" USING btree ("created_date");

CREATE INDEX "idx_c1_role_department_code" ON "c1_role" USING btree ("department_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_role_end_date" ON "c1_role" USING btree ("end_date");

CREATE INDEX "idx_c1_role_flag_code" ON "c1_role" USING btree ("flag_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_role_last_modified_by_code" ON "c1_role" USING btree ("last_modified_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_role_name" ON "c1_role" USING btree ("name" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_role_parent_code" ON "c1_role" USING btree ("parent_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_role_parent_tree_code" ON "c1_role" USING btree ("parent_tree_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_role_parent_tree_name" ON "c1_role" USING btree ("parent_tree_name" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_role_permission_begin_date" ON "c1_role_permission" USING btree ("begin_date");

CREATE UNIQUE INDEX "idx_c1_role_permission_business_code" ON "c1_role_permission" USING btree ("system_info_code", "site_code", "role_code", "permission_code", "end_date");

CREATE INDEX "idx_c1_role_permission_created_by_code" ON "c1_role_permission" USING btree ("created_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_role_permission_created_date" ON "c1_role_permission" USING btree ("created_date");

CREATE INDEX "idx_c1_role_permission_department_code" ON "c1_role_permission" USING btree ("department_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_role_permission_end_date" ON "c1_role_permission" USING btree ("end_date");

CREATE INDEX "idx_c1_role_permission_flag_code" ON "c1_role_permission" USING btree ("flag_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_role_permission_last_modified_by_code" ON "c1_role_permission" USING btree ("last_modified_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_role_permission_permission_code" ON "c1_role_permission" USING btree ("permission_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_role_permission_personal_code" ON "c1_role_permission" USING btree ("personal_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_role_permission_role_code" ON "c1_role_permission" USING btree ("role_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_role_permission_role_permission_id" ON "c1_role_permission" USING btree ("role_permission_id" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_role_permission_sd" ON "c1_role_permission" USING btree ("state_code", "data_state_code");

CREATE INDEX "idx_c1_role_permission_site_code" ON "c1_role_permission" USING btree ("site_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_role_permission_system_info_code" ON "c1_role_permission" USING btree ("system_info_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_role_personal_code" ON "c1_role" USING btree ("personal_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_role_role_code" ON "c1_role" USING btree ("role_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_role_role_id" ON "c1_role" USING btree ("role_id" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_role_site_code" ON "c1_role" USING btree ("site_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_role_system_info_code" ON "c1_role" USING btree ("system_info_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_role_type_code" ON "c1_role" USING btree ("type_code" "pg_catalog"."varchar_pattern_ops");

CREATE UNIQUE INDEX "idx_c1_site_business_code" ON "c1_site" USING btree ("system_info_code", "site_code", "end_date");

CREATE INDEX "idx_c1_site_created_by_code" ON "c1_site" USING btree ("created_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_site_created_date" ON "c1_site" USING btree ("created_date");

CREATE INDEX "idx_c1_site_department_code" ON "c1_site" USING btree ("department_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_site_dsd" ON "c1_site" USING btree ("department_code", "state_code", "data_state_code");

CREATE INDEX "idx_c1_site_end_date" ON "c1_site" USING btree ("end_date");

CREATE INDEX "idx_c1_site_flag_code" ON "c1_site" USING btree ("flag_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_site_last_modified_by_code" ON "c1_site" USING btree ("last_modified_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_site_name" ON "c1_site" USING btree ("name" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_site_site_code" ON "c1_site" USING btree ("site_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_site_site_id" ON "c1_site" USING btree ("site_id" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_site_system_info_code" ON "c1_site" USING btree ("system_info_code" "pg_catalog"."varchar_pattern_ops");

CREATE UNIQUE INDEX "idx_c1_system_info_business_code" ON "c1_system_info" USING btree ("system_info_code", "end_date");

CREATE INDEX "idx_c1_system_info_created_by_code" ON "c1_system_info" USING btree ("created_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_system_info_created_date" ON "c1_system_info" USING btree ("created_date");

CREATE INDEX "idx_c1_system_info_department_code" ON "c1_system_info" USING btree ("department_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_system_info_end_date" ON "c1_system_info" USING btree ("end_date");

CREATE INDEX "idx_c1_system_info_flag_code" ON "c1_system_info" USING btree ("flag_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_system_info_last_modified_by_code" ON "c1_system_info" USING btree ("last_modified_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_system_info_name" ON "c1_system_info" USING btree ("name" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_system_info_site_code" ON "c1_system_info" USING btree ("site_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_system_info_system_info_code" ON "c1_system_info" USING btree ("system_info_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_system_info_system_info_id" ON "c1_system_info" USING btree ("system_info_id" "pg_catalog"."varchar_pattern_ops");

CREATE UNIQUE INDEX "idx_c1_token_info_business_code" ON "c1_token_info" USING btree ("system_info_code", "site_code", "token_info_code", "end_date");

CREATE INDEX "idx_c1_token_info_client_id" ON "c1_token_info" USING btree ("client_id" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_token_info_created_by_code" ON "c1_token_info" USING btree ("created_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_token_info_created_date" ON "c1_token_info" USING btree ("created_date");

CREATE INDEX "idx_c1_token_info_department_code" ON "c1_token_info" USING btree ("department_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_token_info_end_date" ON "c1_token_info" USING btree ("end_date");

CREATE INDEX "idx_c1_token_info_flag_code" ON "c1_token_info" USING btree ("flag_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_token_info_last_modified_by_code" ON "c1_token_info" USING btree ("last_modified_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_token_info_scope" ON "c1_token_info" USING btree ("scope" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_token_info_system_info_code" ON "c1_token_info" USING btree ("system_info_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_token_info_token_info_code" ON "c1_token_info" USING btree ("token_info_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_token_info_token_info_id" ON "c1_token_info" USING btree ("token_info_id" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_token_info_user_code" ON "c1_token_info" USING btree ("user_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_address_address" ON "c1_user_address" USING btree ("address" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_address_area_code" ON "c1_user_address" USING btree ("area_code" "pg_catalog"."varchar_pattern_ops");

CREATE UNIQUE INDEX "idx_c1_user_address_business_code" ON "c1_user_address" USING btree ("org_code", "address", "user_code", "end_date");

CREATE INDEX "idx_c1_user_address_city_code" ON "c1_user_address" USING btree ("city_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_address_country_code" ON "c1_user_address" USING btree ("country_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_address_created_by_code" ON "c1_user_address" USING btree ("created_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_address_created_date" ON "c1_user_address" USING btree ("created_date");

CREATE INDEX "idx_c1_user_address_department_code" ON "c1_user_address" USING btree ("department_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_address_end_date" ON "c1_user_address" USING btree ("end_date");

CREATE INDEX "idx_c1_user_address_flag_code" ON "c1_user_address" USING btree ("flag_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_address_is_default" ON "c1_user_address" USING btree ("is_default" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_address_last_modified_by_code" ON "c1_user_address" USING btree ("last_modified_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_address_org_code" ON "c1_user_address" USING btree ("org_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_address_personal_code" ON "c1_user_address" USING btree ("personal_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_address_province_code" ON "c1_user_address" USING btree ("province_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_address_region_code" ON "c1_user_address" USING btree ("region_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_address_site_code" ON "c1_user_address" USING btree ("site_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_address_system_info_code" ON "c1_user_address" USING btree ("system_info_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_address_type_code" ON "c1_user_address" USING btree ("type_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_address_user_address_id" ON "c1_user_address" USING btree ("user_address_id" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_address_user_code" ON "c1_user_address" USING btree ("user_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_age" ON "c1_user" USING btree ("age");

CREATE INDEX "idx_c1_user_area_code" ON "c1_user" USING btree ("area_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_birthday" ON "c1_user" USING btree ("birthday");

CREATE UNIQUE INDEX "idx_c1_user_business_code" ON "c1_user" USING btree ("org_code", "user_code", "end_date");

CREATE INDEX "idx_c1_user_call_name" ON "c1_user" USING btree ("call_name" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_city_code" ON "c1_user" USING btree ("city_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_contact" ON "c1_user" USING btree ("contact" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_country_code" ON "c1_user" USING btree ("country_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_created_by_code" ON "c1_user" USING btree ("created_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_created_date" ON "c1_user" USING btree ("created_date");

CREATE UNIQUE INDEX "idx_c1_user_department_business_code" ON "c1_user_department" USING btree ("org_code", "department_code", "user_code", "end_date");

CREATE INDEX "idx_c1_user_department_code" ON "c1_user" USING btree ("department_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_department_created_by_code" ON "c1_user_department" USING btree ("created_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_department_created_date" ON "c1_user_department" USING btree ("created_date");

CREATE INDEX "idx_c1_user_department_department_code" ON "c1_user_department" USING btree ("department_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_department_end_date" ON "c1_user_department" USING btree ("end_date");

CREATE INDEX "idx_c1_user_department_flag_code" ON "c1_user_department" USING btree ("flag_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_department_id" ON "c1_user" USING btree ("department_id" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_department_last_modified_by_code" ON "c1_user_department" USING btree ("last_modified_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_department_org_code" ON "c1_user_department" USING btree ("org_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_department_personal_code" ON "c1_user_department" USING btree ("personal_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_department_site_code" ON "c1_user_department" USING btree ("site_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_department_system_info_code" ON "c1_user_department" USING btree ("system_info_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_department_user_code" ON "c1_user_department" USING btree ("user_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_department_user_department_id" ON "c1_user_department" USING btree ("user_department_id" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_diploma_code" ON "c1_user" USING btree ("diploma_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_email" ON "c1_user" USING btree ("email" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_end_date" ON "c1_user" USING btree ("end_date");

CREATE INDEX "idx_c1_user_first_name" ON "c1_user" USING btree ("first_name" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_flag_code" ON "c1_user" USING btree ("flag_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_folk_code" ON "c1_user" USING btree ("folk_code" "pg_catalog"."varchar_pattern_ops");

CREATE UNIQUE INDEX "idx_c1_user_group_business_code" ON "c1_user_group" USING btree ("system_info_code", "site_code", "user_group_code", "end_date");

CREATE INDEX "idx_c1_user_group_created_by_code" ON "c1_user_group" USING btree ("created_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_group_created_date" ON "c1_user_group" USING btree ("created_date");

CREATE INDEX "idx_c1_user_group_department_code" ON "c1_user_group" USING btree ("department_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_group_end_date" ON "c1_user_group" USING btree ("end_date");

CREATE INDEX "idx_c1_user_group_flag_code" ON "c1_user_group" USING btree ("flag_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_group_ibe" ON "c1_user_group_user" USING btree ("user_group_id", "begin_date", "end_date");

CREATE INDEX "idx_c1_user_group_last_modified_by_code" ON "c1_user_group" USING btree ("last_modified_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_group_name" ON "c1_user_group" USING btree ("name" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_group_org_code" ON "c1_user_group" USING btree ("org_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_group_parent_code" ON "c1_user_group" USING btree ("parent_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_group_parent_tree_code" ON "c1_user_group" USING btree ("parent_tree_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_group_parent_tree_name" ON "c1_user_group" USING btree ("parent_tree_name" "pg_catalog"."varchar_pattern_ops");

CREATE UNIQUE INDEX "idx_c1_user_group_permission_business_code" ON "c1_user_group_permission" USING btree ("system_info_code", "site_code", "user_group_code", "permission_code", "end_date");

CREATE INDEX "idx_c1_user_group_permission_created_by_code" ON "c1_user_group_permission" USING btree ("created_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_group_permission_created_date" ON "c1_user_group_permission" USING btree ("created_date");

CREATE INDEX "idx_c1_user_group_permission_department_code" ON "c1_user_group_permission" USING btree ("department_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_group_permission_end_date" ON "c1_user_group_permission" USING btree ("end_date");

CREATE INDEX "idx_c1_user_group_permission_flag_code" ON "c1_user_group_permission" USING btree ("flag_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_group_permission_last_modified_by_code" ON "c1_user_group_permission" USING btree ("last_modified_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_group_permission_org_code" ON "c1_user_group_permission" USING btree ("org_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_group_permission_permission_code" ON "c1_user_group_permission" USING btree ("permission_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_group_permission_personal_code" ON "c1_user_group_permission" USING btree ("personal_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_group_permission_sd" ON "c1_user_group_permission" USING btree ("state_code", "data_state_code");

CREATE INDEX "idx_c1_user_group_permission_site_code" ON "c1_user_group_permission" USING btree ("site_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_group_permission_system_info_code" ON "c1_user_group_permission" USING btree ("system_info_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_group_permission_user_group_code" ON "c1_user_group_permission" USING btree ("user_group_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_group_permission_user_group_permission_id" ON "c1_user_group_permission" USING btree ("user_group_permission_id" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_group_personal_code" ON "c1_user_group" USING btree ("personal_code" "pg_catalog"."varchar_pattern_ops");

CREATE UNIQUE INDEX "idx_c1_user_group_role_business_code" ON "c1_user_group_role" USING btree ("system_info_code", "site_code", "user_group_code", "role_code", "end_date");

CREATE INDEX "idx_c1_user_group_role_created_by_code" ON "c1_user_group_role" USING btree ("created_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_group_role_created_date" ON "c1_user_group_role" USING btree ("created_date");

CREATE INDEX "idx_c1_user_group_role_department_code" ON "c1_user_group_role" USING btree ("department_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_group_role_end_date" ON "c1_user_group_role" USING btree ("end_date");

CREATE INDEX "idx_c1_user_group_role_flag_code" ON "c1_user_group_role" USING btree ("flag_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_group_role_last_modified_by_code" ON "c1_user_group_role" USING btree ("last_modified_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_group_role_org_code" ON "c1_user_group_role" USING btree ("org_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_group_role_personal_code" ON "c1_user_group_role" USING btree ("personal_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_group_role_role_code" ON "c1_user_group_role" USING btree ("role_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_group_role_site_code" ON "c1_user_group_role" USING btree ("site_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_group_role_system_info_code" ON "c1_user_group_role" USING btree ("system_info_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_group_role_user_group_code" ON "c1_user_group_role" USING btree ("user_group_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_group_role_user_group_role_id" ON "c1_user_group_role" USING btree ("user_group_role_id" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_group_site_code" ON "c1_user_group" USING btree ("site_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_group_system_info_code" ON "c1_user_group" USING btree ("system_info_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_group_type_code" ON "c1_user_group" USING btree ("type_code" "pg_catalog"."varchar_pattern_ops");

CREATE UNIQUE INDEX "idx_c1_user_group_user_business_code" ON "c1_user_group_user" USING btree ("system_info_code", "site_code", "user_group_code", "user_code", "end_date");

CREATE INDEX "idx_c1_user_group_user_created_by_code" ON "c1_user_group_user" USING btree ("created_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_group_user_created_date" ON "c1_user_group_user" USING btree ("created_date");

CREATE INDEX "idx_c1_user_group_user_department_code" ON "c1_user_group_user" USING btree ("department_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_group_user_end_date" ON "c1_user_group_user" USING btree ("end_date");

CREATE INDEX "idx_c1_user_group_user_flag_code" ON "c1_user_group_user" USING btree ("flag_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_group_user_group_code" ON "c1_user_group" USING btree ("user_group_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_group_user_group_id" ON "c1_user_group" USING btree ("user_group_id" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_group_user_iube" ON "c1_user_group_user" USING btree ("user_group_id", "user_code");

CREATE INDEX "idx_c1_user_group_user_last_modified_by_code" ON "c1_user_group_user" USING btree ("last_modified_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_group_user_org_code" ON "c1_user_group_user" USING btree ("org_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_group_user_personal_code" ON "c1_user_group_user" USING btree ("personal_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_group_user_sd" ON "c1_user_group_user" USING btree ("state_code", "data_state_code");

CREATE INDEX "idx_c1_user_group_user_site_code" ON "c1_user_group_user" USING btree ("site_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_group_user_system_info_code" ON "c1_user_group_user" USING btree ("system_info_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_group_user_user_code" ON "c1_user_group_user" USING btree ("user_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_group_user_user_group_code" ON "c1_user_group_user" USING btree ("user_group_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_group_user_user_group_user_id" ON "c1_user_group_user" USING btree ("user_group_user_id" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_id_card_code" ON "c1_user" USING btree ("id_card_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_id_card_no" ON "c1_user" USING btree ("id_card_no" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_last_modified_by_code" ON "c1_user" USING btree ("last_modified_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_last_name" ON "c1_user" USING btree ("last_name" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_locus" ON "c1_user" USING btree ("locus" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_mobile_phone" ON "c1_user" USING btree ("mobile_phone" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_name" ON "c1_user" USING btree ("name" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_name_pinyin" ON "c1_user" USING btree ("name_pinyin" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_org_begin_date" ON "c1_user_org" USING btree ("begin_date");

CREATE UNIQUE INDEX "idx_c1_user_org_business_code" ON "c1_user_org" USING btree ("org_code", "user_code", "end_date");

CREATE INDEX "idx_c1_user_org_code" ON "c1_user" USING btree ("org_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_org_created_by_code" ON "c1_user_org" USING btree ("created_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_org_created_date" ON "c1_user_org" USING btree ("created_date");

CREATE INDEX "idx_c1_user_org_department_code" ON "c1_user_org" USING btree ("department_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_org_end_date" ON "c1_user_org" USING btree ("end_date");

CREATE INDEX "idx_c1_user_org_flag_code" ON "c1_user_org" USING btree ("flag_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_org_last_modified_by_code" ON "c1_user_org" USING btree ("last_modified_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_org_org_code" ON "c1_user_org" USING btree ("org_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_org_personal_code" ON "c1_user_org" USING btree ("personal_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_org_site_code" ON "c1_user_org" USING btree ("site_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_org_system_info_code" ON "c1_user_org" USING btree ("system_info_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_org_user_code" ON "c1_user_org" USING btree ("user_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_org_user_org_id" ON "c1_user_org" USING btree ("user_org_id" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_password_" ON "c1_user" USING btree ("password_" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_password_salt" ON "c1_user" USING btree ("password_salt" "pg_catalog"."varchar_pattern_ops");

CREATE UNIQUE INDEX "idx_c1_user_permission_business_code" ON "c1_user_permission" USING btree ("system_info_code", "site_code", "user_code", "permission_code", "end_date");

CREATE INDEX "idx_c1_user_permission_created_by_code" ON "c1_user_permission" USING btree ("created_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_permission_created_date" ON "c1_user_permission" USING btree ("created_date");

CREATE INDEX "idx_c1_user_permission_department_code" ON "c1_user_permission" USING btree ("department_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_permission_end_date" ON "c1_user_permission" USING btree ("end_date");

CREATE INDEX "idx_c1_user_permission_flag_code" ON "c1_user_permission" USING btree ("flag_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_permission_last_modified_by_code" ON "c1_user_permission" USING btree ("last_modified_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_permission_org_code" ON "c1_user_permission" USING btree ("org_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_permission_permission_code" ON "c1_user_permission" USING btree ("permission_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_permission_personal_code" ON "c1_user_permission" USING btree ("personal_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_permission_site_code" ON "c1_user_permission" USING btree ("site_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_permission_system_info_code" ON "c1_user_permission" USING btree ("system_info_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_permission_ube" ON "c1_user_permission" USING btree ("user_code", "begin_date", "end_date");

CREATE INDEX "idx_c1_user_permission_ul" ON "c1_user_permission" USING btree ("user_code", "last_modified_date");

CREATE INDEX "idx_c1_user_permission_user_code" ON "c1_user_permission" USING btree ("user_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_permission_user_permission_id" ON "c1_user_permission" USING btree ("user_permission_id" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_personal_code" ON "c1_user" USING btree ("personal_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_private_password" ON "c1_user" USING btree ("private_password" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_private_password_salt" ON "c1_user" USING btree ("private_password_salt" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_profession_code" ON "c1_user" USING btree ("profession_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_province_code" ON "c1_user" USING btree ("province_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_qq" ON "c1_user" USING btree ("qq" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_region_code" ON "c1_user" USING btree ("region_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_role_begin_date" ON "c1_user_role" USING btree ("begin_date");

CREATE UNIQUE INDEX "idx_c1_user_role_business_code" ON "c1_user_role" USING btree ("system_info_code", "site_code", "user_code", "role_code", "end_date");

CREATE INDEX "idx_c1_user_role_created_by_code" ON "c1_user_role" USING btree ("created_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_role_created_date" ON "c1_user_role" USING btree ("created_date");

CREATE INDEX "idx_c1_user_role_department_code" ON "c1_user_role" USING btree ("department_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_role_end_date" ON "c1_user_role" USING btree ("end_date");

CREATE INDEX "idx_c1_user_role_flag_code" ON "c1_user_role" USING btree ("flag_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_role_last_modified_by_code" ON "c1_user_role" USING btree ("last_modified_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_role_org_code" ON "c1_user_role" USING btree ("org_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_role_personal_code" ON "c1_user_role" USING btree ("personal_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_role_role_code" ON "c1_user_role" USING btree ("role_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_role_sd" ON "c1_user_role" USING btree ("state_code", "data_state_code");

CREATE INDEX "idx_c1_user_role_site_code" ON "c1_user_role" USING btree ("site_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_role_system_info_code" ON "c1_user_role" USING btree ("system_info_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_role_ube" ON "c1_user_role" USING btree ("user_code", "begin_date", "end_date");

CREATE INDEX "idx_c1_user_role_ul" ON "c1_user_role" USING btree ("user_code", "last_modified_date");

CREATE INDEX "idx_c1_user_role_user_code" ON "c1_user_role" USING btree ("user_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_role_user_role_id" ON "c1_user_role" USING btree ("user_role_id" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_sd" ON "c1_user" USING btree ("state_code", "data_state_code");

CREATE INDEX "idx_c1_user_sds" ON "c1_user" USING btree ("state_code", "data_state_code", "site_code");

CREATE INDEX "idx_c1_user_sex_code" ON "c1_user" USING btree ("sex_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_site_code" ON "c1_user" USING btree ("site_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_system_info_code" ON "c1_user" USING btree ("system_info_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_telephone" ON "c1_user" USING btree ("telephone" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_usd" ON "c1_user" USING btree ("user_code", "state_code", "data_state_code");

CREATE INDEX "idx_c1_user_user_code" ON "c1_user" USING btree ("user_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_user_user_id" ON "c1_user" USING btree ("user_id" "pg_catalog"."varchar_pattern_ops");

CREATE UNIQUE INDEX "idx_c1_wf_task_handle_business_code" ON "c1_wf_task_handle" USING btree ("wf_task_handle_id" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_wf_task_handle_business_type_code" ON "c1_wf_task_handle" USING btree ("business_type_code" "pg_catalog"."varchar_pattern_ops");

CREATE UNIQUE INDEX "idx_c1_wf_task_handle_retry_business_code" ON "c1_wf_task_handle_retry" USING btree ("wf_task_handle_id", "handle_code");

CREATE INDEX "idx_c1_wf_task_handle_retry_created_date" ON "c1_wf_task_handle_retry" USING btree ("created_date");

CREATE INDEX "idx_c1_wf_task_handle_retry_last_retry_date" ON "c1_wf_task_handle_retry" USING btree ("last_retry_date");

CREATE INDEX "idx_c1_wf_variable_begin_date" ON "c1_wf_variable" USING btree ("begin_date");

CREATE UNIQUE INDEX "idx_c1_wf_variable_business_code" ON "c1_wf_variable" USING btree ("org_code", "department_code", "wf_key", "end_date");

CREATE INDEX "idx_c1_wf_variable_created_by_code" ON "c1_wf_variable" USING btree ("created_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_wf_variable_created_date" ON "c1_wf_variable" USING btree ("created_date");

CREATE INDEX "idx_c1_wf_variable_department_code" ON "c1_wf_variable" USING btree ("department_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_wf_variable_end_date" ON "c1_wf_variable" USING btree ("end_date");

CREATE INDEX "idx_c1_wf_variable_flag_code" ON "c1_wf_variable" USING btree ("flag_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_wf_variable_last_modified_by_code" ON "c1_wf_variable" USING btree ("last_modified_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_wf_variable_org_code" ON "c1_wf_variable" USING btree ("org_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_wf_variable_personal_code" ON "c1_wf_variable" USING btree ("personal_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_wf_variable_site_code" ON "c1_wf_variable" USING btree ("site_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_wf_variable_system_info_code" ON "c1_wf_variable" USING btree ("system_info_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_wf_variable_user_begin_date" ON "c1_wf_variable_user" USING btree ("begin_date");

CREATE UNIQUE INDEX "idx_c1_wf_variable_user_business_code" ON "c1_wf_variable_user" USING btree ("org_code", "department_code", "user_code", "wf_key", "end_date");

CREATE INDEX "idx_c1_wf_variable_user_created_by_code" ON "c1_wf_variable_user" USING btree ("created_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_wf_variable_user_created_date" ON "c1_wf_variable_user" USING btree ("created_date");

CREATE INDEX "idx_c1_wf_variable_user_department_code" ON "c1_wf_variable_user" USING btree ("department_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_wf_variable_user_end_date" ON "c1_wf_variable_user" USING btree ("end_date");

CREATE INDEX "idx_c1_wf_variable_user_flag_code" ON "c1_wf_variable_user" USING btree ("flag_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_wf_variable_user_last_modified_by_code" ON "c1_wf_variable_user" USING btree ("last_modified_by_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_wf_variable_user_org_code" ON "c1_wf_variable_user" USING btree ("org_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_wf_variable_user_personal_code" ON "c1_wf_variable_user" USING btree ("personal_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_wf_variable_user_site_code" ON "c1_wf_variable_user" USING btree ("site_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_wf_variable_user_system_info_code" ON "c1_wf_variable_user" USING btree ("system_info_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_wf_variable_user_user_code" ON "c1_wf_variable_user" USING btree ("user_code" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_wf_variable_user_wf_key" ON "c1_wf_variable_user" USING btree ("wf_key" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_wf_variable_user_wf_variable_user_id" ON "c1_wf_variable_user" USING btree ("wf_variable_user_id" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_wf_variable_wf_key" ON "c1_wf_variable" USING btree ("wf_key" "pg_catalog"."varchar_pattern_ops");

CREATE INDEX "idx_c1_wf_variable_wf_variable_id" ON "c1_wf_variable" USING btree ("wf_variable_id" "pg_catalog"."varchar_pattern_ops");
