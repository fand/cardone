﻿DROP TABLE IF EXISTS C1_WF_VARIABLE;

DROP TABLE IF EXISTS C1_WF_VARIABLE_USER;

/*==============================================================*/
/* Table: C1_WF_VARIABLE                                        */
/*==============================================================*/
CREATE TABLE C1_WF_VARIABLE
(
   WF_VARIABLE_ID       VARCHAR(36) NOT NULL COMMENT '工作流变量标识',
   SYSTEM_INFO_CODE     VARCHAR(36) COMMENT '系统信息代码',
   SITE_CODE            VARCHAR(36) COMMENT '站点代码',
   ORG_CODE             VARCHAR(36) COMMENT '组织代码',
   DEPARTMENT_CODE      VARCHAR(36) COMMENT '部门代码',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_CODE      VARCHAR(36) COMMENT '创建人代码',
   CREATED_DATE         DATETIME COMMENT '创建时间',
   LAST_MODIFIED_BY_CODE VARCHAR(36) COMMENT '最后修改人代码',
   LAST_MODIFIED_DATE   DATETIME COMMENT '最后修改时间',
   STATE_CODE           VARCHAR(36) COMMENT '状态代码',
   DATA_STATE_CODE      VARCHAR(36) COMMENT '数据状态代码',
   VERSION_             INT DEFAULT 0 COMMENT '版本',
   WF_ID                VARCHAR(36) COMMENT '工作流标识',
   ROLE_CODES           VARCHAR(1024) COMMENT '角色代码集合',
   PERMISSION_CODES     VARCHAR(1024) COMMENT '许可代码集合',
   WF_KEY               VARCHAR(255) COMMENT '工作流键',
   CONTENT              VARCHAR(4000) COMMENT '正文',
   PRIMARY KEY (WF_VARIABLE_ID)
);

ALTER TABLE C1_WF_VARIABLE COMMENT '工作流变量';

/*==============================================================*/
/* Table: C1_WF_VARIABLE_USER                                   */
/*==============================================================*/
CREATE TABLE C1_WF_VARIABLE_USER
(
   WF_VARIABLE_USER_ID  VARCHAR(36) NOT NULL COMMENT '工作流变量与用户标识',
   SYSTEM_INFO_CODE     VARCHAR(36) COMMENT '系统信息代码',
   SITE_CODE            VARCHAR(36) COMMENT '站点代码',
   ORG_CODE             VARCHAR(36) COMMENT '组织代码',
   DEPARTMENT_CODE      VARCHAR(36) COMMENT '部门代码',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_CODE      VARCHAR(36) COMMENT '创建人代码',
   CREATED_DATE         DATETIME COMMENT '创建时间',
   LAST_MODIFIED_BY_CODE VARCHAR(36) COMMENT '最后修改人代码',
   LAST_MODIFIED_DATE   DATETIME COMMENT '最后修改时间',
   STATE_CODE           VARCHAR(36) COMMENT '状态代码',
   DATA_STATE_CODE      VARCHAR(36) COMMENT '数据状态代码',
   VERSION_             INT DEFAULT 0 COMMENT '版本',
   WF_ID                VARCHAR(36) COMMENT '工作流标识',
   ROLE_CODES           VARCHAR(1024) COMMENT '角色代码集合',
   PERMISSION_CODES     VARCHAR(1024) COMMENT '许可代码集合',
   WF_KEY               VARCHAR(255) NOT NULL COMMENT '工作流键',
   CONTENT              VARCHAR(4000) COMMENT '正文',
   USER_CODE            VARCHAR(36) NOT NULL COMMENT '用户代码',
   PRIMARY KEY (WF_VARIABLE_USER_ID)
);

ALTER TABLE C1_WF_VARIABLE_USER COMMENT '工作流变量与用户';

