/*==============================================================*/
/* DBMS name:      ORACLE Version 11g                           */
/* Created on:     2013/9/24 10:28:47                           */
/*==============================================================*/


ALTER TABLE "FD_BRAND"
   DROP CONSTRAINT FK_FD_BRAND_TYPE_ID_FD_DICTI;

DROP INDEX "FD_BRAND_TYPE_ID_FK_1";

DROP TABLE "FD_BRAND" CASCADE CONSTRAINTS;

/*==============================================================*/
/* Table: "FD_BRAND"                                            */
/*==============================================================*/
CREATE TABLE "FD_BRAND" 
(
   "ID"                 VARCHAR2(36)         NOT NULL,
   "TYPE_ID"            VARCHAR2(36),
   "CODE"               VARCHAR2(256)        NOT NULL,
   "NAME"               VARCHAR2(256)        NOT NULL,
   "WEIGHT"             INTEGER              NOT NULL,
   "ORDER_BY_NUM"          INTEGER              DEFAULT 0 NOT NULL,
   "CONTENT"            VARCHAR2(2048),
   "BEGIN_DA_TI"        DATE                 DEFAULT SYSDATE NOT NULL,
   "END_DA_TI"          DATE                 DEFAULT 'TO_DATE(''99991231'',''yyyymmdd'')' NOT NULL,
   CONSTRAINT PK_FD_BRAND PRIMARY KEY ("ID")
);

COMMENT ON TABLE "FD_BRAND" IS
'���';

COMMENT ON COLUMN "FD_BRAND"."ID" IS
'��ʶ';

COMMENT ON COLUMN "FD_BRAND"."TYPE_ID" IS
'�������';

COMMENT ON COLUMN "FD_BRAND"."CODE" IS
'���';

COMMENT ON COLUMN "FD_BRAND"."NAME" IS
'����';

COMMENT ON COLUMN "FD_BRAND"."WEIGHT" IS
'Ȩ��';

COMMENT ON COLUMN "FD_BRAND"."ORDER_BY_NUM" IS
'�����';

COMMENT ON COLUMN "FD_BRAND"."CONTENT" IS
'����';

COMMENT ON COLUMN "FD_BRAND"."BEGIN_DA_TI" IS
'��ʼʱ��';

COMMENT ON COLUMN "FD_BRAND"."END_DA_TI" IS
'����ʱ��';

/*==============================================================*/
/* Index: "FD_BRAND_TYPE_ID_FK_1"                               */
/*==============================================================*/
CREATE INDEX "FD_BRAND_TYPE_ID_FK_1" ON "FD_BRAND" (
   "TYPE_ID" ASC
);

